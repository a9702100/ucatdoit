<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePushMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_messages', function (Blueprint $table) {
            $table->id();
            $table->integer('member_id');
            $table->string('token');
            $table->string('type');
            $table->boolean('good')->default(1);
            $table->boolean('reply')->default(1);
            $table->boolean('friend')->default(1);
            $table->boolean('together')->default(1);
            $table->boolean('ask')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_messages');
    }
}
