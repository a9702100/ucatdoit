<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TogetherController;
use App\Http\Controllers\FeedController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\HeartController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\AskController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SettingsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [AuthController::class, 'index']);

// 업데이트 점검 페이지
// Route::get('/', [AuthController::class, 'tester']); 

// 데이터베이스 조작
// Route::get('/test', [AuthController::class, 'test']); 

// 광고 페이지
Route::get('/advertise', [HomeController::class, 'advertise']);

Route::prefix('account')->group(function(){
    Route::post('/isMember', [AuthController::class, 'isMember']);
    Route::get('/sns_join', [AuthController::class, 'snsJoin']);
    Route::put('/sns_join', [AuthController::class, 'agreeUpdate']);
    Route::post('/insertToken', [AuthController::class, 'insertToken']);
    
    //로그인 관련
    Route::prefix('login')->group(function(){
        Route::get('/', [AuthController::class, 'login']);
        Route::post('/', [AuthController::class, 'loginCheck'])->name('account.check');
    });
   
    //소셜로그인
    Route::get('/kakao_login', [AuthController::class, 'kakao_login']);
    Route::get('/naver_login', [AuthController::class, 'naver_login']);
    Route::get('/google_login', [AuthController::class, 'google_login']);
    Route::post('/apple_login', [AuthController::class, 'apple_login']);
    Route::get('/facebook_login', [AuthController::class, 'facebook_login']);

    //회원가입 관련
    Route::prefix('join')->group(function(){
        Route::get('/', [AuthController::class, 'join']);
        Route::post('/', [AuthController::class, 'joinStore'])->name('join.store');
        Route::post('/input_regex', [AuthController::class, 'inputCheck']);
        Route::post('/input_dupl', [AuthController::class, 'inputDupli']);
        Route::post('/member_chk', [AuthController::class, 'member_chk']);
    });
    //계정 찾기 관련
    Route::prefix('find')->group(function(){
        Route::get('/', [AuthController::class, 'find']);
        Route::get('/show', [AuthController::class, 'findShow']);
        Route::post('/{id}', [AuthController::class, 'findUpdate']);
    });

    //뉴비 닉네임 바꾸기
    Route::post('/newbie', [AuthController::class, 'newbie_nick']);
    Route::post('/newbie_apply', [AuthController::class, 'newbie_apply']);

    //본인인증
    Route::post('verify', [AuthController::class, 'verify']);

    //로그아웃
    Route::get('/logout', [AuthController::class, 'logout']);
    //계정삭제
    Route::delete('/', [AuthController::class, 'memberDestroy']);
    Route::post('/passwdCheck', [AuthController::class, 'passwdCheck']);
});

Route::get('/share/{id}', [FeedController::class, 'feedShare']);
Route::get('/use-contents', [AuthController::class, 'useContents']);

Route::group(['middleware'=>['auth.member']], function(){
    // 메인 페이지
    Route::prefix('home')->group(function(){
        Route::get('/', [HomeController::class, 'home']);
        Route::post('/getImage', [HomeController::class, 'getImage']);
        Route::post('/more', [HomeController::class, 'moreFeed']);
    });
    
    // 검색 관련
    Route::prefix('search')->group(function(){
        Route::get('/', [SearchController::class, 'search']);
        Route::post('/', [SearchController::class, 'searchKeyword']);
        Route::get('/show', [SearchController::class, 'searchShow']);
        Route::post('/friend', [SearchController::class, 'searchFriend']);
        Route::post('/member', [SearchController::class, 'searchMember']);
        Route::post('/more', [SearchController::class, 'moreFeed']);
    });
    
    // 투게더 페이지
    Route::prefix('together')->group(function(){
        Route::get('/', [TogetherController::class, 'together']);
        Route::get('/create', [TogetherController::class, 'togetherCreate']);
        Route::post('/', [TogetherController::class, 'togetherStore']);
        Route::get('/edit/{id}', [TogetherController::class, 'togetherEdit']);
        Route::get('/show/{id}', [TogetherController::class, 'togetherShow']);
        Route::put('/{id}', [TogetherController::class, 'togetherUpdate']);
        Route::delete('/{id}', [TogetherController::class, 'togetherDestroy']);

        Route::get('/notice/show/{id}', [TogetherController::class, 'noticeShow']);
        Route::get('/notice/create', [TogetherController::class, 'noticeCreate']);
        Route::post('/notice', [TogetherController::class, 'noticeStore']);
        Route::get('/notice/edit/{id}', [TogetherController::class, 'noticeEdit']);
        Route::post('/notice/{id}', [TogetherController::class, 'noticeUpdate']);
        Route::delete('/notice/{id}', [TogetherController::class, 'noticeDestroy']);

        Route::get('/post/create', [TogetherController::class, 'postCreate']);
        Route::post('/post', [TogetherController::class, 'postStore']);
        Route::get('/post/edit/{id}', [TogetherController::class, 'postEdit']);
        Route::post('/post/{id}', [TogetherController::class, 'postUpdate']);
        Route::delete('/post/{id}', [TogetherController::class, 'postDestroy']);

        Route::post('/partiIn', [TogetherController::class, 'partiIn']);
        Route::post('/partiOut', [TogetherController::class, 'partiOut']);
        Route::post('/partiEnter', [TogetherController::class, 'partiEnter']);

        Route::post('/member/out', [TogetherController::class, 'memberOut']);
        Route::delete('/member', [TogetherController::class, 'memberDestroy']);

        Route::post('/fileUpload', [TogetherController::class, 'fileUpload']);
        Route::post('/search', [TogetherController::class, 'search']);
        Route::post('/upload', [TogetherController::class, 'upload']);
    });
  
    // 하트 누르기
    Route::prefix('heart')->group(function(){
        Route::post('/', [HeartController::class, 'heartStore']);
        Route::delete('/', [HeartController::class, 'heartDestroy']);
        Route::post('/list/{id}', [HeartController::class, 'heartList']);
    });
    
    // 피드 페이지
    Route::prefix('feed')->group(function(){
        Route::get('/', [FeedController::class, 'feed']);
        Route::post('/', [FeedController::class, 'feedStore']);
        Route::get('/{id}/edit', [FeedController::class, 'feedEdit']);
        Route::put('/{id}', [FeedController::class, 'feedUpdate']);
        Route::delete('/{id}', [FeedController::class, 'feedDestroy']);

        Route::get('/{id}/show', [FeedController::class, 'feedShow']);

        Route::post('/fileUpload', [FeedController::class, 'fileUpload']); // 파일 미리전송
    });
   
    Route::post('/conceal', [HomeController::class, 'conceal']);
    Route::get('download', [FeedController::class, 'download']);
    
    // 활동 이력 페이지
    Route::prefix('history')->group(function(){
        Route::get('/', [HistoryController::class, 'history']);
        Route::post('/', [HistoryController::class, 'historyCreate']);
        Route::delete('/', [HistoryController::class, 'historyDestroy']);
    });

    Route::prefix('settings')->group(function(){
        Route::get('/', [SettingsController::class, 'settings']);
        Route::post('/', [SettingsController::class, 'settingsUpdate']);
    });
    
    
    // 내 프로필 페이지
    Route::prefix('profile')->group(function(){
        Route::get('/', [ProfileController::class, 'profile']);
        Route::put('/', [ProfileController::class, 'profileUpdate']); // 프로필 편집(수정)

        Route::post('/fileUpload/{type}', [ProfileController::class, 'fileUpload']); // 파일 미리 업로드
        Route::post('/inputCheck', [ProfileController::class, 'inputCheck']);

        // 고양이 관련
        Route::get('/mycats', [ProfileController::class, 'catsIndex']);
        Route::get('/mycats/create', [ProfileController::class, 'catsCreate']);
        Route::post('/mycats', [ProfileController::class, 'catsStore']);
        Route::get('/mycats/{id}', [ProfileController::class, 'catsShow']);
        Route::delete('/mycats/{id}', [ProfileController::class, 'catsDestroy']);
        Route::get('/mycats/{id}/edit', [ProfileController::class, 'catsEdit']);
        Route::put('/mycats/{id}', [ProfileController::class, 'catsUpdate']);
        
        // 친구 관련
        Route::get('/myfriends', [ProfileController::class, 'myfriends']);
        Route::get('/friendcats', [ProfileController::class, 'friendcats']);
        Route::post('/relation', [ProfileController::class, 'relation']);

        // 피드 추가 로드
        Route::post('/more', [ProfileController::class, 'moreFeed']);
    });



    // 댓글 페이지 
    Route::prefix('reply')->group(function(){
        Route::get('/{id}', [ReplyController::class, 'reply']); // 댓글 목록 보기
        Route::post('/{id}', [ReplyController::class, 'replyStore']); // 댓글 작성
        Route::delete('/{id}', [ReplyController::class, 'replyDestroy']); // 댓글(메인) 삭제
    });

    // 이벤트 페이지
    Route::prefix('event')->group(function(){
        Route::get('/', [EventController::class, 'event']);
        Route::post('/', [EventController::class, 'eventStore']);
        Route::get('/success', [EventController::class, 'eventSuccess']);
        Route::get('/{id}', [EventController::class, 'eventShow']);
        Route::post('/getInfo', [EventController::class, 'lastGetInfo']);
    });


    // 문의하기 페이지
    Route::prefix('ask')->group(function(){
        Route::get('/', [AskController::class, 'ask']);
        Route::post('/', [AskController::class, 'askStore']);
        Route::delete('/', [AskController::class, 'askDestroy']);
    });

    // 신고하기 페이지
    Route::prefix('report')->group(function(){
        Route::get("/", [ReportController::class, 'report']);
        Route::post("/", [ReportController::class, 'reportStore']);
    });

    Route::get('/notice/{id}', [HomeController::class, 'noticeShow']);

});


Route::get('/guest/{writing}', [EventController::class, 'guestPage']);

////////////////////////////////////
// 관리자 페이지
////////////////////////////////////

Route::prefix('admin')->group(function(){
    Route::get('/login', [AdminController::class, 'admin']);
    Route::post('/check', [AdminController::class, 'adminCheck']);
    Route::get('/logout', [AdminController::class, 'adminLogout']);

    Route::group(['middleware'=> ['auth.master']], function(){
        Route::get('/home', [AdminController::class, 'home']);
    
        // 회원 관리
        Route::prefix('member')->group(function(){
            Route::get('/', [AdminController::class, 'member']);
            Route::get('/excel', [AdminController::class, 'member_excel']);
            Route::get('/{id}', [AdminController::class, 'memberShow']);
            Route::delete('/{id}', [AdminController::class, 'memberDestroy']);
        });
        Route::get('master', [AdminController::class, 'master']);
        
        // 게시글 관리 :: 게시물
        Route::prefix('feed')->group(function(){
            Route::get('/', [AdminController::class, 'feed']);
            Route::get('/{id}', [AdminController::class, 'feedShow']);
            Route::delete('/', [AdminController::class, 'feedDestroy']);
        });

        // 게시글 관리 :: 댓글
        Route::prefix('reply')->group(function(){
            Route::get('/', [AdminController::class, 'reply']);
            Route::delete('/', [AdminController::class, 'replyDestroy']);
        });

        // 게시글 관리 :: 태그
        Route::prefix('tag')->group(function(){
            Route::get('/', [AdminController::class, 'tag']);
            Route::delete('/', [AdminController::class, 'tagDestroy']);
        });
        // 투게더 관리
        Route::prefix('together')->group(function(){
            Route::get('/', [AdminController::class, 'together']);
        });
        // 문의하기
        Route::prefix('ask')->group(function(){
            Route::get('/', [AdminController::class, 'ask']);
            Route::get('/{id}', [AdminController::class, 'askShow']);
            Route::post('/{id}', [AdminController::class, 'askUpdate']);
        });
        // 신고하기
        Route::prefix('report')->group(function(){
            Route::get('/', [AdminController::class, 'report']);
            Route::get('/show/{report}',[AdminController::class, 'reportShow']);
            Route::post('/update/{id}', [AdminController::class, 'reportUpdate']);
        });
        // 푸시
        Route::prefix('push')->group(function(){
            Route::get('/', [AdminController::class, 'push']);
            Route::post('/send', [AdminController::class, 'send_message']);
            Route::post('/getTokens', [AdminController::class, 'getTokens']);

            Route::get('/history', [AdminController::class, 'pushHistory']);

            Route::get('/template', [AdminController::class, 'pushTemplate']);
            Route::get('/template/{id}', [AdminController::class, 'getPushTemplate']);
            Route::post('/template', [AdminController::class, 'pushTemplateStore']);
            Route::put('/template/{id}', [AdminController::class, 'pushTemplateUpdate']);
            Route::delete('/template/{id}', [AdminController::class, 'pushTemplateDestroy']);
        });

        // 공지사항 이벤트 통합
        Route::prefix('writing')->group(function(){
            Route::get('/', [AdminController::class, 'writing']);
            Route::get('/create', [AdminController::class, 'writingCreate']);
            Route::post('/', [AdminController::class, 'writingStore']);
            Route::get('/edit/{id}', [AdminController::class, 'writingEdit']);
            Route::post('/update/{id}', [AdminController::class, 'writingUpdate']);
            Route::delete('/{id}', [AdminController::class, 'writingDestroy']);

            Route::post('/active', [AdminController::class, 'writingActive']);
            
            Route::get('/partiList', [AdminController::class, 'partiList']);
            Route::get('/excelSave', [AdminController::class, 'parti_excel']);
        });
    
        Route::post('/upload', [AdminController::class, 'upload']);
    });
});