<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, user-scaleable=no" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>너만없는 고양이 :: 관리자 페이지</title>    
    <link rel="stylesheet" href="{{asset('admin/css/common.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css">
	@yield('style')

</head>
<body>
    <div id="wrap">
		<header id="header">
			@include('layouts.admin.header')
		</header>
		<!-- //헤더 -->
		<nav id="nav">
			@include('layouts.admin.nav')
		</nav>
		<!-- //메뉴 -->
		@yield('contents')
    </div>
</body>
</html>

<script type="text/javascript" src="{{asset('admin/js/jquery-3.5.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/script.js')}}"></script>
@yield('script')