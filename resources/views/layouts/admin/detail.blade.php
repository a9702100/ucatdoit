<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('admin/css/common.css')}}">
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
    @yield('style')
    <title>@yield('title')</title>
</head>
<body>
    @yield('contents')
</body>
</html>

<script type="text/javascript" src="{{asset('admin/js/jquery-3.5.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('js/swiper.min.js')}}"></script>
@yield('script')