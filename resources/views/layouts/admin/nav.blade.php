<ul>
    <li>
        <a href="{{url('/admin/home')}}">대시보드</a>
    </li>
    <li>
        <p class="arrow">
            회원 관리 <img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/member?state=all')}}">회원 리스트</a></li>
            <li><a href="{{url('/admin/master')}}">관리자 리스트</a></li>
        </ul>
    </li>
    <li>
        <p class="arrow">
            피드 관리 <img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/feed')}}">피드 리스트</a></li>
            <li><a href="{{url('/admin/reply')}}">댓글 리스트</a></li>
            <li><a href="{{url('/admin/tag')}}">태그 리스트</a></li>
        </ul>
    </li>
    <li>
        <p class="arrow">
            투게더 관리 <img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/together')}}">투게더 리스트</a></li>
        </ul>
    </li>
    <li>
        <p class="arrow">
            이벤트/공지사항 관리<img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/writing')}}">이벤트/공지사항 목록</a></li>
        </ul>
    </li>
    <li>
        <p class="arrow">
            문의내역 관리 <img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/ask')}}">문의내역</a></li>
        </ul>
    </li>
    <li>
        <p class="arrow">
            신고하기 <img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/report')}}">신고내역</a></li>
        </ul>
    </li>
    <li>
        <p class="arrow">
            푸시 메세지<img src="{{asset('images/icon/icon_arrow_down_m.svg')}}" alt="up arrow">
        </p>
        <ul class="sub-menu">
            <li><a href="{{url('/admin/push')}}">메세지 발송</a></li>
            <li><a href="{{url('/admin/push/history')}}">메세지 전송내역</a></li>
            <li><a href="{{url('/admin/push/template')}}">메세지 템플릿</a></li>
        </ul>
    </li>
</ul>