<ul class="col-group">
    <li><a href="{{url('/home')}}"><img src="{{asset('images/icon/nav/home.svg')}}" alt=""></a></li>
    <li><a href="{{url('/together')}}"><img src="{{asset('images/icon/nav/together.svg')}}" alt="" style="width: 28px;"></a></li>
    <li><a href="{{url('/feed')}}"><img src="{{asset('images/icon/nav/plus.svg')}}" alt=""></a></li>
    <li><a href="{{url('/history')}}" class="bell"><img src="{{asset('images/icon/nav/bell.svg')}}" alt=""><div class="red-dot {{ isset($isHistory) && $isHistory ? '' : 'hide'}}"></div></a></li>
    <li><a href="{{url('/profile')}}"><img src="{{asset('images/icon/nav/user.svg')}}" alt=""></a></li>
</ul>
<!-- 아이폰사이즈에 맞게 밑에가 올라와 있습니다 -->