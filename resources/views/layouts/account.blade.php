<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>너만없는고양이</title>
    <link rel="stylesheet" type="text/css" href="{{asset('css/common.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    @yield('style')
</head>
<body>
    @yield('contents')
</body>
</html>

<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
@yield('script')