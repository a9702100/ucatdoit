@extends('layouts.home')
@section('contents')
@if($popups->count() > 0)
<div class="main-popup-box hide">
    <div class="inner">
        <div class="swiper-container popup-swiper">
            <div class="swiper-wrapper">
                @foreach($popups as $popup)
                <div class="swiper-slide">
                    <div class="img-box">
                        <img src="{{asset('storage/uploads/writings/'.$popup->files->where('reference','advertise')->first()->fn)}}" alt="">
                        @if($popup->type == "notice")
                        <div class="col-group"><a href="{{url('/event/'.$popup->id)}}">자세히 보기</a></div>
                        @else
                        <div class="col-group"><a href="{{url('/event/'.$popup->id)}}">이벤트 참여하기</a></div>
                        @endif
                    </div>  
                </div>
                @endforeach
            </div>
            <div class="swiper-pagination popup-pagination"></div>
        </div>
        <div class="btn-box col-group">
            <button onclick="close_24();">하루동안 보지 않기</button>
            <button class="btn-close">닫기</button>
        </div>
    </div>
</div>
@endif
<div id="wrap" class="main-wrap back-wrap" data-member="{{$member_id}}">
    <div>
        <div class="sub-head col-group">
            @if($isEvent > 0)
            <a class="isEvent" href="{{url('/event?tab=1')}}"><img src="{{asset('images/icon/icon-gift.svg')}}" alt=""><div class="red"></div></a>
            @else 
            <a href="{{url('/event?tab=1')}}"><img src="{{asset('images/icon/icon-gift.svg')}}" alt=""><div class="red"></div></a>
            @endif
            <p class="logo"><a href="{{url('/home')}}"><img src="{{asset('images/logo.svg')}}" alt=""></a></p> 
            <a href="{{url('/search')}}"><img src="{{asset('images/icon/icon-search.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            @if($ongoings->count() > 0)
            <div class="event-box swiper-container vertical-slide">
                <ul class="swiper-wrapper">
                    @foreach($ongoings as $ongoing)
                    <li class="swiper-slide">
                        <a href="{{url('/event/'.$ongoing->id)}}" class="col-group">
                            <div class="col-group">
                                @switch($ongoing->type)
                                    @case("notice")
                                        <span>공지사항</span>
                                        @break
                                    @case("event")
                                        <span>이벤트</span>
                                        @break
                                    @case("experience")
                                        <span>체험단</span>
                                        @break
                                    @case("affiliate")
                                        <span>제휴할인</span>
                                        @break
                                @endswitch
                                <p>{{$ongoing->title}}</p>
                            </div>
                            <div class="col-group">
                                <label style="font-size:12px">자세히보기</label><img src="{{asset('images/icon/icon_arrow_right_s.svg')}}" alt="">
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="rank-box swiper-container mySwiper">
                <h2 class="col-group"><img src="{{asset('images/fire.png')}}" alt="">주간 인기 순위</h2>
                <ul class="col-group swiper-wrapper">
                    @foreach($ranking as $rank)
                    <li class="swiper-slide">
                        <div class="img-box" data-fn="{{$rank->id}}">
                            @if(isset($rank->files))
                                @if($rank->files->first()->ext == "mp4")
                                <a href="{{url('/feed/'.$rank->id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$rank->files->first()->reference)}}" alt="" class=""></a>
                                @else 
                                <a href="{{url('/feed/'.$rank->id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$rank->files->first()->fn)}}" alt="" class=""></a>
                                @endif
                            @endif
                        </div>
                        <div class="cnt-box">
                            <img src="{{asset('images/icon/icon-like-on.svg')}}">
                            <span>{{number_format($rank->heart_cnt)}}</span>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="stroy-box">
                <ul id="story">
                    @foreach($feeds as $feed)
                    <li id="story{{$feed->id}}">
                        <div class="container-all">
                            <div class="top col-group">
                                <div class="left">
                                    <div class="box">
                                        @if(isset($feed->feedable->upload))
                                        <img class="my-img" src="{{asset('storage/uploads/profile/'.$feed->feedable->upload->fn)}}" alt="">
                                        @else
                                        <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                        @endif
                                    </div> 
                                    <a href="{{url('/profile?other='.$feed->feedable->id)}}">{{$feed->feedable->nick}}</a>
                                    @if($feed->feedable->id != $member_id && $feed->feedable->friend->where('my_idx',$member_id)->isEmpty())
                                    <button class="rel{{$feed->feedable->id}}" onclick="relation('{{$feed->feedable->id}}','apply');"><span>친구신청</span></button>
                                    @endif
                                </div>
                                <div class="right">
                                    <img class="btn-dots" src="{{asset('images/icon/icon-dots.svg')}}" alt="">
                                    <div class="dot-fixed">
                                        @if(session('member') == $feed->feedable->id)
                                        <p class="col-group"><a class="col-group" onclick="edit('{{$feed->id}}')">수정하기<img src="{{asset('images/icon/icon-pencil.svg')}}" alt=""></a></p>
                                        <p class="col-group"><a class="col-group" onclick="del_confirm('{{$feed->id}}')">삭제하기<img src="{{asset('images/icon/icon-trash.svg')}}" alt=""></a></p>
                                        @else 
                                        <p class="col-group"><a class="col-group" onclick="conceal_confirm('{{$feed->id}}')">차단하기<img src="{{asset('images/icon/icon-blocked.svg')}}" alt=""></a></p>
                                        <p class="col-group"><a class="col-group" onclick="report('{{$feed->feedable->id}}', '{{$feed->id}}')">신고하기<img src="{{asset('images/icon/icon-warning.svg')}}" alt=""></a></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="img-box">
                                <div id="feed{{$feed->id}}" class="swiper-container main-slide">       
                                    <ul class="swiper-wrapper">
                                        @foreach($feed->files as $file)
                                        <li class="swiper-slide" style="margin-top: 0">
                                            @if($file->ext == "mp4")
                                            <video poster="{{asset('storage/uploads/feed/'.$file->reference)}}" controls muted loop autoplay playsinline controlsList="nodownload" preload="metadata">
                                                <source src="{{asset('storage/uploads/feed/'.$file->fn)}}" type="video/mp4" alt="">
                                                죄송합니다. 이 브라우저에서 지원하지 않는 영상입니다
                                            </video>
                                            @else 
                                            <img src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt="">
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div id="swiper-page{{$feed->id}}" class="main-pagination"></div>
                            </div>
                            <div class="center">
                                <div class="col-group">
                                    <div class="btn-like col-group" data-feed="{{$feed->id}}">
                                        @if($feed->heart->where('member_id',$member_id)->isNotEmpty()) 
                                        <img class="like{{$feed->id}}"  data-act="1" data-id="{{$feed->id}}" src="{{asset('images/icon/icon-like-on.svg')}}" alt="">
                                        @else                                     
                                        <img class="like{{$feed->id}}" data-act="0" data-id="{{$feed->id}}" src="{{asset('images/icon/icon-like-off.svg')}}" alt="">
                                        @endif
                                    </div>
                                    <div class="reply-list col-group" data-feed="{{$feed->id}}">
                                        <img src="{{asset('images/icon/icon-reply.svg')}}" alt=""><span>{{number_format($feed->replies->count())}}</span>
                                    </div>
                                </div>
                                <div class="btn-share" data-feed="{{$feed->id}}">
                                    <img src="{{asset('images/icon/icon-share.svg')}}" alt="">
                                </div>
                            </div>
                            <div class="likecnt like-list" data-feed="{{$feed->id}}">
                                좋아요 <span id="likecnt{{$feed->id}}">{{number_format($feed->heart->count())}}</span>개
                            </div>
                            <div class="txt-box">
                                <pre>{{$feed->content}}</pre>
                                <div class="tag col-group">
                                @foreach($feed->tags as $tag)
                                    <a href="{{url('/search/show?content='.$tag->content)}}">#{{$tag->content}}</a>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @if($feeds->isNotEmpty())
                <div id="moreBtn" class="more-button">
                    <a class="moreFeed" data-load="0" onclick="more_feed('{{$feeds->last()->id}}');"></a>
                </div>
                @endif
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
    <div class="popup-box share hide">
        <div class="inner">
            <div class="share-popup">
                <div>
                    <p>공유하기</p>
                    <div class="col-group">
                        <a onclick="kakao_link();"><img src="{{asset('images/sns-kakao.png')}}" alt=""></a>
                        <a onclick="facebook_link();"><img src="{{asset('images/sns-facebook.png')}}" alt=""></a>
                        <a onclick="share_link();"><img src="{{asset('images/sns-link.svg')}}" alt=""></a>
                    </div> 
                </div>
                <div class="col-group btn">
                    <button class="bold btn-close">닫기</button>
                </div>
            </div>
        </div>
    </div>
    <div id="confirm-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                    </div>
                    <div id="confirm-btn" style="display:flex">
                        <button onclick="confirm_false();">취소</button>
                        <button onclick="confirm_true();">확인</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="alert-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3 id="alert-tit">알림</h3>
                        <p id="alert-msg"></p>
                    </div>
                    <button id="alert-yes" onclick="alert_close();">확인</button>
                </div>
            </div>
        </div>
    </div>
    @if($newbie)
    <div id="newbie-box" class="join-wrap-popup popup02">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>닉네임변경</h3>
                        <p>가입을 축하합니다<br>원활한 이용을 위해 닉네임을 입력해주세요
                            <input type="text" id="nick" placeholder="닉네임을 입력하세요">
                            <span id="nick-msg"></span>
                        </p>
                    </div>
                    <button onclick="apply_nick();">닉네임 적용</button>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<div class="like-box hide">
    <div class="sub-head col-group">
        <a onclick="javascript:history.back(); return false;">
            <img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt="">
        </a>
        <p>좋아요</p> 
        <img id="refresh" class="no-img" src="{{asset('images/icon/icon-clock.svg')}}" alt="">
    </div>
    <div class="sub-cont">
        <div class="empty hide">
            <img src="{{asset('images/icon/icon-like-off.svg')}}"  alt="">
            <span>아직 좋아요를 누른 집사가 없습니다</span>
        </div>
        <div class="full">
            
        </div>
    </div>
</div>
<div class="reply-box hide">
    <div class="sub-head col-group">
        <a onclick="javascript:history.back(); return false;">
            <img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt="">
        </a>
        <p>댓글</p> 
        <img class="no-img" src="{{asset('images/icon/icon-clock.svg')}}" alt="">
    </div>
    <div id="cmtInfo" class="sub-head col-group hide">
        <a onclick="javascript:history.back(); return false;">
            <img src="{{asset('images/icon/icon_close_w.svg')}}" alt="">
        </a>
        <p><span id="sel_cnt"></span>개 선택됨</p> 
        <div class="col-group">
            <img id="btn-conceal" class="button hide" src="{{asset('images/icon/visibility_off_white_24dp.svg')}}" alt="">
            <img id="btn-delete" class="button hide" src="{{asset('images/icon/delete_white_24dp.svg')}}" alt="">
        </div>
    </div>
    <div class="sub-cont">
        <div class="empty hide">
            <img src="{{asset('images/icon/icon-reply.svg')}}" alt="">
            <span>아직 댓글이 없습니다</span>
        </div>
        <div class="full">
            
        </div>
    </div>
    <div class="sub-foot">
        <div class="towho col-group hide" data-group="">
            <span class="msg">
                <strong class="nick">빨간새우</strong>에게 답글 다는 중...
            </span>
            <a onclick="javascript:history.back(); return false;">
                <img src="{{asset('images/icon/icon_close_s.svg')}}" alt="">
            </a>
        </div>
        <div class="input-box" data-level="" data-feed="">
            <input type="text" id="content" placeholder="댓글을 입력하세요">
            <button onclick="replyStore();">작성</button>
        </div>
    </div>
    <div class="at hide">
        <div class="searching">
            <img src="{{asset('images/icon/icon-loading.gif')}}" alt="">
            <span>닉네임 검색중...</span>
        </div>
        <div class="result-wrap hide">
            <div class="result">
                <div class="left">
                    <span class="profile">
                        <img src="{{asset('images/profile-img.svg')}}" alt="">
                    </span>
                </div>
                <div class="right">
                    <p>빨간새우</p>
                    <p>아직 고양이는 없어요</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="report-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3 id="confirm-tit">신고하기</h3>
                    <p id="confirm-msg">지적 재산권 침해를 신고하는 경우를 제외하고 <br> 회원님의 신고는 익명으로 처리됩니다. <br> 누군가 위급한 상황에 있다고 생각된다면 <br>  즉시 현지 응급 서비스 기관에 연락하시기 바랍니다.</p>
                </div>
                <div class="questions">
                    <div class="question">
                        <span>이 계정을 신고하는 이유는 무엇인가요?</span>
                        <select name="" id="answer1">
                            <option value="members" data-answer="members">계정 신고</option>
                            <option value="feeds" data-answer="feeds">게시물 신고</option>
                        </select>
                    </div>
                    <div class="question">
                        <span>신고 대상에 대한 분류를 선택해주세요</span>
                        <select name="" id="answer2">
                            <option class="members" value="타인 사칭">타인 사칭</option>
                            <option class="members" value="적합하지 않은 컨텐츠 게시">적합하지 않은 컨텐츠 게시</option>
                            <option class="feeds" value="스팸" hidden>스팸</option>
                            <option class="feeds" value="나체 이미지 또는 성적행위" hidden>나체 이미지 또는 성적행위</option>
                            <option class="feeds" value="마음에 들지 않습니다" hidden>마음에 들지 않습니다</option>
                            <option class="feeds" value="혐오 발언 또는 상징" hidden>혐오 발언 또는 상징</option>
                            <option class="feeds" value="사기 또는 거짓" hidden>사기 또는 거짓</option>
                            <option class="feeds" value="거짓 정보" hidden>거짓 정보</option>
                            <option class="feeds" value="따돌림 또는 괴롭힘" hidden>따돌림 또는 괴롭힘</option>
                            <option class="feeds" value="폭력 또는 위험한 단체" hidden>폭력 또는 위험한 단체</option>
                            <option class="feeds" value="지적 재산권 침해" hidden>지적 재산권 침해</option>
                            <option class="feeds" value="자살 또는 자해" hidden>자살 또는 자해</option>
                        </select>
                    </div>
                </div>
                <div style="display: flex">
                    <button id="report-no">취소</button>
                    <button id="report-yes">신고하기</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
    .popup-swiper {border-radius: 8px}
</style>
@endsection
@section('script')
<script src="https://developers.kakao.com/sdk/js/kakao.js"></script>
<script src="{{asset('js/share.js')}}"></script>
<script src="{{asset('js/pulltorefresh.js')}}"></script>
<script>
// ready 
$(document).ready(function(){
    //로그인 유지
    const member = $("#wrap").data("member");
    localStorage.setItem("member",member);

    // 팝업 관련
    const date1 = getToday();
    const date2 = localStorage.getItem("today");

    if(date1 == date2 && date2 != null){
        // 팝업 열지 않음
        $(".main-popup-box").addClass("hide");
    }else{
        // 
        localStorage.removeItem('today');
        $(".main-popup-box").removeClass("hide");
    }           
    
    const filter = "win16|win32|win64|mac|"; // PC일 경우 가능한 값
    if(navigator.platform){
        if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
            console.log("Mobile conn..");
            let ua = navigator.userAgent.toLowerCase();
            // alert(ua); 
            if(ua.indexOf('android') > -1){
                // 안드로이드
                getToken(member);
            }else if(ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1 || ua.indexOf('macintosh') > -1){
                // 애플
                webkit.messageHandlers.callbackHandler.postMessage("MessageBody");
            }else{
                // 아이폰, 안드로이드 외
            }
        }else{
            console.log("PC conn");
        }
    }

    // 팝업 슬라이드
    new Swiper(".popup-swiper",{
        slidePerView: 1,
        autoHeight: true,
        pagination: {
            el: ".popup-pagination",
        }
    });

})

// 피드 슬라이드 - 각 피드당 슬라이드 매기기
$(".main-slide").each(function(index){
    new Swiper($(this)[0],{
        autoHeight: true,
        pagination: {
            el: $(".main-pagination")[index],
        }
    });
})

// 랭킹 슬라이드
new Swiper(".mySwiper", {
    slidesPerView: 4,
    loopSlides : 1,
    spaceBetween: 20,
    loop: true,
    autoplay: {
        delay: 2000,
    },
    breakpoints : {
        375 : {
            slidesPerView: 3
        },
        425 : {
            slidesPerView: 4
        }
    }
});

// 메인 이벤트 위아래 슬라이드
new Swiper(".vertical-slide", {
    spaceBetween:0,
    direction: "vertical",
    loop: true,
    autoplay:true,
    autoHeight: true,
});

// 수정하기, 삭제하기, 신고하기 버튼 제어
$(document).on("click",".btn-dots", function(){
    let index = $(".btn-dots").index(this); // 몇번째?
    if($(".dot-fixed").eq(index).is(":visible")){
        // 선택한 것이 열려진 상태면
        $(".dot-fixed").eq(index).hide();
    }else{
        // 선택한 것이 닫힌 상태면
        $(".dot-fixed").hide();
        $(".dot-fixed").eq(index).show();
    }
});

// 공유하기
var feed_id = "";
$(document).on("click",".btn-share",function(){
    const index = $(".btn-share").index($(this));
    feed_id = $(".btn-share").eq(index).data("feed");

    $(".popup-box").removeClass("hide");
});

// 피드 수정
function edit(id){
    location.href = "/feed/"+id+"/edit";
}

// 피드 삭제
function destroy(id){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/feed/"+id,
        type : "delete",
        dataType : "json",
        global : false,
        success : function(data){
           if(data["success"]){
               alert("삭제되었습니다");
               location.reload();
           }
        }
    })   
}

// 피드 차단
function conceal(id){
    $.ajax({
        headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/conceal",
        type : "post",
        data : {"concealable_type":"feeds", "concealable_id":id},
        dataType : "json",
        global : false, 
        success : function(data){
            if(data["success"]){
                $("#story"+id).remove();
            }
        }
    })
}

// 알림창 확인
$(document).on("click", "#alert-yes", function(){
    let direct = $("#alert-yes").data("home");

    console.log(direct);
    $('#alert-box').addClass("hide");
    if(direct == "true"){
        location.href = "/";
    }
});

var r_member = ""; // 신고할 멤버
var r_feed   = ""; // 신고할 피드
// 신고하기
function report(member, feed){
    r_member = member;
    r_feed   = feed;

    $("#report-box").removeClass("hide");
}

// 신고창 취소
$(document).on("click", "#report-no", function(){
    $("#report-box").addClass("hide");
});

// 신고창 확인
$(document).on("click", "#report-yes", function(){
    let reportable_type  = $("#answer1").val();  // 신고 타입
    let reason           = $("#answer2").val();  // 신고 사유

    let data = {"id": 0, "type": reportable_type, "reason": reason};
    if(reportable_type == "members"){
        data["id"] = r_member;
    }else {
        data["id"] = r_feed;
    }


    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type : "post",
        url : "/report",
        data : data,
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $("#report-box").addClass("hide");
                if(data["after"] == "home"){
                    $("#alert-yes").data("home", "true");
                }
                $("#alert-tit").html("신고하기");
                if(reportable_type == "members"){
                    $("#alert-msg").html("정상적으로 신고접수되었으며<br>더이상 이 계정을 노출시키지 않습니다");
                }else{
                    $("#story"+r_feed).remove();
                    $("#alert-msg").html("정상적으로 신고접수되었습니다");
                }
                $(".dot-fixed").hide();
                $("#alert-box").removeClass("hide");
            }
        }
    })


});

// 신고 사유 선택값에 따른 옵션 변경
$(document).on("change","#answer1", function(){
    let answer = $("#answer1 option:selected").data("answer");

    if(answer == "feeds"){
        // 게시물 신고
        $("#answer2 option").eq(2).prop("selected", true);
    }else{
        // 계정 신고
        $("#answer2 option").eq(0).prop("selected", true);
    }

    for (let i = 0; i < $("#answer2 option").length; i++) {
        if($("#answer2 option").eq(i).hasClass(answer)){
            $("#answer2 option").eq(i).css("display", "block");
        }else{
            $("#answer2 option").eq(i).css("display", "none");
        }
        
    }
});

// 삭제 확인용 confirm
function del_confirm(id){
    let confirm_btn = "<button onclick='confirm_false();'>취소</button>\
                      <button onclick=\"confirm_true('"+id+"','delete');\">확인</button>";

    $('#confirm-msg').html("모든 내용이 삭제됩니다<br>삭제하시겠습니까?");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide");
}

// 차단 확인용 confirm
function conceal_confirm(id){
    let confirm_btn = "<button onclick='confirm_false();'>취소</button>\
                      <button onclick=\"confirm_true('"+id+"','conceal');\">확인</button>";

    $('#confirm-msg').html("차단하면 더이상 노출되지않습니다<br>해당 피드를 차단하시겠습니까?");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide"); 
}

function confirm_true(id, action){
    if(action == "delete"){
        destroy(id);
    }else if(action == "conceal"){
        conceal(id);   
    }
    $("#confirm-box").addClass("hide");
}

function confirm_false(){
    $("#confirm-box").addClass('hide');
}

// function alert_close(){
//     $("#alert-box").addClass("hide");
// }

//프로필 이동 - 좋아요, 댓글 공통
$(document).on("click",".people .left, .comment .profile", function(event){
    event.stopPropagation();
    let member_id = $(this).data("member");
    history.pushState(null, null, "/home");
    location.href = "/profile?other="+member_id;
});

// 친구 관계 변경 :: 피드 , 좋아요
function relation(id, action, where = ""){
    $.ajax({
        headers : {'X-CSRF-TOKEN' : $("meta[name='csrf-token']").attr('content')},
        url : "/profile/relation",
        type : "post",
        data : {"friend_idx" : id, "action" : action},
        dataType : "json",
        global : false,
        success : function(data){
            if(where == "like"){
                if(action == "apply"){
                    $("#bt"+id).attr("onclick","relation('"+id+"','wait','like');").html("수락 대기");
                }else if(action == "wait"){
                    $("#bt"+id).attr("onclick","relation('"+id+"','apply','like');").html("친구 신청");
                }
            }
            if(action == "apply"){
                $(".rel"+id).remove();
            }
        }
    })
        
}

/* 좋아요 관련 */
// 좋아요 누름
$(document).on("click",".btn-like", function(){
    let index = $(this).data("feed"); // 좋아요 눌린 피드 고유번호
    let act    = $(".like"+index).data("act"); // 누른 버튼의 action 값 (0: 빈하트 /1: 하트)
    let id     = $(".like"+index).data("id");  // 피드의 아이디
    let method = act == 0 ? "post" : "delete"; 
    let heart_cnt = Number($("#likecnt"+index).text());
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/heart", 
        type : method,
        data : {"act":act, "heartable_type": "feeds", "heartable_id": id},
        dataType : "json",
        global : false, 
        success : function(data){
            if(act == 0){
                // 빈하트 -> 하트
                $(".like"+index).attr("src", "{{asset('images/icon/icon-like-on.svg')}}");
                $(".like"+index).data("act", "1");
                $("#likecnt"+index).html(heart_cnt+1);
            }else{
                // 하트 -> 빈하트
                $(".like"+index).attr("src", "{{asset('images/icon/icon-like-off.svg')}}");
                $(".like"+index).data("act", "0");
                $("#likecnt"+index).html(heart_cnt-1);
            }
        }
    })

})

// 좋아요 리스트 오픈
$(document).on("click",".like-list", function(){
    let index = $(this).data("feed"); // 피드 고유번호
    if($(".like-box").hasClass("hide") || $(".like-box").hasClass("slideToBot")){
        history.pushState(null, null, "2"); // 가짜 히스토리 추가 -- 뒤로가기 제어를 위함
        $(".like-box").removeClass("hide").removeClass("slideToBot").addClass("slideToTop");
        $("body").addClass("modal-open");


        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            url : "/heart/list/"+index,
            type : "post",
            data : {"heartable_type" : "feeds"},
            dataType : "json",
            success : function(data){
                if(data["success"]){
                    console.log(data["hearts"]);
                    if(data["hearts"].length > 0){
                        // 좋아요 있음
                        $(".like-box .empty").addClass("hide");
                        $(".like-box .full").removeClass("hide").empty();
                        data["hearts"].forEach(heart => {
                            let str = "<div class=\"people\">\
                                        <div class=\"left col-group\" data-member=\""+heart["member_id"]+"\">";
                                            if(heart["profile"] == ""){
                                                str += "<img src=\"/images/profile-img.svg\" alt=\"\">";    
                                            }else{
                                                str += "<img src=\"/storage/uploads/profile/"+heart["profile"]+"\" alt=\"\" >";
                                            }
                                            str += "<span>"+heart["nick"]+"</span>\
                                        </div>\
                                        <div class=\"right col-group\">";
                                        switch (heart["isFriend"]) {
                                            case "me":
                                                str += "<button>나</button>";
                                                break;
                                            case "not":
                                                str += "<button id=\"bt"+heart["member_id"]+"\" onclick=\"relation('"+heart["member_id"]+"','apply','like');\">친구 신청</button>";
                                                break;
                                            case "apply":
                                                str += "<button id=\"bt"+heart["member_id"]+"\" onclick=\"relation('"+heart["member_id"]+"','apply','like');\">친구 신청</button>";
                                                break;;
                                            case "wait":
                                                str += "<button id=\"bt"+heart["member_id"]+"\" onclick=\"relation('"+heart["member_id"]+"','wait','like');\">수락 대기</button>";
                                                break;
                                            case "friend":
                                                str += "<button>친구</button>";
                                                break;
                                        }
                                        str += "</div>\
                                    </div>";
        
                            $(".like-box .full").append(str);
                        });
                    }else{
                        // 좋아요 없음
                        $(".like-box .empty").removeClass("hide");
                        $(".like-box .full").addClass("hide");
                    }
                }
            }
        })
    }
})

/* 댓글 관련 */
// 댓글 리스트 오픈
$(document).on("click",".reply-list", function(){
    let index = $(this).data("feed"); // 피드 고유번호

    if($(".reply-box").hasClass("hide") || $(".reply-box").hasClass("slideToRight")){
        history.pushState(null, null, "1"); // 가짜 히스토리 추가 -- 뒤로가기 제어를 위함
        $(".reply-box").removeClass("hide").removeClass("slideToRight").addClass("slideToLeft");
        $("body").addClass("modal-open");

        $.ajax({
            headers : {"X-CSRF-TOKEN" : $("meta[name='csrf-token']").attr("content")},
            url : "/reply/"+index,
            type : "GET",
            dataType : "json",
            success : function(data){
                $(".reply-list").data("writer",data["writer"]);
                if(data["status"] == "empty"){
                    $(".reply-box .empty").removeClass("hide");
                    $(".reply-box .full").addClass("hide");
                }else{
                    $(".reply-box .empty").addClass("hide");
                    $(".reply-box .full").empty().removeClass("hide");

                    data["replies"].forEach(reply => {
                        let str = "";
                        if(reply["level"] == 1){
                            str += "<div id=\"cmt"+reply["id"]+"\" class=\"comment\" data-own=\""+reply["own"]+"\" data-id=\""+reply["id"]+"\">";
                        }else{
                            str += "<div id=\"cmt"+reply["id"]+"\" class=\"comment sub\" data-own=\""+reply["own"]+"\" data-id=\""+reply["id"]+"\">";
                        }
                                str += "<div class=\"left\">\
                                            <span class=\"profile\" data-member=\""+reply["member_id"]+"\">";
                                            if(reply["member_fn"] == ""){
                                                str += "<img src=\"/images/profile-img.svg\" alt=\"\">";
                                            }else{
                                                str += "<img src=\"/storage/uploads/profile/"+reply["member_fn"]+"\" alt=\"\">";
                                            }                    
                                    str += "</span>\
                                        </div>\
                                        <div class=\"right\">\
                                            <div class=\"cont\">\
                                                <p>\
                                                    <span class=\"profile\" data-member=\""+reply["member_id"]+"\">"+reply["nick"]+"</span> ";
                                                    str += reply["content"];
                                        str += "</p>\
                                            </div>\
                                            <div class=\"ref\">\
                                                <span class=\"created\">"+timeForToday(reply["created_at"])+"</span>\
                                                <button onclick=\"towho('"+reply["id"]+"',"+reply["level"]+",'"+reply["nick"]+"');\">답글 달기</button>\
                                            </div>\
                                        </div>\
                                    </div>";
                        $(".reply-box .full").append(str);
                    });
                }
                $(".input-box").data("level","1");
                $(".input-box").data("feed",index);
            }
        })

    }
})

// 답글 달기
function towho(target, level, nick){
    event.stopPropagation();
    if($(".towho").hasClass("hide")){
        $(".towho").data("target", target); // 답글 타겟 번호
        $(".towho .nick").html(nick); // 닉네임 설정
        $(".input-box").data("level", level+1);
        $("#content").val("@"+nick+" ").focus();
        $(".towho").removeClass("hide");
        history.pushState(null, null, "4");
    }
}

// 댓글 작성 (@호출 기능)
let front_str = "";
let back_str  = "";
$(document).on("input","#content", function(){
    let text      = $("#content").val();        // 현재까지 입력한 텍스트
    let cursor    = this.selectionEnd;          // 현재 커서 끝 위치
    let cut_text  = text.substr(0, cursor);     // 처음부터 커서 위치까지의 텍스트
    let at_pos    = cut_text.lastIndexOf("@");  // cut_text에서 @의 위치 (뒤에서부터)
    let blank_pos = cut_text.lastIndexOf(" ");  // cut_text에서 공백의 위치 (뒤에서부터)

    if(at_pos > blank_pos && at_pos != cut_text.length -1){
        let find_str = cut_text.substr(at_pos+1, cursor); // 찾으려는 닉네임 문자열
        front_str = text.substr(0, at_pos+1);
        back_str  = text.substr(cursor);
        console.log("조건 충분");
        console.log(find_str);
        
        if($(".at").hasClass("hide")){
            if($(".towho").hasClass("hide")){
                $('.at').removeClass("b-124").addClass("b-80");
            }else{
                $('.at').removeClass("b-80").addClass("b-124");
            }
            $(".at").removeClass("hide").removeClass("slideToTopReverse").addClass("slideToTop");
        }

        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            url : "/search/member",
            type : "post",
            data : {"find_str" : find_str},
            dataType : "json",
            success : function(data){
                if(data["success"]){
                    // 검색 결과 있음
                    $(".searching").addClass("hide");
                    $(".result-wrap").empty().removeClass("hide");

                    data["members"].forEach(member => {
                        let str = "<div class=\"result\" onclick=\"call_member('"+member["nick"]+"');\">\
                                        <div class=\"left\">\
                                            <span class=\"profile\">";
                                    if(member["profile"] == ""){
                                        str += "<img src=\"/images/profile-img.svg\" alt=\"\">";    
                                    }else{
                                        str += "<img src=\"/storage/uploads/profile/"+member["profile"]+"\" alt=\"\" >";
                                    }    
                                    str += "</span>\
                                        </div>\
                                        <div class=\"right\">\
                                            <p>"+member["nick"]+"</p>";
                                            if(member["intro"] != null){
                                                str += "<p>"+member["intro"]+"</p>";
                                            }
                                str += "</div>\
                                    </div>";
                        $(".result-wrap").append(str);
                    })
                }else{
                    // 검색 결과 없음
                    $(".searching").removeClass("hide");
                    $(".result-wrap").addClass("hide");
                    if(!$(".at").hasClass("hide") || $(".at").hasClass("slideToTop")){
                        $(".at").removeClass("slideToTop").addClass("hide");
                    }            
                }
            }
        })
    }else{
        if(!$(".at").hasClass("hide") || $(".at").hasClass("slideToTop")){
            $(".at").removeClass("slideToTop").addClass("hide");
        }   
        console.log("조건 불충분");
    }

})

// 호출 기능 누르고 댓글 내용 변경
function call_member(nick){
    const combine = front_str+nick+" "+back_str;
    $("#content").val(combine);

    $(".at").removeClass("slideToTop").addClass("slideToTopReverse");
    $("#content").focus();
}

let a = []; // 삭제
let b = []; // 숨김
// 댓글 리스트 클릭 이벤트
$(document).on("click", ".comment", function(event){
    let active = $(".comment.active").length;
    let writer = $(".reply-list").data("writer"); 
    let own    = $(this).data("own");
    console.log(writer+","+own);

    if(!$(this).hasClass("active")){
        if(active == "0"){
            history.pushState(null, null, "3");
            $("#cmtInfo").removeClass("hide");
            $(".sub-foot").addClass("hide");
        }
        if(writer == "me"){
            a.push($(".comment").index(this));
            if(own != "me"){
                b.push($(".comment").index(this));
            }
        }else{
            if(own == "me"){
                a.push($(".comment").index(this));
            }else{
                b.push($(".comment").index(this));
            }
        }
        $("#sel_cnt").html(active+1);
        $(this).addClass("active");
    }else{
        $("#sel_cnt").html(active-1);
        $(this).removeClass("active");
        if(writer == "me"){
            a = a.filter((element) => element !== $(".comment").index(this));
            if(own != "me"){
               b = b.filter((element) => element !== $(".comment").index(this));
            }
        }else{
            if(own == "me"){
               a = a.filter((element) => element !== $(".comment").index(this));
            }else{
               b = b.filter((element) => element !== $(".comment").index(this));
            }
        }
        if(active == "1"){
            history.back();
        }
    }

    if(a.length > 0){
        $("#btn-delete").removeClass("hide");
    }else{
        $("#btn-delete").addClass("hide");
    }
    if(b.length > 0){
        $("#btn-conceal").removeClass("hide");
    }else{
        $("#btn-conceal").addClass("hide");
    }

    console.log(a);
    console.log(b);
})

// 선택 댓글 삭제 :: 알림창
$(document).on("click","#btn-delete", function(){
    let a_cnt = a.length;
    let confirm_btn = "<button onclick=\"confirm_false();\">취소</button>\
                      <button onclick=\"reply_delete('');\">확인</button>";


    $("#confirm-msg").html(a_cnt+"건의 댓글을 삭제하시겠습니까?<br>상위 댓글이라면 하위 댓글까지 삭제됩니다");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide");
});

// 선택 댓글 삭제 :: 확인
function reply_delete(){    
    $("#confirm-box").addClass("hide");
    let feed = $(".input-box").data("feed");
    let nums = [];
    a.forEach(index => {
        nums.push($(".comment").eq(index).data("id"));
    })
    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr('content')},
        url : "/reply/"+feed,
        type : "delete",
        data : {"nums":nums},
        dataType : "json",
        success : function(data){
            $("#alert-tit").html("알림");
            $("#alert-msg").html(data["msg"]);
            $("#alert-box").removeClass("hide");

            history.back(); // 선택됨 해제
            $(".reply-box .full").empty();
            $(".reply-list").data("writer",data["writer"]);
            if(data["status"] == "empty"){
                $(".reply-box .empty").removeClass("hide");
                $(".reply-box .full").addClass("hide");
            }else{
                $(".reply-box .empty").addClass("hide");
                $(".reply-box .full").empty().removeClass("hide");

                data["replies"].forEach(reply => {
                    let str = "";
                    if(reply["level"] == 1){
                        str += "<div id=\"cmt"+reply["id"]+"\" class=\"comment\" data-own=\""+reply["own"]+"\" data-id=\""+reply["id"]+"\">";
                    }else{
                        str += "<div id=\"cmt"+reply["id"]+"\" class=\"comment sub\" data-own=\""+reply["own"]+"\" data-id=\""+reply["id"]+"\">";
                    }
                            str += "<div class=\"left\">\
                                        <span class=\"profile\" data-member=\""+reply["member_id"]+"\">";
                                        if(reply["member_fn"] == ""){
                                            str += "<img src=\"/images/profile-img.svg\" alt=\"\">";
                                        }else{
                                            str += "<img src=\"/storage/uploads/profile/"+reply["member_fn"]+"\" alt=\"\">";
                                        }                    
                                str += "</span>\
                                    </div>\
                                    <div class=\"right\">\
                                        <div class=\"cont\">\
                                            <p>\
                                                <span class=\"profile\" data-member=\""+reply["member_id"]+"\">"+reply["nick"]+"</span> ";
                                                str += reply["content"];
                                    str += "</p>\
                                        </div>\
                                        <div class=\"ref\">\
                                            <span class=\"created\">"+timeForToday(reply["created_at"])+"</span>\
                                            <button onclick=\"towho('"+reply["id"]+"',"+reply["level"]+",'"+reply["nick"]+"');\">답글 달기</button>\
                                        </div>\
                                    </div>\
                                </div>";
                    $(".reply-box .full").append(str);
                });
            }
            $(".input-box").data("level","1");
            $(".input-box").data("feed", feed);
        }
    })
}

// 선택 댓글 차단 :: 알림창
$(document).on("click", "#btn-conceal", function(){
    let b_cnt = b.length;
    let confirm_btn = "<button onclick=\"confirm_false();\">취소</button>\
                      <button onclick=\"reply_conceal();\">확인</button>";


    $("#confirm-msg").html(b_cnt+"건의 댓글을 차단하시겠습니까?<br>더이상 노출되지 않습니다");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide");
})

// 선택 댓글 차단 :: 확인
function reply_conceal(){
    $("#confirm-box").addClass("hide");
    let feed = $(".input-box").data("feed");
    let nums = [];
    b.forEach(index => {
        nums.push($(".comment").eq(index).data("id"));
    })

    $.ajax({
        headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/conceal",
        type : "post",
        data : {"concealable_type":"replies", "concealable_id":0, "nums":nums, 'id':feed},
        dataType : "json",
        success : function(data){
            $("#alert-tit").html("알림");
            $("#alert-msg").html(data["msg"]);
            $("#alert-box").removeClass("hide");

            history.back();
            $(".reply-box .full").empty();
            $(".reply-list").data("writer",data["writer"]);
            if(data["status"] == "empty"){
                $(".reply-box .empty").removeClass("hide");
                $(".reply-box .full").addClass("hide");
            }else{
                $(".reply-box .empty").addClass("hide");
                $(".reply-box .full").empty().removeClass("hide");

                data["replies"].forEach(reply => {
                    let str = "";
                    if(reply["level"] == 1){
                        str += "<div id=\"cmt"+reply["id"]+"\" class=\"comment\" data-own=\""+reply["own"]+"\" data-id=\""+reply["id"]+"\">";
                    }else{
                        str += "<div id=\"cmt"+reply["id"]+"\" class=\"comment sub\" data-own=\""+reply["own"]+"\" data-id=\""+reply["id"]+"\">";
                    }
                            str += "<div class=\"left\">\
                                        <span class=\"profile\" data-member=\""+reply["member_id"]+"\">";
                                        if(reply["member_fn"] == ""){
                                            str += "<img src=\"/images/profile-img.svg\" alt=\"\">";
                                        }else{
                                            str += "<img src=\"/storage/uploads/profile/"+reply["member_fn"]+"\" alt=\"\">";
                                        }                    
                                str += "</span>\
                                    </div>\
                                    <div class=\"right\">\
                                        <div class=\"cont\">\
                                            <p>\
                                                <span class=\"profile\" data-member=\""+reply["member_id"]+"\">"+reply["nick"]+"</span> ";
                                                str += reply["content"];
                                    str += "</p>\
                                        </div>\
                                        <div class=\"ref\">\
                                            <span class=\"created\">"+timeForToday(reply["created_at"])+"</span>\
                                            <button onclick=\"towho('"+reply["id"]+"',"+reply["level"]+",'"+reply["nick"]+"');\">답글 달기</button>\
                                        </div>\
                                    </div>\
                                </div>";
                    $(".reply-box .full").append(str);
                });
            }
            $(".input-box").data("level","1");
            $(".input-box").data("feed", feed);
        }
    })
}

// 댓글 저장
function replyStore(){
    let text  = $("#content").val();
    let level = $(".input-box").data("level");
    let feed  = $(".input-box").data("feed");

    let send_data = {};  
    if(level == 1){
        send_data = {"content": text, "level": level};
    }else if(level > 1){
        send_data = {"content": text, "level": level, "target":$(".towho").data("target")};
    }


    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/reply/"+feed,
        type : "POST",
        data : send_data,
        dataType : "json",
        success : function(data){
            if(!data["success"]){
                $("#alert-tit").html("알림");
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
                return false;
            }
            let str = "";
                if(data["reply"]["level"] == 1){
                    str += "<div id=\"cmt"+data["reply"]["id"]+"\" class=\"comment\" data-own=\"me\" data-id=\""+data["reply"]["id"]+"\">";
                }else{
                    str += "<div id=\"cmt"+data["reply"]["id"]+"\" class=\"comment sub\" data-own=\"me\" data-id=\""+data["reply"]["id"]+"\">";
                }
        str += "<div class=\"left\">\
                    <span class=\"profile\" data-member=\""+data["reply"]["member_id"]+"\">";
                    if(data["reply"]["member_fn"] == ""){
                        str += "<img src=\"/images/profile-img.svg\" alt=\"\">";
                    }else{
                        str += "<img src=\"/storage/uploads/profile/"+data["reply"]["member_fn"]+"\" alt=\"\">";
                    }                    
            str += "</span>\
                </div>\
                <div class=\"right\">\
                    <div class=\"cont\">\
                        <p>\
                            <span class=\"profile\" data-member=\""+data["reply"]["member_id"]+"\">"+data["reply"]["nick"]+"</span> ";
                            str += data["reply"]["content"];
                str += "</p>\
                    </div>\
                    <div class=\"ref\">\
                        <span class=\"created\">"+timeForToday(data["reply"]["created_at"])+"</span>\
                        <button onclick=\"towho('"+data["reply"]["id"]+"',"+data["reply"]["level"]+",'"+data["reply"]["nick"]+"');\">답글 달기</button>\
                    </div>\
                </div>\
            </div>";

            if($(".reply-box .full").hasClass("hide")){
                $(".reply-box .empty").addClass("hide");
                $(".reply-box .full").empty().removeClass("hide");
            }

            if(data["reply"]["level"] == "1"){
                $(".reply-box .full").prepend(str);
            }else{
                $("#cmt"+$(".towho").data("target")).after(str);
                $(".input-box").data("level", 1);
                history.back();
            }
            $("#content").val("");
        }

    })
}

// 뒤로가기 이벤트 제어
window.onpopstate = function(event){
    if($(".reply-box").hasClass("slideToLeft")){
        // 댓글 관련 뒤로가기
        if(!$("#cmtInfo").hasClass("hide")){
            // 댓글 선택 취소
            $(".comment").removeClass("active");
            $("#cmtInfo").addClass("hide");
            $(".sub-foot").removeClass("hide");
            history.replaceState(null, null, "1");
            a = [];
            b = [];
            return false;
        }
        if(!$(".towho").hasClass("hide")){
            // 답글 작성 닫기
            $(".towho").addClass("hide");
            $(".input-box").data("level", 1);
            $("#content").val("");
            history.replaceState(null, null, "1");
            return false;
        }
        $(".reply-box").removeClass("slideToLeft").addClass("slideToRight");
        $("#content").val("");
        $(".at").addClass("hide");
    }else if($(".like-box").hasClass("slideToTop")){
        // 좋아요 관련 뒤로가기
        $(".like-box").removeClass("slideToTop").addClass("slideToBot");
    }
    $("body").removeClass("modal-open");
}

//뉴비 닉네임 받을 때 중복 체크(유효성검사)
$(document).on("input","#nick",function(){
    let nick = $("#nick").val();

    if($(this).val() != ""){
        // 입력값이 존재할 때
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url : "/account/newbie",
            type : "post",
            data : {"nick":nick},
            dataType : "json",
            global : false,
            success : function(data){
                if(data["success"]){
                    // 올바른 규칙
                    $("#nick-msg").addClass("green").html(data["msg"]);
                }else{
                    // 규칙에 어긋남
                    $("#nick-msg").removeClass("green").html(data["msg"]);
                }
            }
        });
    }else{
        // 입력값이 비어있을 때
        $("#nick-msg").html("");
    }
});

function apply_nick(){
    let nick = $("#nick").val();

    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/account/newbie_apply",
        type : "post",
        data : {"nick":nick},
        dataType : "json",
        global : false,
        success : function(data){
            if(data["success"]){
                $("#newbie-box").addClass("hide");
                alert(data["msg"]);
            }else{
                $('#nick-msg').removeClass("green").html(data["msg"]);
            }
        }
    });
}

// 하루동안 보지 않기
function close_24(){
    localStorage.setItem("today", getToday()); 
}

// 오늘 날짜 형식 바꾸기 [년/월/일]
function getToday(){
    const date = new Date();
    const year = date.getFullYear();
    const month = ("0"+ (1 + date.getMonth())).slice(-2);
    const day = ("0" + date.getDate()).slice(-2);

    return year + "/" + month + "/" + day;
}

// 올린시간
function timeForToday(value) {
    const today = new Date();
    const timeValue = new Date(value);

    const betweenTime = Math.floor((today.getTime() - timeValue.getTime()) / 1000 / 60);
    if (betweenTime < 1) return '방금전';
    if (betweenTime < 60) {
        return `${betweenTime}분전`;
    }

    const betweenTimeHour = Math.floor(betweenTime / 60);
    if (betweenTimeHour < 24) {
        return `${betweenTimeHour}시간전`;
    }

    const betweenTimeDay = Math.floor(betweenTime / 60 / 24);
    if (betweenTimeDay < 365) {
        return `${betweenTimeDay}일전`;
    }

    return `${Math.floor(betweenTimeDay / 365)}년전`;
}

// 스크롤 내려 피드 불러오기
$(window).scroll(function(){
    var scrT = $(window).scrollTop();

    if(scrT > ($(document).height() - $(window).height()) * 0.7 ){
        if($(".moreFeed").data("load") == "0"){
            $(".moreFeed").click();
            $(".moreFeed").data("load", "1");
        }
    }else{
        var offset = $(window).scrollTop() + $(window).height() / 2,
        $videos = $("video");
        if($videos.length > 0){
            $videos.each(function(i){
                var $video = $(this),
                item_top = $video.offset().top, item_h = $video.height();
                if(($video.offset().top) < offset && (item_top + item_h) > offset){
                    if(!$video.hasClass('video_on')){
                        $video.get(0).play();
                        $video.addClass("video_on");
                    }
                }else{
                    if($video.hasClass('video_on')){
                        $video.get(0).pause();
                        $video.removeClass('video_on');
                    }
                }
            })
        }
    }
})


function more_feed(feed_id){
    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/home/more",
        type : "post",
        data : {"feed_id":feed_id},
        dataType : "json",
        success : function(data){
            console.log(data["feeds"])
            if(data["success"]){
                // 추가로 불러올 글이 있음
                data["feeds"].forEach(feed => {
                    let str = "<li id=\"story"+feed["id"]+"\">\
                                    <div class=\"container-all\">\
                                        <div class=\"top col-group\">\
                                            <div class=\"left\">\
                                                <div class=\"box\">";
                                                if(feed["member_fn"] != ""){
                                                    str += "<img class=\"my-img\" src=\"{{asset('storage/uploads/profile/')}}/"+feed["member_fn"]+"\" alt=''>";
                                                }else{
                                                    str += "<img class=\"no-my-img\" src=\"{{asset('images/profile-img.svg')}}\" alt=''>";
                                                }
                                    str += "</div>\
                                            <a href=\"{{url('/profile?other=')}}"+feed["feedable_id"]+"\">"+feed["nick"]+"</a>";  
                                if(feed["isFriend"]){
                                    str += "<button class=\"rel"+feed["feedable_id"]+"\" onclick=\"relation('"+feed["feedable_id"]+"','apply');\"><span>친구신청</span></button>";
                                }
                             str += "</div>\
                                    <div class=\"right\">\
                                        <img class=\"btn-dots\" src=\"{{asset('images/icon/icon-dots.svg')}}\" alt=''>\
                                        <div class=\"dot-fixed\">";
                                        if(feed["isWriter"]){
                                            str += "<p class=\"col-group\"><a class=\"col-group\" onclick=\"edit('"+feed["id"]+"')\">수정하기<img src=\"{{asset('images/icon/icon-pencil.svg')}}\" alt=''></a></p>\
                                                    <p class=\"col-group\"><a class=\"col-group\" onclick=\"del_confirm('"+feed["id"]+"')\">삭제하기<img src=\"{{asset('images/icon/icon-trash.svg')}}\" alt=''></a></p>";
                                        }else{
                                            str += "<p class=\"col-group\"><a class=\"col-group\" onclick=\"conceal_confirm('"+feed["id"]+"')\">차단하기<img src=\"{{asset('images/icon/icon-blocked.svg')}}\" alt=''></a></p>\
                                                    <p class=\"col-group\"><a class=\"col-group\" href=\"{{url('/report?type=feeds&id=')}}"+feed["id"]+"\">신고하기<img src=\"{{asset('images/icon/icon-warning.svg')}}\" alt=''></a></p>";
                                        }
                                str += "</div>\
                                    </div>\
                                </div>\
                                <div class=\"img-box\">\
                                    <div id=\"feed"+feed["id"]+"\" class=\"swiper-container main-slide\">\
                                        <ul class=\"swiper-wrapper\">";
                                    feed["files"].forEach(file => {                                        
                                        str += "<li class=\"swiper-slide\" style=\"margin-top:0;\">";
                                            if(file["ext"] == "mp4"){
                                               str += "<video poster=\"{{asset('storage/uploads/feed/')}}/"+file["reference"]+"\" controls muted loop autoplay playsinline controlsList='nodownload' preload='metadata'>\
                                                            <source src=\"{{asset('storage/uploads/feed/')}}/"+file["fn"]+"\" alt=''>\
                                                       </video>";
                                            }else{
                                               str += "<img src=\"{{asset('storage/uploads/feed/')}}/"+file["fn"]+"\" alt=''>";
                                            }
                                        str += "</li>";
                                    });
                                    str +="</ul>\
                                    </div>\
                                    <div id=\"swiper-page"+feed["id"]+"\" class=\"main-pagination\"></div>\
                                </div>\
                                <div class=\"center\">\
                                    <div class=\"col-group\">\
                                        <div class=\"btn-like col-group\" data-feed=\""+feed["id"]+"\">";
                                    if(feed["isHeart"]){   
                                        str += "<img class=\"like"+feed["id"]+"\" data-act=\"1\" data-id=\""+feed["id"]+"\" src=\"{{asset('images/icon/icon-like-on.svg')}}\" alt=''>";
                                    }else{
                                        str += "<img class=\"like"+feed["id"]+"\" data-act=\"0\" data-id=\""+feed["id"]+"\" src=\"{{asset('images/icon/icon-like-off.svg')}}\" alt=''>";
                                    }
                                str += "</div>\
                                        <div class=\"reply-list col-group\" data-feed=\""+feed["id"]+"\">\
                                            <img src=\"{{asset('images/icon/icon-reply.svg')}}\" alt=\"\"><span>"+feed["replies"].length+"</span>\
                                        </div>\
                                    </div>\
                                    <div class=\"btn-share\" data-feed=\""+feed["id"]+"\">\
                                        <img src=\"{{asset('images/icon/icon-share.svg')}}\" alt=\"\">\
                                    </div>\
                                </div>\
                                <div class=\"likecnt like-list\" data-feed=\""+feed["id"]+"\">\
                                    좋아요 <span id=\"likecnt"+feed["id"]+"\">"+feed["heart"].length+"</span>개\
                                </div>\
                                <div class=\"txt-box\">\
                                    <pre>"+feed["content"]+"</pre>\
                                    <div class=\"tag col-group\">";
                                    feed["tags"].forEach(tag => { 
                                        str += "<a href=\"{{url('/search/show?content=')}}"+tag["content"]+"\">#"+tag["content"]+"</a>";
                                    });
                            str += "</div>\
                                </div>\
                            </div>\
                        </li>";

                    $("#story").append(str);
                    new Swiper($("#feed"+feed["id"]),{
                        autoHeight: true,
                        pagination: {
                            el: $("#swiper-page"+feed["id"]),
                        }
                    });
                });
                $(".dot-fixed").hide();
                $("#moreBtn").html("<a class=\"moreFeed\" data-load=\"0\" onclick=\"more_feed('"+data["last"]+"')\"></a>");
            }else{
                // 추가로 불러올 글이 없음
                $("#moreBtn").html("<a>더이상 불러올 글이 없음</a>");

            }
        }
    })
}

// 기타..

function download(filename){
    location.href = "/download?fn="+filename;
}

function getToken(id){
    window.Android.getToken(id);
}

function tokenTake(t){
    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/account/insertToken",
        data : {"token":t, "token_type": "Apple"},
        type : "post",
        dataType : "json",
        global : false,
        success : function(data){
            if(data["success"]){
                console.log(data["msg"]);
            }
        }
    })
}

function tokenPocket(token, id){
    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/account/insertToken",
        data : {"token":token, 'member_id': id},
        type : "post",
        dataType : "json",
        global : false,
        success : function(data){
            if(data["success"]){
                console.log(data["msg"]);
            }
        }
    })
}
</script>
@endsection