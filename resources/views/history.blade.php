@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap history-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>최근 7일간 이력</p> 
            <a href="{{url('/settings')}}"><img src="{{asset('/images/icon/settings_black_24dp.svg')}}" alt="" onclick="confirm();"></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            @foreach ($histories as $history)
            <div class="history" data-url="{{$history->direct_url}}">
                <div class="left">
                    <span class="profile" data-member="{{$history->sender}}">
                        @if($history->fn != "")
                        <img src="{{asset('storage/uploads/profile/'.$history->fn)}}" alt="">
                        @else
                        <img src="{{asset('images/profile-img.svg')}}" alt="">
                        @endif
                    </span>
                </div>
                <div class="center">
                    <p class="main-msg"><strong>{{$history->nick}}</strong>{{$history->main_msg}}</p>
                    <p class="sub-msg">
                        @php
                            echo $history->sub_msg
                        @endphp
                    </p>
                    <span>{{$history->passing}}</span>
                </div>
                @if($history->isThumb)
                <div class="right">
                    <div class="img-wrap">
                        <img src="{{asset('storage/uploads/'.$history->detail_type.'/'.$history->thumb_nail)}}" alt="">
                    </div>
                </div>
                @endif
            </div>
            @endforeach
        </div>
    </div>  
</div>

@endsection
@section('style')
<style>
.sub-head > p {position: absolute; left: 50%; transform: translateX(-50%)}

.history-wrap .sub-cont {min-height:calc(100vh - 128px); max-height: calc(100% - 128px); margin-top: 48px; }
.history-wrap .sub-cont .history {width: 100%; min-height: 70px; display: flex; justify-content: flex-start; padding: 10px; border-bottom: 1px solid #f2f2f2}
.history-wrap .sub-cont .history:active {background: #fff5f3;}
.history-wrap .sub-cont .history .left {margin-right: 10px;}
.history-wrap .sub-cont .history .left > .profile {width: 45px; height: 45px; border: 1px solid #f2f2f2; border-radius: 50%; background-color: white; overflow: hidden;}
.history-wrap .sub-cont .history .left > .profile > img {width: 100%;}
.history-wrap .sub-cont .history .center {display: flex; flex-direction: column; width: calc(100% - 120px); padding-right: 10px;}
.history-wrap .sub-cont .history .center > span {font-size: 12px;}
.history-wrap .sub-cont .history .center .main-msg {font-size: 14px;}
.history-wrap .sub-cont .history .center .sub-msg {font-size: 12px; margin-bottom: 5px}
.history-wrap .sub-cont .history .right {}
.history-wrap .sub-cont .history .right .img-wrap {width: 60px; height: 60px; border: 1px solid #d2d2d2;}
.history-wrap .sub-cont .history .right .img-wrap > img {width: 100%; height:100%; background: white; object-fit: cover; object-position: center}




</style>
@endsection
@section('script')
<script>
function confirm(){
    $("#confirm-box").removeClass("hide");
}
function confirm_btn(thing){
    if(thing == "cancel"){
        $("#confirm-box").addClass("hide");
    }else if(thing == "confirm"){
        $("#confirm-box").addClass("hide");
        destroy();
    }
}
function alert_btn(){
    $("#alert-box").addClass("hide");
}
$(document).on("click",".btn-close",function(){
    const index      = $(".btn-close").index($(this));
    const history_id = $(".btn-close").eq(index).data("id");
    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/history",
        type : "delete",
        data : {"type":"one", "history_id":history_id},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $(".btn-close").eq(index).parent().parent().remove();
            }
        }
    })
});

function destroy(){
    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/history",
        type : "delete",
        data : {"type":"all"},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $(".tag").empty();
                $('#alert-box').removeClass('hide');
            }
        }
    })
}

$(document).on("click",".history", function(event){
    
    location.href = $(this).data("url");
})

$(document).on("click", ".profile", function(event){
    event.stopPropagation();

    const member = $(this).data("member");
    location.href = "/profile?other="+member;
})
</script>
@endsection