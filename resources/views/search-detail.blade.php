@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>#{{$content}}</p> 
            <a class="no-img"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <ul class="tabs col-group">
                <li class="tab-link current" data-tab="tab-1">인기게시물</li>
                <li class="tab-link" data-tab="tab-2">최근게시물</li>
            </ul>
            <div id="tab-1" class="tab-content current">
                <div class="wrap-grid container" style="padding: 16px">
                    @foreach($hots as $hot)
                    @if($hot->taggable->files->count() > 0)
                    <div class="card">                        
                        @if($hot->taggable->files->first()->ext == "mp4")
                        <a href="{{url('/feed/'.$hot->taggable_id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$hot->taggable->files->first()->reference)}}" alt=""></a>
                        @else 
                        <a href="{{url('/feed/'.$hot->taggable_id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$hot->taggable->files->first()->fn)}}" alt=""></a>
                        @endif
                    </div>
                    @endif
                    @endforeach
                </div>
                <div id="moreBtn1" class="more-button">
                    <a class="moreFeed" data-load="0" onclick="more_feed('{{$page}}','hot');"></a>
                </div>
            </div>
            <div id="tab-2" class="tab-content">
                <div class="wrap-grid container" style="padding:16px">
                    @foreach($recents as $recent)
                    @if($recent->taggable->files->count() > 0)
                    <div class="card">
                        @if($recent->taggable->files->first()->ext == "mp4")
                        <a href="{{url('/feed/'.$recent->taggable_id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$recent->taggable->files->first()->reference)}}" alt=""></a>
                        @else 
                        <a href="{{url('/feed/'.$recent->taggable_id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$recent->taggable->files->first()->fn)}}" alt=""></a>
                        @endif
                    </div>
                    @endif
                    @endforeach
                </div>
                <div id="moreBtn2" class="more-button">
                    <a class="moreFeed" data-load="0" onclick="more_feed('{{$page}}', 'recent');"></a>
                </div>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
@endsection

@section('script')
<script>
    var th = $("#tab-1").hasClass("current") ? 0 : 1;
    // 스크롤 내려 피드 불러오기
    $(window).scroll(function(){
        var scrT = $(window).scrollTop();
        th = $("#tab-1").hasClass("current") ? 0 : 1;

        if(scrT > ($(document).height() - $(window).height()) * 0.7 ){
            if($(".moreFeed").eq(th).data("load") == "0"){
                $(".moreFeed").eq(th).click();
                $(".moreFeed").eq(th).data("load", "1");
            }
        }
    })

    function more_feed(page, tab){
        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            url : "/search/more",
            type : "post",
            data : {"page": page, "tab": tab, "content":"{{$content}}"},
            dataType : "json",
            success : function(data){
                let th = tab == "hot" ? 0 : 1;
                if(data["success"]){
                    // 추가 로드된 피드
                    
                    data["feeds"].forEach(feed => {
                        if(feed["src"] != "no-img"){
                            let src = feed["src"];
                            let url = feed["url"];

                            let str = "<div class=\"card\">";
                            str += `<a href=\"{{url('${url}')}}\"><img src=\"{{asset('${src}')}}\" alt=\"\"></a>`;
                            str += "</div>";

                            $('.wrap-grid').eq(th).append(str);
                        }
                        $(".more-button").eq(th).html("<a class=\"moreFeed\" data-load=\"0\" onclick=\"more_feed('"+data["page"]+"','"+tab+"')\"></a>");
                    })
                }else{
                    $(".more-button").eq(th).empty();
                }
            }
        })
    }
</script>
@endsection

