@extends('layouts.base')
@section('contents')
<div id="wrap" class="login-wrap find-wrap declare-wrap" data-type="{{$type}}" data-id="{{$id}}">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            @if($type == "feeds")
            <p>게시글 신고</p> 
            @elseif($type == "replies")
            <p>댓글 신고</p>
            @elseif($type == "together_posts")
            <p>투게더 포스트 신고</p> 
            @elseif($type == "together_replies")
            <p>투게더 댓글 신고</p> 
            @endif
            <a onclick="javascript:history.back(); return false;"><img class="no-img" src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-body">
            <div class="info-wrap">
                <p class="message">
                    위급한 문제가 있다며<br>
                    회원님의 신고는 익명으로 처리됩니다.<br>
                    즉시 도움을 받기 바랍니다.
                </p>
            </div>
            <div class="find-tit">
                <h4>신고사유 선택</h4>
            </div>
            <div class="sub-cont">
                <div class="radio-box">
                    <ul>
                        <li>
                            <input type="radio" id="radio01" name="reason" value="욕설, 비방, 차별, 혐오" checked>
                            <label for="radio01"><span>욕설, 비방, 차별, 혐오</span></label>
                        </li>
                        <li>
                            <input type="radio" id="radio02" name="reason" value="홍보, 영리목적">
                            <label for="radio02"><span>홍보, 영리목적</span></label>
                        </li>
                        <li>
                            <input type="radio" id="radio03" name="reason" value="불법정보">
                            <label for="radio03"><span>불법정보</span></label>
                        </li>
                        <li>
                            <input type="radio" id="radio04" name="reason" value="음란, 청소년 유해">
                            <label for="radio04"><span>음란, 청소년 유해</span></label>
                        </li>
                        <li>
                            <input type="radio" id="radio05" name="reason" value="개인 정보 노출, 유포, 거래">
                            <label for="radio05"><span>개인 정보 노출, 유포, 거래</span></label>
                        </li>
                        <li>
                            <input type="radio" id="radio06" name="reason" value="도배, 스팸">
                            <label for="radio06"><span>도배, 스팸</span></label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="foot-box">
                <div>
                    <button class="btn-login gray" onclick="cancel();">취소</button>
                    <button class="btn-login red" onclick="report();">확인</button>
                </div>  
            </div>  
        </div>
        <!-- 서브바디 -->
    </div> 
    <div id="report-alert" class="join-wrap-popup find-wrap-popup declare-wrap-popup hide">
        <div class="inner">
            <div class="inner-box">
                <div class="col-group">
                    <div>
                        <img src="{{asset('images/color-warning.svg')}}" alt="">
                        <p>신고가 완료되었습니다.</p>
                    </div>
                    <button class="btn-close" onclick="alert_close();">닫기</button>
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection

@section('script')
<script>
const type   = $("#wrap").data("type");
const id     = $("#wrap").data("id");

function cancel(){
    history.back(); 
    return false;
}

function report(){
    const reason = $("input[name=reason]:checked").val();

    $.ajax({
        headers : {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr('content')},
        url : "/report",
        type : "post",
        data : {"type":type, "id": id, "reason":reason},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $("#report-alert").removeClass('hide');
            }
        }
    });
}

function alert_close(){
    $("#report-alert").addClass("hide");
    history.back();
    return false; 
}
</script>
@endsection