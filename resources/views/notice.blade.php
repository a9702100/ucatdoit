@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>공지사항</p> 
            <a class="no-img"><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div>
        <div class="sub-cont notice-wrap">
            <div class="title">
                <h3>{{$notice->title}}</h3>
                <h5>게시기간 : {{date_format(new DateTime($notice->start_date), 'y.m.d')}} ~ {{date_format(new DateTime($notice->end_date), 'y.m.d')}}</h5>
            </div>
            <pre>
                @php 
                echo $notice->content;
                @endphp
            </pre>
        </div>
    </div>  
</div>
@endsection