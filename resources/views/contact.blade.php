@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div class="together-adm-wrap contact-wrap">
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>1:1 문의하기</p> 
            <a class="no-img" href=""><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 투게더 정보에서 오른쪽에 close버튼 눌리게 -->
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <div class="cats-add-wrap">
                <ul class="tabs col-group">
                    <li class="tab-link current" data-tab="tab-1">문의하기</li>
                    <li class="tab-link" data-tab="tab-2">문의내역</li>
                </ul>
                <div id="tab-1" class="tab-content current">
                    <div class="enter-box container-all">
                        <div class="contact-select">  
                            <label>문의 유형</label>
                            <select id="type">
                                <option value="단순 문의">단순 문의</option>
                                <option value="광고 문의">광고 문의</option>
                            </select>
                        </div>
                        <div>
                            <label for="question">문의 내용</label>
                            <textarea name="" id="question" placeholder="문의 내용을 입력해주세요.(2000자 이내)"></textarea>
                        </div>
                    </div>
                    <div class="button-box">
                        <button onclick="register();">문의하기</button>
                    </div>
                </div>
                <!-- 탭1 -->
                <div id="tab-2" class="tab-content ">
                    <ul class="contact-group">
                        @foreach ($asks as $ask)
                        <li id="ask{{$ask->id}}">
                            <div class="top">
                                <h3>[{{$ask->process}}] {{$ask->type}}</h3>
                                <img class="trash" src="{{asset('images/icon/icon-trash.svg')}}" onclick="destroy('{{$ask->id}}')">
                            </div>
                            <div class="mid">
                                <p>{{$ask->question}}</p>
                            </div>
                            <div class="bot">
                                <a onclick="answerShow('{{$ask->id}}');">답변 보기<img id="arrow{{$ask->id}}" class="" src="{{asset('images/icon/icon_arrow_up_m.svg')}}"></a>
                                <span class="created_at">{{date_format($ask->created_at, 'y.m.d H:i')}}</span>
                            </div>
                            <div id="answer{{$ask->id}}" class="answer hide">
                                <pre>{{$ask->answer == "" ? "답변 대기중입니다" : $ask->answer}}</pre>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- 탭2 -->
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
    <div id="alert-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p id="alert-msg"></p>
                    </div>
                    <div id="alert-btn" style="display:flex">
                        <button onclick="confirm_true();">확인</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="confirm-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                    </div>
                    <div id="confirm-btn" style="display:flex">
                        <button onclick="confirm_btn('false');">취소</button>
                        <button onclick="confirm_true();">확인</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
function alert_close(){
    $("#alert-box").addClass("hide");
}

function alert_success(){
    location.reload();
}

function register(){
    const type = $("#type option:selected").val();
    const question = $("#question").val();

    $.ajax({
        headers : {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
        url : "/ask",
        type : "post",
        data : {"type":type, "question":question},
        dataType : "json",
        success : function(data){
            let btn = "";
            if(data["success"]){
                btn = "<button onclick='alert_success();'>확인</button>";
            }else{
                btn = "<button onclick='alert_close();'>확인</button>";
            }
            $("#alert-msg").html(data["msg"]);
            $("#alert-btn").html(btn);
            $("#alert-box").removeClass("hide");
        }
    })
}

function answerShow(id){
    if($("#arrow"+id).hasClass("rotate_up")){
        // 위를 보고있는 화살표
        $("#arrow"+id).removeClass("rotate_up").addClass("rotate_down");
    }else{
        $("#arrow"+id).removeClass("rotate_down").addClass("rotate_up");
    }
    $("#answer"+id).slideToggle();
}

function destroy(id){
    let btns = "<button onclick=\"confirm_btn('false');\">취소</button>\
                <button onclick=\"confirm_btn('"+id+"');\">확인</button>";
    $("#confirm-msg").html("삭제하시겠습니까?");
    $("#confirm-btn").html(btns);
    $("#confirm-box").removeClass("hide");
}

function confirm_btn(thing){
    $("#confirm-box").addClass('hide');
    if(thing != "false"){
        $.ajax({
            headers:{"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            url : "/ask",
            type : 'delete',
            data : {"ask_id":thing},
            dataType : "json",
            success : function(data){
                if(data["success"]){
                    $("#ask"+thing).remove(); 
                    $("#alert-msg").html("삭제되었습니다");
                    $("#alert-btn").html("<button onclick='alert_close();'>확인</button>");
                    $("#alert-box").removeClass("hide");
                }
            }
        })
    }
}
</script>
@endsection