@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>투게더 공지사항</p> 
            <a></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02 together-wrap together-notice-wrap">
            <div class="enter-box container-all">
                <div class="tit">
                    <p>{{$notice->title}}</p>
                </div>
                <div class="content">
                @php
                    echo $notice->content;
                @endphp
                </div>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
@endsection