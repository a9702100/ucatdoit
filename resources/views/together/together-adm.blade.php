@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap" data-together = "{{$together->id}}">
    <div class="together-adm-wrap">
        <div class="sub-head col-group">
            <a href="{{url('/together/show/'.$together->id)}}"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>관리자 페이지</p> 
            <a onclick="refresh();"><img src="{{asset('images/icon/icon-clock.svg')}}" alt=""></a>
        </div>
        <!-- 투게더 정보에서 오른쪽에 close버튼 눌리게 -->
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <div class="cats-add-wrap">
            <ul class="tabs col-group">
                <li class="tab-link {{ $tab == "1" ? "current" : ""}}" data-tab="tab-1">회원관리</li>
                <li class="tab-link {{ $tab == "2" ? "current" : ""}}" data-tab="tab-2">공지사항</li>
                <li class="tab-link {{ $tab == "3" ? "current" : ""}}" data-tab="tab-3">투게더 정보</li>
            </ul>
            <div id="tab-1" class="tab-content {{ $tab == "1" ? "current" : ""}}">
                <div class="radio-box col-group">
                    <ul class="col-group">
                        <li class="col-group radio-list">
                            <input type="radio" id="radio1" name="list" checked>
                            <label for="radio1">회원목록</label>
                        </li>
                        <li class="col-group last">
                            <input type="radio" id="radio2" name="list">
                            <label for="radio2">추방목록</label>
                        </li>
                    </ul>                        
                </div>
                    <!-- 라디오 -->
                <div class="table-box container">
                    <div class="col-group">
                        <p>회원목록</p>
                        <p><span>{{$members->total()}}</span>/{{$together->capacity}}</p>
                    </div>
                    <table>
                        <colgroup>
                            <col width="10%">
                            <col width="40%">
                            <col width="25%">
                            <col width="25%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>닉네임</th>
                                <th>게시글</th>
                                <th>상태</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $num = $members->firstItem();
                            @endphp
                            @foreach($members as $member)
                            <tr>
                                <td>{{$num++}}</td>
                                <td><a href="{{url('/profile?other='.$member->member_id)}}">{{$member->member->nick}}</a></td>
                                <td>{{$member->post_cnt}}</td>
                                <td>
                                    <select name="state" data-id="{{$member->member_id}}">
                                        <option value="normal">정상</option>
                                        <option value="block">추방</option>
                                    </select>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>     
                </div>
                <div class="table-box container hide">
                    <div class="col-group">
                        <p>추방목록</p>
                        <p>{{$blocks->total()}}명</p>
                    </div>
                    <table>
                        <colgroup>
                            <col width="10%">
                            <col width="40%">
                            <col width="25%">
                            <col width="25%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>닉네임</th>
                                <th>추방일</th>
                                <th>추방해제</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $num2 = $blocks->firstItem();
                            @endphp
                            @foreach($blocks as $block)
                            <tr>
                                <td>{{$num2++}}</td>
                                <td><a href="{{url('/profile?other='.$block->member_id)}}">{{$block->member->nick}}</a></td>
                                <td>{{$block->updated_at->format('Y-m-d')}}</td>
                                <td><button onclick="member_destroy('{{$block->member_id}}')">해제</button></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>     
                </div>
            </div>
            <!-- 탭1 -->
            <div id="tab-2" class="tab-content {{ $tab == "2" ? "current" : ""}}">
                    <!-- 라디오 -->
                <div class="table-box container">
                    <div class="col-group">
                        <p>전체 : {{$notices->total()}}</p>
                        <button class="btn-add" onclick="notice_add('{{$together->id}}');">공지추가</button>
                    </div>
                    <table>
                        <colgroup>
                            <col width="20%">
                            <col width="50%">
                            <col width="30%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>제목</th>
                                <th>상태</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $num = $notices->firstItem();    
                            @endphp
                            @foreach($notices as $notice)
                            <tr>
                                <td>{{$num++}}</td>
                                <td class="tit"><a href="{{url('/together/notice/edit/'.$notice->id)}}">{{$notice->title}}</a></td>
                                <td><button onclick="notice_destory('{{$notice->id}}')">삭제</button></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>     
                </div>
            </div>
            <!-- 탭2 -->
            <div id="tab-3" class="tab-content {{ $tab == "3" ? "current" : ""}}">
                <div class="enter-box container-all">
                    <div>
                        <label for="name">투게더 이름</label>
                        <input type="text" id="title" placeholder="투게더 이름을 입력하세요" value="{{$together->title}}">
                    </div>
                    <div class="select-box">
                        <input type="hidden" id="addr1" value="{{$together->addr1}}">
                        <input type="hidden" id="addr2" value="{{$together->addr2}}">
                        <label for="area">지역 선택</label>
                        <ul class="col-group">
                            <li>
                                <select name="sido">
                                </select>
                            </li>
                            <li>
                                <select name="gu">
                                </select>
                            </li>
                        </ul>
                    </div>
                    <!-- 시/도 -->
                    <div>
                        <label for="capacity">인원수(최대1000명)</label>
                        <input type="number" id="capacity" placeholder="제한 인원수를 입력해주세요." value="{{$together->capacity}}">
                    </div>
                    <div>
                        <label for="intro">소개내용</label>
                        <textarea name="" id="intro" placeholder="소개 내용을 입력해주세요.">{{$together->intro}}</textarea>
                    </div>
                    <div class="img-box last">
                        <div class="tit">
                            <label>대표 이미지</label>
                            <label for="imgFile">
                                사진선택<img src="{{asset('images/icon/icon-camera.svg')}}">
                            </label>
                        </div>
                        <div class="preview">
                            <img id="headImg" src="{{asset('storage/uploads/together/'.$together->files->fn)}}" data-fn="{{$together->files->fn}}" alt="">
                        </div>
                        <input type="file" id="imgFile" accept="image/*" style="display: none">
                    </div>
                    <div class="button-delete">
                        <button onclick="together_destroy('{{$together->id}}')">투게더 삭제</button>
                    </div>
                </div>
                <div class="button-box">
                    <button onclick="together_update('{{$together->id}}');">수정하기</button>
                </div>
            </div>
            <!-- 탭3 -->
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div class="wrap-progress hide">
    <div class="inner">
        <img src="{{asset('images/icon/icon-loading.gif')}}" alt="">
    </div>
</div>
<div id="alert-box" class="join-wrap-popup popup-ver popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3 id="alert-tit">알림</h3>
                    <p id="alert-msg">내용 문구를 입력하세요</p>
                </div>
                <div id="alert-btn">
                    <button onclick="alert_close();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>투게더 삭제</h3>
                    <p id="confirm-msg">삭제 후 복구 할 수 없습니다<br>정말 삭제하시겠습니까?</p>
                </div>
                <div style="display:flex">
                    <button onclick="confirm_false();">취소</button>
                    <button onclick="confirm_true('{{$together->id}}');">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
.button-delete {width: 100%; padding: 10px; text-align: right}
.button-delete button {width: 30%; padding: 10px; border-radius: 5px; background: tomato;}
</style>
@endsection
@section('script')
<script src="{{asset('js/address.js')}}"></script>
<script>
$(document).ajaxStart(function(){
    $(".wrap-progress").removeClass("hide");
});
$(document).ajaxStop(function(){
    $(".wrap-progress").addClass("hide");
});


const together_id = $("#wrap").data("together"); // 투게더 번호

// 회원 또는 차단 목록 선택
$(document).on("click","input[name=list]",function(){
    const index = $("input[name=list]").index(this);

    if(index == "0"){
        // 회원목록 선택
        $(".table-box").eq(1).addClass("hide");
        $(".table-box").eq(0).removeClass("hide");
    }else if(index == "1"){
        // 추방목록 선택
        $(".table-box").eq(0).addClass("hide");
        $(".table-box").eq(1).removeClass("hide");
    }
});

$(document).on("change","select",function(){
    const index      = $("select").index(this);
    const member_id  = $("select").eq(index).data("id"); // 회원번호
    const state      = $("select option:selected").eq(index).val();

    if(state == "block"){
        if(confirm("선택된 회원을 추방시키겠습니까?\n모든 게시글이 삭제됩니다")){
            $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                url : "/together/member/out",
                type : "post",
                data : {"together_id":together_id, "member_id":member_id},
                dataType : "json",
                success : function(data){
                    if(data["success"]){
                        alert(data["msg"]);
                        location.reload();
                    }
                }
            })
        }else{
            // 취소누름 
            $("select").eq(index).find("option:eq(0)").prop("selected",true);
        }
    }
});

function member_destroy(id){
    if(confirm("해제 후 해당 회원이 다시 투게더에 참가할 수 있습니다.\n해제하시겠습니까?")){
        $.ajax({
            headers: {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
            url : "/together/member",
            type : "delete",
            data : {"together_id":together_id, "member_id":id},
            dataType : "json",
            success : function(data){
                if(data["success"]){
                    alert(data["msg"]);
                    location.reload();
                }
            }
        })
    }
}

function notice_add(id){
    location.href = "/together/notice/create?id="+id;
}

function notice_destory(id){
    if(confirm("삭제하시겠습니까?")){
        $.ajax({
            headers: {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
            url : "/together/notice/"+id,
            type : "delete",
            dataType : "json",
            success : function(data){
                if(data["success"]){
                    alert(data["msg"]);
                    location.reload();
                }
            }
        })
    }
}


function together_update(id){
    const title = $("#title").val();
    const addr1 = $("select[name=sido] option:selected").val();
    const addr2 = $("select[name=gu] option:selected").val();
    const capacity = $("#capacity").val();
    const intro = $("#intro").val();
    const imgFile = $("#headImg").data("fn") == undefined ? "" : $("#headImg").data("fn");

    $.ajax({
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")},
        url : "/together/"+id,
        type : "put",
        data : {"title":title, "addr1":addr1, "addr2":addr2, "capacity":capacity, "intro":intro, "imgFile":imgFile},
        dataType : "json",
        success : function(data){
            $("#alert-msg").html(data["msg"]);
            if(data["success"]){
                $("#alert-btn").html("<button onclick=\"alert_close('success');\">확인</button>");
            }else{
                $("#alert-btn").html("<button onclick=\"alert_close('fail');\">확인</button>");
            }
            $('#alert-box').removeClass("hide");
        }
    })
}

$(document).on("change","#imgFile", function(){
    if($(this)[0].files.length < 1){

    }else{
        readImage(this);
    }
});

function readImage(input){
    let formData = new FormData();
    formData.append('imgFile', input.files[0]);
    formData.append("file_type", "togethers");
    
    $(".wrap-progress").removeClass("hide");
    $.ajax({
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/together/fileUpload",
        type : "post",
        data : formData, 
        processData : false,
        contentType : false, 
        dataType : "json",
        success : function(data){
            if(data["success"]){
                // 압축 성공
                let str = "<img id='headImg' src=\'{{asset('storage/uploads/temp')}}/"+data["fn"]+"\' alt='' data-fn='"+data["fn"]+"'>";
                $(".preview").html(str);
            }else{
                //압축 실패
                $("#alert-msg").html(data["msg"]);
                $("#alert-btn").html("<button onclick=\"alert_close('fail');\">확인</button>");
                $('#alert-box').removeClass("hide");
            }
        }
    }); // ajax 끝
}
function refresh(){
    location.reload();
}

function alert_close(result){
    $('#alert-box').addClass("hide");
    if(result == "success"){
        location.href= "/together/edit/"+together_id+"?tab=3";
    }else if(result == "fail"){
        
    }
}

function together_destroy(id){
    $("#confirm-box").removeClass("hide");
}

function confirm_false(){
    $("#confirm-box").addClass("hide");
}

function confirm_true(id){
    $("#confirm-box").addClass("hide");
    $.ajax({
        headers:{"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/together/"+id,
        type : "delete",
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            location.href = "/together";
        }
    })
}
</script>
@endsection