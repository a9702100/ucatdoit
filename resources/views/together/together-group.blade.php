@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap back-wrap">
    <div>
        <div class="sub-head col-group">
            <a href="{{url('/together')}}"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>{{$together->title}}</p> 
            <a class="{{$together->member_id == $member_id ? "" : "no-img"}}" href="{{url('/together/edit/'.$together->id)}}"><img src="{{asset('images/icon/icon-gear-b.svg')}}" alt=""></a>
            <!-- 내가 만든 모임방은 있고 기본입장은 오른쪽 버튼 없습니다 -->
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont together-wrap together-adm-wrap">
            <div class="search-head">
                <p class="col-group">
                    <img src="{{asset('images/icon/icon-search-g.svg')}}" alt="">
                    <input type="text" id="keyword" placeholder="작성자 검색" data-id="{{$together->id}}">
                </p>
            </div>
            <!-- 검색창 -->
            <div class="radio-box col-group">
                <ul class="col-group">
                    <li class="col-group radio-list">
                        <input type="radio" id="radio1" name="posts" value="전체" checked>
                        <label for="radio1">전체</label>
                    </li>
                    <li class="col-group last">
                        <input type="radio" id="radio2" name="posts" value="내글">
                        <label for="radio2">내 글 보기</label>
                    </li>
                    <li class="col-group last">
                        <input type="radio" id="radio3" name="posts" value="검색 결과">
                        <label for="radio3">검색결과</label>
                    </li>
                </ul>                        
            </div>
                <!-- 라디오 -->
            <div class="event-box notice-box swiper-container vertical-slide">
                <ul class="swiper-wrapper">
                    @if($notices->count() < 1)
                        <li class="col-group swiper-slide">
                            <div>
                                <a class="col-group">
                                    <span>공지</span>
                                    <p>등록된 공지사항이 없습니다</p>
                                </a>
                            </div>
                            <img src="{{asset('images/icon/icon_arrow_right_s.svg')}}" alt="">
                        </li>
                    @else 
                        @foreach($notices as $notice)
                        <li class="col-group swiper-slide">
                            <div>
                                <a class="col-group" href="{{url('/together/notice/show/'.$notice->id)}}">
                                    <span>공지</span>
                                    <p>{{$notice->title}}</p>
                                </a>
                            </div>
                            <img src="{{asset('images/icon/icon_arrow_right_s.svg')}}" alt="">
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div id="posts_all" class="stroy-box">
                <ul>
                    @foreach($posts_all as $post)
                    <li>
                        <div class="container-box">
                            <div class="top col-group">
                                <div class="left">
                                    <div class="box">
                                        @if(isset($post->member->upload))
                                        <img class="my-img" src="{{asset('storage/uploads/profile/'.$post->member->upload->fn)}}" alt="">
                                        @else
                                        <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                        @endif
                                    </div>
                                    <a href="{{url('/profile?other='.$post->member->id)}}">{{$post->member->nick}}</a>
                                </div>
                                <div class="right">
                                    <img class="btn-dots" src="{{asset('images/icon/icon-dots.svg')}}" alt="">
                                    <div class="dot-fixed">
                                        @if(session('member') == $post->member->id)
                                        <p class="col-group"><a class="col-group" onclick="edit('{{$post->id}}')">수정하기<img src="{{asset('images/icon/icon-pencil.svg')}}" alt=""></a></p>
                                        <p class="col-group"><a class="col-group" onclick="del_confirm('{{$post->id}}')">삭제하기<img src="{{asset('images/icon/icon-trash.svg')}}" alt=""></a></p>
                                        @else 
                                        <p class="col-group"><a class="col-group" href="{{url('/report?type=together_posts&id='.$post->id)}}">신고하기<img src="{{asset('images/icon/icon-warning.svg')}}" alt=""></a></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="img-box">
                                <ul>
                                    <li>
                                        @php
                                            echo $post->content; 
                                        @endphp
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{-- <div class="center col-group">
                            <div class="col-group">
                                <a href="/pages/reply.html">댓글 <span>0</span></a>
                            </div>
                        </div> --}}
                    </li>
                    @endforeach
                </ul>
            </div>
            <div id="posts_me" class="stroy-box hide">
                <ul>
                    @foreach($posts_me as $post)
                    <li>
                        <div class="container-box">
                            <div class="top col-group">
                                <div class="left">
                                    <div class="box">
                                        @if(isset($post->member->upload))
                                        <img class="my-img" src="{{asset('storage/uploads/profile/'.$post->member->upload->fn)}}" alt="">
                                        @else
                                        <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                        @endif
                                    </div>
                                    <a href="{{url('/profile?other='.$post->member->id)}}">{{$post->member->nick}}</a>
                                </div>
                                <div class="right">
                                    <img class="btn-dots" src="{{asset('images/icon/icon-dots.svg')}}" alt="">
                                    <div class="dot-fixed">
                                        @if(session('member') == $post->member->id)
                                        <p class="col-group"><a class="col-group" onclick="edit('{{$post->id}}')">수정하기<img src="{{asset('images/icon/icon-pencil.svg')}}" alt=""></a></p>
                                        <p class="col-group"><a class="col-group" onclick="del_confirm('{{$post->id}}')">삭제하기<img src="{{asset('images/icon/icon-trash.svg')}}" alt=""></a></p>
                                        @else 
                                        <p class="col-group"><a class="col-group" href="{{url('/report?type=together_posts&id='.$post->id)}}">신고하기<img src="{{asset('images/icon/icon-warning.svg')}}" alt=""></a></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="img-box">
                                <ul>
                                    <li>
                                        @php
                                            echo $post->content; 
                                        @endphp
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{-- <div class="center col-group">
                            <div class="col-group">
                                <a href="/pages/reply.html">댓글 <span>0</span></a>
                            </div>
                        </div> --}}
                    </li>
                    @endforeach
                </ul>
            </div>
            <div id="posts_search" class="stroy-box hide">
                <ul>
                    
                </ul>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
    <div class="btn-add-fixed">
        <a href="{{url('/together/post/create?id='.$together->id)}}">
            <img src="{{asset('images/icon/together-add.svg')}}" alt="">
        </a>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                </div>
                <div id="confirm-btn" style="display:flex">
                    <button onclick="confirm_false();">취소</button>
                    <button onclick="confirm_true();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
// 메인 이벤트 위아래 슬라이드
new Swiper(".vertical-slide", {
    // loopFillGroupWithBlank : true,
    // slidesPerView: 1,
    // slidesPerGroup : 1,
    spaceBetween:0,
    direction: "vertical",
    loop: true,
    autoplay:true,
    pauseOnFocus: true,
    autoHeight: true,
    autoplayDisableOnInteraction: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        },
});

// 수정하기, 삭제하기, 신고하기 버튼 제어
$(".dot-fixed").hide();
$(document).on("click",".btn-dots", function(){
    let index = $(".btn-dots").index(this); // 몇번째?
    if($(".dot-fixed").eq(index).is(":visible")){
        // 선택한 것이 열려진 상태면
        $(".dot-fixed").eq(index).hide();
    }else{
        // 선택한 것이 닫힌 상태면
        $(".dot-fixed").hide();
        $(".dot-fixed").eq(index).show();
    }
});

$(document).on("click, change","input[name=posts]", function(){
    const check = $("input[name=posts]:checked").val();

    if(check == "전체"){
        $("#posts_me").addClass("hide");
        $("#posts_search").addClass("hide");
        $("#posts_all").removeClass("hide");
    }else if(check == "내글"){
        $("#posts_all").addClass("hide");
        $("#posts_search").addClass("hide");
        $("#posts_me").removeClass("hide");
    }else{
        $("#posts_all").addClass("hide");
        $("#posts_me").addClass("hide");
        $("#posts_search").removeClass("hide");
    }
});

// 피드 수정
function edit(id){
    location.href = "/together/post/edit/"+id;
}

function del_confirm(id){
    // 삭제 확인용 confirm
    let confirm_btn = "<button onclick='confirm_false();'>취소</button>\
                      <button onclick=\"confirm_true('"+id+"','delete');\">확인</button>";

    $('#confirm-msg').html("모든 내용이 삭제됩니다<br>삭제하시겠습니까?");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide");
}

function confirm_true(id, action){
    if(action == "delete"){
        destroy(id);
        $("#confirm-box").addClass("hide");
    }
}

function confirm_false(){
    $("#confirm-box").addClass('hide');
}

function destroy(id){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/together/post/"+id,
        type : "delete",
        dataType : "json",
        success : function(data){
           if(data["success"]){
               alert("삭제되었습니다");
               location.reload();
           }
        }
    })   
}

// 작성자 검색
$(document).on("input","#keyword",function(){
    const together_id = $(this).data("id");
    const keyword = $(this).val();

    $("input[name=posts]").eq(2).prop("checked",true);
    $("#posts_all").addClass("hide");
    $("#posts_me").addClass("hide");
    $("#posts_search>ul").empty();
    $("#posts_search").removeClass("hide");

    $.ajax({
        headers : {"X-CSRF-TOKEN" : $("meta[name='csrf-token']").attr("content")},
        url : "/together/search",
        type : "post",
        data : {"type":"post", "together_id":together_id, "keyword":keyword},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                data["posts"].forEach(post => {
                    let str = "<li>\
                            <div class=\"container-box\">\
                                <div class=\"top col-group\">\
                                    <div class=\"left\">\
                                        <div class=\"box\">";
                        if(post["fn"] != ""){
                            str += "<img class=\"my-img\" src=\"{{asset('storage/uploads/profile/')}}/"+post["fn"]+"\" alt=''>";
                        }else{
                            str += "<img class=\"no-my-img\" src=\"{{asset('images/profile-img.svg')}}\" alt=''>";
                        }
                              str +=          "</div>\
                                        <a href=\"{{url('/profile?other=')}}/"+post["member_id"]+"\">"+post["nick"]+"</a>\
                                    </div>\
                                    <div class=\"right\">\
                                        <img class=\"btn-dots\" src=\"{{asset('images/icon/icon-dots.svg')}}\" alt=''>\
                                        <div class=\"dot-fixed\">";
                        if(post["writer"] == "true"){
                           str += "<p class=\"col-group\"><a class=\"col-group\" onclick=\"edit('"+post["id"]+"')\">수정하기<img src=\"{{asset('images/icon/icon-pencil.svg')}}\" alt=''></a></p>\
                            <p class=\"col-group\"><a class=\"col-group\" onclick=\"del_confirm('"+post["id"]+"')\">삭제하기<img src=\"{{asset('images/icon/icon-trash.svg')}}\" alt=''></a></p>";
                        }else{
                           str += "<p class=\"col-group\"><a class=\"col-group\" href=\"{{url('/report?type=together_posts&id=')}}/"+post["id"]+"\">신고하기<img src=\"{{asset('images/icon/icon-warning.svg')}}\" alt=''></a></p>";
                        }

                            str += "                </div>\
                                    </div>\
                                </div>\
                                <div class=\"img-box\">\
                                    <ul>\
                                        <li>";
                                            str += post["content"]; 
                            str += "   </li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </li>";
                    $("#posts_search>ul").append(str);
                });
                $(".dot-fixed").hide();
            }else{
                let str = "<li>\
                            <div class=\"container-box\">\
                                <p style='text-align:center'>"+data["msg"]+"</p>\
                                </div>\
                    </li>";
                $("#posts_search>ul").html(str);
            }
        }
    })
})
</script>
@endsection