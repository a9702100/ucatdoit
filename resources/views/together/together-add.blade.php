@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>투게더</p> 
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02 together-wrap">
            <div class="enter-box container-all">
                <div>
                    <label for="name">투게더 이름</label>
                    <input type="text" id="title" placeholder="투게더 이름을 입력하세요">
                </div>
                <div class="select-box">
                    <label for="area">지역 선택</label>
                    <ul class="col-group">
                        <li>
                            <select name="sido">
                            </select>
                        </li>
                        <li>
                            <select name="gu">
                            </select>
                        </li>
                    </ul>
                </div>
                <!-- 시/도 -->
                <div>
                    <label for="capacity">인원수(최대1000명)</label>
                    <input type="text" id="capacity" placeholder="제한 인원수를 입력해주세요.">
                </div>
                <div>
                    <label for="intro">소개내용</label>
                    <textarea name="" id="intro" placeholder="소개 내용을 입력해주세요."></textarea>
                </div>
                <div class="img-box last">
                    <div class="tit">
                        <label>대표 이미지</label>
                        <label for="imgFile">
                            사진선택<img src="{{asset('images/icon/icon-camera.svg')}}">
                        </label>
                    </div>
                    <div class="preview">

                    </div>
                    <input type="file" id="imgFile" accept="image/*" style="display: none">
                </div>
            </div>
            <div class="button-box">
                <button onclick="register();">생성하기</button>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div id="alert-box" class="join-wrap-popup popup-ver popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3 id="alert-tit">알림</h3>
                    <p id="alert-msg">내용 문구를 입력하세요</p>
                </div>
                <div id="alert-btn">
                    <button onclick="alert_close();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">작성중인 내용은 지워집니다<br>뒤로 이동하시겠습니까?</p>
                </div>
                <div style="display:flex">
                    <button onclick="confirm_false();">취소</button>
                    <button onclick="confirm_true();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap-progress hide">
    <div class="inner">
        <p>업로드 중입니다</p>
        <p>잠시만 기다려주세요</p>
        <progress id="progressBar" max="100" value="0"></progress>
        <p id="progress_rate">0%</p>
    </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/address.js')}}"></script>
<script>
function register(){
    const title = $("#title").val();
    const addr1 = $("select[name=sido] option:selected").val();
    const addr2 = $("select[name=gu] option:selected").val();
    const capacity = $("#capacity").val();
    const intro = $("#intro").val();
    const imgFile = $("#headImg").data("fn") == undefined ? "" : $("#headImg").data("fn");

    $.ajax({
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr("content")},
        url : "/together",
        type : "post",
        data : {"title":title, "addr1":addr1, "addr2":addr2, "capacity":capacity, "intro":intro, "imgFile":imgFile},
        dataType : "json",
        success : function(data){
            $("#alert-msg").html(data["msg"]);
            if(data["success"]){
                $("#alert-btn").html("<button onclick=\"alert_close('success');\">확인</button>");
            }else{
                $("#alert-btn").html("<button onclick=\"alert_close('fail');\">확인</button>");
            }
            $('#alert-box').removeClass("hide");
        }
    })
}

$(document).on("change","#imgFile", function(){
    if($(this)[0].files.length < 1){

    }else{
        readImage(this);
    }
});

function readImage(input){
    let formData = new FormData();
    formData.append('imgFile', input.files[0]);
    formData.append("file_type", "togethers");
    
    $(".wrap-progress").removeClass("hide");
    $.ajax({
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/together/fileUpload",
        type : "post",
        data : formData, 
        processData : false,
        contentType : false, 
        dataType : "json",
        xhr : function(){
            var xhr = $.ajaxSettings.xhr();
            xhr.upload.onprogress = function(e){
                var percent = e.loaded * 100 / e.total;
                setProgress(percent);
            };
            return xhr;
        },
        success : function(data){
            if(data["success"]){
                // 압축 성공
                let str = "<img id='headImg' src=\'{{asset('storage/uploads/temp')}}/"+data["fn"]+"\' alt='' data-fn='"+data["fn"]+"'>";
                $(".preview").html(str);
            }else{
                //압축 실패
                $("#alert-msg").html(data["msg"]);
                $("#alert-btn").html("<button onclick=\"alert_close('fail');\">확인</button>");
                $('#alert-box').removeClass("hide");
            }
        }
    }); // ajax 끝
}

function setProgress(per){
    $("#progressBar").val(per);
    $("#progress_rate").html(Math.round(per)+"%");
    if(per >= 100){
        $(".wrap-progress").addClass("hide");
    }
}

function alert_close(result){
    $('#alert-box').addClass("hide");
    if(result == "success"){
        location.href = "/together";
    }else if(result == "fail"){
        
    }
}
</script>
@endsection