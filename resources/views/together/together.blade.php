@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>투게더</p> 
            <a href="{{url('/together/create')}}"><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont together-wrap">
            <div class="select-box">
                <ul class="col-group">
                    <li>
                        <select name="sido">
                        </select>
                    </li>
                    <li>
                        <select name="gu">
                        </select>
                    </li>
                </ul>
            </div>
            <!-- 시/도 -->
            <div class="search-head">
                <p class="col-group">
                    <img src="{{asset('images/icon/icon-search-g.svg')}}" alt="">
                    <input type="text" id="keyword" placeholder="검색어를 입력하세요.">
                </p>
            </div>
            <!-- 검색창 -->
            <div class="container">
                <ul class="tabs col-group">
                    <li class="tab-link current" data-tab="tab-1">전체</li>
                    <li class="tab-link" data-tab="tab-2">참여한 투게더</li>
                    <li class="tab-link" data-tab="tab-3">내 투게더</li>
                </ul>
                <div id="tab-1" class="tab-content current">
                    {{-- <div class="col-group click-order">
                        최근순
                        <img class="btn-share" src="{{asset('images/icon/icon-order-down.svg')}}" alt="">
                        <div class="order-fixed">
                            <p class="col-group"><a>최근순</a></p>
                            <p class="col-group"><a>인기순</a></p>
                        </div>
                    </div> --}}
                    <ul class="default1">
                        @if($together_all->count() < 1)
                            <li class="empty">
                                <p>생성된 투게더가 없습니다.</p>
                                <p>투게더를 생성해보세요</p>
                            </li>
                        @else 
                        @foreach($together_all as $together)
                        <li class="attend col-group" onclick="partiEnter('{{$together->id}}');">
                            <div class="left">
                                @if(isset($together->files))
                                    <img src="{{('storage/uploads/together/'.$together->files->fn)}}" alt="">
                                @else 
                                    <img src="{{('images/profile-img.svg')}}" alt="">
                                @endif
                            </div>
                            <div class="right">
                                <div class="top col-group">
                                    <p>{{$together->addr1}} > {{$together->addr2}}</p>
                                    <span>{{$together->member_cnt}}/{{$together->capacity}}</span>
                                </div>
                                <h3>{{$together->title}}</h3>
                                <h4>{{$together->intro}}</h4>
                                <div class="bottom">
                                    @if($together->member_id == Session::get('member'))
                                    <button>내 투게더</button>                                    
                                    @elseif($together->isParti == "1")
                                    <button onclick="partiOut('{{$together->id}}');">나가기</button>                                    
                                    @else 
                                    <button onclick="partiIn('{{$together->id}}');">참가하기</button>
                                    @endif
                                </div>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                    <ul class="result1">
                        
                    </ul>
                </div>
                <div id="tab-2" class="tab-content">
                    <ul class="default2">
                        @if($together_all->count() < 1)
                            <li class="empty">
                                <p>생성된 투게더가 없습니다.</p>
                                <p>투게더를 생성해보세요</p>
                            </li>
                        @else
                        @foreach($together_parti as $together)
                        <li class="attend col-group" onclick="partiEnter('{{$together->id}}');">
                            <div class="left">
                                @if(isset($together->files))
                                    <img src="{{('storage/uploads/together/'.$together->files->fn)}}" alt="">
                                @else 
                                    <img src="{{('images/profile-img.svg')}}" alt="">
                                @endif
                            </div>
                            <div class="right">
                                <div class="top col-group">
                                    <p>{{$together->addr1}} > {{$together->addr2}}</p>
                                    <span>{{$together->member_cnt}}/{{$together->capacity}}</span>
                                </div>
                                <h3>{{$together->title}}</h3>
                                <h4>{{$together->intro}}</h4>
                                <div class="bottom">
                                    <button onclick="partiOut('{{$together->id}}');">나가기</button>
                                </div>
                            </div>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                    <ul class="result2">
                        
                    </ul>
                </div>
                <div id="tab-3" class="tab-content">
                    <ul>
                        @if($together_me->count() < 1)
                        <li class="empty">
                            <p>생성한 투게더가 없습니다.</p>
                            <p>투게더를 생성해보세요</p>
                        </li>
                        @else 
                            @foreach($together_me as $together)
                            <li class="attend col-group">
                                <div class="left">
                                    @if(isset($together->files))
                                    <img src="{{asset('storage/uploads/together/'.$together->files->fn)}}" alt="">
                                    @else 
                                    <img src="{{asset('images/profile-img.svg')}}" alt="">
                                    @endif
                                </div>
                                <div class="right">
                                    <div class="top col-group">
                                        <p>{{$together->addr1}} > {{$together->addr2}}</p>
                                        <a href="{{url('/together/edit/'.$together->id)}}"><img src="{{asset('images/icon/icon-gear-b.svg')}}" alt=""></a>
                                    </div>
                                    <h3>{{$together->title}}</h3>
                                    <h4>{{$together->intro}}</h4>
                                    <div class="bottom">
                                        <button class="btn-enter" onclick="location.href='{{url('/together/show/'.$together->id)}}'">들어가기</button>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        @endif

                    </ul>
                </div>
            </div>
            <!-- 탭 -->
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div id="alert-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="alert-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                </div>
                <button onclick="alert_btn();">확인</button>
            </div>
        </div>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                </div>
                <div id="confirm-btn" style="display:flex">
                    <button onclick="confirm_btn('cancel');">취소</button>
                    <button onclick="confirm_true();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/address.js')}}"></script>
<script>
function partiEnter(id){
    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/together/partiEnter",
        type : "post",
        data : {"together_id": id},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                // 참가 회원
                location.href = "/together/show/"+id;
            }else{
                if(data["state"] == "not"){
                    // 아직 참가 목록에 없음   
                    partiIn(id);
                }else{
                    // 차단된 회원
                    $("#alert-msg").html(data["msg"]);
                    $("#alert-box").removeClass("hide");
                }
            }
        }
    })
}

function partiIn(id){
    event.stopPropagation();
    let btn = "<button onclick=\"confirm_btn();\">취소</button>\
               <button onclick=\"register('"+id+"');\">확인</button>";

    $("#confirm-msg").html("해당 투게더에 참가하시겠습니까?");
    $("#confirm-btn").html(btn);
    $("#confirm-box").removeClass("hide");
}

function partiOut(id){
    event.stopPropagation();
    let btn = "<button onclick=\"confirm_btn();\">취소</button>\
               <button onclick=\"destroy('"+id+"');\">확인</button>";

    $("#confirm-msg").html("더이상 해당 투게더에서<br>활동을 그만두시겠습니까?");
    $("#confirm-btn").html(btn);
    $("#confirm-box").removeClass("hide");
}

function confirm_btn(){
    $("#confirm-box").addClass("hide");
}

function alert_btn(){
    $("#alert-box").addClass("hide");
}

// 투게더 참가
function register(together_id){
    confirm_btn();

    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/together/partiIn",
        type : "post",
        data : {"together_id":together_id},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                location.href="/together/show/"+together_id;
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        }
    })
}

// 투게더 탈퇴
function destroy(together_id){
    confirm_btn();

    $.ajax({
        headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/together/partiOut",
        type : "post",
        data : {"together_id":together_id},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                location.reload();
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        }
    })
}

// 지역을 변경해서 검색
$(document).on("change","select[name=sido], select[name=gu]",function(){
    const sido    = $("select[name=sido] option:selected").val();
    const gugun   = $("select[name=gu] option:selected").val();
    const keyword = $("#keyword").val();


    if(sido == ""){
        $(".result1").hide();
        $(".result2").hide();
        $(".default1").show();
        $(".default2").show();
    }else{
        $(".default1").hide();
        $(".default2").hide();
        $(".result1").show();
        $(".result2").show();

    }

    $.ajax({
        headers: {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/together/search",
        type : "post",
        data : {"type":"together", "sido":sido, "gugun":gugun, "keyword":keyword},
        dataType : "json",
        success : function(data){
            $(".result1").empty();
            $(".result2").empty();
            if(data["success"]){
                data["together_all"].forEach(together => {
                    let str = "<li class=\"attend col-group\" onclick=\"partiEnter('"+together["id"]+"');\">\
                            <div class=\"left\">";
                    if(together["fn"] != ""){
                        str += "<img src=\"{{('storage/uploads/together/')}}/"+together["fn"]+"\" alt=''>";
                    }else{
                        str += "<img src=\"{{('images/profile-img.svg')}}\" alt=''>";
                    }
                    str +="</div>\
                            <div class=\"right\">\
                                <div class=\"top col-group\">\
                                    <p>"+together["addr1"]+" > "+together["addr2"]+"</p>\
                                    <span>"+together["member_cnt"]+"/"+together["capacity"]+"</span>\
                                </div>\
                                <h3>"+together["title"]+"</h3>\
                                <h4>"+together["intro"]+"</h4>\
                                <div class=\"bottom\">";
                    if(together["isParti"] == "2"){
                        str += "<button>내 투게더</button>";
                    }else if(together["isParti"] == "1"){
                        str += "<button onclick=\"partiOut('"+together["id"]+"');\">나가기</button>";
                    }else{
                        str += "<button onclick=\"partiIn('"+together["id"]+"');\">참가하기</button>";
                    }                                 
                        str += "</div>\
                            </div>\
                        </li>";                    
                    $(".result1").append(str);
                });
                
                data["together_parti"].forEach(together => {
                    let str = "<li class=\"attend col-group\" onclick=\"partiEnter('"+together["id"]+"');\">\
                            <div class=\"left\">";
                    if(together["fn"] != ""){
                        str += "<img src=\"{{('storage/uploads/together/')}}/"+together["fn"]+"\" alt=''>";
                    }else{
                        str += "<img src=\"{{('images/profile-img.svg')}}\" alt=''>";
                    }
                    str +="</div>\
                            <div class=\"right\">\
                                <div class=\"top col-group\">\
                                    <p>"+together["addr1"]+" > "+together["addr2"]+"</p>\
                                    <span>"+together["member_cnt"]+"/"+together["capacity"]+"</span>\
                                </div>\
                                <h3>"+together["title"]+"</h3>\
                                <h4>"+together["intro"]+"</h4>\
                                <div class=\"bottom\">";
                        str += "<button onclick=\"partiOut('"+together["id"]+"');\">나가기</button>";
                        str += "</div>\
                            </div>\
                        </li>";                    
                    $(".result2").append(str);
                });
                
                
            }else{
                let str = "<li class='empty'>\
                            <p>검색결과가 없습니다</p>\
                            </li>";
                $(".result1").html(str);
                $(".result2").html(str);
            }
        }
    })
});

// 키워드를 입력해서 검색
$(document).on("input", "#keyword", function(){
    const sido    = $("select[name=sido] option:selected").val();
    const gugun   = $("select[name=gu] option:selected").val();
    const keyword = $("#keyword").val();

    $(".default1").hide();
    $(".default2").hide();
    $(".result1").show();
    $(".result2").show();


    $.ajax({
        headers: {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/together/search",
        type : "post",
        data : {"type":"together", "sido":sido, "gugun":gugun, "keyword":keyword},
        dataType : "json",
        success : function(data){
            $(".result1").empty();
            $(".result2").empty();
            if(data["success"]){
                data["together_all"].forEach(together => {
                    let str = "<li class=\"attend col-group\" onclick=\"partiEnter('"+together["id"]+"');\">\
                            <div class=\"left\">";
                    if(together["fn"] != ""){
                        str += "<img src=\"{{('storage/uploads/together/')}}/"+together["fn"]+"\" alt=''>";
                    }else{
                        str += "<img src=\"{{('images/profile-img.svg')}}\" alt=''>";
                    }
                    str +="</div>\
                            <div class=\"right\">\
                                <div class=\"top col-group\">\
                                    <p>"+together["addr1"]+" > "+together["addr2"]+"</p>\
                                    <span>"+together["member_cnt"]+"/"+together["capacity"]+"</span>\
                                </div>\
                                <h3>"+together["title"]+"</h3>\
                                <h4>"+together["intro"]+"</h4>\
                                <div class=\"bottom\">";
                                    console.log(together["isParti"]);
                    if(together["isParti"] == "2"){
                        str += "<button>내 투게더</button>";
                    }else if(together["isParti"] == "1"){
                        str += "<button onclick=\"partiOut('"+together["id"]+"');\">나가기</button>";
                    }else{
                        str += "<button onclick=\"partiIn('"+together["id"]+"');\">참가하기</button>";
                    }                                 
                        str += "</div>\
                            </div>\
                        </li>";                    
                    $(".result1").append(str);
                });
                
                data["together_parti"].forEach(together => {
                    let str = "<li class=\"attend col-group\" onclick=\"partiEnter('"+together["id"]+"');\">\
                            <div class=\"left\">";
                    if(together["fn"] != ""){
                        str += "<img src=\"{{('storage/uploads/together/')}}/"+together["fn"]+"\" alt=''>";
                    }else{
                        str += "<img src=\"{{('images/profile-img.svg')}}\" alt=''>";
                    }
                    str +="</div>\
                            <div class=\"right\">\
                                <div class=\"top col-group\">\
                                    <p>"+together["addr1"]+" > "+together["addr2"]+"</p>\
                                    <span>"+together["member_cnt"]+"/"+together["capacity"]+"</span>\
                                </div>\
                                <h3>"+together["title"]+"</h3>\
                                <h4>"+together["intro"]+"</h4>\
                                <div class=\"bottom\">";
                        str += "<button onclick=\"partiOut('"+together["id"]+"');\">나가기</button>";
                        str += "</div>\
                            </div>\
                        </li>";                    
                    $(".result2").append(str);
                });
                
                
            }else{
                let str = "<li class='empty'>\
                            <p>검색결과가 없습니다</p>\
                            </li>";
                $(".result1").html(str);
                $(".result2").html(str);
            }
        }
    })
})
</script>
@endsection