@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap" data-together="{{$together_id}}" data-type="together_notices" data-dir="together/notice">
    <div>
        <div class="sub-head col-group">
            <a onclick="cancel();">취소</a>
            @if(isset($notice))
            <p>공지사항 수정</p> 
            <a onclick="update('{{$together_id}}','{{$notice->id}}');">수정</a>
            @else 
            <p>공지사항 추가</p> 
            <a onclick="register('{{$together_id}}');">작성</a>
            @endif
            
        </div>
        <div class="sub-cont sub-cont02 together-wrap">
            <div class="enter-box container-all" style="height: calc(100vh - 200px) !important;">
                <div class="tit">
                    {{-- <label for="title">공지사항 제목</label> --}}
                    <input id="title" type="text" placeholder="공지사항 제목을 입력하세요" value="{{isset($notice) ? $notice->title : ""}}">
                </div>
                <textarea id="editor">{{isset($notice) ? $notice->content : ""}}</textarea>  
            </div>
        </div>
    </div>  
</div>
<div id="alert-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="alert-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                </div>
                <button id="alert-btn" onclick="confirm_true();">확인</button>
            </div>
        </div>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">작성중인 내용은 지워집니다<br>뒤로 이동하시겠습니까?</p>
                </div>
                <div id="confirm-btn" style="display:flex">
                    <button onclick="confirm_btn('false');">취소</button>
                    <button onclick="confirm_btn('true');">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{asset('js/tinyMCE.js')}}"></script>
<script>
    function tinymceUploadImg(blobInfo, success, failure, progress) {
       let formData = new FormData();
       formData.append('imageFile', blobInfo.blob(), blobInfo.filename());
       formData.append("file_type","together_notices");
    
       $.ajax({
           headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
           url : "/together/upload",
           type : "post",
           processData : false,
           contentType : false, 
           data : formData,
           dataType : "json",
           success : function(data){
               return success('/storage/uploads/together_notices/'+data["fn"]);
           },error : function(){
               alert("관리자에게 문의해주세요");
           }
       })
    }
</script>
<script src="{{asset('js/editor.js')}}"></script>
<script>
const together_id = $("#wrap").data("together");

function cancel(){
    $('#confirm-box').removeClass("hide");
}

function confirm_btn(thing){
    if(thing == "false"){
        $("#confirm-box").addClass("hide");
    }else if(thing == "true"){
        $("#confirm-box").addClass("hide");
        location.href = "/together/edit/"+together_id+"?tab=2";
    }
}

function register(id){
    const formData = new FormData();
    formData.append("title", $("#title").val());
    formData.append("content", tinymce.get('editor').getContent());
    formData.append("together_id", id);

    const img_arr = tinymce.activeEditor.dom.select('img');
    if(img_arr.length > 0){
        let save_list = [];
        img_arr.forEach(element => {
            const img_path = tinymce.activeEditor.dom.getAttrib(element, "src");
            const arr = img_path.split("/");

            save_list.push(arr[4]);
        })

        formData.append("save_list[]", save_list);
    }

    $.ajax({
        headers : {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
        url : "/together/notice",
        type : "post",
        processData: false,
        contentType: false,
        data : formData,
        dataType : "json",
        success : function(data){
            $("#alert-msg").html(data["msg"]);
            if(data["success"]){
                $("#alert-btn").attr("onclick","alert_btn('success');");
            }else{
                $("#alert-btn").attr("onclick","alert_btn('fail');");
            }
            $("#alert-box").removeClass("hide");
        }
    })
}

function update(together_id, notice_id){
    const formData = new FormData();
    formData.append("title", $("#title").val());
    formData.append("together_id", together_id);
    formData.append("content", tinymce.get("editor").getContent());

    const img_arr = tinymce.activeEditor.dom.select('img');
    if(img_arr.length > 0){
        let save_list = [];
        img_arr.forEach(element => {
            const img_path = tinymce.activeEditor.dom.getAttrib(element, "src");
            const arr = img_path.split("/");
    
            save_list.push(arr[4]);
        })
        formData.append("save_list[]", save_list);
    }

    $.ajax({
        headers : {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
        url : "/together/notice/"+notice_id,
        type : "post",
        processData: false,
        contentType: false,
        data : formData,
        dataType : "json",
        success : function(data){
            $("#alert-msg").html(data["msg"]);
            if(data["success"]){
                $("#alert-btn").attr("onclick","alert_btn('success');");
            }else{
                $("#alert-btn").attr("onclick","alert_btn('fail');");
            }
            $("#alert-box").removeClass("hide");
        }
    })
}



function alert_btn(thing){
    if(thing == "success"){
        $("#alert-box").addClass("hide");
        location.href = "/together/edit/"+together_id+"?tab=2";
    }else if(thing == "fail"){
        $("#alert-box").addClass("hide");
    }
}
</script>
@endsection