@extends('layouts.base')
@section('contents')
<div id="wrap" class="main-wrap">
    @if(isset($event))
    <div>
        <div style="position: fixed; top:0;left:0; z-index: 200; width:100%; height: 48px; background:white; border-bottom: 1px solid #f2f2f2; text-align:center; padding: 10px;">
            <img src="{{asset('images/logo.svg')}}" style="width: 100px">
        </div>
        {{-- <div class="sub-head col-group">
            <a class="no-img"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            @switch($event->type)
                @case("experience")
                <p>체험단 이벤트</p> 
                    @break
                @case("affiliate")
                <p>제휴할인 이벤트</p> 
                    @break
                @default
                <p>이벤트/공지사항</p>  
            @endswitch
            <a class="no-img"><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div> --}}
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02">
            <div class="cats-add-wrap event-wrap back-wrap">
                <div class="event-head">
                    <p class="title">{{$event->title}}</p>
                    <p class="period">{{$event->start_date}}~{{$event->end_date}}</p>
                </div>
                <div class="contents">
                    @php 
                        echo $event->content;
                    @endphp
                </div>
                @if($event->form == "privacy" || $event->form == "contents")
                <div class="top-box">
                    <p>정보를 빠짐없이 기입해주셔야 참여가 완료됩니다.</p>
                </div>
                <div class="enter-box container-all">
                    <div class="col-group">
                        <label for="name">이름</label>
                        <div class="enter">   
                            <input type="text" id="name" placeholder="이름을 입력해주세요.">
                        </div>
                    </div>
                    <div class="col-group">
                        <label for="address">주소</label>
                        <div class="enter">
                            <div class="col-group">
                                <input type="text" id="postcode" placeholder="우편번호" readonly>
                                <button onclick="execDaumPostcode();">주소검색</button>
                            </div> 
                            <input type="text" id="roadAddr" placeholder="도로명주소" readonly>
                            <input type="text" id="detailAddr" placeholder="상세주소">
                        </div>     
                    </div>
                    <div class="col-group">
                        <label for="phone">전화번호</label>
                        <div class="enter col-group last">
                            <input type="number" id="ph1" placeholder="010">
                            <input class="pd" type="number" id="ph2" placeholder="1234">
                            <input type="number" id="ph3" placeholder="5678">
                        </div>
                    </div>
                </div>
                <!-- input -->
                <div class="check-box container-all">
                    <h3>이용자 동의</h3>
                    <div class="check-privacy">
                        <div class="col-group">
                            <input type="checkbox" id="agree">
                            <label for="agree"><span>개인정보처리 방침 이용 동의(필수)</span></label>
                            <p onclick="location.href='{{url('/account/join?step=4')}}'">보기</p>
                        </div>     
                        @if ($event->form == "contents")
                        <div class="col-group">
                            <input type="checkbox" id="agree2">
                            <label for="agree2"><span>콘텐츠 활용 동의서(선택)</span></label>
                            <p onclick="location.href='{{url('/use-contents')}}'">보기</p>
                        </div>     
                        @endif
                    </div>
                </div>
                <div class="button-box">
                    <button onclick="register();">참여하기</button> 
                </div>
                @elseif($event->form == "link")
                <div class="button-box">
                    <a href="{{$event->form_link}}">페이지 이동</a>
                </div>
                @endif
            </div>
        </div>
        <!-- 서브바디 -->
    </div> 
    @else
        <script>
            alert("존재하지 않는 이벤트 페이지입니다. 로그인페이지로 이동됩니다.");
            location.href = "/";
        </script>
    @endif
</div>
<div class="search-addr hide">
    <div class="sub-head top">
        <a class="no-img"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
        <p>주소검색</p> 
        <a onclick="addr_close();"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
    </div>
    <div id="addr_container"></div>
</div>
<div id="alert-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="alert-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                </div>
                <button onclick="alert_btn('confirm');">확인</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script>
// 우편번호 찾기 찾기 화면을 넣을 element
var element_wrap = document.getElementById('addr_container');

function execDaumPostcode() {
    // 현재 scroll 위치를 저장해놓는다.
    new daum.Postcode({
        oncomplete: function(data) {
            // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var addr = ''; // 주소 변수
            var extraAddr = ''; // 참고항목 변수

            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
            if(data.userSelectedType === 'R'){
                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }
                // 조합된 참고항목을 해당 필드에 넣는다.
                document.getElementById("detailAddr").value = extraAddr;
            
            } else {
                document.getElementById("detailAddr").value = '';
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('postcode').value = data.zonecode;
            document.getElementById("roadAddr").value = addr;
            // 커서를 상세주소 필드로 이동한다.
            document.getElementById("detailAddr").focus();
            $(".search-addr").addClass("hide");
        },
        // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
        onresize : function(size) {
            element_wrap.style.height = 'calc(100vh - 48px)';
        },
        width : '100%',
        height : '100%'
    }).embed(element_wrap);

    // iframe을 넣은 element를 보이게 한다.
    element_wrap.style.display = 'block';
    $(".search-addr").removeClass("hide");
}

function addr_close(){
    $(".search-addr").addClass("hide");
}

function register(){
    alert("로그인 이후 가능한 이벤트입니다.");
    location.href = "/";
}

function alert_btn(thing){
    if(thing == "confirm"){
        $("#alert-box").addClass("hide");
    }
}
</script>
@endsection