@extends('layouts.base')
@section('contents')
<div id="wrap" class="login-wrap join-wrap">
    <div>
        <div class="sub-head col-group">
            <img class="no-img" src="" alt="">
            <p>이벤트 신청완료</p> 
            <a href="{{url('/home')}}"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <div class="sussess-box">
            <div>
                <img class="event-img" src="{{asset('images/event-cat.png')}}" alt="">
                <p>빠밤! 이벤트 신청완료!</p>
            </div>
        </div>
    </div>
</div>
@endsection