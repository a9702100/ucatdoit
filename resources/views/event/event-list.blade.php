@extends('layouts.home')
@section('style')
<style>
.select-wrap {width:100%; background: white; padding: 5px; text-align: right;}
.select-wrap select {padding: 5px 25px 5px 5px; border: 1px solid #999; background: url('/images/icon/icon_arrow_down_m.svg') no-repeat 95% 50%; border-radius: 0px; -webkit-appearance: none; -moz-appearance: none; appearance: none; background-size: 20px;}
</style>
@endsection
@section('contents')    
<div id="wrap" class="main-wrap">
    <div class="back-wrap">
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>이벤트/공지사항</p> 
            <a class="no-img"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <ul class="tabs col-group">
                <li class="tab-link {{$tab == "1" ? "current" : ""}}" data-tab="tab-1">이벤트/공지</li>
                <li class="event tab-link {{$tab == "2" ? "current" : ""}}" data-tab="tab-2">체험단</li>
                <li class="tab-link {{$tab == "3" ? "current" : ""}}" data-tab="tab-3">제휴할인</li>
            </ul>
            <div class="select-wrap">
                <select name="status">
                    <option value="on" {{$selected == "on" ? "selected" : ""}}>진행중인 이벤트</option>
                    <option value="soon" {{$selected == "soon" ? "selected" : ""}}>예정된 이벤트</option>
                    <option value="off" {{$selected == "off" ? "selected" : ""}}>마감된 이벤트</option>
                </select>
            </div>
            <div id="tab-1" class="tab-content {{$tab == "1" ? "current" : ""}}">
                {{-- 이벤트, 공지사항 --}}
                <ul class="event-wrap">
                    @if($events->count() < 1)
                        <li class="no-cont">
                            @if($selected == "on")
                            <p>진행중인 이벤트 또는 공지사항이 없습니다</p>
                            @elseif($selected == "soon")
                            <p>예정된 이벤트 또는 공지사항이 없습니다</p>
                            @elseif($selected == "off")
                            <p>마감된 이벤트 또는 공지사항이 없습니다</p>
                            @endif
                        </li>
                    @else 
                        @foreach($events as $event)
                        <li class="writing-wrap" onclick="eventShow('{{$event->id}}');">
                            <div class="banner">
                                <img src="{{asset('storage/uploads/writings/'.$event->files->where('reference','banner')->first()->fn)}}" alt="">
                                @if(new DateTime($event->end_date) < today())
                                <span class="status finish">마감</span>
                                @elseif(new DateTime($event->start_date) <= today() && new DateTime($event->end_date) >= today())
                                <span class="status">진행중</span>
                                @else 
                                <span class="status finish">예정</span>
                                @endif
                            </div>
                            <div class="contents">
                                <p class="title">{{$event->title}}</p>
                                <p>{{date_format(new DateTime($event->start_date), 'Y.m.d')}} ~ {{date_format(new DateTime($event->end_date), 'Y.m.d')}}</p>
                            </div>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div id="tab-2" class="tab-content {{$tab == "2" ? "current" : ""}}">
                {{-- 체험단 이벤트 --}}
                <ul class="event-wrap">
                    @if($experiences->count() < 1)
                        <li class="no-cont">
                            @if($selected == "on")
                            <p>진행중인 체험단 이벤트가 없습니다</p>
                            @elseif($selected == "soon")
                            <p>예정된 체험단 이벤트가 없습니다</p>
                            @elseif($selected == "off")
                            <p>마감된 체험단 이벤트가 없습니다</p>
                            @endif
                        </li>
                    @else 
                        @foreach($experiences as $experience)
                        <li class="writing-wrap" onclick="eventShow('{{$experience->id}}');">
                            <div class="banner">
                                <img src="{{asset('storage/uploads/writings/'.$experience->files->where('reference','banner')->first()->fn)}}" alt="">
                                @if(new DateTime($experience->end_date) < today())
                                <span class="status finish">마감</span>
                                @elseif(new DateTime($experience->start_date) <= today() && new DateTime($experience->end_date) >= today())
                                <span class="status">진행중</span>
                                @else 
                                <span class="status finish">예정</span>
                                @endif
                            </div>
                            <div class="contents">
                                <p class="title">{{$experience->title}}</p>
                                <p>{{date_format(new DateTime($experience->start_date), 'Y.m.d')}} ~ {{date_format(new DateTime($experience->end_date), 'Y.m.d')}}</p>
                            </div>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div id="tab-3" class="tab-content {{$tab == "3" ? "current" : ""}}">
                {{-- 제휴할인 이벤트 --}}
                <ul class="event-wrap">
                    @if($affiliates->count() < 1)
                        <li class="no-cont">
                            @if($selected == "on")
                            <p>진행중인 제휴할인 이벤트가 없습니다</p>
                            @elseif($selected == "soon")
                            <p>예정된 제휴할인 이벤트가 없습니다</p>
                            @elseif($selected == "off")
                            <p>마감된 제휴할인 이벤트가 없습니다</p>
                            @endif
                        </li>
                    @else 
                        @foreach($affiliates as $affiliate)
                        <li class="writing-wrap" onclick="eventShow('{{$affiliate->id}}');">
                            <div class="banner">
                                <img src="{{asset('storage/uploads/writings/'.$affiliate->files->where('reference','banner')->first()->fn)}}" alt="">
                                @if(new DateTime($affiliate->end_date) < today())
                                <span class="status finish">마감</span>
                                @elseif(new DateTime($affiliate->start_date) <= today() && new DateTime($affiliate->end_date) >= today())
                                <span class="status">진행중</span>
                                @else 
                                <span class="status finish">예정</span>
                                @endif
                            </div>
                            <div class="contents">
                                <p class="title">{{$affiliate->title}}</p>
                                <p>{{date_format(new DateTime($affiliate->start_date), 'Y.m.d')}} ~ {{date_format(new DateTime($affiliate->end_date), 'Y.m.d')}}</p>
                            </div>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div> 
</div>
@endsection

@section('script')
<script>
function eventShow(id){
    location.href = "/event/"+id;
}

$(document).on("change","select[name='status']", function(){
    let status = $(this).val();
    let tab  = 1;

    $(".tab-link").each(function(index, el){
        if($(".tab-link").eq(index).hasClass("current")){
            tab = index + 1;
        }
    })

    location.href = "/event?tab="+tab+"&status="+status;
})
</script>
@endsection