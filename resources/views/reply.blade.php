@extends('layouts.home')
@section('style')
<style>
    .sel-reply{
        background: #FEF9F8;
    }
</style>

@section('contents')
<div id="wrap" class="main-wrap" data-feed="{{$feed_id}}">
    <div>
        <div class="sub-head col-group">
            <a href="javascript:history.back();"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>댓글</p> 
            <img id="refresh" src="{{asset('images/icon/icon-clock.svg')}}" alt="">
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <div>
                <ul id="reply-box" class="reply">
                    @foreach($replies as $reply)
                    <li class="col-group" id="reply{{$reply->id}}">
                        <div class="left col-group">
                            <div class="box">
                                @if(isset($reply->member->upload))
                                <img class="my-img" src="{{asset('storage/uploads/profile/'.$reply->member->upload->fn)}}" alt="">
                                @else 
                                <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                @endif
                            </div>
                            <div>
                                <a class="col-group">{{$reply->member->nick}}<span>{{date_format($reply->created_at, 'y.m.d H:i')}}</span></a>
                                <p class="reply-w">{{$reply->content}}</p >
                                <p><a class="r-reply" href="{{url('/reply/answer?feed='.$feed_id.'&reply='.$reply->id)}}">답글</a></p>
                            </div>
                        </div>
                        <div class="right">
                            <img class="btn-dots" src="{{asset('images/icon/icon-dots.svg')}}" alt="">
                            <div class="dot-fixed">
                                @if(session('member') == $reply->member->id)
                                <p class="col-group"><a onclick="edit('{{$reply->id}}')"> 수정하기<img src="{{asset('images/icon/icon-pencil.svg')}}" alt=""></a></p>
                                <p class="col-group"><a onclick="del_confirm('{{$reply->id}}')">삭제하기<img src="{{asset('images/icon/icon-trash.svg')}}" alt=""></a></p>
                                @else 
                                <p class="col-group"><a class="col-group" onclick="conceal_confirm('{{$reply->id}}');">차단하기<img src="{{asset('images/icon/icon-blocked.svg')}}" alt=""></a></p>
                                <p class="col-group"><a class="col-group" href="{{url('/report?type=replies&id='.$reply->id)}}">신고하기<img src="{{asset('images/icon/icon-warning.svg')}}" alt=""></a></p>
                                @endif
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <div id="reg" class="reply-box">
                    <p class="col-group">
                        <input type="text" id="content" placeholder="댓글을 입력하세요.">
                        <button onclick="register('{{$feed_id}}');">등록</button>
                    </p>
                </div>
                <div id="edit" class="reply-box edit hide">
                    <div>
                        <img class="btn-close" src="{{asset('images/icon/icon_close.svg')}}" alt="" onclick="edit_close();">
                        <span>댓글 수정</span>
                    </div>
                    <p class="col-group">
                        <input type="text" id="content2" placeholder="댓글을 입력하세요.">
                        <button onclick="update();">수정</button>
                    </p>
                </div>
            </div>
        </div>
    </div>  
    <div id="alert-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p id="alert-msg"></p>
                    </div>
                    <button onclick="alert_close();">확인</button>
                </div>
            </div>
        </div>
    </div>
    <div id="confirm-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                    </div>
                    <div id="confirm-btn" style="display:flex">
                        <button onclick="confirm_false();">취소</button>
                        <button onclick="confirm_true();">확인</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
const feed_id = $("#wrap").data("feed");

// 수정하기, 삭제하기, 신고하기 버튼 제어
$(".dot-fixed").hide();
$(document).on("click",".btn-dots", function(){
    let index = $(".btn-dots").index(this); // 몇번째?
    if($(".dot-fixed").eq(index).is(":visible")){
        // 선택한 것이 열려진 상태면
        $(".dot-fixed").eq(index).hide();
    }else{
        // 선택한 것이 닫힌 상태면
        $(".dot-fixed").hide();
        $(".dot-fixed").eq(index).show();
    }
});

//알림창 닫기
function alert_close(){
    $("#alert-box").addClass("hide");
}

//새로고침
$(document).on("click","#refresh",function(){
    location.reload();  
})

//댓글 등록
function register(id){
    const content = $('#content').val();

    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/reply/"+id,
        type : "post",
        data : {"content":content},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $('#content').val("");
                let str = "<li class='col-group' id='reply"+data["reply_id"]+"'>\
                            <div class='left col-group'><div class='box'>";
                if(data["member_fn"] != ""){
                    str +=  "<img class='my-img' src=\"/storage/uploads/profile/"+data["member_fn"]+"\" alt=''>";   
                }else{
                    str +=  "<img class='no-my-img' src=\"{{asset('images/profile-img.svg')}}\" alt=''>";                
                }
                    str +=  "</div><div>\
                                    <a class='col-group'>"+data["nick"]+"<span>"+moment(data["created_at"]).format('HH:mm')+"</span></a>\
                                    <p class='reply-w'>"+content+"</p>\
                                    <p><a class='r-reply' href=\"/reply/answer?feed="+data["feed_id"]+"&reply="+data["reply_id"]+"\">답글</a></p>\
                                </div>\
                            </div>\
                            <div class='right'>\
                                <img class='btn-dots' src=\"{{asset('images/icon/icon-dots.svg')}}\" alt=''>\
                                <div class='dot-fixed'>\
                                    <p class='col-group'><a onclick='edit("+data["reply_id"]+")'> 수정하기<img src=\"{{asset('images/icon/icon-pencil.svg')}}\" alt=''></a></p>\
                                    <p class='col-group'><a onclick='del_confirm("+data["reply_id"]+")'>삭제하기<img src=\"{{asset('images/icon/icon-trash.svg')}}\" alt=''></a></p>\
                                </div>\
                            </div>\
                        </li>";
                $("#reply-box").prepend(str);
                $(".dot-fixed").hide();
                $('html, body').animate({scrollTop: '0'}, 400); // 화면 최상단 이동

                // 히스토리 추가 
                $.ajax({
                    headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                    url : "/history",
                    type : "post",
                    data : {"action":"reply", "what":"feed", "what_id":id}
                    
                })
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        }
    })
}

// 댓글 수정하기
function edit(id){
    // 댓글 인풋 내용 초기화
    $("#content2").val("");
    // 선택 배경 분홍색으로 
    $("[id^=reply]").removeClass("sel-reply");
    $("#reply"+id).addClass("sel-reply");
    $(".dot-fixed").hide();
    // 댓글 입력창 전환
    $("#reg").addClass("hide");
    $('#edit').removeClass("hide");
    // 댓글 수정번호 지정
    $("#edit").data("reply", id);
    // 수정하려는 내용
    let content = $("#reply"+id).find(".reply-w").text();
    $("#content2").val(content);

}

// 댓글 수정 취소
function edit_close(){
    // 댓글 인풋 내용 초기화
    $("#content2").val("");
    // 분홍색 제거
    $("[id^=reply]").removeClass("sel-reply");
    // 댓글 입력창 전환(수정창 닫기)
    $("#edit").addClass("hide");
    $("#reg").removeClass("hide");
    // 댓글 수정 번호 초기화
    $("#edit").data("reply","");
}

// 댓글 수정
function update(){
    let id       = $("#edit").data("reply"); // 수정하려는 댓글 번호
    let content  = $("#content2").val();

    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/reply/"+feed_id,
        type : "put",
        data : {"id":id, "content":content},
        dataType : "json",
        success : function(data){

            $('#alert-msg').html(data["msg"]); 
            $("#alert-box").removeClass("hide");

            if(data["success"]){
                $("#reply"+id).find(".reply-w").text(content); // 댓글 내용 변경
                edit_close(); // 수정 -> 등록 버튼으로 변경
            }
        }
    })
}

// 댓글 삭제 알람 
function del_confirm(id){
    $(".dot-fixed").hide();
    $("#confirm-msg").html("하위 댓글까지 모두 삭제됩니다<br>삭제하시겠습니까?");
    let str = "<button onclick='confirm_false();'>취소</button>\
    <button onclick=\"confirm_true('"+id+"','delete');\">확인</button>";
    $("#confirm-btn").html(str);
    $("#confirm-box").removeClass("hide");
}

function conceal_confirm(id){
    // 차단 확인용 confirm
    let confirm_btn = "<button onclick='confirm_false();'>취소</button>\
                      <button onclick=\"confirm_true('"+id+"','conceal');\">확인</button>";

    $('#confirm-msg').html("차단하면 더이상 노출되지않습니다<br>해당 댓글을 차단하시겠습니까?");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide"); 
}

function confirm_false(){
    $("#confirm-box").addClass("hide");
}

function confirm_true(id, action){
    if(action == "delete"){
        destroy(id);
    }else if(action == "conceal"){
        conceal(id);
    }
    $("#confirm-box").addClass("hide");
    
}

function destroy(id){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/reply/"+id,
        type : "delete",
        dataType: "json",
        success : function(data){
            $('#reply'+id).remove();
            $('#alert-msg').html(data["msg"]);
            $("#alert-box").removeClass("hide");
        }
    })
}

function conceal(id){
    $.ajax({
        headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/conceal",
        type : "post",
        data : {"concealable_type":"replies", "concealable_id":id},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $("#reply"+id).remove();
            }
        }
    })
}
</script>
@endsection