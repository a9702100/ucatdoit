@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a href="{{url('/event')}}"><img src="{{asset('images/icon/icon-gift.svg')}}" alt=""></a>
            <a class="logo" href="{{url('/home')}}"><img src="{{asset('images/logo.svg')}}" alt=""></a> 
            <a href="{{url('/search')}}"><img src="{{asset('images/icon/icon-search.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02 back-wrap">
            <div class="profile-head friend-head">
                <ul class="profile tag">
                    <li class="col-group">
                        <div class="left col-group">
                            <div class="box">
                                @if(isset($friend->upload))
                                <img class="my-img" src="{{asset('storage/uploads/profile/'.$friend->upload->fn)}}" alt="">
                                @else 
                                <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                @endif
                            </div>
                            <div>
                                <a>{{$friend->nick}}</a>
                                <p>{{$friend->intro}}</p>
                            </div>
                        </div>
                        <div class="right" id="relation">
                            @if($relation == "not")
                                <a onclick="relation('{{$friend->id}}','apply')"><img src="{{asset('images/icon/icon-friends.png')}}" alt="친구버튼"></a>
                            @elseif($relation == "friend")
                                <a onclick="relation('{{$friend->id}}','del');"><img src="{{asset('images/icon/icon-minus.png')}}" alt="친구버튼"></a>
                            @elseif($relation == "wait")
                                <a onclick="relation('{{$friend->id}}','wait')"><img src="{{asset('images/icon/icon-friends.png')}}" alt="친구버튼"></a>
                            @endif
                                <a class="ellipsis">
                                    <div>
                                        <img src="{{asset('images/icon/icon-dots.svg')}}" alt="확장버튼">
                                    </div>
                                    <div onclick="block('{{$friend->id}}');">
                                        <img src="{{asset('images/icon/icon-blocked.svg')}}" alt="차단버튼">
                                    </div>
                                    <div onclick="report('{{$friend->id}}');">
                                        <img src="{{asset('images/icon/icon-warning.svg')}}" alt="신고버튼">
                                    </div>
                                </a>
                        </div>
                    </li>
                </ul>
                <div class="col-group">
                    <div class="left">
                        <a href="{{url('/profile/mycats?other='.$friend->id)}}"><p>고양이</p></a>
                        <a href="{{url('/profile/mycats?other='.$friend->id)}}"><h3>{{$friend->cats->count()}}</h3></a>
                    </div>
                    <div class="right">
                        <a href="{{url('/profile/myfriends?other='.$friend->id)}}"><p>친구</p></a>
                        <a href="{{url('/profile/myfriends?other='.$friend->id)}}"><h3>{{$friend->friend->where('relation', 'friend')->count()}}</h3></a>
                    </div>
                </div>
            </div>
            <!-- 마이페이지 상단 -->
            <div class="profile-cont friend-cont">
                @if($feeds->count() == 0)
                <div class="no-cont col-group">
                    <p>등록된 포스트가 없습니다.</p>
                </div>
                @else 
                <div class="wrap-grid container">
                    @foreach($feeds as $feed)
                    @if($feed->files->count() > 0)
                    <div class="card">
                        @if($feed->files->first()->ext == "mp4")
                        <a href="{{url('/feed/'.$feed->id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$feed->files->first()->reference)}}" alt=""></a>
                        @else 
                        <a href="{{url('/feed/'.$feed->id.'/show')}}"><img src="{{asset('storage/uploads/feed/'.$feed->files->first()->fn)}}" alt=""></a>
                        @endif
                    </div>
                    @endif
                    @endforeach
                </div>
                <div id="moreBtn" class="more-button">
                    <a class="moreFeed" data-load="0" onclick="more_feed('{{$feeds->last()->id}}','{{$friend->id}}');"></a>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div id="alert-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3 id="alert-tit">알림</h3>
                        <p id="alert-msg"></p>
                    </div>
                    <button id="alert-yes" data-home="false">확인</button>
                </div>
            </div>
        </div>
    </div>
    <div id="confirm-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3 id="confirm-tit">알림</h3>
                        <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                    </div>
                    <div id="confirm-btn" style="display:flex">
                        <button id="confirm-no">취소</button>
                        <button id="confirm-yes">확인</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="report-box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3 id="confirm-tit">신고하기</h3>
                        <p id="confirm-msg">지적 재산권 침해를 신고하는 경우를 제외하고 <br> 회원님의 신고는 익명으로 처리됩니다. <br> 누군가 위급한 상황에 있다고 생각된다면 <br>  즉시 현지 응급 서비스 기관에 연락하시기 바랍니다.</p>
                    </div>
                    <div class="questions">
                        <div class="question">
                            <span>이 계정을 신고하는 이유는 무엇인가요?</span>
                            <select name="" id="answer1">
                                <option value="members" data-answer="members">계정 신고</option>
                                <option value="etc" data-answer="etc">게시물 또는 댓글 신고</option>
                            </select>
                        </div>
                        <div class="question">
                            <span>신고 대상에 대한 분류를 선택해주세요</span>
                            <select name="" id="answer2">
                                <option class="members" value="타인 사칭">타인 사칭</option>
                                <option class="members" value="적합하지 않은 컨텐츠 게시">적합하지 않은 컨텐츠 게시</option>
                                <option class="etc" value="스팸" hidden>스팸</option>
                                <option class="etc" value="나체 이미지 또는 성적행위" hidden>나체 이미지 또는 성적행위</option>
                                <option class="etc" value="마음에 들지 않습니다" hidden>마음에 들지 않습니다</option>
                                <option class="etc" value="혐오 발언 또는 상징" hidden>혐오 발언 또는 상징</option>
                                <option class="etc" value="사기 또는 거짓" hidden>사기 또는 거짓</option>
                                <option class="etc" value="거짓 정보" hidden>거짓 정보</option>
                                <option class="etc" value="따돌림 또는 괴롭힘" hidden>따돌림 또는 괴롭힘</option>
                                <option class="etc" value="폭력 또는 위험한 단체" hidden>폭력 또는 위험한 단체</option>
                                <option class="etc" value="지적 재산권 침해" hidden>지적 재산권 침해</option>
                                <option class="etc" value="자살 또는 자해" hidden>자살 또는 자해</option>
                            </select>
                        </div>
                    </div>
                    <div style="display: flex">
                        <button id="report-no">취소</button>
                        <button id="report-yes">신고하기</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
let target_id = "";
function relation(id, action){
    target_id = id;

    switch (action) {
        case "apply":
            $("#confirm-tit").html("친구 신청");
            $("#confirm-msg").html("이 회원에게 친구 신청을 하시겠습니까?");
            $("#confirm-yes").data("action", "apply");
            $("#confirm-box").removeClass("hide");
            break;
        case "wait":
            $("#alert-tit").html("수락 대기중");
            $("#alert-msg").html("상대의 친구수락을 기다려주세요");
            $("#alert-box").removeClass("hide");
            break;
        case "del":
            $("#confirm-tit").html("친구 삭제");
            $("#confirm-msg").html("이 회원을 나의 친구 목록에서 삭제하시겠습니까?");
            $("#confirm-yes").data("action", "del");
            $("#confirm-box").removeClass("hide");
            break;
        default:
            $("#alert-tit").html("오류");
            $("#alert-msg").html("일시적으로 친구관련 이벤트 오류 발생했습니다 <br> 잠시후 이용해주세요");
            $("#alert-box").removeClass("hide");
            break;
    }
}

// 신고하기
function report(id){
    target_id = id;

    $("#report-box").removeClass("hide");

}

// 알림창 확인
$(document).on("click", "#alert-yes", function(){
    let direct = $("#alert-yes").data("home");

    console.log(direct);
    $('#alert-box').addClass("hide");
    if(direct == "true"){
        location.href = "/";
    }
});

// 확인창 취소
$(document).on("click", "#confirm-no", function(){
    $('#confirm-box').addClass("hide");
});

// 확인창 확인
$(document).on("click", "#confirm-yes", function(){
    let action = $("#confirm-yes").data("action");
    $('#confirm-box').addClass("hide");

    // id :: 친구 고유번호 / action :: 행동 (a : 신청, w : 수락대기중, y : 친구 수락)
    $.ajax({
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        url : "/profile/relation",
        type : "post",
        data : {"friend_idx":target_id, "action":action},
        dataType : "json",
        success : function(data){            
            let str = "";
            if(action == "p_block"){
                $("#alert-tit").html("알림");
                $("#alert-msg").html("이 회원이 차단되었습니다.<br>확인을 누르면 홈으로 이동됩니다");
                $("#alert-yes").data("home", "true");
                $("#alert-box").removeClass("hide");
            }else if(action == "apply"){
                // 신청 -> 수락 대기중
                str = "<a onclick=\"relation('"+target_id+"','wait')\"><img src=\"{{asset('images/icon/icon-friends.png')}}\" alt=\"친구버튼\"></a>\
                        <a onclick=\"report('"+target_id+"');\"><img src=\"{{asset('images/icon/icon-alert.png')}}\" alt=\"신고버튼\"></a>";
            }else if(action == "del"){
                // 친구 삭제
                str = "<a onclick=\"relation('"+target_id+"','apply')\"><img src=\"{{asset('images/icon/icon-friends.png')}}\" alt=\"친구버튼\"></a>\
                        <a onclick=\"report('"+target_id+"');\"><img src=\"{{asset('images/icon/icon-alert.png')}}\" alt=\"신고버튼\"></a>";
            }
        
            $("#relation").html(str);

            if(action == "apply"){
                $("#alert-tit").html("친구 신청");
                $("#alert-msg").html("친구 신청되었습니다 <br>상대의 친구수락을 기다려주세요");
                $("#alert-box").removeClass("hide");
            }else if(action == "del"){
                $("#alert-tit").html("친구 삭제");
                $("#alert-msg").html("나의 친구 목록에서 삭제되었습니다");
                $("#alert-box").removeClass("hide");
            }
        }
    })
});

// 신고창 취소
$(document).on("click", "#report-no", function(){
    $("#report-box").addClass("hide");
});

// 신고창 확인
$(document).on("click", "#report-yes", function(){
    let reportable_id    = target_id;            // 신고대상 고유번호
    let reportable_type  = $("#answer1").val();  // 신고 타입
    let reason           = $("#answer2").val();  // 신고 사유

    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type : "post",
        url : "/report",
        data : {"id": reportable_id, "type": reportable_type, "reason": reason},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $("#report-box").addClass("hide");
                if(data["after"] == "home"){
                    $("#alert-yes").data("home", "true");
                }
                $("#alert-tit").html("신고하기");
                if(reportable_type == "members"){
                    $("#alert-msg").html("정상적으로 신고접수되었으며<br>더이상 이 계정을 노출시키지 않습니다");
                }else{
                    $("#alert-msg").html("정상적으로 신고접수되었습니다");
                }
                $("#alert-box").removeClass("hide");
            }
        }
    })


});

// 신고 사유 선택값에 따른 옵션 변경
$(document).on("change","#answer1", function(){
    let answer = $("#answer1 option:selected").data("answer");

    if(answer == "etc"){
        // 게시물 또는 댓글 신고
        $("#answer2 option").eq(2).prop("selected", true);
    }else{
        // 계정 신고
        $("#answer2 option").eq(0).prop("selected", true);
    }

    for (let i = 0; i < $("#answer2 option").length; i++) {
        if($("#answer2 option").eq(i).hasClass(answer)){
            $("#answer2 option").eq(i).css("display", "block");
        }else{
            $("#answer2 option").eq(i).css("display", "none");
        }
        
    }
});

// 차단하기
function block(id){
    target_id = id;

    $('#confirm-msg').html("이 회원을 차단하시겠습니까?<br>해당 회원의 모든 활동을 볼 수 없습니다");
    $("#confirm-yes").data("action", "p_block");
    $('#confirm-box').removeClass("hide");
}

$(document).on("click", ".ellipsis", function(){
    $(this).toggleClass("active");
})

// 스크롤 내려 피드 불러오기
$(window).scroll(function(){
    var scrT = $(window).scrollTop();

    if(scrT > ($(document).height() - $(window).height()) * 0.7 ){
        if($(".moreFeed").data("load") == "0"){
            $(".moreFeed").click();
            $(".moreFeed").data("load", "1");
        }
    }
})

function more_feed(feed_id, profile_id){
    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/profile/more",
        type : "post",
        data : {"feed_id": feed_id, "profile_id": profile_id},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                // 추가 로드된 피드
                data["feeds"].forEach(feed => {
                    let url = "";
                    let src = "";
                    let str = "<div class=\"card\">";
                    if(feed["files"][0]["ext"] == "mp4"){
                        // 첫 매체가 동영상 이면 썸네일
                        url = "feed/"+feed["id"]+"/show";
                        src = "storage/uploads/feed/"+feed["files"][0]["reference"];
                    }else{
                        // 사진이면 사진 출력
                        url = "feed/"+feed["id"]+"/show";
                        src = "storage/uploads/feed/"+feed["files"][0]["fn"];
                    }
                    str += `<a href=\"{{url('${url}')}}\"><img src=\"{{asset('${src}')}}\" alt=\"\"></a>`;
                    str += "</div>";

                    $('.wrap-grid').append(str);
                    $("#moreBtn").html("<a class=\"moreFeed\" data-load=\"0\" onclick=\"more_feed('"+data["last"]+"')\"></a>");
                })
            }else{
                $("#moreBtn").empty();
            }
        }
    })
}
</script>
@endsection