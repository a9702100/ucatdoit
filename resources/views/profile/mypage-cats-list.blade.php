@extends('layouts.base')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>고양이 목록</p> 
            <a class="no-img" href=""><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02">
            <div class="cats-add-wrap ">
                <ul class="cats-list ">
                    <li>
                        <div class="swiper-container cat-slide">
                            <div class="swiper-wrapper">
                                @if($cat->files)
                                @foreach($cat->files as $img)
                                <div class="swiper-slide">
                                    <img class="top-img " src="{{asset('storage/uploads/cat/'.$img->fn)}}" alt="">
                                </div>
                                @endforeach
                                @else 
                                <div class="swiper-slide">
                                    <img class="top-img " src="{{asset('images/profile-img.svg')}}" alt="">
                                </div>
                                @endif
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="container">
                            <div class="swiper-slide">   
                                <div class="col-group list-top">
                                    <img src="{{asset('images/icon/nav/together.svg')}}" alt="">
                                    <h3>{{$cat->name}}</h3>
                                </div>
                                <p class="list-cont">생일 : {{$cat->age}}</p>
                                <p class="list-cont">성별 : {{$cat->gender}}</p>
                                <p class="list-cont">특기 : {{$cat->ability}}</p>
                                <p class="list-cont">소개 : {{$cat->intro}}</p>
                            </div>
                        </div>
                    </li>
                </ul>
                @if(session('member') == $cat->member_id)
                <div class="button-box col-group">
                    <button onclick="edit('{{$cat->id}}')">수정하기</button>
                    <button onclick="del_confirm('{{$cat->id}}')">삭제하기</button>
                </div> 
                @endif
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">모든 내용이 삭제됩니다<br>삭제하시겠습니까?</p>
                </div>
                <div id="confirm-btn" style="display:flex">
                    <button onclick="confirm_false();">취소</button>
                    <button onclick="confirm_true();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
var swiper = new Swiper(".cat-slide", {
    slidesPerView: 1,
    autoHeight: true,
    spaceBetween: 8,
    autoHeight: true,
    pagination: {
    el: ".swiper-pagination",
    },
});

function del_confirm(id){
      // 삭제 확인용 confirm
      let confirm_btn = "<button onclick='confirm_false();'>취소</button>\
                      <button onclick=\"confirm_true('"+id+"','delete');\">확인</button>";

    $('#confirm-msg').html("해당 항목을 목록에서 지우시겠습니까?");
    $("#confirm-btn").html(confirm_btn);
    $("#confirm-box").removeClass("hide");
}

function confirm_true(id, action){
    if(action == "delete"){
        destroy(id);
        $("#confirm-box").addClass("hide");
    }
}

function confirm_false(){
    $('#confirm-box').addClass("hide");
}

function destroy(id){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/profile/mycats/"+id,
        type : "delete",
        dataType : "json",
        success : function(data){
           if(data["success"]){
                alert("삭제되었습니다");
                location.href="/profile/mycats";
           }
        }
    })   
}

function edit(id){
    location.href="/profile/mycats/"+id+"/edit";
}
</script>
@endsection