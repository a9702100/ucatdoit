@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap" data-other="{{$other}}">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>친구목록</p> 
            <img onclick="refresh();" src="{{asset('images/icon/icon-clock.svg')}}" alt="">
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02">
            <div class="mypage-detail">
                <div class="search-head">
                    <p class="col-group">
                        <img src="{{asset('images/icon/icon-search-g.svg')}}" alt="">
                        <input type="text" id="keyword" placeholder="검색어를 입력하세요.">
                    </p>
                </div>
                <div class="search">
                    <div class="title"><span>검색결과</span></div>
                    <div class="loading hide">
                        <img src="{{asset('images/icon/icon-loading.gif')}}" alt="로딩 이미지">
                    </div>
                    <div class="placeholder">검색할 닉네임을 입력하세요</div>
                    <div class="result">

                    </div>
                </div>
                <div class="tab-list">
                    <ul class="tabs col-group">
                        <li class="tab-link {{$tab == "1" ? "current" : ""}} {{$other != $member_id ? "friend" : ""}}" data-tab="tab-1">친구목록</li>
                        @if($other == $member_id)
                        <li class="tab-link {{$tab == "2" ? "current" : ""}}" data-tab="tab-2">친구신청</li>
                        <li class="tab-link {{$tab == "3" ? "current" : ""}}" data-tab="tab-3">차단</li>
                        @endif
                    </ul>
                    <div id="tab-1" class="tab-content  {{$tab == "1" ? "current" : ""}}">
                        @if($other == $member_id)
                        <div class="list">
                            <h3>받은 요청</h3>
                            <ul class="profile">
                                @isset($from)
                                    @foreach($from as $item)
                                    <li class="col-group">
                                        <div class="left col-group">
                                            @if($item->member->upload)
                                            <img class="my-img" src="{{asset('storage/uploads/profile/'.$item->member->upload->fn)}}" alt="">
                                            @else
                                            <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                            @endif
                                            <a href="{{url('/profile?other='.$item->member->id)}}">{{$item->member->nick}}</a>
                                        </div>
                                        <div class="right">
                                            <button onclick="relation('{{$item->member->id}}','yes')">수락</button>
                                            <button onclick="relation('{{$item->member->id}}','no')" class="gray">거절</button>
                                        </div>
                                    </li>
                                    @endforeach
                                @endisset
                            </ul>
                        </div>
                        @endif
                        <div class="list last">
                            @if($other == $member_id)
                            <h3>내 친구</h3>
                            @endif
                            <ul class="profile">
                                @isset($friends)
                                    @foreach($friends as $friend)
                                    <li class="col-group">
                                        <div class="left col-group">
                                            @if(isset($friend->member->upload))
                                            <img class="my-img" src="{{asset('storage/uploads/profile/'.$friend->member->upload->fn)}}" alt="">
                                            @else 
                                            <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                            @endif
                                            <a href="{{url('/profile?other='.$friend->member->id)}}">{{$friend->member->nick}}</a>
                                        </div>
                                        <div class="right">
                                            @if($other == $member_id)
                                            <button onclick="relation('{{$friend->member->id}}','block')">차단</button>
                                            <button onclick="relation('{{$friend->member->id}}','del')"class="gray">삭제</button>
                                            @endif
                                        </div>
                                    </li>
                                    @endforeach
                                @endisset
                            </ul>
                        </div>
                    </div>
                    <!-- 탭1 -->
                    @if($other == $member_id)
                    <div id="tab-2" class="tab-content {{$tab == "2" ? "current" : ""}}">
                        <ul class="profile">
                            @isset($to)
                                @foreach($to as $item)
                                <li class="col-group">
                                    @if($item->member != null)
                                    <div class="left col-group">
                                        @if($item->member->upload != null)
                                        <img class="my-img" src="{{asset('storage/uploads/profile/'.$item->member->upload->fn)}}" alt="">
                                        @else
                                        <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                        @endif
                                        <a href="{{url('/profile?other='.$item->member->id)}}">{{$item->member->nick}}</a>
                                        
                                    </div>
                                    <div class="right"><button onclick="relation('{{$item->member->id}}','wait')">신청취소</button></div>
                                    @endif
                                </li>
                                @endforeach
                            @endisset
                        </ul>
                    </div>
                    <!-- 탭2 -->
                    <div id="tab-3" class="tab-content {{$tab == "3" ? "current" : ""}}">
                        <ul class="profile">
                            @isset($blocks)
                                @foreach($blocks as $block)
                                <li class="col-group">
                                    <div class="left col-group">
                                        @if($block->member->upload)
                                        <img class="my-img" src="{{asset('storage/uploads/profile/'.$block->member->upload->fn)}}" alt="">
                                        @else
                                        <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                        @endif
                                        <a>{{$block->member->nick}}</a>
                                    </div>
                                    <div class="right"><button onclick="relation('{{$block->member->id}}','unlock')">차단해제</button></div>
                                </li>
                                @endforeach
                            @endisset
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection

@section('script')
<script>
function relation(id, action, where){
    // id :: 친구 고유번호 / action :: 행동 (a : 신청, w : 수락대기중, y : 친구 수락)
    $.ajax({
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        url : "/profile/relation",
        type : "post",
        data : {"friend_idx":id, "action":action},
        dataType : "json",
        success : function(data){           
            if(where == "search"){
                let btn = $(".relation"+id);
                if(action == "apply"){
                    btn.attr("onclick","relation('"+id+"','wait','search')");
                    btn.html("신청취소");
                }else if(action == "wait" || action == "unlock"){
                    btn.parent().parent().remove();
                }else if(action == "yes"){
                    btn.attr("onclick","");
                    btn.html("친구상태");

                    // 히스토리 추가 
                    $.ajax({
                        headers:{"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
                        url : "/history",
                        type : "post",
                        data : {"action":"friend","what":"friend","what_id":id}
                    })
                }
            }else{
                if(action == "unlock"){
                    location.href = "/profile/myfriends?tab=3";
                }else if(action == "wait"){
                    location.href = "/profile/myfriends?tab=2";
                }else{
                    if(action == "yes"){
                        // 히스토리 추가 
                        $.ajax({
                            headers:{"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
                            url : "/history",
                            type : "post",
                            data : {"action":"friend","what":"friend","what_id":id}
                        })    
                    }
                    location.href = "/profile/myfriends?tab=1";
                }
            }

        }
    })
}

function refresh(){
    location.reload();
}

const other = $("#wrap").data("other");

$("#keyword").on({
    focus: function(){
        $(".tab-list").hide();
        $(".search").show();
    },
    input : function(){
        $(".placeholder").addClass("hide");
        let keyword = $("#keyword").val();
        if(keyword.length < 1){
            // $(".search").hide();
            // $(".result").empty();
            // $(".tab-list").show();
            location.reload();
        }else{
            $(".tab-list").hide();
            $(".search").show();
            $(".loading").removeClass("hide");
            $.ajax({
                headers: {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                url : "/search/friend",
                type : "post",
                data : {"other":other, "keyword":keyword},
                dataType : "json",
                success : function(data){
                    $(".loading").addClass("hide");
                    $(".result").removeClass("hide");
                    if(data["friends_cnt"] > 0){
                        // 검색 결과 있음 :: 계정 검색
                        let str = "<ul class=\"profile\">";
                        data["result_friends"].forEach(friend => {    
                            str += "<li class=\"col-group\">\
                                        <div class=\"left col-group\">";
                            if(friend["fn"] != ""){
                                str += "<img class=\"my-img\" src=\"{{asset('storage/uploads/profile')}}/"+friend["fn"]+"\" alt=''>";
                            }else{
                                str += "<img class=\"no-my-img\" src=\"{{asset('images/profile-img.svg')}}\" alt=''>";
                            }
                            str += "<a href='/profile?other="+friend["id"]+"'>"+friend["nick"]+"</a>\
                                        </div>\
                                           <div class=\"right\">";
                            if(friend["relation"] == "me"){
                                str += "<button>내 계정</button>";
                            }else if(friend["relation"] == "friend"){
                                str += "<button>현재친구</button>";
                            }else if(friend["relation"] == "wait"){
                                str += "<button class=\"relation"+friend["id"]+"\" onclick=\"relation('"+friend["id"]+"','wait','search')\">신청취소</button>";
                            }else if(friend["relation"] == "not"){
                                str += "<button class=\"relation"+friend["id"]+"\" onclick=\"relation('"+friend["id"]+"','apply','search')\">친구신청</button>";
                            }else if(friend["relation"] == "yes"){
                                str += "<button class=\"relation"+friend["id"]+"\" onclick=\"relation('"+friend["id"]+"','yes','search')\">친구수락</button>";
                            }else if(friend["relation"] == "block"){
                                str += "<button class=\"relation"+friend["id"]+"\" onclick=\"relation('"+friend["id"]+"','unlock','search')\">차단해제</button>";
                            }
                            str += "</div>\
                            </li>";
                        });
                        str += "</ul>";
                        $(".result").html(str);
                    }else{
                        // 검색 결과 없음 :: 계정 검색
                        let str = "<div class='placeholder'><p>\'"+keyword+"\'에 대한<br> 검색 결과가 없습니다</p></div>";
                        $(".result").html(str);
                    }
                }
            })
        }
    }
})
</script>
@endsection