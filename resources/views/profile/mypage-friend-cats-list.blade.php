@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>고양이 목록</p> 
            <a class="no-img"><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02">
            @if($cats->count() == 0)
            <div class="cat-wrap col-group">
                <div class="no-cat">
                    <img src="{{asset('images/icon/icon-nocats.svg')}}" alt="">
                    <h3>현재 등록된 고양이가 없습니다.</h3>
                </div>
            </div>
            @else 
            {{-- 고양이 신분증 목록 불러오기 --}}
            <div class="swiper-container card-wrap">
                <div class="swiper-pagination"></div>
                <div class="swiper-wrapper card-area">
                    @foreach($cats as $key => $cat)
                    <div class="swiper-slide card-slide">
                        <div class="group">
                            <div class="card-border" id="cat{{$key}}">
                                <div class="card">
                                    <div class="header"></div>
                                    <div class="content">
                                        <div class="left">
                                            <div class="swiper-container cat-slide">
                                                <div class="swiper-wrapper">
                                                    @if($cat->files)
                                                    @foreach($cat->files as $img)
                                                    <div class="swiper-slide img-slide">
                                                        <a href="{{url('/profile/mycats/'.$cat->id)}}"><img src="{{asset('storage/uploads/cat/'.$img->fn)}}" alt=""></a>
                                                    </div>
                                                    @endforeach
                                                    @else 
                                                    <div class="swiper-slide img-slide">
                                                        <img src="{{asset('images/profile-img.svg')}}" alt="">
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <p class="line">
                                                <span class="title">이름</span><span class="desc">{{$cat->name}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">성별</span><span class="desc">{{$cat->gender}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">생일</span><span class="desc">{{$cat->age}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">특징</span><span class="desc">{{$cat->ability}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">집사</span><span class="desc">{{$cat->member->nick}}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="footer">
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="button">
                                <button onclick="cardShot('{{$key}}');">이미지로 저장하기</button>
                            </div> --}}
                        </div>
                    </div>
                    @endforeach 
                </div>
            </div>
            @endif
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
@endsection

@section('script')
<script>
var swiper = new Swiper(".card-wrap", {
    initialSlide : 1,
    slidesPerView: 1,
    spaceBetween: 8,
    pagination: {
        el : ".swiper-pagination",
        type : "bullets",
        clickable : true,
        bulletClass : 'card-bullet',
        bulletActiveClass : 'card-bullet-active'
    }
});
</script>
@endsection