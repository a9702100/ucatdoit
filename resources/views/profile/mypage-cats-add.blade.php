@extends('layouts.base')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="cancel();"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            @if(isset($cat))
            <p>고양이 수정</p> 
            @else 
            <p>고양이 추가</p> 
            @endif
            <a class="no-img" href=""><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02">
            <div class="cats-add-wrap swiper-container mySwiper">
                <div class="both">
                    <div class="plus">
                        <div class="col-group">
                            <div>
                                <input type="file" id="camera" multiple accept="image/*">
                                <label for="camera"><img src="{{asset('images/icon/icon-camera.svg')}}" alt=""></label>
                                <p><span id="img-cnt">{{isset($cat) ? $cat->files->count(): "0"}}</span>/10</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-container uploadMySwiper col-group">
                        <ul id="img-box" class="img-add swiper-wrapper">
                            @if(isset($cat))
                            @foreach($cat->files as $file)
                            <li class="swiper-slide">
                                <img class="cat-fn" src="{{asset('storage/uploads/cat/'.$file->fn)}}" alt="" data-fn="{{$file->fn}}">
                                <button class="btn-close"><img src="{{asset('images/icon/icon-close-b.svg')}}" alt=""></button>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div> 
                <div class="enter-box container">
                    <div>
                        <label for="name">이름</label>
                        <input type="text" id="name" value="{{isset($cat) ? $cat->name : ""}}" placeholder="고양이 이름을 입력해주세요.">
                    </div>
                    <div>
                        <label for="age">생일(선택)</label>
                        <div class="col-group">
                            <select class="did" name="yy" id="year"></select>
                            <select class="did" name="mm" id="month"></select>
                            <select class="did" name="dd" id="day"></select>
                        </div>
                    </div>
                    <div>
                        <label for="age">성별(선택)</label>
                        <input type="text" id="gender" value="{{isset($cat) ? $cat->gender : ""}}" placeholder="성별을 10자 이하로 입력해주세를">
                    </div>
                    <div>
                        <label for="age">특징(선택)</label>
                        <input type="text" id="ability" value="{{isset($cat) ? $cat->ability : ""}}" placeholder="특징를 10자 이하로 입력해주세요">
                    </div>
                    <div>
                        <label for="intro">소개내용</label>
                        <textarea name="" id="intro" placeholder="소개 내용을 입력해주세요.">{{isset($cat) ? $cat->intro : ""}}</textarea>
                    </div>  
                </div>
                <!-- input -->
                <div class="button-box">
                    @if(isset($cat))
                    <button onclick="update('{{$cat->id}}');">수정하기</button>
                    @else 
                    <button onclick="register();">저장하기</button>
                    @endif
                </div>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div id="alert-box" class="join-wrap-popup popup-ver hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3 id="alert-tit">알림</h3>
                    <p id="alert-msg">내용 문구를 입력하세요</p>
                </div>
                <button onclick="alert_close();">확인</button>
            </div>
        </div>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">작성중인 내용은 지워집니다<br>뒤로 이동하시겠습니까?</p>
                </div>
                <div style="display:flex">
                    <button onclick="confirm_false();">취소</button>
                    <button onclick="confirm_true();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="wrap-progress hide">
    <div class="inner">
        <p>업로드 중입니다</p>
        <p>잠시만 기다려주세요</p>
        <progress id="progressBar" max="100" value="0"></progress>
        <p id="progress_rate">0%</p>
    </div>
</div> --}}
<div class="wrap-progress hide">
    <div class="inner">
        <p>업로드 중입니다</p>
        <p>잠시만 기다려주세요</p>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){            
    var now = new Date();
    var year = now.getFullYear();
    var mon = (now.getMonth() + 1) > 9 ? ''+(now.getMonth() + 1) : '0'+(now.getMonth() + 1); 
    var day = (now.getDate()) > 9 ? ''+(now.getDate()) : '0'+(now.getDate());           
    //년도 selectbox만들기               
    for(var i = 1900 ; i <= year ; i++) {
        $('#year').append('<option value="' + i + '">' + i + '년</option>');    
    }

    // 월별 selectbox 만들기            
    for(var i=1; i <= 12; i++) {
        var mm = i > 9 ? i : "0"+i ;            
        $('#month').append('<option value="' + mm + '">' + mm + '월</option>');    
    }
    
    // 일별 selectbox 만들기
    for(var i=1; i <= 31; i++) {
        var dd = i > 9 ? i : "0"+i ;            
        $('#day').append('<option value="' + dd + '">' + dd+ '일</option>');    
    }
    $("#year  > option[value="+year+"]").attr("selected", "true");        
    $("#month  > option[value="+mon+"]").attr("selected", "true");    
    $("#day  > option[value="+day+"]").attr("selected", "true");       
  
})

var uploadSlider = new Swiper(".uploadMySwiper", {
    slidesPerView: 'auto',
    spaceBetween: 0,
});

var sel_files = []; // 파일 배열
$(".cat-fn").each(function(){
    sel_files.push($(this).data("fn"));
})

// 팝업창 닫기    
function alert_close(){
    $("#alert-box").addClass('hide');
}

// 피드 창 닫기
function cancel(){
    $("#confirm-box").removeClass("hide");
}

// 확인창 : 확인 누름
function confirm_true(){
    location.href="/profile/mycats";
}

// 확인창 : 취소 누름
function confirm_false(){
    $("#confirm-box").addClass("hide");
}

// 피드 업로드
function register(){

    const name    = $("#name").val();
    const age     = $("#year option:selected").val()+"."+$("#month option:selected").val()+"."+$("#day option:selected").val();
    const gender  = $("#gender").val();
    const ability = $("#ability").val();
    const intro   = $("#intro").val();

    $(".wrap-progress").removeClass("hide");
    $.ajax({
        headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/profile/mycats",
        type : "post",
        data : {"name":name, "age":age, "intro":intro, "gender":gender, "ability":ability, "sel_files":sel_files},
        dataType : "json",
        // xhr : function(){
        //     var xhr = $.ajaxSettings.xhr();
        //     xhr.upload.onprogress = function(e){
        //         var percent = Math.round(e.loaded * 100 / e.total);
        //         setProgress(percent);
        //         if(percent == 100){
        //             $(".wrap-progress").addClass("hide");
        //         }
        //     };
        //     return xhr;},
        success : function(data){
            if(data["success"]){
                alert("업로드 되었습니다");
                location.href = "/profile/mycats";
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        }
    })
}

function update(id){
    const name    = $("#name").val();
    const age     = $("#year option:selected").val()+"."+$("#month option:selected").val()+"."+$("#day option:selected").val();
    const gender  = $("#gender").val();
    const ability = $("#ability").val();
    const intro   = $("#intro").val();

    $(".wrap-progress").removeClass("hide");
    $.ajax({
        headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/profile/mycats/"+id,
        type : "put",
        data : {"name":name, "age":age, "gender":gender, "ability":ability, "intro":intro, "sel_files":sel_files},
        dataType : "json",
        // xhr : function(){
        //     var xhr = $.ajaxSettings.xhr();
        //     xhr.upload.onprogress = function(e){
        //         var percent = e.loaded * 100 / e.total;
        //         setProgress(percent);
        //         if(percent == 100){
        //             $(".wrap-progress").addClass("hide");
        //         }
        //     };
        //     return xhr;
        // },
        success : function(data){
            if(data["success"]){
                alert("업로드 되었습니다");
                location.href = "/profile/mycats";
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        },error : function(){

        }
    })
}

let del_files = [];
// 피드 사진 삭제 
$(document).on("click",".btn-close",function(){
    let img_cnt  = $('#img-cnt').text(); // 현재 추가된 이미지(동영상)의 수
    let index    = $(".btn-close").index(this); // 클릭한 X의 인덱스 

    $("#img-cnt").html(--img_cnt);
    $(this).parent("li").remove();
    sel_files.splice(index, 1);
    available = available + 1;
    if($(this).prev().data("type") == "old"){
        del_files.push($(this).prev().data("fn"));
    }
    console.log(available);
    console.log(sel_files);
    console.log(del_files);
});

let available = 10 - $("#img-cnt").text(); // 파일 등록 가능 수


// 피드 사진 추가
$(document).on("change","#camera", function(){
    let oFile = $(this)[0].files;
    if(oFile.length < 1){

    }else{
        if(available < 1){
            $("#alert-msg").html("업로드는 최대 10장까지 가능합니다");
            $("#alert-box").removeClass("hide");
        }else{
            readImage(this);
        }
    }
});

$(document).ajaxStart(function(){
    $(".wrap-progress").removeClass("hide");
});

$(document).ajaxStop(function(){
    $(".wrap-progress").addClass("hide");
});

function readImage(input) {
    // 인풋 태그에 파일이 있는 경우
    let img_cnt  = $('#img-cnt').text();
    if(input.files && input.files[0]) {

        var filesArr = Array.prototype.slice.call(input.files);

        // 업로드 최대 허용 숫자에 도달하면 -> 선택 수를 줄여줌
        if(filesArr.length > available){
            filesArr.splice(available); // 최대 가능 수 만큼 자름
        }

        filesArr.forEach(function(f){
            if(sel_files.length < 10){
                // 파일 실서버 전송
                $(".wrap-progress").removeClass('hide');
                available--;
                let formData = new FormData();
                formData.append('upload_img', f);
                $.ajax({
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url : "/profile/fileUpload/cats",
                    type : "post",
                    data : formData, 
                    processData : false,
                    contentType : false, 
                    dataType : "json",
                    success : function(data){
                        if(data["success"]){
                            // 압축 성공
                            let str = "<li class='swiper-slide'>\
                                        <img src=\'{{asset('storage/uploads/temp')}}/"+data["fn"]+"\' alt='' data-fn='"+data["fn"]+"'>\
                                        <button class='btn-close'><img src=\'{{asset('images/icon/icon-close-b.svg')}}\' alt=''></button>\
                                    </li>";
                            uploadSlider.appendSlide(str);
                            sel_files.push(data["fn"]);
                            $("#img-cnt").html(++img_cnt);
                        }else{
                            //압축 실패
                            $("#alert-msg").html(data["msg"]);
                            $("#alert-box").removeClass("hide");
                            available = available + 1;
                        }
                    }
                }); // ajax 끝
            }
        }); // foreach  끝
    }
}
// function readImage(input) {
//     // 인풋 태그에 파일이 있는 경우
//     let img_cnt  = $('#img-cnt').text();
//     if(input.files && input.files[0]) {

//         var filesArr = Array.prototype.slice.call(input.files);

//         // 업로드 최대 허용 숫자에 도달하면 -> 선택 수를 줄여줌
//         if(filesArr.length > available){
//             filesArr.splice(available); // 최대 가능 수 만큼 자름
//         }

//         filesArr.forEach(function(f){
//             if(sel_files.length < 10){
//                 // 파일 실서버 전송
//                 $(".wrap-progress").removeClass('hide');
//                 available--;
//                 let formData = new FormData();
//                 formData.append('upload_img', f);
//                 $.ajax({
//                     headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
//                     url : "/profile/fileUpload/cats",
//                     type : "post",
//                     data : formData, 
//                     processData : false,
//                     contentType : false, 
//                     dataType : "json",
//                     xhr : function(){
//                         var xhr = $.ajaxSettings.xhr();
//                         xhr.upload.onprogress = function(e){
//                             var percent = e.loaded * 100 / e.total;
//                             setProgress(percent);
//                             if(percent == 100){
//                                 $(".wrap-progress").addClass("hide");
//                             }
//                         };
//                         return xhr;
//                     },
//                     success : function(data){
//                         if(data["success"]){
//                             // 압축 성공
//                             let str = "<li class='swiper-slide'>\
//                                         <img src=\'{{asset('storage/uploads/temp')}}/"+data["fn"]+"\' alt='' data-fn='"+data["fn"]+"'>\
//                                         <button class='btn-close'><img src=\'{{asset('images/icon/icon-close-b.svg')}}\' alt=''></button>\
//                                     </li>";
//                             uploadSlider.appendSlide(str);
//                             sel_files.push(data["fn"]);
//                             $("#img-cnt").html(++img_cnt);
//                         }else{
//                             //압축 실패
//                             $("#alert-msg").html(data["msg"]);
//                             $("#alert-box").removeClass("hide");
//                             available = available + 1;
//                         }
//                     }
//                 }); // ajax 끝
//             }
//         }); // foreach  끝
//     }
// }

function setProgress(per){
    $("#progressBar").val(per);
    $("#progress_rate").html(Math.round(per)+"%");
}
</script>
@endsection