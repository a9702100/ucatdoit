@extends('layouts.home')
@section('style')
<style>
.card.cert_b {background-image : url('/images/cert2.png'); background-size:contain;}
.card.cert_b .header {background-color: transparent !important; justify-content: center}
.card.cert_b .content {background-color: transparent !important;}
.card.cert_b .footer {background-color: transparent !important;}
.card.cert_b .mark {display: none;}
.card.cert_b .flat-cat {display: none;}

.background {position: absolute; top: 0; left: 50%; transform: translate(-50%, 50%); display: flex; align-items: center}
.background label {margin-left: 5px ; margin-right: 10px;}
.background label:last-child {margin-right : 0}
</style>
@endsection
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group">
            <a href="{{url('/profile')}}"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>고양이 목록</p> 
            <a class="no-img"><img src="{{asset('images/icon/icon-plus.svg')}}" alt=""></a>
        </div>
        <div class="sub-cont sub-cont02">
            <div class="swiper-container card-wrap">
                <div class="swiper-pagination"></div>
                <div class="swiper-wrapper card-area">
                    <div class="swiper-slide card-slide">
                        {{-- 슬라이드 :: 신분증 추가하기 --}}
                        <div class="group">
                            <div class="card-border">
                                <div class="card empty">
                                    <a href="{{url('/profile/mycats/create')}}">
                                        <img src="{{asset('images/icon/icon-nocats.svg')}}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="msg">
                                <p>플러스 버튼을 눌러<br>고양이 신분증을 추가해보세요</p>
                            </div>
                        </div>
                    </div>
                    {{-- 고양이 신분증 목록 불러오기 --}}
                    @foreach($cats as $key => $cat)
                    <div class="swiper-slide card-slide">
                        <div class="group">
                            <div class="background">
                                <input type="radio" id="sel{{2*$key}}" name="bg{{$key}}" checked value="A"><label for="sel{{2*$key}}">배경A</label>
                                <input type="radio" id="sel{{2*$key+1}}" name="bg{{$key}}" value="B"><label for="sel{{2*$key+1}}">배경B</label>
                            </div>
                            <div class="card-border" id="cat{{$key}}">
                                <div class="card" data-nth = "{{$key}}">
                                    <div class="header"></div>
                                    <div class="content">
                                        <div class="left">
                                            <div class="swiper-container cat-slide">
                                                <div class="swiper-wrapper">
                                                    @if($cat->files)
                                                    @foreach($cat->files as $img)
                                                    <div class="swiper-slide img-slide">
                                                        <a href="{{url('/profile/mycats/'.$cat->id)}}"><img src="{{asset('storage/uploads/cat/'.$img->fn)}}" alt=""></a>
                                                    </div>
                                                    @endforeach
                                                    @else 
                                                    <div class="swiper-slide img-slide">
                                                        <img src="{{asset('images/profile-img.svg')}}" alt="">
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right">
                                            <p class="line">
                                                <span class="title">이름</span><span class="desc">{{$cat->name}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">성별</span><span class="desc">{{$cat->gender}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">생일</span><span class="desc">{{$cat->age}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">특징</span><span class="desc">{{$cat->ability}}</span>
                                            </p>
                                            <p class="line">
                                                <span class="title">집사</span><span class="desc">{{$cat->member->nick}}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        {{-- <img src="{{asset('images/logo.svg')}}" alt=""> --}}
                                    </div>
                                    {{-- <img src="{{asset('images/mark.png')}}" alt="" class="mark">
                                    <img src="{{asset('images/flat-cat.png')}}" alt="" class="flat-cat"> --}}
                                </div>
                            </div>
                            <div class="button">
                                <button onclick="cardShot('{{$key}}');">이미지로 저장하기</button>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection
@section('script')
<script src="{{asset('js/html2canvas.js')}}"></script>
<script>
var swiper = new Swiper(".card-wrap", {
    initialSlide : 1,
    slidesPerView: 1,
    spaceBetween: 8,
    pagination: {
        el : ".swiper-pagination",
        type : "bullets",
        clickable : true,
        bulletClass : 'card-bullet',
        bulletActiveClass : 'card-bullet-active',
        clickableClass : 'card-clickable'
    }
});

var swiper = new Swiper(".cat-slide", {
    slidesPerView: 1,
    spaceBetween: 8
});

function cardShot(index) { 
    const today = new Date().format('yyyy_MM_dd');
    card = $("#cat"+index)[0];
    html2canvas(card).then(function (canvas){
        var myImage = canvas.toDataURL();
        downloadURI(myImage, "cat_"+today+".png");
    }); 
}

function downloadURI(uri, name){
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
}

$(document).on("click", "[name^=bg]", function(){
    let target = $(this).attr("name");
    let target_num = Number(target.substr(2));
    let bg_type = $(this).val();

    if(bg_type == "A"){
        $(".card").eq(target_num + 1).removeClass("cert_b");
    }else if(bg_type =="B"){
        $(".card").eq(target_num + 1).addClass("cert_b");
    }

})

Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

</script>
@endsection
