@extends('layouts.base')
@section('contents')
<div id="wrap" class="main-wrap back-wrap">
    <div>
        <div class="sub-head col-group">
            <a class="no-img"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p class="logo"><img src="{{asset('images/logo.svg')}}" alt=""></p> 
            <a class="no-img"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <div class="stroy-box">
                <ul>
                    <li class="container-all">
                        @if($isFeed)
                        <div class="top col-group">
                            <div class="left">
                                <div class="box">
                                    @if($feed->feedable->upload)
                                    <img class="my-img" src="{{asset('storage/uploads/profile/'.$feed->feedable->upload->fn)}}" alt="">
                                    @else 
                                    <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                    @endif
                                </div>   
                                <a>{{$feed->feedable->nick}}</a>
                            </div>
                            <div class="right">
                                
                            </div>
                        </div>
                        <div class="img-box">
                            <div id="feed{{$feed->id}}" class="swiper-container main-slide">       
                                <ul class="swiper-wrapper">
                                    @foreach($feed->files as $file)
                                    <li class="swiper-slide">
                                        @if($file->ext == "mp4")
                                        <video poster="{{asset('storage/uploads/feed/'.$file->reference)}}" controls muted loop autoplay playsinline controlsList="nodownload" preload="metadata">
                                            <source src="{{asset('storage/uploads/feed/'.$file->fn)}}" type="video/mp4" alt="">
                                            죄송합니다. 이 브라우저에서 지원하지 않는 영상입니다
                                        </video>
                                        @else 
                                        <img src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt="" >
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div id="swiper-page{{$feed->id}}" class="main-pagination"></div>
                        </div>
                        <div class="center col-group">
                            
                        </div>
                        <div class="txt-box">
                            <pre>{{$feed->content}}</pre>
                            <div class="tag col-group">
                            @foreach($feed->tags as $tag)
                                <a>#{{$tag->content}}</a>
                            @endforeach
                            </div>
                        </div>
                        @else 
                        <div class="feed_empty">
                            존재하지 않거나 삭제된 피드 입니다
                        </div>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div class="download_popup hide">
    <div class="inner active">
        <img class="close" src="{{asset('images/icon/icon_close.svg')}}" alt="">
        <div class="tit">
            <img src="{{asset('images/logo.svg')}}" alt="">
        </div>
        <div class="content">
            <p>더 많은 고양이를 보고 싶고,</P>
            <p>집사들과 소통하고 싶다면?</p>        
        </div>
        <div class="button-group">
            <p><a href="https://play.google.com/store/apps/details?id=com.roarcompany.cat">구글스토어에서 설치</a></p>
            <p><a href="https://roarcompany.page.link/?link=https://ucat-dev.com/&apn=com.roarcompany.cat">앱으로 이동</a></p>
            {{-- <span><a>앱스토어에서 설치</a></span> --}}
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
.feed_empty{display: flex; justify-content: center; align-items: center}
.download_popup{position: fixed; top:0; left:0;  width: 100%; height: 100%; background: rgba(0,0,0,0.5); z-index:500; display: flex; justify-content: center; align-items: center}
.download_popup .inner {width: 60%; height: 300px; border-radius: 5px; background:white; padding: 10px; display: flex; flex-direction: column; justify-content: space-between; max-width: 400px}
.download_popup .inner .close {width: 25px; height: 25px;}
.download_popup .inner .tit {width: 100%; height:60px; padding: 5px; text-align: center}
.download_popup .inner .tit img {width: 80%; height: auto}
.download_popup .inner .content {padding: 5px 10px; text-align: center}
.download_popup .inner .content p:last-child{padding-left:10px;}
.download_popup .inner .button-group {padding: 5px; display: flex; flex-direction: column; align-items:center;}
.download_popup .inner .button-group p {border: 1px solid #ddd; border-radius:50px; padding: 5px 10px; margin-bottom: 10px; width:80%; text-align: center}

.download_popup.hide {display: none}
.download_popup .inner.active {animation: fadein 1s; -webkit-animation: fadein 1s}
@keyframes fadein {
    from {
        opacity: 0;
    }
    to {
        opacity: 1;
    }
}
@-webkit-keyframes fadein {
    from {
        opacity: 0;
    }
    to {
        opacity: 1;
    }
}

</style>
@endsection
@section('script')
<script>
// 피드 슬라이드 - 각 피드당 슬라이드 매기기
$(".main-slide").each(function(index){
    new Swiper($(this)[0],{
        autoHeight: true,
        pagination: {
            el: $(".main-pagination")[index],
        }
    });
})

$(document).on("click",".close",function(){
    $(".download_popup").addClass("hide");
})

$(document).ready(function(){
    setTimeout(function(){
        $(".download_popup").removeClass("hide");
    }, 5000);
})
</script>
@endsection