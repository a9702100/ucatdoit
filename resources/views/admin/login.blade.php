<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="google-signin-client_id" content="997748481059-4pcglcbvfqpo1lgpbckckfk4ro4kvgb8.apps.googleusercontent.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>너만없어고양이</title>
    <link rel="stylesheet" href="{{asset('admin/css/common.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">

    <script type="text/javascript" src="{{asset('admin/js/jquery-3.5.1.min.js')}}"></script> 
</head>
<body>
    <div class="login_wrap">
        <h5>관리자 페이지</h5>
        <img src="{{asset('images/logo.svg')}}" alt="로고">

        <div class="container">
            <form action="/admin/check" method="POST">
                @csrf
                <div class="input-group">
                    <input type="text" id="userId" name="userId" placeholder="아이디를 입력하세요" value="{{old('userId')}}">
                    @error('userId')
                    <span>{{$message}}</span>
                    @enderror
                    @if(Session::get('fail_userId'))
                    <span>{{Session::get('fail_userId')}}</span>
                    @endif
                </div>
                <div class="input-group">
                    <input type="password" id="passwd" name="passwd" placeholder="비밀번호를 입력하세요" value="{{old('passwd')}}">
                    @error('passwd')
                    <span>{{$message}}</span>
                    @enderror
                    @if(Session::get('fail_passwd'))
                    <span>{{Session::get('fail_passwd')}}</span>
                    @endif
                </div>
                <input type="submit" value="로그인">
            </form>
            @if(Session::get('abnormal'))
            <div class="error-box">
                <span>{{Session::get('abnormal')}}</span>
            </div>
            @endif
        </div>
    </div>
</body>

</html>