@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>푸시 메세지 템플릿</h2>
        </div>
    </div>
    <div class="wrap-cont">
        <div class="btn-group">
            <button onclick="add_template();">템플릿 추가</button>
        </div>
        <div class="templates">
            @foreach ($templates as $template)
            <div class="template" data-id="{{$template->id}}">
                <div class="name">
                    <input type="text" name="name" placeholder="템플릿 이름" value="{{$template->name}}" readonly>
                </div>
                <div class="message">
                    <div class="content">
                        <label>알림 제목</label>
                        <input name="main_msg" type="text" value="{{$template->main_msg}}">
                    </div>
                    <div class="content">
                        <label>알림 내용</label>
                        <textarea name="sub_msg" id="" cols="30" rows="5">{{$template->sub_msg}}</textarea>
                    </div>
                </div>
                <div class="footer">
                    <button class="delete">삭제</button>
                    <button class="save">저장</button>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</main>
@endsection

@section('style')
<style>
.wrap-cont {min-width: 1100px; padding: 15px;}
.wrap-cont .btn-group {justify-content: flex-start; gap: 0 10px;}

.wrap-cont .templates {display: flex; gap: 20px; flex-wrap: wrap}
.wrap-cont .templates .template {border: 1px solid black; width: calc((100% - 40px) / 3); padding: 20px;}
.wrap-cont .templates .template .name {display: flex; align-items: center; margin-bottom: 10px;}
.wrap-cont .templates .template .name input[type="text"] {border: none; outline: 0; padding: 8px 0; font-size: 16px; border-bottom: 1px solid black;}
.wrap-cont .templates .template .message {padding: 10px 0;}
.wrap-cont .templates .template .message .content {margin-bottom: 10px;}
.wrap-cont .templates .template .message .content label::before {content:""; background: tomato; border: 3px solid tomato; margin-right: 10px;}
.wrap-cont .templates .template .message .content input[type="text"], textarea {margin-top: 10px; margin-bottom: 10px; padding: 8px;}
.wrap-cont .templates .template .footer {display: flex; justify-content: flex-end; align-items: center; padding: 5px 0; gap: 0 10px;}
.wrap-cont .templates .template .footer button:hover {background: rgba(0,0,0,0.1)}
</style>
@endsection

@section('script')
<script>
function add_template(){
    // 템플릿 추가 
    let str = "<div class=\"template\">\
                <div class=\"name\">\
                    <input type=\"text\" name=\"name\" placeholder=\"템플릿 이름\">\
                </div>\
                <div class=\"message\">\
                    <div class=\"content\">\
                        <label>알림 제목</label>\
                        <input name=\"main_msg\" type=\"text\">\
                    </div>\
                    <div class=\"content\">\
                        <label>알림 내용</label>\
                        <textarea name=\"sub_msg\" id=\"\" cols=\"30\" rows=\"5\"></textarea>\
                    </div>\
                </div>\
                <div class=\"footer\">\
                    <button class=\"delete\">삭제</button>\
                    <button class=\"save\">저장</button>\
                </div>\
            </div>";
    
    $(".templates").append(str);
}

$(document).on("click", ".delete", function(){
    let index = $(".delete").index(this);

    if($('.template').eq(index).data("id") == undefined){
        // 아직 저장되지 않은 템플릿
        $(".template").eq(index).remove();
    }else{
        // 저장되어있는 템플릿 삭제
        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            url : "/admin/push/template/"+$(".template").eq(index).data("id"),
            type : "delete",
            success : function(data){
                alert(data["msg"]);
                $(".template").eq(index).remove();
            },error : function(){
                alert("삭제 오류");
            }
        })
    }
})

$(document).on("click", ".save", function(){
    let index = $(".save").index(this);
    let url   = "";
    let type  = "";

    if($('.template').eq(index).data("id") == undefined){
        // 아직 저장되지 않은 템플릿
        type = "POST";
        url = "/admin/push/template";
    }else{
        // 저장되어있는 템플릿 업데이트(저장)
        type = "PUT";
        url = "/admin/push/template/"+$(".template").eq(index).data("id");
    }
    let name = $("input[name='name']").eq(index).val();
    let main_msg = $("input[name='main_msg']").eq(index).val();
    let sub_msg = $("textarea[name='sub_msg']").eq(index).val();

    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type : type,
        data : {"name":name, "main_msg":main_msg, "sub_msg":sub_msg},
        url : url,
        success : function(data){
            alert(data["msg"]);
            if(data["success"]){
                if(type == "POST"){
                    $(".template").eq(index).data("id", data["id"]);
                }
            }
        }
    })
})
</script>
@endsection