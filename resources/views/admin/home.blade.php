@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="dashboard">
        <div class="wrap-tit">
            <h2>대시보드</h2>
        </div>
        <div class="wrap-cont">
            <div class="situation">
                <div class="box">
                    <div class="top">
                        <div class="img orange">
                            <img src="{{asset('images/icon/nav/user.svg')}}" alt="">
                        </div>
                        <div class="txt">
                            <div class="tit">가입한 회원</div>
                            <div class="num">{{$today_member}}</div>
                        </div>
                    </div>
                    <div class="bot">
                        <span>전체 회원</span>
                        <span>{{$total_member}}</span>
                    </div>
                </div>
                <div class="box">
                    <div class="top">
                        <div class="img green">
                            <img src="{{asset('images/icon/icon-feed.svg')}}" alt="">
                        </div>
                        <div class="txt">
                            <div class="tit">작성된 피드</div>
                            <div class="num">{{$today_feed}}</div>
                        </div>
                    </div>
                    <div class="bot">
                        <span>전체 피드</span>
                        <span>{{$total_feed}}</span>
                    </div>
                </div>
                <div class="box">
                    <div class="top">
                        <div class="img blue">
                            <img src="{{asset('images/icon/icon-ask.svg')}}" alt="">
                        </div>
                        <div class="txt">
                            <div class="tit">1:1 문의하기</div>
                            <div class="num">{{$today_ask}}</div>
                        </div>
                    </div>
                    <div class="bot">
                        <span>답변 대기중</span>
                        <span>{{$total_ask}}</span>
                    </div>
                </div>
                <div class="box">
                    <div class="top">
                        <div class="img red">
                            <img src="{{asset('images/icon/icon-warning.svg')}}" alt="">
                        </div>
                        <div class="txt">
                            <div class="tit">신고접수</div>
                            <div class="num">{{$today_report}}</div>
                        </div>
                    </div>
                    <div class="bot">
                        <span>전체 신고접수</span>
                        <span>{{$total_report}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection