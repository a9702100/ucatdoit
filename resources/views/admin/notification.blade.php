
<main class="member-wrap">
    <div class="container">
        <div class="wrap-tit">
            <h2>푸시 메세지 전체 전송</h2>
        </div>
        <div>
            <input type="text" id="message" placeholder="전송할 메세지 입력">
        </div>
        <button onclick="send_noti();">전송</button>
    </div>
</main>
<script src="/resources/js/address.js"></script>        
<script>
function send_noti()
 {
    var message = $("#message").val();
    if(confirm("메세지를 전송하시겠습니까?")){
        $.ajax({
            url : "/adm/MessagePush/sendMessage",
            data : {"message": message},
            type : "post",
            dataType : "json",
            success : function(data){
                if(data["success"]){
                    alert(data["msg"]);
                }else{
                    alert(data["msg"]);
                }
            }
        })
    }
 }
</script>
@section('script')
<script>
let APNSToken = "";

function signal(){
    try{
        webkit.messageHandlers.callbackHandler.postMessage("MessageBody");
    }catch(err){
        alert(err);
    }
}
function whoami(){
    iam = "";
    if(navigator.userAgent.match(/iPhone|iPod|iPad|Macintosh/i)){
        iam = "apple";
    }else{
        iam = "ETC";
    }
    alert(iam);
}
function call_log(){
    window.Android.call_log('연동테스트');
}
function getToken(){
    window.Android.getToken();
}
function tokenPocket(token){
    // console.log(token);
    // alert(token);
    $.ajax({
        url : "/adm/Home/insertToken",
        data : {"token":token},
        type : "post",
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
        }
    })
}
function send_noti(message){
    $.ajax({
        url : "/adm/Home/send_noti",
        data : {"title": "너만없는 고양이", "body": message},
        type : "post",
        dataType : "json",
        success : function(data){
                alert(data["msg"]);
            if(data["success"]){
                console.log(data["result"]);
            }
        }
    })
}

function testMsg(){
    const a = "6adf3fba8f3ee35d1ccfda3edc254dde6476c20451dec023875173aa7523f1ef";
    $.ajax({
        url : "/adm/Home/apns",
        data : {"APNSToken": a},
        type : "post",
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            if(data["success"]){
                console.log(data["result"]);
            }
        }
    })
}

function tokenTake(t){
    APNSToken = t;
    alert(APNSToken);
}
</script>
@endsection
