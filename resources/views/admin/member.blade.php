@extends('layouts.admin.base')
@section('contents')
<main class="member-wrap">
    <div class="container">
        <div class="wrap-tit">
            <h2>회원 리스트</h2>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong>{{$members->total()}}</strong> 건</p>
                </div>
                <div class="right">
                    <div class="search">
                        <label>상태</label>
                        <select id="state">
                            <option value="all" {{$state == "all" ? "selected" : ""}}>전체</option>
                            <option value="normal" {{$state == "normal" ? "selected" : ""}}>정상</option>
                            <option value="delete" {{$state == "delete" ? "selected" : ""}}>탈퇴</option>
                            <option value="black" {{$state == "black" ? "selected" : ""}}>블랙</option>
                        </select>
                        <input type="text" name="" id="search_word" placeholder="닉네임을 입력하세요">
                        <a class="xi-search xi-x" onclick="search_word();"></a>
                    </div>
                    <a class="button" href="{{url('/admin/member/excel')}}">Excel 저장</a>                     
                </div>
            </div>   

            <div class="table-wrap">
                <table>
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="25%">
                        <col width="25%">
                        <col width="20%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>고유번호</th>
                            <th>닉네임[아이디/sns타입]</th>
                            <th>휴대폰번호</th>
                            <th>가입일</th>
                            <th>상태</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($members->count() < 1)
                            <tr>
                                <td colspan="5">선택된 상태의 회원이 없거나 검색결과가 없습니다</td>
                            </tr>
                        @else 
                        @php
                            $num = $members->firstItem();    
                            @endphp
                        @foreach($members as $member)
                        <tr>
                            <td>{{$num++}}</td>
                            <td>{{$member->id}}</td>
                            <td><a onclick="detail('{{$member->id}}');">{{$member->nick}} ({{$member->userId != "" ? $member->userId : $member->sns_type}})</a></td>
                            <td>{{$member->phone != "" ? $member->phone : "등록된 번호 없음"}}</td>
                            <td>{{date_format($member->created_at, 'Y-m-d H:i:s')}}</td>
                            @if($member->state == "normal")
                            <td>정상</td>
                            @elseif($member->state == "delete")
                            <td>탈퇴</td>
                            @elseif($member->state == "black")
                            <td>블랙</td>
                            @endif
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div> 
            {{$members->withQueryString()->links()}}
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
$(document).on("change", "#state", function(){
    const state = $("#state option:selected").val();
    location.href = "/admin/member?state="+state;
})

function detail(member){
    url = "/admin/member/"+member;
    window.open(url, "_blank", "height=600, width=500, left=50px, top=50px, resizable=no", false);
}

function search_word(){
    const state = $("#state option:selected").val();
    const word = $("#search_word").val();

    location.href = "/admin/member?state="+state+"&word="+word;    
}

$(document).on("keydown", "#search_word", function(e){
    if (e.originalEvent.code == "Enter"){
        search_word();
    };
})
</script>      
@endsection