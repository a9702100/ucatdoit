@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>문의내역 목록</h2>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong>{{$asks->total()}}</strong>건</p>
                </div>
                <div class="right">
                    <div class="search">
                        <label>문의 유형</label>
                        <select id="type">
                            <option value="전체" {{$type == "" ? "selected" : ""}}>전체</option>
                            <option value="단순 문의" {{$type == "단순 문의" ? "selected" : ""}}>단순 문의</option>
                            <option value="광고 문의" {{$type == "광고 문의" ? "selected" : ""}}>광고 문의</option>
                        </select>
                        <label>처리상태</label>
                        <select id="process">
                            <option value="전체" {{$process == "" ? "selected" : ""}}>전체</option>
                            <option value="접수" {{$process == "접수" ? "selected" : ""}}>접수</option>
                            <option value="답변 완료" {{$process == "답변 완료" ? "selected" : ""}}>답변 완료</option>
                        </select>
                    </div>
                </div>
                
            </div>   
            <!-- //검색창 -->
            <div>
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col width="10%">
                            <col width="10%">
                            <col width="30%">
                            <col width="25%">
                            <col width="15%">
                            <col width="10%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>문의유형</th>
                                <th>작성자(아이디)</th>
                                <th>접수일</th>
                                <th>처리상태</th>
                                <th>자세히보기</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($asks->count() < 1)
                                <tr>
                                    <td colspan="5">접수된 문의내역이 없습니다</td>
                                </tr>
                            @else 
                            @php
                                $num = $asks->firstItem();    
                                @endphp
                            @foreach($asks as $ask)
                            <tr>
                                <td>{{$num++}}</td>
                                <td>{{$ask->type}}</td>
                                <td>{{$ask->member->nick}} ({{$ask->member->userId != "" ? $ask->member->userId : $ask->member->sns_type}})</td>
                                <td>{{date_format($ask->created_at, 'Y-m-d H:i')}}</td>
                                <td>{{$ask->process}}</td>
                                <td><a class="xi-angle-right" href="{{url('/admin/ask/'.$ask->id)}}"></a></td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                {{$asks->links()}}
            </div> 
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
$(document).on("change","#type, #process", function(){
    const type = $("#type option:selected").val();
    const process = $("#process option:selected").val();

    location.href = "/admin/ask?type="+type+"&process="+process;
});
</script>
@endsection