@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>피드 리스트</h2>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong>{{$feeds->total()}}</strong>건</p>
                </div>
                <div class="right">
                    <div class="search">
                        <select id="type">
                            <option value="nick" {{$sort == "" || $sort == "nick" ? "selected" : ""}}>닉네임</option>
                            <option value="userId" {{$sort == "userId" ? "selected" : ""}}>아이디</option>
                            <option value="content" {{$sort == "content" ? "selected" : ""}}>내용</option>
                        </select>
                        <input type="text" id="keyword" placeholder="검색 단어를 입력하세요" value="{{$keyword}}">
                    </div>
                    <a class="button" onclick="search_sort();">검색</a>
                    <a class="button" onclick="checkDel();">삭제</a>
                </div>
            </div>   
            <!-- //추가 -->
            <div class="table-wrap">
                <table>
                    <colgroup>
                        <col width="5%">
                        <col width="10%">
                        <col width="35%">
                        <col width="30%">
                        <col width="20%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkHead"></th>
                            <th>번호</th>
                            <th>내용</th>
                            <th>작성자(아이디)</th>
                            <th>작성일</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($feeds->count() < 1)
                            <tr> 
                                <td colspan="5">작성된 게시물이 없거나 검색결과가 없습니다</td>
                            </tr>
                        @else 
                        @php
                            $num = $feeds->firstItem();    
                            @endphp
                        @foreach($feeds as $feed)
                        <tr>
                            <td><input type="checkbox" name="check" data-id="{{$feed->id}}"></td>
                            <td>{{$num++}}</td>
                            <td><a onclick="detail('{{$feed->id}}');">{{$feed->content}}</a></td>
                            <td>{{$feed->feedable->nick}} ({{$feed->feedable->userId != "" ? $feed->feedable->userId : $feed->feedable->sns_type}})</td>
                            <td>{{date_format($feed->created_at, 'Y-m-d H:i:s')}}</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div> 
            {{$feeds->links()}}
        </div>
    </div>
</main>
<div class="loading hide">
    <div class="msg">
        <span>삭제 진행중입니다.</span>
        <span>잠시만 기다려주십시오</span>
    </div>
    <img src="{{asset('images/icon/icon-loading.gif')}}" alt="">
</div>
@endsection
@section('script')
<script>
// 전체 선택 / 선택 해제
$("#checkHead").on("click",function(){
    if($("#checkHead").prop("checked")){ 
        $("input[name=check]").prop("checked",true);
    }else{
        $("input[name=check]").prop("checked",false);
    }
 });
 // 선택된 개수 비교해서 전체 선택 체크박스 상태 바꾸기
var checkLength = $("input[name=check]").length; // 전체 체크박스 개수
 $("input[name=check]").on("click",function(){
    var checkChecked = $("input[name=check]:checked").length;
    if(checkLength == checkChecked){
        $("#checkHead").prop("checked", true);
    }else{
        $("#checkHead").prop("checked", false);
    }
 });

//  체크된 상태에서 삭제
function checkDel(){
    var checkNum = [];
    $("input[name=check]:checked").each(function(){
        checkNum.push($(this).data("id"));
    });

    if(checkNum.length == 0){
        alert("삭제할 피드를 선택하세요");
    }else{
        if(confirm(checkNum.length+" 건의 항목을 선택했습니다.\n정말 삭제하시겠습니까?")){
            $.ajax({
                headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
                url : "/admin/feed",
                type : "delete",
                data : {"checkNum":checkNum},
                dataType : "json",
                success : function(data){
                   if(data["success"]) {
                       alert(data["msg"]);
                       location.reload();
                   }
                }
            });
        }
    }
}

function detail(feed){
    url = "/admin/feed/"+feed;
    window.open(url, "_blank", "height=600, width=500, left=50px, top=50px, resizable=no", false);
}

$(document).ajaxStart(function(){
    $(".loading").removeClass("hide");
})

$(document).ajaxStop(function(){
    $(".loading").addClass('hide');
})

function search_sort(){
    const type = $("#type option:selected").val();
    const keyword = $("#keyword").val();

    location.href = "/admin/feed?sort="+type+"&keyword="+keyword;
}
</script>
@endsection