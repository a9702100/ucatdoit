@extends('layouts.admin.base')
@section('contents')
<main class="member-wrap">
    <div class="container">
        <div class="wrap-tit">
            <h2>관리자 리스트</h2>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong>{{$masters->total()}}</strong> 건</p>
                </div>
                <div class="right">
                                    
                </div>
            </div>   

            <div class="table-wrap">
                <table>
                    <colgroup>
                        <col width="10%">
                        <col width="30%">
                        <col width="30%">
                        <col width="20%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>닉네임[아이디/sns타입]</th>
                            <th>휴대폰번호</th>
                            <th>가입일</th>
                            <th>상태</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($masters->count() < 1)
                            <tr>
                                <td colspan="5">관리자로 등록된 회원이 없습니다</td>
                            </tr>
                        @else 
                        @php
                            $num = $masters->firstItem();    
                            @endphp
                        @foreach($masters as $master)
                        <tr>
                            <td>{{$num++}}</td>
                            <td>{{$master->nick}} ({{$master->userId != "" ? $master->userId : $master->sns_type}})</td>
                            <td>{{$master->phone != "" ? $master->phone : "등록된 번호 없음"}}</td>
                            <td>{{date_format($master->created_at, 'Y-m-d H:i:s')}}</td>
                            <td>관리자</td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

                <div class="pagination">
                    {{$masters->links()}}
                </div> 
                <!-- //페이지버튼 -->
            </div> 
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
// 전체 선택 / 선택 해제
$("#checkHead").on("click",function(){
    if($("#checkHead").prop("checked")){ 
        $("input[name=check]").prop("checked",true);
    }else{
        $("input[name=check]").prop("checked",false);
    }
 });
 // 선택된 개수 비교해서 전체 선택 체크박스 상태 바꾸기
var checkLength = $("input[name=check]").length; // 전체 체크박스 개수
 $("input[name=check]").on("click",function(){
    var checkChecked = $("input[name=check]:checked").length;
    if(checkLength == checkChecked){
        $("#checkHead").prop("checked", true);
    }else{
        $("#checkHead").prop("checked", false);
    }
 });

//  체크된 상태에서 삭제
 function checkDel(){
    var checkNum = [];
    $("input[name=check]:checked").each(function(){
        checkNum.push($(this).data("idx"));
    });

    if(checkNum.length == 0){
        alert("선택된 항목이 없습니다.");
    }else{
        if(confirm(checkNum.length+" 건의 항목을 선택했습니다.\n정말 삭제하시겠습니까?")){
            $.ajax({
                url : "/adm/Member/memberDel",
                type : "post",
                data : {"checkNum":checkNum},
                dataType : "json",
                success : function(data){
                    alert(data["msg"]);
                    location.reload();
                }
            });
        }
    }
 }
</script>      
@endsection