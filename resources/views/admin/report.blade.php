@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>신고내역 목록</h2>
            <ul>
                <li><a href="/adm/Home">HOME</a></li>
                <li><strong>신고내역 목록</strong></li>
            </ul>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong>{{$reports->total()}}</strong>건</p>
                </div>
                <div class="right">
                    <div class="search">
                    </div>
                </div>
            </div>   
            <!-- //검색창 -->
            <div>
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col width="10%">
                            <col width="30%">
                            <col width="30%">
                            <col width="10%">
                            <col width="10%">
                            <col width="10%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>번호</th>
                                <th>신고사유</th>
                                <th>작성자(아이디)</th>
                                <th>처리상태</th>
                                <th>타겟</th>
                                <th>바로가기</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($reports->count() < 1)
                                <tr>
                                    <td colspan="5">접수된 신고내역이 없습니다</td>
                                </tr>
                            @else 
                            @php
                                $num = $reports->firstItem();    
                                @endphp
                            @foreach($reports as $report)
                            <tr>
                                <td>{{$num++}}</td>
                                <td>{{$report->reason}}</td>
                                <td>{{$report->member->nick}}({{$report->member->userId != "" ? $report->member->userId : $report->member->sns_type}})</td>
                                <td>
                                    {{$report->process}}
                                </td>
                                <td>{{$report->reportable_type == "feeds" ? "피드" : "댓글"}}</td>
                                <td>
                                    <a onclick="detail('{{$report->id}}');">새창 열기</a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- //공지사항 리스트 -->
                <div class="pagination">
                    {{$reports->links()}}
                </div> 
                <!-- 페이지버튼 -->
            </div> 
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
// 전체 선택 / 선택 해제
$("#checkHead").on("click",function(){
    if($("#checkHead").prop("checked")){ 
        $("input[name=check]").prop("checked",true);
    }else{
        $("input[name=check]").prop("checked",false);
    }
 });
 // 선택된 개수 비교해서 전체 선택 체크박스 상태 바꾸기
var checkLength = $("input[name=check]").length; // 전체 체크박스 개수
 $("input[name=check]").on("click",function(){
    var checkChecked = $("input[name=check]:checked").length;
    if(checkLength == checkChecked){
        $("#checkHead").prop("checked", true);
    }else{
        $("#checkHead").prop("checked", false);
    }
 });

//  체크된 상태에서 삭제
 function checkDel(){
    var checkNum = [];
    $("input[name=check]:checked").each(function(){
        checkNum.push($(this).data("idx"));
    });

    if(checkNum.length == 0){
        alert("선택된 항목이 없습니다.");
    }else{
        if(confirm(checkNum.length+" 건의 항목을 선택했습니다.\n정말 삭제하시겠습니까?")){
            $.ajax({
                url : "/adm/Notice/noticeDel",
                type : "post",
                data : {"checkNum":checkNum},
                dataType : "json",
                success : function(data){
                   if(data["success"]) {
                       alert(data["msg"]);
                       location.reload();
                   }
                }
            });
        }
    }
 }

$("input[name=state]").change(function(){
    const sel = $("input[name=state]:checked").val();
    $.ajax({
        url : "/adm/Notice/appear",
        type : "post",
        data : {"idx":sel},
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            location.reload();
        }
    })
})

function detail(report){
    url = "/admin/report/show/"+report;
    window.open(url, "_blank", "height=600, width=500, left=50px, top=50px, resizable=no",false);
}
</script>
@endsection