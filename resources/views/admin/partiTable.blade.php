@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>[{{$event->title}}] :: 참여목록 </h2>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong>{{$partis->total()}}</strong>건</p>
                </div>
                <div class="right">
                    <div class="search">
                        <label>시작날짜</label>
                        <input type="date" name="" id="start_date">
                        <label>끝날짜</label>
                        <input type="date" name="" id="end_date">
                    </div>
                    <a class="button" onclick="excelSave('{{$event->id}}')">Excel저장</a>
                    <a class="button" href="{{url('/admin/writing?type='.$event->type)}}">이벤트 목록</a>
                </div>
            </div>   
            <!-- //검색창 -->
            <div class="table-wrap">
                <table>
                    <colgroup>
                        <col width="10%">
                        <col width="10%">
                        <col width="15%">
                        <col width="15%">
                        <col width="15%">
                        <col width="15%">
                        <col width="10%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>번호</th>
                            <th>고유번호</th>
                            <th>아이디</th>
                            <th>닉네임</th>
                            <th>이름</th>
                            <th>전화번호</th>
                            <th>컨텐츠 이용동의</th>
                            <th>참여일</th>
                        </tr>
                        @if($partis->count() < 1)
                        <tr>
                            <td colspan="8">이벤트 참여한 회원이 없습니다</td>
                        </tr>
                        @else 
                        @php
                            $num = $partis->firstItem();
                        @endphp
                        @foreach ($partis as $parti)
                            <tr>
                                <td>{{$partis->total() - $num++ +1}}</td>
                                <td>{{$parti->member->id}}</td>
                                <td>{{$parti->member->userId}}</td>
                                <td>{{$parti->member->nick}}</td>
                                <td>{{$parti->name}}</td>
                                <td>{{$parti->phone}}</td>
                                <td>{{$parti->agree == "1" ? "동의" : "거부"}}</td>
                                <td>{{date_format($parti->created_at, 'y.m.d')}}</td>
                            </tr>
                        @endforeach
                        @endif
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div> 
            {{$partis->withQueryString()->links()}}
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
function excelSave(id){
    let start_date = $("#start_date").val();
    let end_date   = $("#end_date").val();

    if(start_date == "" || end_date == ""){
        alert("저장할 날짜를 선택해주세요");
        return false;
    }

    if(start_date > end_date){
        alert("시작날짜가 끝날짜보다 클 수 없습니다.");
        return false;
    }

    location.href = "/admin/writing/excelSave?id="+id+"&start_date="+start_date+"&end_date="+end_date;
}
</script>
@endsection