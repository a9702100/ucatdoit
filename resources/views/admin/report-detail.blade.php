@extends('layouts.admin.detail')
@section('title')
    신고내역 :: 상세보기
@endsection
@section('contents')
<div class="detail-wrap">
    <h3>신고 상세내용</h3>
    <div class="line">
        <div class="tit">신고자</div>
        <div class="content">
            {{$report->member->nick}}({{$report->member->userId != "" ? $report->member->userId : $report->member->sns_type}})
        </div>
    </div>
    <div class="line">
        <div class="tit">신고사유</div>
        <div class="content">
            {{$report->reason}}
        </div>
    </div>
    <div class="br"></div>
    @if(isset($target))
    @if(isset($isFeed) && !$isFeed)
    <div class="line">
        <div class="tit">내용</div>
        <div class="content">
            관리자에 의해 삭제된 피드입니다
        </div>
    </div>
    @elseif(isset($isReply) && !$isReply)
    <div class="line">
        <div class="tit">내용</div>
        <div class="content">
            관리자에 의해 삭제된 댓글입니다
        </div>
    </div>
    @else
    <div class="line">
        <div class="tit">작성자</div>
        <div class="content">
            @if($target == "feeds")
            {{$feed->feedable->nick}}
            @elseif($target == "replies")
            {{$reply->member->nick}}
            @endif
        </div>
    </div>
    <div class="line">
        <div class="tit">내용</div>
        <div class="content">
            @if($target == "feeds")
                <div class="swiper-container">
                    <ul class="swiper-wrapper">
                        @foreach($feed->files as $file)
                        <li class="swiper-slide">
                            @if($file->ext == "mp4")
                            <video src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt=""controls muted loop autoplay></video>
                            @else 
                            <img src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt="">
                            @endif
                        </li>
                        @endforeach
                    </ul>
                    <div class="swiper-pagination"></div>
                </div>
            @elseif($target == "replies")
            {{$reply->content}}
            @endif
        </div>
    </div>
    @endif
    @endif
    <div class="br"></div>
    <div class="line">
        <div class="tit">처리상태</div>
        <div class="content radio-group">
            <input name="process" type="radio" id="register" value="접수" {{$report->process == "접수" ? "checked" : ""}}><label for="register">접수</label>
            <input name="process" type="radio" id="delete" value="삭제" {{$report->process == "삭제" ? "checked" : ""}}><label for="delete">삭제</label>
            <input name="process" type="radio" id="problem" value="문제없음" {{$report->process == "문제없음" ? "checked" : ""}}><label for="problem">문제없음</label>
        </div>
    </div>
    <div id="process" class="line {{$report->process == "접수" ?  "hide" : ""}}">
        <div class="tit">처리결과</div>
        <div class="content">
            <input type="text" id="result" placeholder="내용을 입력하세요" value="{{isset($report) ? $report->result : ""}}" {{$report->process == "접수" ? "" : "readonly"}}>
        </div>
    </div>
    <div class="btn-group"> 
        <a class="cancel" onclick="cancel();">취소</a>
        <a class="confirm" onclick="register();">확인</a>
    </div>
</div>
<div class="loading hide">
    <img src="{{asset('images/icon/icon-loading.gif')}}" alt=''>
</div>
@endsection
@section('script')
    
<script>
function cancel(){
    if(confirm("입력하신 내용은 삭제됩니다\n현재 창을 닫으시겠습니까?")){
        window.close();
    }
}
function register(){
    let process  = $("input[name=process]:checked").val();
    let result   = "";
    if(process != "접수"){
        result = $("#result").val();
    }

    $.ajax({
        headers: {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
        url : "/admin/report/update/{{$report->id}}",
        type : "post",
        data : {"process":process, "result":result},
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            if(data["success"]){
                window.opener.location.reload();
                window.close();
            }
        }
    })
}

new Swiper($(".swiper-container"),{
    autoHeight:true,
    pagination:{
        el:$(".swiper-pagination")
    }
});

$(document).on("click","input[name=process]",function(){
    const process = $("input[name=process]:checked").val();

    if(process == "접수"){
        if(!$("#process").hasClass("hide")){
            $("#process").addClass("hide");
        }
    }else{
        // 삭제 또는 문제없음
        if($('#process').hasClass("hide")){
            $("#process").removeClass("hide");
        }
    }
});

$(document).ajaxStart(function(){
    $(".loading").removeClass('hide');
})
$(document).ajaxStop(function(){
    $('.loading').addClass("hide");
})
</script>
@endsection