@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container event-container">
        <div class="wrap-tit">
            @if(isset($writing))
            <h2>이벤트/공지사항 수정</h2>
            @else 
            <h2>이벤트/공지사항 등록</h2>
            @endif
        </div>
        <div class="line">
            <div class="tit">글 타입</div>
            <div class="content">
                <select id="type">
                    @if(isset($writing))
                    <option value="notice" {{$writing->type == "notice" ? "selected" : ""}}>공지사항</option>
                    <option value="event" {{$writing->type == "event" ? "selected" : ""}}>이벤트</option>
                    <option value="experience" {{$writing->type == "experience" ? "selected" : ""}}>체험단</option>
                    <option value="affiliate" {{$writing->type == "affiliate" ? "selected" : ""}}>제휴할인</option>
                    @else 
                    <option value="notice" {{$type == "notice" ? "selected" : ""}}>공지사항</option>
                    <option value="event" {{$type == "event" ? "selected" : ""}}>이벤트</option>
                    <option value="experience" {{$type == "experience" ? "selected" : ""}}>체험단</option>
                    <option value="affiliate" {{$type == "affiliate" ? "selected" : ""}}>제휴할인</option>
                    @endif
                </select>
            </div>
        </div>
        
        <div class="line">
            <div class="tit">제목</div>
            <div class="content">
                <input type="text" id="title" placeholder="제목을 입력하세요" value="{{isset($writing) ? $writing->title : ""}}">
            </div>
        </div>
        <div class="line">
            <div class="tit">광고 시작날짜</div>
            <div class="content">
                <input id="start_date" type="text" class="start_date" placeholder="광고 시작날짜" value="{{isset($writing) ? date_format(new DateTime($writing->start_date), 'Y-m-d H:i') : ''}}">
            </div>
        </div>
        <div class="line">
            <div class="tit">광고 마감날짜</div>
            <div class="content">
                <input id="end_date" type="text" class="end_date" placeholder="광고 마감날짜" value="{{isset($writing) ? date_format(new DateTime($writing->end_date), 'Y-m-d H:i') : ''}}">
            </div>
        </div>
        <div class="line">
            <div class="tit">양식 추가</div>
            <div class="content radio-group">
                <input id="no" name="form" type="radio" value="no" {{(isset($writing) && $writing->form == "no") || !isset($writing)  ? "checked" : ""}}><label for="no">추가 안함</label>
                <input id="privacy" name="form" type="radio" value="privacy" {{isset($writing) && $writing->form == "privacy" ? "checked" : ""}}><label for="privacy">개인정보수집</label>
                <input id="f-contents" name="form" type="radio" value="contents" {{isset($writing) && $writing->form == "contents" ? "checked" : ""}}><label for="f-contents">개인정보수집+컨텐츠이용동의</label>
                <input id="link" name="form" type="radio" value="link" {{isset($writing) && $writing->form == "link" ? "checked" : ""}}><label for="link">링크연결</label>
            </div>
        </div>
        <div id="link_conn" class="line {{isset($writing) && $writing->form == "link" ? "" : "hide"}}">
            <div class="tit">링크 연결</div>
            <div class="content">
                <input id="form_link" type="text"  placeholder="페이지 주소를 입력하세요" value="{{isset($writing) ? $writing->form_link : ""}}">
            </div>
        </div>
        <div class="line">
            <div class="tit">이미지</div>
            <div class="content">
                <div class="img-list">
                    <div class="event-img">
                        <h3>팝업 이미지[768*900]</h3>
                        @if(isset($writing) && $writing->files->where('reference','advertise')->isNotEmpty())
                        <div class="img-wrap">
                            <img class="" id="advertise_img" src="{{asset('storage/uploads/writings/'.$writing->files->where('reference','advertise')->first()->fn)}}">
                        </div>
                        <div class="txt-wrap">
                            <label for="advertise">파일선택</label><input type="text" id="advertise_fn" placeholder="파일을 선택하세요" value="{{$writing->files->where('reference','advertise')->first()->fn_ori}}" readonly>
                            <input id="advertise" type="file" style="display:none" accept="image/*">
                        </div>
                        @else 
                        <div class="img-wrap">
                            <img class="no-img" id="advertise_img" src="{{asset('admin/images/icon/no-img.png')}}">
                        </div>
                        <div class="txt-wrap">
                            <label for="advertise">파일선택</label><input type="text" id="advertise_fn" placeholder="파일을 선택하세요" value="" readonly>
                            <input id="advertise" type="file" style="display:none" accept="image/*">
                        </div>
                        @endif
                    </div>
                    <div class="event-img">
                        <h3>이벤트/공지사항 배너[768*325]</h3>
                        @if(isset($writing) && $writing->files->where('reference','banner')->isNotEmpty())
                        <div class="img-wrap">
                            <img class="" id="banner_img" src="{{asset('storage/uploads/writings/'.$writing->files->where('reference','banner')->first()->fn)}}">
                        </div>
                        <div class="txt-wrap">
                            <label for="banner">파일선택</label><input type="text" id="banner_fn" placeholder="파일을 선택하세요" value="{{$writing->files->where('reference','banner')->first()->fn_ori}}" readonly>
                            <input id="banner" type="file" style="display:none" accept="image/*">
                        </div>
                        @else 
                        <div class="img-wrap">
                            <img class="no-img" id="banner_img" src="{{asset('admin/images/icon/no-img.png')}}">
                        </div>
                        <div class="txt-wrap">
                            <label for="banner">파일선택</label><input type="text" id="banner_fn" placeholder="파일을 선택하세요" value="" readonly>
                            <input id="banner" type="file" style="display:none" accept="image/*">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="textarea">
            <textarea name="" id="editor" cols="30" rows="10" placeholder="내용을 입력하세요">{{isset($writing) ? $writing->content : ""}}</textarea>
        </div>
        <div class="btn-group"> 
            <a class="cancel" onclick="cancel();">취소</a>
            @if(isset($writing))
            <a class="confirm" onclick="update('{{$writing->id}}');">수정</a>
            @else 
            <a class="confirm" onclick="register();">확인</a>
            @endif
        </div>
    </div>
</main>
<div class="uploading hide">
    <img src="{{asset('images/icon/icon-loading.gif')}}" alt="">
</div>
@endsection
@section('style')
<link rel="stylesheet" href="{{asset('admin/css/datepicker.min.css')}}"/>
<link rel="stylesheet" href="{{asset('css/font.css')}}"/>
@endsection
@section('script')
<script src="{{asset('admin/js/datepicker.js')}}"></script>
<script src="{{asset('admin/js/datepicker.ko.js')}}"></script>
<script src="{{asset('admin/js/tinyMCE.js')}}"></script>
<script>
 function tinymceUploadImg(blobInfo, success, failure, progress) {
	let formData = new FormData();
	formData.append('imageFile', blobInfo.blob(), blobInfo.filename());
	formData.append("file_type","writings");
 
	$.ajax({
		headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
		url : "/admin/upload",
		type : "post",
		processData : false,
		contentType : false, 
		data : formData,
		dataType : "json",
		success : function(data){
			return success('/storage/uploads/writings/'+data["fn"]);
		},error : function(){
            alert("관리자에게 문의해주세요");
        }
	})
 }
</script>
<script src="{{asset('admin/js/editor.js')}}"></script>
<script>
$(".start_date").datepicker({
    language: 'ko',
    timepicker: true,
    autoClose: true,
    onSelect: function(date){
        var endNum = date;
        $(".end_date").datepicker({
            minDate: new Date(endNum)
        });
    }
}).data("datepicker");
$(".end_date").datepicker({
    language: 'ko',
    timepicker: true,
    autoClose: true,
    onSelect: function(date){
        var startNum = date;
        $(".start_date").datepicker({
            maxDate: new Date(startNum)
        });
    }
}).data("datepicker");

function cancel(){
    if(confirm("현재 작성중인 내용은 삭제됩니다\n이전 페이지로 이동하시겠습니까?")){
        location.href = "/admin/writing?type="+$("#type option:selected").val();
    }
}

function register(){
    const formData = new FormData();
    formData.append("type", $("#type option:selected").val());
    formData.append("title", $('#title').val());
    formData.append("start_date", $("#start_date").val());
    formData.append("end_date", $("#end_date").val());
    formData.append("form", $("input[name=form]:checked").val());
    if($("input[name='form']:checked").val() == "link"){
        formData.append("form_link", $('#form_link').val());
    }
    formData.append("advertise_fn", $("#advertise_fn").val());
    formData.append("banner_fn", $("#banner_fn").val());
    formData.append("advertise_file", $("#advertise")[0].files[0]);
    formData.append("banner_file", $("#banner")[0].files[0]);
    formData.append("content", tinymce.get('editor').getContent());

    const img_arr = tinymce.activeEditor.dom.select('img');
    if(img_arr.length > 0){
        let save_list = [];
        img_arr.forEach(element => {
            const img_path = tinymce.activeEditor.dom.getAttrib(element, "src");
            const arr = img_path.split("/");
    
            save_list.push(arr[4]);
        })
        formData.append("save_list[]", save_list);
    }

    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/admin/writing",
        type : "post",
        processData : false,
        contentType : false,
        data : formData,
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            if(data["success"]){
                location.href = "/admin/writing?type="+$("#type option:selected").val();
            }
        }
    })
}

function update(id){
    const formData = new FormData();
    formData.append("type", $("#type option:selected").val());
    formData.append("title", $('#title').val());
    formData.append("start_date", $("#start_date").val());
    formData.append("end_date", $("#end_date").val());
    formData.append("form", $("input[name=form]:checked").val());
    if($("input[name='form']:checked").val() == "link"){
        formData.append("form_link", $('#form_link').val());
    }
    formData.append("advertise_fn", $("#advertise_fn").val());
    formData.append("banner_fn", $("#banner_fn").val());
    formData.append("advertise_file", $("#advertise")[0].files[0]);
    formData.append("banner_file", $("#banner")[0].files[0]);
    formData.append("content", tinymce.get('editor').getContent());

    const img_arr = tinymce.activeEditor.dom.select('img');
    if(img_arr.length > 0){
        let save_list = [];
        img_arr.forEach(element => {
            const img_path = tinymce.activeEditor.dom.getAttrib(element, "src");
            const arr = img_path.split("/");
    
            save_list.push(arr[4]);
        })
        formData.append("save_list[]", save_list);
    }

    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/admin/writing/update/"+id,
        type : "post",
        processData : false,
        contentType : false,
        data : formData,
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            if(data["success"]){
                location.href = "/admin/writing?type="+$("#type option:selected").val();
            }
        }
    }) 
}

function readImage(input, id) {
    // 인풋 태그에 파일이 있는 경우
    if(input.files && input.files[0]) {
        // FileReader 인스턴스 생성
        const reader = new FileReader()

        // 이미지가 로드가 된 경우
        reader.onload = e => {

            $("#"+id+"_img").removeClass("no-img").attr("src",e.target.result);
            $("#"+id+"_fn").val(input.files[0].name);
            
        }

        // reader가 이미지 읽도록 하기
        reader.readAsDataURL(input.files[0])
    }
}

$(document).on("change","#advertise, #banner", function(){
    let target_id = $(this).attr("id");

    if($(this)[0].files.length == 0){
        // 파일 선택 취소 누름 :: 내용 초기화
        $("#"+target_id+"_img").addClass("no-img").attr("src","{{asset('admin/images/icon/no-img.png')}}");
        $("#"+target_id+"_fn").val("");
    }else{
        // 파일 있음
        readImage($(this)[0], target_id);
    }

})

// 폼추가 선택 
$(document).on("click", "input[name='form']",function(){
    const form = $("input[name='form']:checked").val();
    
    if(form == "link"){
        $("#link_conn").removeClass("hide");
    }else{
        $("#link_conn").addClass("hide");
    }

})

$(document).ajaxStart(function(){
    $(".uploading").removeClass("hide");
});
$(document).ajaxStop(function(){
    $(".uploading").addClass("hide");
});
</script>
@endsection