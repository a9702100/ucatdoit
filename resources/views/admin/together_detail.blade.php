@extends('layouts.admin.base')
@section('contents')

<main class="member-wrap">
    <div class="container">
        <div class="wrap-tit">
            <h2>투게더 상세페이지</h2>
        </div>
        <div class="wrap-cont">
            <div class="btn-group">
                <button class="btn-delete" onclick="">삭제</button>
            </div>                    
            <div class="wrap-cont-box">
                <div class="enquiry-detail-top">
                    <ul>
                        <li>
                            <strong>지역</strong>
                            <span></span>
                        </li>
                        <li>
                            <strong>투게더명</strong>
                            <span></span>
                        </li>
                        <li>
                            <strong>관리자</strong>
                            <span></span>
                        </li>
                        <li>
                            <strong>가입 회원수</strong>
                            <span>명</span>
                        </li>
                        <li>
                            <strong>최대 회원수</strong>
                            <span>명</span>
                        </li>
                        <li>
                            <strong>게시글 수</strong>
                            <span>개</span>
                        </li>
                    </ul>
                    <!-- 문의사항 -->
                </div>
                <a class="registration" href="/adm/Together">목록</a>
            </div>
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
var checkNum = [];
function btnDel(together_idx){
    checkNum.push(together_idx);
    if(confirm("모든 정보가 삭제됩니다\n정말 삭제하시겠습니까?")){
        $.ajax({
            url : "/adm/Together/togetherDel",
            type : "post",
            data : {"checkNum":checkNum},
            dataType : "json",
            success : function(data){
                if(data["success"]) {
                    alert(data["msg"]);
                    location.href="/adm/Together";
                }
            }
        });
    }
}
</script>      
@endsection