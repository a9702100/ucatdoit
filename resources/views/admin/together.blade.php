@extends('layouts.admin.base')
@section('contents')
<main class="member-wrap">
    <div class="container">
        <div class="wrap-tit">
            <h2>투게더 목록</h2>
            <ul>
                <li>HOME</li>
                <li><strong>투게더 관리</strong></li>
            </ul>
        </div>
        <div class="wrap-cont">
            <div class="cont-top">
                <div class="left">
                    <p>총 <strong></strong>건</p>
                </div>
                <div class="right">
                    <div class="search">
                        <label>시/도 선택</label>
                        <select id="addr1" name="sido">

                        </select>
                        <label>구/군 선택</label>
                        <select id="addr2" name="gugun">
                            
                        </select>
                    </div>
                    <a href="" class="button">삭제</a>
                </div>
            </div>   
            <div class="cont-wrap">   
                <div class="table-wrap">
                    <table>
                        <colgroup>
                            <col width="5%">
                            <col width="5%">
                            <col width="25%">
                            <col width="15%">
                            <col width="15%">
                            <col width="15%">
                            <col width="10%">
                            <col width="10%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="checkHead" name="check"></th>
                                <th>번호</th>
                                <th>투게더명</th>
                                <th>지역[시/도]</th>
                                <th>지역[구/군]</th>
                                <th>관리자</th>
                                <th>회원수(명)</th>
                                <th>생성일</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $num = $togethers->firstItem();
                            @endphp
                            @foreach ($togethers as $together)
                            <tr>
                                <td><input type="checkbox" name="check"></td>
                                <td>{{$num++}}</td>
                                <td>{{$together->title}}</td>
                                <td>{{$together->addr1}}</td>
                                <td>{{$together->addr2}}</td>
                                <td>{{$together->member->nick}}</td>
                                <td>{{$together->member_cnt}}</td>
                                <td>{{date_format($together->created_at, 'Y-m-d')}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>          
            </div> 
        </div>
    </div>
</main>
@endsection
@section('script')
<script src="{{asset('admin/js/address.js')}}"></script>        
<script>
// 전체 선택 / 선택 해제
 $("#checkHead").on("click",function(){
    if($("#checkHead").prop("checked")){ 
        $("input[name=check]").prop("checked",true);
    }else{
        $("input[name=check]").prop("checked",false);
    }
 });
 // 선택된 개수 비교해서 전체 선택 체크박스 상태 바꾸기
var checkLength = $("input[name=check]").length; // 전체 체크박스 개수
 $("input[name=check]").on("click",function(){
    var checkChecked = $("input[name=check]:checked").length;
    if(checkLength == checkChecked){
        $("#checkHead").prop("checked", true);
    }else{
        $("#checkHead").prop("checked", false);
    }
 });

//  체크된 상태에서 삭제
 function checkDel(){
    var checkNum = [];
    $("input[name=check]:checked").each(function(){
        checkNum.push($(this).data("idx"));
    });

    if(checkNum.length == 0){
        alert("선택된 항목이 없습니다.");
    }else{
        if(confirm(checkNum.length+" 건의 항목을 선택했습니다.\n정말 삭제하시겠습니까?")){
            $.ajax({
                url : "/adm/Together/togetherDel",
                type : "post",
                data : {"checkNum":checkNum},
                dataType : "json",
                success : function(data){
                   if(data["success"]) {
                       alert(data["msg"]);
                       location.reload();
                   }
                }
            });
        }
    }
 }

 $(document).on("change","")
</script>
@endsection