@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>푸시 메세지 전송</h2>
        </div>
        <div class="wrap-cont">
            <fieldset class="target">
                <legend>타겟 설정</legend>
                <div class="group">
                    <label for="all"><input id="all" name="target" type="radio" value="all" {{isset($_GET["page"]) ? "" : "checked"}}>회원 전체</label>
                    <label for="sel"><input id="sel" name="target" type="radio" value="sel" {{isset($_GET["page"]) ? "checked" : ""}}>회원 선택</label>
                </div>
                <div class="member-list">
                    <div id="search" class="table-box-wrap">
                        <div style="display: flex; position: absolute; right:0; top:0;">
                            <select name="sort" style="width: 90px">
                                <option value="nick">닉네임</option>
                                <option value="id">고유번호</option>
                            </select>
                            <input type="text" id="sort_word" style="width: calc(100% - 90px); padding-right: 25px;">
                            <a class="xi-search" style="position: absolute; right: 5px; top: 50%; transform: translateY(-50%); cursor: pointer" onclick="getTokens();"></a>
                        </div>
                        <h3>회원 검색 결과 [<span id="result_cnt">0</span>건]</h3>
                        <div class="table-box">
                            <table>
                                <colgroup>
                                    <col width="10%">
                                    <col width="20%">
                                    <col width="40%">
                                    <col width="30%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th style="width: 46.01px"><input type="checkbox" class="selectAll" data-type="result"></td>
                                        <th style="width: 92.04px">고유번호</td>
                                        <th style="width: 184.08px">닉네임</td>
                                        <th style="width: 155.07px">토큰타입</td>
                                    </tr>
                                </thead>
                                <tbody id="result_tokens">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="move-btns">
                        <button class="xi-arrow-right xi-x" onclick="move_right();"></button>
                        <button class="xi-arrow-left xi-x" onclick="move_left();"></button>
                    </div>
                    <div id="selected" class="table-box-wrap">
                        <h3>메세지 받을 회원 목록 [<span id="receive_cnt">0</span>건]</h3>
                        <div class="table-box">
                            <table>
                                <colgroup>
                                    <col width="10%">
                                    <col width="20%">
                                    <col width="40%">
                                    <col width="30%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th style="width: 46.01px"><input type="checkbox" class="selectAll" data-type="receive"></td>
                                        <th style="width: 92.04px">고유번호</td>
                                        <th style="width: 184.08px">닉네임</td>
                                        <th style="width: 155.07px">토큰타입</td>
                                    </tr>
                                </thead>
                                <tbody id="receive_tokens">
                                {{-- @for ($i = 0; $i < 10; $i++)   
                                    <tr>
                                        <td><input type="checkbox" name="" id=""></td>
                                        <td>{{$i}}</td>
                                        <td>빨간새우</td>
                                        <td>Android</td>
                                    </tr>
                                @endfor --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset class="alarm">
                <legend>메세지 입력</legend>
                <div class="common">
                    <p>아래 미리보기는 휴대기기에서 메세지가 어떻게 표시될지 대략적으로 보여줍니다.
                    실제 메세지 렌더링은 기기에 따라 달라집니다. 정확한 결과를 보려면 실제 기기로 확인하세요</p>
                </div>
                <ul class="preview-msg">
                    <li class="left">
                        <ul>
                            <li>
                                <div class="description">Android</div>
                                <div class="android preview">
                                    <div class="header">
                                        <span class="icon"><img src="{{asset('favicon.png')}}" alt=""></span>
                                        <span class="appname">너만없는 고양이</span>
                                        <span class="time">오전 09:00</span>
                                    </div>
                                    <div class="messages">
                                        <div class="title">
                                            <p class="msg-title">너만없는 고양이</p>
                                        </div>
                                        <div class="message">
                                            <p class="msg-text">아무개님이 회원님의 게시글에 댓글을 달았습니다</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="description">iOS</div>
                                <div class="ios preview">
                                    <div class="header">
                                        <div class="group">
                                            <span class="icon"><img src="{{asset('favicon.png')}}" alt=""></span>
                                            <span class="appname">너만없는 고양이</span>
                                        </div>
                                        <span class="time">NOW</span>
                                    </div>
                                    <div class="messages">
                                        <div class="title">
                                            <p class="msg-title">너만없는 고양이</p>
                                        </div>
                                        <div class="message">
                                            <p class="msg-text">아무개님이 회원님의 게시글에 댓글을 달았습니다</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="right">
                        <div class="search">
                            <select id="template">
                                <option value="">직접입력</option>
                                @foreach ($templates as $template)
                                    <option value="{{$template->id}}">{{$template->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="title">✓ 알림 제목 [선택]</div>
                        <div class="contents">
                            <input type="text" id="title" placeholder="알림 제목을 입력하세요">
                        </div>
                        <div class="title">✓ 알림 내용 [필수]</div>
                        <div class="contents">
                            <textarea id="contents" placeholder="알림 내용을 입력하세요" rows="5"></textarea>
                        </div>
                    </li>
                </ul>
            </fieldset>
            <div class="button">
                <button onclick="send_message();">메세지 전송</button>
            </div>
        </div>
    </div>
</main>
@endsection

@section('style')
<style>
.wrap-cont {width: 1100px; }
.wrap-cont .common {padding: 10px; border: 1px solid black; background: bisque; margin-bottom: 25px;}
.wrap-cont .common p {white-space: pre-line; line-height: 1.2; text-align: center; font-weight: 400;}
.wrap-cont .description {padding: 10px; margin: 10px; border: 1px solid black; width: 400px; font-weight: 800}
.wrap-cont .preview {border: 1px solid black; min-height: 100px; padding: 10px; margin: 10px; width: 400px; background: #eee;}
.wrap-cont .preview .header {display: flex; justify-content: flex-start; align-items: flex-end; padding-bottom: 10px}
.wrap-cont .preview .header .icon, .appname, .time {margin-right: 5px; font-size: 12px}
.wrap-cont .preview .messages {}
.wrap-cont .preview .messages .title {padding-bottom: 5px}
.wrap-cont .preview .messages .title>p {font-weight: 600; white-space: nowrap; overflow: hidden; text-overflow: ellipsis}
.wrap-cont .preview .messages .message>p {word-break: break-all}

.wrap-cont .preview-msg {display: flex;}
.wrap-cont .preview-msg .left { display: inline-block;}
.wrap-cont .preview-msg .right { display: inline-block; width: calc(100% - 430px); padding : 10px;}

.wrap-cont .ios {border-radius: 10px;}
.wrap-cont .ios .header {display: flex; justify-content: space-between}
.wrap-cont .ios .header .group {display: flex; align-items: flex-end}


.wrap-cont .alarm {padding: 20px 20px 10px 20px; margin: 25px; border:1px solid black;}
.wrap-cont .alarm legend {background: tomato; color: white; padding: 10px;}
.wrap-cont .alarm .title {margin-bottom: 5px;}
.wrap-cont .alarm .contents{margin-bottom: 10px;}
.wrap-cont .alarm .contents input[type="text"] {border-radius: 5px; padding: 10px; font-size: 16px;}
.wrap-cont .alarm .contents input[type="text"]:focus { outline: auto}
.wrap-cont .alarm .contents textarea {padding: 10px; font-size: 16px; border-radius: 5px; font-family: auto;}
.wrap-cont .alarm .search {margin-bottom: 20px;}
.wrap-cont .alarm .search select {width: 100%;}

.wrap-cont .target {padding: 20px; margin: 25px; border: 1px solid black;}
.wrap-cont .target legend {background: tomato; color: white; padding: 10px;}
.wrap-cont .target .group {display: flex;}
.wrap-cont .target .group input[type="radio"] {appearance: auto; width: auto;}
.wrap-cont .target .group label {margin-right: 20px; padding: 0 10px;}
.wrap-cont .target .member-list {width: 100%; height: 300px; display: flex; margin-top: 10px;}
.wrap-cont .target .member-list .table-box-wrap {width: calc((100% - 50px) / 2); position: relative; border: 1px solid #ccc;}
.wrap-cont .target .member-list .table-box-wrap h3 {padding: 10px}
.wrap-cont .target .member-list .table-box-wrap .table-box {max-height: 213px; overflow: auto; overflow-x:hidden; margin-top: 48px}
.wrap-cont .target .member-list .table-box-wrap .table-box table {width: 100%; min-width: 100%; table-layout: fixed; border-spacing: 0; border-collapse: collapse;}
.wrap-cont .target .member-list .table-box-wrap .table-box table thead tr {position: absolute; top: 36px;}
.wrap-cont .target .member-list .table-box-wrap .table-box table thead tr th {font-weight: normal; width: 120px; height: 40px; background: darksalmon; color: #fff;}
.wrap-cont .target .member-list .table-box-wrap .table-box table td { text-align: center; height: 40px; border-top: 1px solid #ccc;}
.wrap-cont .target .member-list .table-box-wrap .table-box table tr {display: inline-block; width: 100%; table-layout: fixed;}
.wrap-cont .target .member-list .table-box-wrap .table-box table tbody tr {display: table-row;}

.wrap-cont .target .member-list .move-btns {width: 50px; display: flex; flex-direction: column; justify-content: center; align-content: center; padding: 0 5px;}
.wrap-cont .target .member-list .move-btns button {min-width: fit-content; padding: 5px; margin-bottom: 5px; background: cornsilk}
.wrap-cont .target .member-list .move-btns button:hover {background: rgb(216, 210, 186);}

.wrap-cont .button {text-align: center; margin-bottom: 10px;}
.wrap-cont .button button:hover {background: tomato; color: white}
</style>
@endsection

@section('script')
<script>
let except = [];

$(document).ready(function(){
    if($("[name=target]:checked").val() == "all"){
        $(".member-list").hide();
    }
})    
$(document).on("input", "#title, #contents", function(){
    const id    = $(this).attr("id");
    const value = $(this).val();

    if(id == "title"){
        if(value == ""){
            $('.msg-title').html("너만없는 고양이");
        }else{
            $('.msg-title').html(value);
        }
    }else if(id == "contents"){
        if(value == ""){
            $('.msg-text').html('알림 내용이 들어갈 자리입니다');
        }else{
            $('.msg-text').html(value);
        }
    }
})

// 체크박스 관련
$(document).on("click",".selectAll", function(){
    let type = $(this).data('type');
    let checked = $(this).prop("checked");
    $("[name='"+type+"']").each(function(index, el){
        $("[name='"+type+"']").eq(index).prop("checked", checked);
    })
})

$(document).on("click", "[name='result'], [name='receive']", function(){
    let name = $(this).attr("name");
    let index = name == "result" ? 0 : 1;

    if($(".selectAll").eq(index).prop("checked")){
        // 전체 체크에 체크 되어 있을때 
        if($("[name='"+name+"']").length != $("[name='"+name+"']:checked").length){
            $(".selectAll").eq(index).prop("checked", false);
        }
    }else{
        // 전체 체크에 체크 해제 되어있을 때
        if($("[name='"+name+"']").length == $("[name='"+name+"']:checked").length){
            $(".selectAll").eq(index).prop("checked", true);
        }
    }
});

// 결과 -> 회원 목록으로 이동
function move_right(){
    let result_cnt  = Number($("#result_cnt").text());
    let receive_cnt = Number($("#receive_cnt").text());

    if($("[name='result']:checked").length < 1){
        alert("오른쪽으로 넘길 회원을 선택하세요");
        return false;
    }

    for(let i = 0; i < $("[name='result']:checked").length; i++){
        let id    = $("[name='result']:checked").eq(i).val();
        let nick  = $("[name='result']:checked").eq(i).data("nick");
        let token = $("[name='result']:checked").eq(i).data("token");

        let str = "<tr>\
                    <td><input type=\"checkbox\" name=\"receive\" value=\""+id+"\" data-type=\""+token+"\"></td>\
                    <td>"+id+"</td>\
                    <td>"+nick+"</td>\
                    <td>"+token+"</td>\
                </tr>";
        $("#receive_tokens").append(str);
        except.push(id);
    }

    $("#result_cnt").html(result_cnt-$("[name='result']:checked").length);
    $("#receive_cnt").html(receive_cnt+$("[name='result']:checked").length);
    $(".selectAll").eq(0).prop("checked", false);
    $("[name='result']:checked").parent().parent().remove();
}    

// 회원 목록 -> 결과로 이동
function move_left(){
    let receive_cnt = Number($("#receive_cnt").text());

    if($("[name='receive']:checked").length < 1){
        alert("삭제할 회원을 선택하세요");
        return false;
    }

    for(let i = 0; i < $("[name='receive']:checked").length; i++){
        let index = $("[name='receive']").index($("[name='receive']:checked").eq(i));

        except.splice(i, 1);
    }

    $("#receive_cnt").html(receive_cnt-$("[name='receive']:checked").length);
    $(".selectAll").eq(1).prop("checked", false);
    $("[name='receive']:checked").parent().parent().remove();
    console.log(except);
}

$(document).on("change", "[name=target]", function(){
    if($(this).val() == "all"){
        // 라디오 버튼 :: [회원전체] 선택
        $(".member-list").hide();
    }else if($(this).val() == "sel"){
        // 라디오 버튼 :: [회원선택] 선택
        $(".member-list").show();
    }
});

function send_message(){
    const title    = $('#title').val();
    const contents = $("#contents").val();
    const target   = $("[name='target']:checked").val();
    const aos_arr  = [];
    const ios_arr  = [];
    

    if(contents == ""){
        alert("알림 내용을 입력해주세요");
        return false;
    }

    if(target == "sel"){
        // 선택한 회원 발송
        if($("[name='receive']").length < 1){
            alert("메세지 받을 회원 목록이 비어있습니다.");
            return false;
        }

        for(var i = 0; i < $("[name='receive']").length; i++){
            const id   = $("[name='receive']").eq(i).val();   // 유저 아이디
            const type = $("[name='receive']").eq(i).data("type"); // 토큰 타입

            if(type == "Android"){
                // 토큰 타입이 안드로이드
                aos_arr.push(id);
            }else if(type == "Apple"){
                // 토큰 타입이 애플
                ios_arr.push(id);
            }
        }

        if(confirm("선택한 회원에게 전송하시겠습니까?")){
            $.ajax({
                headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                url : "/admin/push/send",
                type : "post",
                data : {"title":title, "message":contents, "target":target, "aos_arr":aos_arr, "ios_arr":ios_arr},
                dataType : "json",
                success : function(data){
                    if(data["success"]){
                        alert(data["msg"]);
                        location.reload();
                    }
                }
            })
        }
        
    }else{
        // 전체 회원 발송
        if(confirm("전체 회원에게 전송하시겠습니까?")){
            $.ajax({
                headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                url : "/admin/push/send",
                type : "post",
                data : {"title":title, "message":contents, "target":target},
                dataType : "json",
                success : function(data){
                    if(data["success"]){
                        alert(data["msg"]);
                        location.reload();
                    }
                }
            })
        }
    }
}

function getTokens(){
    let sort = $("[name=sort]").val();
    let value = $("#sort_word").val();

    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type : "post",
        url : "/admin/push/getTokens",
        data : {"sort":sort, "value": value, 'except': except},
        dataType : "json",
        success : function(data){
            if(!data["success"]){
                alert(data["msg"]);
                return false;
            }

            $("#result_cnt").html(data["tokens"].length);
            $("#result_tokens").empty();
            if(data["tokens"].length < 1){
                let str = "<tr>\
                                <td colspan=\"4\">검색 결과가 없습니다</td>\
                          </tr>";
                $("#result_tokens").append(str);
            }else{
                data["tokens"].forEach(token => {
                    let str = "<tr>\
                                <td><input type=\"checkbox\" name=\"result\" value=\""+token["member_id"]+"\" data-nick=\""+token["nick"]+"\" data-token=\""+token["type"]+"\"></td>\
                                <td>"+token["member_id"]+"</td>\
                                <td>"+token["nick"]+"</td>\
                                <td>"+token["type"]+"</td>\
                            </tr>";
                    $("#result_tokens").append(str);
                });
            }
        }
    })
}

$(document).on("keydown", "#sort_word", function(e){
    if (e.originalEvent.code == "Enter"){
        getTokens();
    };
})

$(document).on("change", "#template", function(){
    if($(this).val() == ""){
        $("#title").val("");
        $("#contents").val("");
        $(".msg-title").html("너만없는 고양이");
        $(".msg-text").html("아무개님이 회원님의 게시글에 댓글을 달았습니다");
    }else{
        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            type : "get",
            url : "/admin/push/template/"+$(this).val(),
            success : function(data){
                if(data["success"]){
                    $("#title").val(data["main_msg"]);
                    $("#contents").val(data["sub_msg"]);
                    $(".msg-title").html(data["main_msg"]);
                    $(".msg-text").html(data["sub_msg"]);
                }else{
                    alert(data["msg"]);
                }
            }
        })
    }
})
</script>
@endsection

