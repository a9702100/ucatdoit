@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>푸시 메세지 전송내역</h2>
        </div>
    </div>
    <div class="wrap-cont">
        <table>
            <colgroup>
                <col width="10%">
                <col width="20%">
                <col width="35%">
                <col width="15%">
                <col width="20%">
            </colgroup>
            <thead>
                <tr>
                    <th>번호</th>
                    <th>전송 시간</th>
                    <th>메세지</th>
                    <th>수신 대상</th>
                    <th>자세히보기</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($histories as $history)
                <tr>
                    <td>{{$histories->total() - $histories->firstItem() - $loop->index + 1}}</td>
                    <td>{{$history->created_at}}</td>
                    <td class="message">
                        <p class="strong">{{$history->main_msg}}</p>
                        <p>{{$history->sub_msg}}</p>
                    </td>
                    <td>{{$history->target}}</td>
                    <td>
                        <a class="xi-angle-right detail-btn"></a>
                    </td>
                </tr>
                <tr>
                    <td class="detail" colspan="5">
                        <p>수신 대상</p>
                        <p><i class="xi-subdirectory-arrow"></i>{{$history->members}}</p>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$histories->withQueryString()->links()}}
    </div>
</main>
@endsection

@section('style')
<style>
.wrap-cont {min-width: 1100px; padding: 20px;}
.wrap-cont table tbody tr td {padding: 10px 20px;}
.wrap-cont table tbody tr td.message {white-space: normal;}
.wrap-cont table tbody tr td p.strong {font-weight: bold; line-height: 1.5;}
.wrap-cont table tbody tr td a.xi-angle-right {transition: 0.3s; cursor: pointer;}
.wrap-cont table tbody tr td.detail {display:none; transition: 0.3s;}
.wrap-cont table tbody tr td.detail.show {display: table-cell;}
</style>
@endsection

@section('script')
<script>
$(document).on("click",".detail-btn", function(event){
    let index = $(".detail-btn").index(this);
    $(this).toggleClass("xi-rotate-90");
    $(".detail").eq(index).toggleClass("show");
})
</script>
@endsection