@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container">
        <div class="wrap-tit">
            @php
                $sel1 = "";
                $sel2 = "";
                $sel3 = "";
                $sel4 = "";

                switch(isset($_GET["type"])?$_GET["type"]:""){
                    case "notice"     : $sel1 = "active"; break;
                    case "event"      : $sel2 = "active"; break;
                    case "experience" : $sel3 = "active"; break;
                    case "affiliate"  : $sel4 = "active"; break;
                    default           : $sel1 = "active";
                }
            @endphp
            <ul class="writing-type">
                <li><a class="{{$sel1}}" href="{{url('/admin/writing?type=notice')}}">공지사항</a></li>
                <li><a class="{{$sel2}}" href="{{url('/admin/writing?type=event')}}">이벤트</a></li>
                <li><a class="{{$sel3}}" href="{{url('/admin/writing?type=experience')}}">체험단</a></li>
                <li><a class="{{$sel4}}" href="{{url('/admin/writing?type=affiliate')}}">제휴할인</a></li>
            </ul>
        </div>
        <div class="wrap-cont">
            <div class="cont-top info">
                <div class="left">
                    <p>총 <strong>{{$writings->total()}}</strong>건</p>
                </div>
                <div class="right">
                    <div class="search">
    
                    </div>
                    <a class= "button" href="{{url('/admin/writing/create?type='.$type)}}">추가</a>
                </div>
            </div>   
            <div class="writings">
                @foreach ($writings as $key => $writing)
                <div class="writing-wrap">
                    <div class="status">
                        @if(new DateTime($writing->end_date) < today())
                            <span class="past">마감</span>
                        @elseif(new DateTime($writing->start_date) <= today() && new DateTime($writing->end_date) >= today())
                            <span class="present">진행중</span>
                        @else 
                            <span class="future">예정</span>
                        @endif
                        <div class="group">
                            @if($writing->form == "privacy" || $writing->form == "contents")
                            <a href="{{url('/admin/writing/partiList?id='.$writing->id)}}">
                                <img class="trash" src="{{asset('images/icon/icon-list.png')}}" style="cursor: pointer">
                            </a>
                            @endif
                            <a href="{{url('/admin/writing/edit/'.$writing->id)}}">
                                <img class="trash" src="{{asset('images/icon/icon-pencil.svg')}}" style="cursor: pointer">
                            </a>
                            <a onclick="destroy('{{$writing->id}}');">
                                <img class="trash" src="{{asset('images/icon/icon-trash.svg')}}" style="cursor: pointer">
                            </a>
                        </div>
                    </div>
                    <div class="banner">
                        <img src="{{asset('storage/uploads/writings/'.$writing->files->where('reference','banner')->first()->fn)}}" alt="">
                    </div>
                    <div class="contents">
                        <p class="active">
                            <input type="radio" id="on{{$key}}" name="active{{$key}}" value="1" {{$writing->active == "1" ? "checked" : ""}} data-id="{{$writing->id}}"><label for="on{{$key}}">ON</label>
                            <input type="radio" id="off{{$key}}" name="active{{$key}}" value="0" {{$writing->active == "0" ? "checked" : ""}} data-id="{{$writing->id}}"><label for="off{{$key}}">OFF</label>
                        </p>
                        <p class="title">{{$writing->title}}</p>
                        <p class="period">광고 기간 : {{date_format(new DateTime($writing->start_date), 'Y.m.d')}} ~ {{date_format(new DateTime($writing->end_date), 'Y.m.d')}}</p>
                        @switch($writing->form)
                            @case("no")
                                <p class="form">추가 양식 : 없음</p>
                                @break
                            @case("privacy")
                                <p class="form">추가 양식 : 개인정보수집</p>
                                <p class="form-detail"> ({{$writing->participation->count()}}명 참여함)</p>
                                @break
                            @case("contents")
                                <p class="form">추가 양식 : 개인정보수집+콘텐츠이용동의</p>
                                <p class="form-detail"> ({{$writing->participation->count()}}명 참여함)</p>
                                @break
                            @case("link")
                                <p class="form">추가 양식 : 링크 연결 </p>
                                <p class="form-detail">({{$writing->form_link}})</p>
                                @break
                        @endswitch
                    </div>
                </div>
                @endforeach
                {{$writings->withQueryString()->links()}}
            </div> 
        </div>
    </div>
</main>
@endsection

@section('style')
<style>
    .writings {display: flex; flex-wrap: wrap; margin-top: 20px;}
    .writing-wrap {width: calc(100% / 4 - 20px); border: 1px solid black; margin-right: 20px; margin-bottom: 20px}
    .writing-wrap .status {padding: 5px;display: flex; justify-content: space-between; align-items: center}
    .writing-wrap .status .group {display: flex;}
    .writing-wrap .status span {padding: 5px; color:white; border-radius: 5px;}
    .writing-wrap .status .past {background: grey;}
    .writing-wrap .status .present {background: tomato;}
    .writing-wrap .status .future {background: green}
    .writing-wrap .banner {}
    .writing-wrap .banner img {width: 100%; height: 100%;}
    .writing-wrap .contents {padding: 10px;}
    .writing-wrap .contents p {margin-bottom: 5px; font-weight: 400;}
    .writing-wrap .contents p:last-child {margin-bottom: 0}
    .writing-wrap .contents .active {text-align: right}
    .writing-wrap .contents .active input[type="radio"]{appearance: auto; width: auto; margin-right: 5px;}
    .writing-wrap .contents .title { font-weight: 600; font-size: 16px; margin-bottom: 10px;}
    .writing-wrap .contents .period { font-weight: 400;}
    .writing-wrap .contents .form-detail {text-align: right}
</style>
@endsection

@section('script')
<script>
$(document).on("click", "input[type='radio']", function(){
    const id     = $(this).data("id");
    const active = $(this).val();

    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        url : "/admin/writing/active",
        type: "post",
        data : {"id": id, "active":active},
        dataType : "json",
        success : function(data){
            if(!data["success"]){
                alert(data["msg"]);
            }
        }
    })
});

function destroy(id){
    if(confirm('삭제하시겠습니까?')){
        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content')},
            url : "/admin/writing/"+id,
            type : "delete",
            dataType : "json",
            success : function(data){
                alert(data["msg"]);
                if(data["success"]){
                    location.reload();
                }
            }
        })
    }
}
</script>
@endsection