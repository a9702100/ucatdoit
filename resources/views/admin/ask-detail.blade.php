@extends('layouts.admin.base')
@section('contents')
<main data-type="notices" data-dir="notice">
    <div class="container">
        <div class="wrap-tit">
            <h2>문의 내용</h2>
            <ul>
                <li><a href="{{url('/admin/home')}}">HOME</a></li>
                <li><a href="{{url('/admin/ask')}}">문의하기</a></li>
                <li><strong>상세보기</strong></li>
            </ul>
        </div>
        <div class="line">
            <div class="tit">작성자</div>
            <div class="content">
                <span>{{$ask->member->nick}} ({{$ask->member->userId != "" ? $ask->member->userId : $ask->member->sns_type}})</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">문의 유형</div>
            <div class="content">
                <span>[{{$ask->type}}]</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">문의 내용</div>
            <div class="content">
                <pre>{{$ask->question}}</pre>
            </div>
        </div>
        <div class="line">
            <div class="tit">처리 상태</div>
            <div class="content">
                <select id="process">
                    <option value="접수" {{$ask->process == "접수" ? "selected" : ""}}>접수</option>
                    <option value="답변 완료" {{$ask->process == "답변 완료" ? "selected" : ""}}>답변 완료</option>
                </select>
            </div>
        </div>
        <div class="line">
            <div class="tit">답변(메세지)</div>
            <div class="content">
                <textarea id="answer" {{$ask->process != "접수" ? "readonly" : ""}}>{{$ask->answer}}</textarea> 
            </div>
        </div>
        <div class="btn-group"> 
            <a class="cancel" onclick="cancel();">취소</a>
            <a class="confirm" onclick="register('{{$ask->id}}');">저장</a>
        </div>
    </div>
</main>
@endsection
@section('style')
<style>
.notice-detail-cont pre {margin: 0 10px;}
.notice-detail-cont textarea {margin: 0; font-size: 14px; height: 150px;}
.notice-detail-cont select {background: white; padding: 0 5px;}
</style>
@endsection
@section('script')
<script>
function register(id){
    const process = $('#process option:selected').val();
    const answer = $('#answer').val();

    $.ajax({
        headers : {'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
        url : "/admin/ask/"+id,
        type : "post",
        data : {"process":process, "answer":answer},
        dataType : "json",
        success : function(data){
            alert(data["msg"]);
            if(data["success"]){
                location.href = "/admin/ask";
            }
        }
    })
}

function cancel(){
    if(confirm("작성중인 내용은 삭제됩니다\n이전 페이지로 이동하시겠습니까?")){
        location.href = "/admin/ask";
    }
}
</script>
@endsection