<?php $num = ($listSize * ($page-1))+1; ?>
<input type="hidden" id = "page" value = "<?= $page?>">
<main>
    <div class="container">
        <div class="wrap-tit">
            <h2>오픈 이벤트 회원 목록</h2>
        </div>
        <div class="wrap-cont">
            <div class="cont-top info">
                <p>총 <strong><?=$cnt?></strong>건</p>
                <div class="search">
                    <!-- <div class="search-wrap">
                        <input type="text" placeholder="검색어를 입력하세요">
                        <button>검색</button>
                    </div> -->
                </div>
            </div>   
            <div class="delete-group">
                <button class="registration" onclick="checkDel();">삭제</button>
            </div>
            <div class="register-group">
                <button class="registration" onclick="location.href='/adm/Event/excelSave?title=open'">엑셀저장</button>
            </div>
            <!-- //검색창 -->
            <div class="cont-wrap">
                <table>
                    <colgroup>
                        <col width="100px">
                        <col width="25%">
                        <col width="25%">
                        <col width="25%">
                        <col width="25%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkHead"></th>
                            <th>번호</th>
                            <th>이름</th>
                            <th>전화번호</th>
                            <th>참여일</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($member as $key => $item) {?>
                            <tr>
                                <td><input type="checkbox" name="check" data-idx=<?=$item["idx"]?>></td>
                                <td><?= $num++ ?></td>
                                <td><a href='/adm/Member/memberDetail?idx=0'></a><?=$item["name"]?></td>
                                <td><?=$item["phone"]?></td>
                                <td><?=$item["reg_date"]?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <!-- //공지사항 리스트 -->
                <div class="pagination">
                    <?php echo $pagination; ?>
                </div> 
                <!-- 페이지버튼 -->
            </div> 
        </div>
    </div>
</main>
<script>
// 전체 선택 / 선택 해제
$("#checkHead").on("click",function(){
    if($("#checkHead").prop("checked")){ 
        $("input[name=check]").prop("checked",true);
    }else{
        $("input[name=check]").prop("checked",false);
    }
 });
 // 선택된 개수 비교해서 전체 선택 체크박스 상태 바꾸기
var checkLength = $("input[name=check]").length; // 전체 체크박스 개수
 $("input[name=check]").on("click",function(){
    var checkChecked = $("input[name=check]:checked").length;
    if(checkLength == checkChecked){
        $("#checkHead").prop("checked", true);
    }else{
        $("#checkHead").prop("checked", false);
    }
 });

//  체크된 상태에서 삭제
 function checkDel(){
    var checkNum = [];
    $("input[name=check]:checked").each(function(){
        checkNum.push($(this).data("idx"));
    });
    
    if(checkNum.length == 0){
        alert("선택된 항목이 없습니다.");
    }else{
        if(confirm(checkNum.length+" 건의 항목을 선택했습니다.\n정말 삭제하시겠습니까?")){
            $.ajax({
                url : "/adm/Event/EventDel",
                type : "post",
                data : {"checkNum":checkNum},
                dataType : "json",
                success : function(data){
                   if(data["success"]) {
                       alert(data["msg"]);
                       location.reload();
                   }
                }
            });
        }
    }
 }

 function saveExcel(){
    $.ajax({
        url: "/adm/Event/excelSave",
        data: {"title" : "open"}
    });
 }
</script>
