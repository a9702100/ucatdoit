@extends('layouts.admin.detail')
@section('title')
    피드 상세보기
@endsection
@section('contents')
<div class="detail-wrap">
    <h3>피드 상세보기</h3>
    <div class="line">
        <div class="tit">작성자</div>
        <div class="content">
            {{$feed->feedable->nick}}({{$feed->feedable->userId != "" ? $feed->feedable->userId : $feed->feedable->sns_type}})
        </div>
    </div>
    <div class="line">
        <div class="tit">작성일</div>
        <div class="content">
            {{date_format($feed->created_at, 'Y-m-d H:i:s')}}
        </div>
    </div>
    <div class="br"></div>
    @if($isFeed)
    <div class="line">
        <div class="tit">내용</div>
        <div class="content">
            <div class="swiper-container">
                <ul class="swiper-wrapper">
                    @foreach($feed->files as $file)
                    <li class="swiper-slide">
                        @if($file->ext == "mp4")
                        <video src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt=""controls muted loop autoplay></video>
                        @else 
                        <img src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt="">
                        @endif
                    </li>
                    @endforeach
                </ul>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
    @elseif(!$isFeed)
    <div class="line">
        <div class="tit">내용</div>
        <div class="content">
            존재하지 않는 피드이거나 삭제된 피드입니다
        </div>
    </div>
    @endif
    <div class="br"></div>
    <div class="btn-group"> 
        <a class="cancel" onclick="cancel();">창 닫기</a>
    </div>
</div>
<div class="loading hide">
    <img src="{{asset('images/icon/icon-loading.gif')}}" alt=''>
</div>
@endsection
@section('script')
<script>
function cancel(){
    window.close();
}

new Swiper($(".swiper-container"),{
    autoHeight:true,
    pagination:{
        el:$(".swiper-pagination")
    }
});
</script>
@endsection