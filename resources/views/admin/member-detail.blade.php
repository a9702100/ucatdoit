@extends('layouts.admin.detail')
@section('title')
    회원 상세보기
@endsection
@section('contents')
<div class="detail-wrap">
    <h3>회원 상세보기</h3>
    <div class="line-tabs">
        <div class="tab active" data-info="member">회원 정보</div>
        <div class="tab" data-info="activity">활동 정보</div>
    </div>
    <div id="member" class="line-group">
        <div class="line">
            <div class="tit">가입 타입</div>
            <div class="content">
                @if($member->sns_type == "")
                <span>일반 가입</span>
                @elseif($member->sns_type == "google")
                <span>구글 가입</span>
                @elseif($member->sns_type == "apple")
                <span>애플 가입</span>
                @elseif($member->sns_type == "facebook")
                <span>페이스북 가입</span>
                @elseif($member->sns_type == "kakao")
                <span>카카오톡 가입</span>
                @elseif($member->sns_type == "naver")
                <span>네이버 가입</span>
                @endif
            </div>
        </div>
        <div class="line">
            <div class="tit">아이디(sns_id)</div>
            <div class="content">
                @if($member->sns_type == "")
                <span>{{$member->userId}}</span>
                @else
                <span>{{$member->sns_id}}</span>
                @endif
            </div>
        </div>
        <div class="line">
            <div class="tit">닉네임</div>
            <div class="content">
                <span>{{$member->nick}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">소개</div>
            <div class="content">
                <span>{{$member->intro}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">전화번호</div>
            <div class="content">
                <span>{{$member->phone}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">상태</div>
            <div class="content">
                @if($member->state == "normal")
                <span>정상</span>
                @elseif($member->state == "black")
                <span>블랙</span>
                @elseif($member->state == "delete")
                <span>탈퇴</span>
                @elseif($member->state == "master")
                <span>관리자</span>
                @endif
            </div>
        </div>
        <div class="line">
            <div class="tit">가입일</div>
            <div class="content">
                <span>{{date_format($member->created_at, 'Y-m-d H:i:s')}}</span>
            </div>
        </div>
    </div>
    <div id="activity" class="line-group hide">
        <div class="line">
            <div class="tit">피드 업로드</div>
            <div class="content">
                <span>{{$member->feed_cnt}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">댓글 작성</div>
            <div class="content">
                <span>{{$member->reply_cnt}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">투게더 참여</div>
            <div class="content">
                <span>개설 : {{$member->make_cnt}} / 참여 : {{$member->parti_cnt}} </span>
            </div>
        </div>
        <div class="line">
            <div class="tit">이벤트 참여</div>
            <div class="content">
                <span>{{$member->event_cnt}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">문의 내역</div>
            <div class="content">
                <span>{{$member->ask_cnt}}</span>
            </div>
        </div>
        <div class="line">
            <div class="tit">신고 받은 수</div>
            <div class="content">
                <span>{{$member->report_cnt}}</span>
            </div>
        </div>
        
    </div>
    <div class="br"></div>
    <div class="btn-group"> 
        <a class="cancel" onclick="cancel();">창 닫기</a>
        <a class="confirm" onclick="destroy('{{$member->id}}');">회원 삭제</a>
    </div>
</div>
<div class="loading hide">
    <img src="{{asset('images/icon/icon-loading.gif')}}" alt=''>
</div>
@endsection
@section('script')
<script>
function cancel(){
    window.close();
}

$(document).on("click",".tab",function(){
    const info  = $(this).data("info");

    if(info == "member"){
        $(this).next().removeClass('active');
        $(this).addClass('active');

        $("#activity").addClass("hide");
        $("#member").removeClass("hide");
    }else if(info == "activity"){
        $(this).prev().removeClass('active');
        $(this).addClass('active');

        $("#member").addClass("hide");
        $("#activity").removeClass("hide");
    }
})

function destroy(id){
    if(confirm("회원 삭제하면 복구할수 없습니다.\n정말 삭제하시겠습니까?")){
        if(confirm("진짜 삭제합니까?")){
            $.ajax({
                headers : {"X-CSRF-TOKEN":$("meta[name='csrf-token']").attr("content")},
                url : "/admin/member/"+id,
                type : "delete",
                dataType : "json",
                success : function(data){
                    alert(data["msg"]);
                    if(data["success"]){
                        window.opener.location.reload();
                        window.close();
                    }
                }
            })
        }
    }
}

$(document).ajaxStart(function(){
    $(".loading").removeClass('hide');
});

$(document).ajaxStop(function(){
    $(".loading").addClass("hide");
})
</script>
@endsection