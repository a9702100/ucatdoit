@extends('layouts.admin.base')
@section('contents')
<main>
    <div class="container post-comment-detail">
        <section class="post-wrap">     
            <div class="list-group">
                <h3>원본 포스트</h3>
                <div class="list_contents">
                    <div class="list-top">
                        <div class="left">
                            <div class="col-group">
                                <div class="profile" href="/adm/Member/memberDetail?idx=<?= $board["mb_idx"] ?>">
                                    <img src="/resources/uploads/member/<?= $board["fn"]?>" alt="" onerror="this.src='/resources/images/profile.png'">
                                </div>
                                <p class="nickname"><a href="/adm/Member/memberDetail?idx=<?= $board["mb_idx"] ?>"><?= $board["nick"]?></a></p>
                                <span class="date"><?= $board["reg_date"]?></span>
                            </div>
                            <textarea class="list-cont" readonly><?= $board["content"]?></textarea>                                    
                        </div>
                        <div class="right list-img">
                            <div class="comment-slide">
                                <?php foreach($upload as $key => $item):?>
                                    <div><img src="/resources/uploads/board/<?= $item["fn"] ?>" alt="" ></div>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                    <ul class="col-group list-hash">
                    	<?php foreach ($hash as $key => $item) :?>
                        <li>#<?= $item["content"]?></li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="comment-group">
                <h3>댓글 리스트</h3>
                <ul class="comment-list">
                    <li class="col-group list-group">
                        <div class="left">
                            <div class="list-top">
                                <div class="col-group">
                                    <div class="profile" href="/adm/Member/memberDetail?idx=<?= $comment["mb_idx"] ?>">
                                        <img src="/resources/uploads/member/<?= $comment["fn"]?>" alt="" onerror="this.src='/resources/images/profile.png'">
                                    </div>
                                    <p class="nickname"><a href="/adm/Member/memberDetail?idx=<?= $comment["mb_idx"] ?>"><?= $comment["nick"]?></a></p>
                                    <span class="date"><?= $comment["reg_date"]?></span>
                                </div>
                            </div>                                
                            <p class="list-cont"><?= $comment["content"]?></p>
                        </div>
                        <div class="right">
                            <div class="btn-group">
                                <button class="btn-delete" onclick="btnDel(<?= $comment["idx"] ?>)">삭제</button>
                            </div>               
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
</main>
@endsection
@section('script')
<script>
var checkNum = [];
function btnDel(comment_idx){
    checkNum.push(comment_idx);
    if(confirm("모든 정보가 삭제됩니다\n정말 삭제하시겠습니까?")){
        $.ajax({
            url : "/adm/Board/BoardDel",
            type : "post",
            data : {"checkNum":checkNum},
            dataType : "json",
            success : function(data){
                if(data["success"]) {
                    alert(data["msg"]);
                    location.href="/adm/Board/Comment";
                }
            }
        });
    }
}
</script>       
@endsection