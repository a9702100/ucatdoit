@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap settings-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>설정</p> 
            <a class="no-img"></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <ul class="area">
                <li class="title"><h3>알림 설정</h3></li>
                <li class="content">
                    <span>좋아요</span>
                    @if($push->good == "1")
                    <i class="xi-toggle-on xi-3x" data-target="good" data-status="1"></i>
                    @else
                    <i class="xi-toggle-off xi-3x" data-target="good" data-status="0"></i>
                    @endif
                </li>
                <li class="content">
                    <span>댓글</span>
                    @if($push->reply == "1")
                    <i class="xi-toggle-on xi-3x" data-target="reply" data-status="1"></i>
                    @else
                    <i class="xi-toggle-off xi-3x" data-target="reply" data-status="0"></i>
                    @endif
                </li>
                <li class="content">
                    <span>친구</span>
                    @if($push->friend == "1")
                    <i class="xi-toggle-on xi-3x" data-target="friend" data-status="1"></i>
                    @else
                    <i class="xi-toggle-off xi-3x" data-target="friend" data-status="0"></i>
                    @endif
                </li>
                <li class="content">
                    <span>투게더</span>
                    @if($push->together == "1")
                    <i class="xi-toggle-on xi-3x" data-target="together" data-status="1"></i>
                    @else
                    <i class="xi-toggle-off xi-3x" data-target="together" data-status="0"></i>
                    @endif
                </li>
                <li class="content">
                    <span>문의내역</span>
                    @if($push->ask == "1")
                    <i class="xi-toggle-on xi-3x" data-target="ask" data-status="1"></i>
                    @else
                    <i class="xi-toggle-off xi-3x" data-target="ask" data-status="0"></i>
                    @endif
                </li>
            </ul>
        </div>
    </div>  
</div>

@endsection
@section('style')
<style>
.settings-wrap .area {border-bottom: 1px solid rgba(0,0,0,0.1)}
.settings-wrap .area .title {padding: 10px;}
.settings-wrap .area .title h3::before {content: ""; margin-right: 10px; border: 3px solid tomato; background-color: tomato;}
.settings-wrap .area .content {display: flex; justify-content: space-between; align-items: center; width: 100%; height: 50px; padding: 0 20px;}
.settings-wrap .area .content i {color: tomato}
</style>
@endsection
@section('script')
<script>
$(document).on("click",".xi-toggle-off, .xi-toggle-on", function(){
    let target = $(this).data("target"); // 선택한 타겟
    let status = $(this).data("status"); // 선택한 타겟의 상태

    if($(this).hasClass("xi-toggle-off")){
        // OFF -> ON
        $(this).removeClass("xi-toggle-off").addClass("xi-toggle-on");
        $(this).data('status', 1);
    }else{
        // ON -> OFF
        $(this).removeClass("xi-toggle-on").addClass("xi-toggle-off");
        $(this).data('status', 0);
    }

    $.ajax({
        headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
        type : "post",
        url : "/settings",
        data : {"target":target, "status": status},
        success : function(data){
            if(data["success"]){
                console.log("변경 완료");
            }
        }
    })
})


</script>
@endsection