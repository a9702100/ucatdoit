@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap join-wrap">
    <div>
        <div class="sub-head col-group">
            <img class="no-img" src="" alt="">
            <p>회원가입</p> 
            <a href="{{url('/')}}"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <div class="sussess-box">
            <div>
                <img src="{{asset('images/check.png')}}" alt="">
                <p>빠밤! 회원가입 완료!</p>
            </div>
        </div>
    </div>
</div>
@endsection