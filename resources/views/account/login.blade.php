@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap">
    <div class="container">
        <h1 class="logo">
            <a href="{{url('/')}}"><img src="{{asset('images/logo.svg')}}" alt=""></a>
        </h1>
        <form action="{{route('account.check')}}" method="POST">
            @csrf
            <div class="login-form">
                <div id="box-userId" class="col-group @error('userId') red-login @enderror ">
                    <img src="{{asset('images/icon/icon-user.svg')}}" alt="">
                    <input type="text" name="userId" id="userId" placeholder="아이디 입력" value="{{old('userId')}}">
                @error('userId')
                    <span>{{$message}}</span>
                @enderror
                @if(Session::get('fail_userId'))
                    <span>{{Session::get('fail_userId')}}</span>
                @endif
                </div>
                <div id="box-passwd" class="col-group @error('passwd') red-login @enderror ">
                    <img src="{{asset('images/icon/icon-lock.svg')}}" alt="">
                    <input type="password" name="passwd" id="passwd" placeholder="비밀번호">
                @error('passwd')
                    <span>{{$message}}</span>
                @enderror
                @if(Session::get('fail_passwd'))
                    <span>{{Session::get('fail_passwd')}}</span>
                @endif
                    <a class="no-eyes"></a>
                </div>
            </div>
            <button type="submit" class="btn-login red-login">로그인</button>
        </form>
        <div class="btn-login-more col-group">
            <a class="btn-find" href="{{url('/account/find')}}">계정 찾기</a>
            <a href="{{url('/account/join')}}">회원가입</a>
        </div>
        <div class="btn-sns">
            <div class="sns-login">
                <span></span>
                <span>소설 로그인</span>
                <span></span> 
            </div>
            <div class="login-box">
                <span>🐱5초만에 회원가입:)</span>
            </div>
            <div class="btn-group">
                <a class="btn-container kakao" href="{{url('/account/login?oauth=kakao')}}">
                    <img src="{{asset('images/kakao_simbol.png')}}">
                    <p>카카오 로그인</p>
                </a>
                <a class="btn-container facebook" href="{{url('/account/login?oauth=facebook')}}">
                    <img src="{{asset('images/fb_simbol.png')}}">
                    <p>Facebook으로 로그인</p>
                </a>
                <a class="btn-container apple" href="{{url('/account/login?oauth=apple')}}">
                    <img src="{{asset('images/apple_simbol.png')}}">
                    <p>Sign in with Apple</p>
                </a>
                <a class="btn-container google" href="{{url('/account/login?oauth=google')}}">
                    <img src="{{asset('images/google_simbol.png')}}">
                    <p>Sign in with Google</p>
                </a>
                <a class="btn-container naver" href="{{url('/account/login?oauth=naver')}}">
                    <img src="{{asset('images/naver_simbol.png')}}">
                    <p>네이버 로그인</p>
                </a>
            </div>
        </div>
    </div>
</div>
@if(Session::get('deleted_account'))
<div id="alert-box" class="join-wrap-popup popup02">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="alert-msg">{{Session::get('deleted_account')}}<br>[삭제일 : {{Session::get('deleted_at')}}]</p>
                </div>
                <button onclick="alert_btn('confirm');">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="isMember hide">
   <img src="{{asset('images/icon/icon-loading.gif')}}" alt="">
</div>
{{-- <div id="status">
</div> --}}
@endsection
@section('script')
<script>
$(document).ready(function(){
    const member = localStorage.getItem("member");
    if(member != null){
        // 로그인한 이력이 있음 :: 존재하는 멤버 번호인지 확인하고 이동
        $.ajax({
            headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
            url : "/account/isMember",
            type : "post",
            data : {"member":member},
            dataType:"json",
            success : function(data){
                if(data["success"]){
                    location.href="/home";
                }
            }
        });
    }
    
})

$(document).ajaxStart(function(){
    $(".isMember").removeClass("hide");
});
$(document).ajaxStop(function(){
    $(".isMember").addClass("hide");
})

function login(){
    location.href = "{{url('/account/login')}}";
}

$("#userId, #passwd").on({
    focus : function(){
        let t_id = $(this).attr("id"); // 이벤트 발생 아이디
        if(!$("#box-"+t_id).hasClass("red-login")){
            $("#box-"+t_id).addClass("red-login");
        }
    },
    blur : function(){
        let t_id = $(this).attr("id"); // 이벤트 발생 아이디
        if($("#box-"+t_id).hasClass("red-login")){
            $("#box-"+t_id).removeClass("red-login");
        }

    }
})

function alert_btn(thing){
    if(thing == "confirm"){
        $('#alert-box').addClass("hide");
    }
}
</script>
@endsection