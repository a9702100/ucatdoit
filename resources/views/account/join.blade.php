@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap join-wrap">
    <div>
        <div class="sub-head col-group">
            <a href="{{url('/account/join?step=1')}}"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>회원가입</p> 
            <img class="no-img" src="{{asset('images/icon/icon_close.svg')}}" alt="">
        </div>
        <!-- 서브헤더 -->
        <div class="sub-body">
            <div class="sub-tit">
                <div>
                    <h2>전화번호 인증</h2>
                    <p>원활한 서비스를 위해 번호인증을 해주세요.</p>
                </div>
            </div>
            <button class="btn-login m-btn" name="logintalk_sejong_verify">본인인증</button>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>

<div id="age14" class="join-wrap-popup hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>서비스 이용제한 안내</h3>
                    <p>
                        만 14세 미만 고객님은<br>
                        가입하실 수 없습니다.
                    </p>
                </div>
                <button onclick="alert_close();">확인</button>
            </div>
        </div>
    </div>
</div>
<div id="veri_chk" class="join-wrap-popup hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>본인인증 확인</h3>
                    <p><br>인증되었습니다</p>
                </div>
                <button onclick="alert_continue();">계속</button>
            </div>
        </div>
    </div>
</div>
<div id="member_chk" class="join-wrap-popup hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>중복가입 확인</h3>
                    <p><br>가입중인 전화번호입니다</p>
                </div>
                <button onclick="alert_member();">확인</button>
            </div>
        </div>
    </div>
</div>
<!-- 14세 이하 불가 -->
@endsection

@section('script')
<script src="https://auth.logintalk.io/js/logintalk.js"></script>
<script>
var phone_num = "";

logintalk({key:"VLH3VH6qk", action:"/account/verify", verify:true, method:"POST"});

function cb(token){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"/account/verify",
        type:"POST",
        data:{"token":token},
        dataType:"json",
        success : function(data){
            console.log(data);
            if(data["result"] == "L101"){
                // 인증완료
                if(calc_age(data["birthday"]) < 14){
                    // 14세미만은 이용할수 없습니다.
                    $("#age14").removeClass("hide");
                }else{
                    member_chk(data["mobile_number"]);
                }
            }else{
               alert("다시시도해주세요"); 
            }
        },error:function(){
        	 alert("관리자에게 문의하세요");
        }
    })
    
	 // 패러미터로 받으신 토큰을 서버로 전달하셔야 합니다.
};
logintalk.callback(cb);

function calc_age(birthday){
    const today = new Date();
    const b_year = birthday.slice(0,4);
    const b_month = birthday.slice(4,6);
    const b_day = birthday.slice(6);
    const birthDate = new Date(b_year, b_month, b_day);

    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if(m < 0 || (m === 0 && today.getDate() < birthDate.getDate())){
        age--;
    }

    return age;
}

function alert_close(){
    $('#age14').addClass("hide");
}
function alert_member(){
    $('#member_chk').addClass("hide");
}

function alert_continue(){
    location.href = "/account/join?step=5&phone="+phone_num;
}

function member_chk(phone){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/account/join/member_chk",
        type : "post",
        data : {"phone":phone},
        dataType : "json",
        success : function(data){
            if(data["duplicate"]){
                // 중복되는 전화번호
                $("#member_chk").removeClass("hide");
            }else{
                // 없는 전화번호
                phone_num = phone;
                $("#veri_chk").removeClass("hide");
            }
        } 
    })
}
</script>
@endsection