@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap join-wrap" data-phone="{{isset($phone) ? $phone : ""}}">
    <div>
        <div class="sub-head col-group">
            <a onclick="back_btn();"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>회원가입</p> 
            <img class="no-img" src="{{asset('images/icon/icon_close.svg')}}" alt="">
        </div>
        <div class="sub-body">
            <div class="sub-tit col-group">
                <div>
                    <h2>회원정보 입력</h2>
                </div>
                <div class="chapter col-group">
                    <p></p>
                    <p class="active"></p>
                    <p></p>
                </div>
                <!-- 동그라미 버튼 -->
            </div>
            <!-- 서브헤더 -->
            <div class="sub-cont">
                <form action = "{{route('join.store')}}" method="post">
                    @csrf
                    <div class="login-form">
                        <input type="hidden" name="phone" value="{{$phone}}">
                        <div>
                            <p>아이디</p>
                            <div id="box-userId" class="col-group @error('userId') {{"red-login"}} @enderror" >
                                <input type="text" id="userId" name="userId" placeholder="아이디 입력" value="{{old('userId')}}">
                                <span id="msg-userId">
                                    @error('userId')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>
                        </div>
                        <div>
                            <p>비밀번호</p>
                            <div id="box-passwd" class="col-group @error('passwd') {{"red-login"}} @enderror" >
                                <div class="box">
                                    <input type="password" id="passwd" name="passwd" placeholder="공백없이 5자~12자" value="{{old('passwd')}}">
                                    <span id="msg-passwd">
                                        @error('passwd')
                                        {{$message}}
                                        @enderror
                                    </span>
                                </div>
                                <a class="no-eyes"></a>
                            </div>
                        </div>
                        <div>
                            <p>닉네임</p>
                            <div id="box-nick" class="col-group last @error('nick') {{"red-login"}} @enderror ">
                                <input type="text" id="nick" name="nick" placeholder="한글,영문,숫자 2~8자" value="{{old('nick')}}">
                                <span id="msg-nick">
                                    @error('nick')
                                    {{$message}}
                                    @enderror
                                </span>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn-login btn-foot">가입하기</button>
                </form>
            </div>
            <!-- 서브바디 -->
        </div>
    </div>  
</div>
@endsection

@section('script')
<script>
    $(document).on("input","#userId, #passwd, #nick",function(){
        let t_id = $(this).attr("id"); // Event 발생 id
        let t_value = $(this).val();

        if($(this).val() != ""){
            // 입력값이 존재할 때
            if(!$("#box-"+t_id).hasClass("red-login")){
                $("#box-"+t_id).addClass("red-login");
            }

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url : "/account/join/input_regex",
                type : "post",
                data : {"target":t_id, "value":t_value},
                dataType : "json",
                success : function(data){
                    if(data["success"]){
                        // 올바른 규칙
                        $("#msg-"+t_id).html("");
                    }else{
                        // 규칙에 어긋남
                        $("#msg-"+t_id).html(data["msg"]);
                    }
                }
            });

        }else{
            // 입력값이 비어있을 때
            $("#box-"+t_id).removeClass("red-login");
            $("#msg-"+t_id).empty();
        }
    });

    function back_btn(){
        if(confirm("본인인증부터 다시 시작합니다\n이전 페이지로 이동하시겠습니까?")){
            location.href = "/account/join?step=1"; 
            return false;
        }
    }
</script>
@endsection