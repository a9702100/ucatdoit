@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap join-wrap">
    <div>
        <div class="sub-head col-group">
            <a href=""><img class="no-img" src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>콘텐츠 활용 동의서</p> 
            <img  src="{{asset('images/icon/icon_close.svg')}}" onclick="back();">
        </div>
        <div class="sub-body sub-privacy container">
            <div>
                <h3>■ 콘텐츠 이용목적</h3>
                <h4>너만 없는 고양이 SNS 매체 (인스타, 유튜브 등)에 당선작 발표, 콘텐츠 재가공 활용</h4>
            </div>
            <div>
                <h3>■ 콘텐츠 이용의 범위</h3>
                <h4>본 컨텐츠를 이용한 영상, SNS업로드, 공모수상작의 2차 저작물 제작</h4>
            </div>
            <div>
                <h3>※ 콘텐츠 활용 동의를 하지 않아도 상품 수령은 그대로 진행됩니다</h3>
            </div>
        </div>
    </div>  
</div>
@endsection

@section('script')
<script>
    function back(){
        history.back();
        return false;
    }
    function privacy(){
        location.href = "{{url('/account/join?step=2')}}";
    }
</script>
@endsection