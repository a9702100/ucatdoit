@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap join-wrap find-wrap">
    <div>
        <div class="sub-head col-group">
            <a href="{{url('/')}}"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>계정찾기</p> 
            <img class="no-img" src="{{asset('images/icon/icon_close.svg')}}" alt="">
        </div>
        <!-- 서브헤더 -->
        <div class="sub-body">
            <div class="sub-tit">
                <div>
                    <h2>전화번호 인증</h2>
                    <p>원활한 서비스를 위해 번호인증을 해주세요.</p>
                </div>
            </div>
            <button class="btn-login" name="logintalk_sejong_verify">본인인증</button>
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<!-- <div class="join-wrap-popup find-wrap-popup">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <p>
                        총 가입 아이디가 5개 이상입니다. <br>
                        다른 아이디를 삭제 후 다시 가입해주세요
                    </p>
                </div>
                <button>확인</button>
            </div>
        </div>
    </div>
</div> -->
<!-- 아이디 5개 이상 -->
<div id="veri_chk" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>본인인증 확인</h3>
                    <p>해당 번호로 가입된 계정이 있습니다</p>
                </div>
                <button onclick="alert_btn('veri_chk');">계속</button>
            </div>
        </div>
    </div>
</div>
<div id="member_chk" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>본인인증 확인</h3>
                    <p><br>해당 번호로 가입된 계정이 없습니다</p>
                </div>
                <button onclick="alert_btn('member_chk');">확인</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://auth.logintalk.io/js/logintalk.js"></script>
<script>
let phone_num = "";
logintalk({key:"VLH3VH6qk", action:"/account/verify", verify:true, method:"POST"});

function cb(token){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url:"/account/verify",
        type:"POST",
        data:{"token":token},
        dataType:"json",
        success : function(data){
            console.log(data);
            if(data["result"] == "L101"){
                // 인증완료
                member_chk(data["mobile_number"]);
            }else{
               alert("다시시도해주세요"); 
            }
        },error:function(){
        	 alert("관리자에게 문의하세요");
        }
    })
    
	 // 패러미터로 받으신 토큰을 서버로 전달하셔야 합니다.
};
logintalk.callback(cb);

function member_chk(phone){
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/account/join/member_chk",
        type : "post",
        data : {"phone":phone},
        dataType : "json",
        success : function(data){
            if(data["duplicate"]){
                // 중복되는 전화번호
                phone_num = phone;
                $("#veri_chk").removeClass("hide");
            }else{
                // 없는 전화번호
                $("#member_chk").removeClass("hide");
            }
        } 
    })
}

function alert_btn(id){
    if(id == "veri_chk"){
        location.href = "/account/find/show?phone="+phone_num;
    }else if(id == "member_chk"){
        $("#"+id).addClass("hide");
    }
}
</script>
@endsection