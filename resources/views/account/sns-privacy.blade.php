@extends('layouts.account')
@section('contents')
<div id="wrap" class="login-wrap join-wrap">
    <div>
        <div class="sub-head col-group">
            <a onclick="back_btn();"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p>이용약관 동의</p> 
            <img class="no-img" src="{{asset('images/icon/icon_close.svg')}}" alt="">
        </div>
        <div class="sub-body">
            <div class="sub-tit col-group">
                <div>
                    <h2>이용약관 동의</h2>
                </div>
                <div class="chapter col-group">
                    <p class="active"></p>
                    <p></p>
                    <p></p>
                </div>
                <!-- 동그라미 버튼 -->
            </div>
            <!-- 서브헤더 -->
            <div class="sub-cont">
                <h3 class="check-privacy col-group">
                    <input type="checkbox" id="box1">
                    <label for="box1"><span>전체동의</span></label>    
                </h3>
                <div class="check-privacy">
                    <div class="col-group">
                        <input type="checkbox" name="agree" id="box2"> 
                        <label for="box2"><span>너만 없는 고양이 이용약관 동의(필수)</span></label>
                        <p onclick="privacy_detail('3');">보기</p>
                    </div>
                </div>
                <div class="check-privacy">
                    <div class="col-group">
                        <input type="checkbox" name="agree" id="box3">
                        <label for="box3"><span>개인정보처리 방침 이용 동의(필수)</span></label>
                        <p onclick="privacy_detail('4');">보기</p>
                    </div>     
                </div>
                    
            </div>  
            <!-- 서브바디 --> 
            <div class="btn-foot">
                <button class="btn-login" onclick="enter('{{$next}}');">다음</button>
            </div>
        </div>
    </div>  
</div>
<div id="agree_chk" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p>서비스를 이용하시려면<br>이용동의가 필요합니다</p>
                </div>
                <button onclick="alert_close();">확인</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function privacy_detail(step){
        // 이용약관
        location.href = "{{url('/account/join?step=')}}"+step;
    }

    function enter(next){
        // 정보입력
        const agree = $("input[name=agree]:checked").length;
        if(agree < 2){
            $('#agree_chk').removeClass("hide");
        }else{
            $.ajax({
                headers : {"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")},
                url : "/account/sns_join",
                type : "put",
                data : {"member_id":next},
                dataType : "json",
                success : function(data){
                    location.href = "/home";
                }
            })
        }
    }

    function back_btn(){
        if(confirm("서비스 이용동의 없이는 어플을 이용하실수 없습니다\n로그인 페이지로 이동하시겠습니까?")){
            location.href="/account/logout";
        }
    }

    // 팝업 알림 - 동의 확인
    function alert_close(){
        $("#agree_chk").addClass('hide');
    }

    // 전체동의 선택시lucas
    $("#box1").on("click",function(){
        let state = $("#box1").is(":checked");
        let leng = $("input[name=agree]").length;
        if(state){
            // 전체동의 선택
            for(var i = 0; i < leng; i++){
                if(!$("input[name=agree]").eq(i).prop("checked")){
                    $("input[name=agree]").eq(i).prop("checked",true);
                }
            }
        }else{
            // 전체동의 해제
            for(var i = 0; i < leng; i++){
                if($("input[name=agree]").eq(i).prop("checked")){
                    $("input[name=agree]").eq(i).prop("checked",false);
                }
            } 
        }
    })

    // 체크버튼 각각 누를경우
    $("input[name=agree]").on("click",function(){
        if($("input[name=agree]:checked").length < 2){
            // 약관 동의 수가 2가 아니면 전체동의 선택 해제
            $("#box1").prop("checked", false);
        }else{
            // 약관 동의 수가 2이면 전체동의 선택 
            $("#box1").prop("checked", true);
        }
    });
</script>
@endsection