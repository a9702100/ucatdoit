@extends('layouts.account')
@section('contents')
    <div id="wrap" class="login-wrap join-wrap find-wrap">
        <div>
            <div class="sub-head col-group">
                <a href="{{url('/account/find')}}"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
                <p>계정찾기</p> 
                <a href="{{url('/')}}"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
            </div>
            <!-- 서브헤더 -->
            <div class="sub-body">
                <div class="sub-tit">
                    <div>
                        <h2>계정찾기 결과</h2>
                        <p>해당 계정에 대해 비밀번호를 재설정하세요</p>
                    </div>
                </div>
                <div class="sub-cont">
                    <div class="radio-box">
                        <p>계정 선택</p>
                        <div>
                            <ul>
                                <li class="col-group">
                                    <div class="col-group">
                                        <input type="radio" id="radio01" name="id" checked>
                                        <label for="radio1"><span>{{$member->userId}}</span></label>
                                    </div>
                                    <span>{{date_format($member->created_at, 'Y/m/d')}} 가입</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="change-box">
                        <p>비밀번호 재설정</p>
                        <div class="col-group">
                            <div>
                                <input type="password" id="passwd" placeholder="최소 8자 이상">
                                <p class="no-eyes"></p>
                            </div>
                            <button class="btn-login red-login red" onclick="pwdChange('{{$member->id}}')">변경</button>
                        </div>
                    </div>
                </div>
                {{-- <div class="foot-box">
                    <div>
                        <button class="btn-login gray" onclick="location.href='/'">메인으로</button>
                        <button class="btn-login red" onclick="location.href='/login.html'">로그인하기</button>
                    </div>  
                </div>   --}}
            </div>
            <!-- 서브바디 -->
        </div>  
    </div>
    <!-- <div class="join-wrap-popup find-wrap-popup">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <p>
                            총 가입 아이디가 5개 이상입니다. <br>
                            다른 아이디를 삭제 후 다시 가입해주세요
                        </p>
                    </div>
                    <button>확인</button>
                </div>
            </div>
        </div>
    </div> -->
    <!-- 아이디 5개 이상일때 팝업 띄어주세용-->
    <div id="err_box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p id="err_msg"></p>
                    </div>
                    <button onclick="alert_close();">확인</button>
                </div>
            </div>
        </div>
    </div>
    <div id="suc_box" class="join-wrap-popup popup02 hide">
        <div class="inner">
            <div class="inner-box">
                <div>
                    <div>
                        <h3>알림</h3>
                        <p>변경되었습니다</p>
                    </div>
                    <button onclick="alert_go();">확인</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
function pwdChange(id){
    const passwd = $("#passwd").val();
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/account/find/"+id,
        type : "post",
        data : {"passwd":passwd},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                $("#suc_box").removeClass("hide");
            }else{
                $("#err_msg").html(data["msg"]);
                $("#err_box").removeClass("hide");
            }
        }
    })
}

function alert_close(){
    $("#err_box").addClass("hide");
}

function alert_go(){
    location.href = "/";
}
</script>
@endsection