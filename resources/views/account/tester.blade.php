<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="google-signin-client_id" content="997748481059-4pcglcbvfqpo1lgpbckckfk4ro4kvgb8.apps.googleusercontent.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>너만없어고양이</title>
    <link rel="stylesheet" href="{{asset('admin/css/common.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
    <style>
        fieldset{border : 1px solid black; margin-top: 10px; padding: 10px;}
        fieldset > legend {padding: 5px 10px;}
        fieldset > p {margin-bottom: 5px}
    </style>
    <script type="text/javascript" src="{{asset('admin/js/jquery-3.5.1.min.js')}}"></script> 
</head>
<body>
    <div class="login_wrap">
        {{-- <h5>관리자 페이지</h5> --}}
        <img src="{{asset('images/logo.svg')}}" alt="로고">

        <div class="container">
            <div class="msg-box">
                <p>업데이트 점검</p>
                <p>02:00 ~ 04:00</p>
            </div>
            <img src="{{asset('images/icon/icon-loading.gif')}}" alt="로딩 이미지" style="width: 45px; height:45px">
            <fieldset>
                <legend>업데이트 항목</legend>
                <p>1. 알람 개별 설정기능 추가 </p>
                <p>2. 이벤트 참여시 자동완성 기능 추가</p>
                <p>3. 그 외 오류 사항 수정</p>
            </fieldset>
            
        </div>
    </div>
</body>

</html>