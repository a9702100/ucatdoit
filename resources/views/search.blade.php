@extends('layouts.home')
@section('contents')
<div id="wrap" class="main-wrap">
    <div>
        <div class="sub-head col-group search-head">
            <a onclick="javascript:history.back(); return false;"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            <p class="col-group">
                <img src="{{asset('images/icon/icon-search-g.svg')}}" alt="">
                <input type="text" id="keyword" placeholder="검색어를 입력하세요." maxlength="10">
            </p>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont">
            <ul class="tabs col-group">
                <li class="tab-link current" data-tab="tab-1">계정</li>
                <li class="tab-link" data-tab="tab-2">태그</li>
            </ul>
            <div id="tab-1" class="tab-content current">
                <div class="recommend">
                    @foreach($strangers as $stranger)
                    <ul class="profile">
                        <li class="col-group">
                            <div class="left col-group">
                                @if(isset($stranger->upload))
                                <img class="my-img" src="{{asset('storage/uploads/profile/'.$stranger->upload->fn)}}" alt="">
                                @else 
                                <img class="no-my-img" src="{{asset('images/profile-img.svg')}}" alt="">
                                @endif
                                <a href="{{url('/profile?other='.$stranger->id)}}">{{$stranger->nick}}</a>
                            </div>
                            <div class="right">
                                <button class="relation{{$stranger->id}}" onclick="relation('{{$stranger->id}}','apply')">친구신청</button>
                            </div>
                        </li>
                    </ul>
                    @endforeach
                </div>
                <div class="search">
                    <div class="placeholder"> 검색할 계정(닉네임)을 입력하세요</div>
                    <div class="loading hide">
                        <img src="{{asset('images/icon/icon-loading.gif')}}" alt="로딩 이미지">
                    </div>
                    <div class="result hide">

                    </div>
                </div>
            </div>
            <div id="tab-2" class="tab-content">
                <div class="recommend">
                    <ul class="profile tag">
                        @foreach($hot_tags as $hot_tag)
                        <li class="col-group">
                            <div class="left col-group">
                                <img class="tag-img" src="{{asset('images/icon/icon-hash.svg')}}" alt="">
                                <div>
                                    <a href="{{url('/search/show?content='.$hot_tag->content)}}">{{$hot_tag->content}}</a>
                                    <p>게시물 {{$hot_tag->total}}건</p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="search">
                    <div class="placeholder"> 검색할 태그를 입력하세요</div>
                    <div class="loading hide">
                        <img src="{{asset('images/icon/icon-loading.gif')}}" alt="로딩 이미지">
                    </div>
                    <div class="result hide">

                    </div>
                </div>
            </div>
        </div>
        <!-- 서브바디 -->
    </div>  
    <div class="popup-box hide ">
        <div class="inner">
            <div>
                <p>최근 검색어를 모두 삭제하시겠습니까?</p>
                <div class="col-group">
                    <button class="btn-close">취소</button>
                    <button class="bold">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$("#keyword").on({
    focus: function(){
        $(".recommend").hide();

        let keyword = $(this).val();
        if(keyword.length > 0){
            $(".result").removeClass("hide");
            $(".placeholder").addClass("hide");
        }else{
            $(".placeholder").removeClass("hide");
            $(".result").addClass("hide");
        }
        $(".search").show();
    },
    input: function(){
        $(".result").empty().addClass("hide");
        $(".placeholder").addClass("hide");
        let keyword = $(this).val();
        if(keyword == ""){
            // 입력값 없음
            $(".loading").addClass("hide");
            $(".result").empty();
            $(".recommend").show();
        }else{
            $(".recommend").hide();
            $(".loading").removeClass("hide");
            $.ajax({
                headers:{'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr("content")},
                url: "/search",
                type : "post",
                data : {"keyword":keyword},
                dataType : "json",
                success : function(data){
                    $(".loading").addClass("hide");
                    $(".result").removeClass("hide");
                    if(data["member_cnt"] > 0){
                        // 검색 결과 있음 :: 계정 검색
                        let str = "<ul class=\"profile\">";
                        data["result_members"].forEach(member => {    
                            str += "<li class=\"col-group\">\
                                        <div class=\"left col-group\">";
                            if(member["fn"] != ""){
                                str += "<img class=\"my-img\" src=\"{{asset('storage/uploads/profile')}}/"+member["fn"]+"\" alt=''>";
                            }else{
                                str += "<img class=\"no-my-img\" src=\"{{asset('images/profile-img.svg')}}\" alt=''>";
                            }
                            str += "<a href='/profile?other="+member["id"]+"'>"+member["nick"]+"</a>\
                                        </div>\
                                           <div class=\"right\">";
                            if(member["relation"] == "me"){
                                str += "<button>내 계정</button>";
                            }else if(member["relation"] == "friend"){
                                str += "<button>친 구</button>";
                            }else if(member["relation"] == "wait"){
                                str += "<button class=\"relation"+member["id"]+"\" onclick=\"relation('"+member["id"]+"','wait')\">수락대기중</button>";
                            }else if(member["relation"] == "not"){
                                str += "<button class=\"relation"+member["id"]+"\" onclick=\"relation('"+member["id"]+"','apply')\">친구신청</button>";
                            }else if(member["relation"] == "yes"){
                                str += "<button class=\"relation"+member["id"]+"\" onclick=\"relation('"+member["id"]+"','yes')\">친구수락</button>";
                            }
                            str += "</div>\
                            </li>";
                        });
                        str += "</ul>";
                        $(".result").eq(0).html(str);
                    }else{
                        // 검색 결과 없음 :: 계정 검색
                        let str = "<div class='placeholder'><p>\'"+keyword+"\'에 대한<br> 검색 결과가 없습니다</p></div>";
                        $(".result").eq(0).html(str);
                    }

                    if(data["tags_cnt"] > 0){
                        // 검색 결과 있음 :: 태그 검색
                        let str = "<ul class=\"profile tag\">";
                        data["result_tags"].forEach(tag => {
                            str += "<li class=\"col-group\">\
                                <div class=\"left col-group\">\
                                    <img class=\"tag-img\" src=\"{{asset('images/icon/icon-hash.svg')}}\" alt=''>\
                                    <div>\
                                        <a href=\"{{url('/search/show?content=')}}"+tag["content"]+"\">"+tag["content"]+"</a>\
                                        <p>게시물 "+tag["total"]+"건</p>\
                                    </div>\
                                </div>\
                            </li>";
                        });
                        str += "</ul>";
                        $(".result").eq(1).html(str);
                    }else{
                        // 검색 결과 없음 :: 태그 검색
                        let str = "<div class='placeholder'><p>\'"+keyword+"\'에 대한<br> 검색 결과가 없습니다</p></div>";
                        $(".result").eq(1).html(str);
                    }
                }
            })
        }
    }

})

function relation(id, action){
    // id :: 친구 고유번호 / action :: 행동 (a : 신청, w : 수락대기중, y : 친구 수락)
    $.ajax({
        headers : {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
        url : "/profile/relation",
        type : "post",
        data : {"friend_idx":id, "action":action},
        dataType : "json",
        success : function(data){            
            let btn = $(".relation"+id);
            let what = "";
            if(action == "apply"){
                // 신청 -> 수락 대기중
                btn.attr("onclick","relation('"+id+"','wait')");
                btn.html("수락대기중");
                what = "apply";
            }else if(action == "wait"){
                // 수락대기중 -> 신청 (신청 취소)
                btn.attr("onclick","relation('"+id+"','apply')");
                btn.html("친구신청");
            }else if(action == "yes"){
                // 친구수락 -> 친구
                btn.attr("onclick","");
                btn.html("친구");
                what = "friend";
            }
        }
    })
}
</script>
@endsection