@extends('layouts.base') 
@section('contents')
<div id="wrap" class="main-wrap" data-id="{{isset($feed) ? $feed->id : ""}}">
    <div>
        <div class="sub-head col-group">
            <a onclick="cancel();"><img src="{{asset('images/icon/icon_arrow_left_s.svg')}}" alt=""></a>
            @if(isset($feed))
            <p>피드 수정</p> 
            @else 
            <p>피드 올리기</p> 
            @endif
            <a onclick="cancel();"><img src="{{asset('images/icon/icon_close.svg')}}" alt=""></a>
        </div>
        <!-- 서브헤더 -->
        <div class="sub-cont sub-cont02 together-wrap">
            <style>
                .alert {display: flex; align-items: center; width: 100%; border: 1px solid black; margin-top:10px; border-radius: 5px}
                .alert .left{width: 20px; padding: 10px;}
                .alert .right{width: calc(100% - 20px); padding: 10px; text-align:center;}
            </style>
            <div class="container">
                <div class="alert">
                    <div class="left">!</div>
                    <div class="right">
                        <p>불쾌감을 주거나 저작권에 위배되는</p>
                        <p>피드를 게시하면 무통보 삭제됩니다</p>
                    </div>
                </div>
            </div>
            <div class="cats-add-wrap">
                <div class="both">
                    <div class="plus">
                        <div class="col-group">
                            <div>
                                <input type="file" id="camera" multiple accept="image/*,video/*">
                                <label for="camera"><img src="{{asset('images/icon/icon-camera.svg')}}" alt=""></label>
                                <p><span id="img-cnt">{{isset($feed) ? count($feed->files) : "0"}}</span>/10</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-container uploadMySwiper col-group">
                        <ul id="img-box" class="img-add swiper-wrapper">
                            @if(isset($feed))
                                @foreach($feed->files as $file)
                                    @if($file->ext == "mp4")
                                    <li class="swiper-slide">
                                        <img class="feed-fn" src="{{asset('storage/uploads/feed/'.$file->reference)}}" alt="" data-fn="{{$file->reference}}" data-mime="video">
                                        <button class="btn-close"><img src="{{asset('images/icon/icon-close-b.svg')}}" alt=""></button>
                                    </li>
                                    @else 
                                    <li class="swiper-slide">
                                        <img class="feed-fn" src="{{asset('storage/uploads/feed/'.$file->fn)}}" alt="" data-fn="{{$file->fn}}" data-mime="image">
                                        <button class="btn-close"><img src="{{asset('images/icon/icon-close-b.svg')}}" alt=""></button>
                                    </li>
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="enter-box container">
                    <div>
                        <label for="introduce">내용</label>
                        <textarea name="" id="content" placeholder="문구를 입력하세요.">{{isset($feed) ? $feed->content : ""}}</textarea>
                    </div>
                    <div>
                        <label for="hash">해시태그</label>
                        <input type="text" id="hashTag" placeholder="키워드를 입력하세요.(최대 8개)">
                    </div>
                    <div id="hash-box" class="col-group">
                        @if(isset($feed))
                            @foreach($feed->tags as $tag)
                            <span class="tag">#{{$tag->content}}</span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- input -->
                <div class="button-box">
                    @if(isset($feed))
                    <button onclick="update();">업로드</button>
                    @else 
                    <button onclick="register();">업로드</button>
                    @endif
                </div>
            </div>
           
        </div>
        <!-- 서브바디 -->
    </div>  
</div>
<div id="alert-box" class="join-wrap-popup popup-ver hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3 id="alert-tit">알림</h3>
                    <p id="alert-msg">내용 문구를 입력하세요</p>
                </div>
                <button onclick="alert_close();">확인</button>
            </div>
        </div>
    </div>
</div>
<div id="confirm-box" class="join-wrap-popup popup02 hide">
    <div class="inner">
        <div class="inner-box">
            <div>
                <div>
                    <h3>알림</h3>
                    <p id="confirm-msg">작성중인 내용은 지워집니다<br>뒤로 이동하시겠습니까?</p>
                </div>
                <div style="display:flex">
                    <button onclick="confirm_false();">취소</button>
                    <button onclick="confirm_true();">확인</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap-progress hide">
    <div class="inner">
        <p>업로드 중입니다</p>
        <p>잠시만 기다려주세요</p>
        <img src="{{asset('images/icon/icon-loading.gif')}}" alt="">
    </div>
</div>
@endsection

@section('script')
<script>
var uploadSlider = new Swiper(".uploadMySwiper", {
    slidesPerView: 'auto',
    spaceBetween: 0,
});

let feed_id = $("#wrap").data("id"); // 피드 아이디

let tags = [];
$(".tag").each(function(){
    let hash = $(this).text();
    tags.push(hash.replace("#",""));
})

var sel_files = []; // 파일 배열
var sel_mimes = []; // 파일 형식 배열
$(".feed-fn").each(function(){
    sel_files.push($(this).data("fn"));
    sel_mimes.push($(this).data("mime"));
})

// 팝업창 닫기    
function alert_close(){
    $("#alert-box").addClass('hide');
}

// 피드 창 닫기
function cancel(){
    $("#confirm-box").removeClass("hide");
}

// 확인창 : 확인 누름
function confirm_true(){
    location.href="/home";
}

// 확인창 : 취소 누름
function confirm_false(){
    $("#confirm-box").addClass("hide");
}

$(document).ajaxStart(function(){
    $(".wrap-progress").removeClass('hide');
});

$(document).ajaxStop(function(){
    $(".wrap-progress").addClass("hide");
});

// 피드 업로드
function register(){

    const content = $("#content").val();
    $.ajax({
        headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/feed",
        type : "post",
        data : {"tags":tags, "content":content, "sel_files":sel_files, "sel_mimes":sel_mimes},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                alert("업로드 되었습니다");
                location.href = "/home";
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        },error : function(){

        }
    })
}

function update(){
    const content = $("#content").val();
    $.ajax({
        headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : "/feed/"+feed_id,
        type : "put",
        data : {"tags":tags, "content":content, "sel_files":sel_files, "sel_mimes":sel_mimes},
        dataType : "json",
        success : function(data){
            if(data["success"]){
                alert("업로드 되었습니다");
                location.href = "/home";
            }else{
                $("#alert-msg").html(data["msg"]);
                $("#alert-box").removeClass("hide");
            }
        },error : function(){

        }
    })
}

// 해시태그 이벤트 적용
$("#hashTag").on({
    focus : function(){
        // $("#hashTag").val("#");
    },
    blur : function(){
        $("#hashTag").val("");
    },
    input : function(){
        let letter = $("#hashTag").val();
        let regex = /[~!@\#$%^&*\()\-=+']/gi;

        if(letter.search(/\s/) > -1){
            console.log("space push");
            // 공백이 있음
            // 해시태그 추가 
            $("#hashTag").blur(); // 포커스 제거
            if(!add_hash($.trim(letter))){
                $("#alert-msg").html("이미 추가된 태그입니다");
                $("#alert-box").removeClass("hide");
            }
        }else if(regex.test(letter)){
            // 특수문자 제거
            let rep = letter.replace(regex, '');
            $("#hashTag").val(rep);
        }
    },
    keydown : function(e){
        let letter = $("#hashTag").val();
        if(e.keyCode == 13){
            // 엔터를 누름
            $("#hashTag").blur(); // 포커스 제거
            if(!add_hash($.trim(letter))){
                $("#alert-msg").html("이미 추가된 태그입니다");
                $("#alert-box").removeClass("hide");
            }
        }
    }
});

// 태그 삭제 
$(document).on("click",".tag",function(){
    let index = $('.tag').index(this);
    tags.splice(index, 1);
    $(this).remove();  
    console.log(tags);
});

// 태그 추가 함수
function add_hash(text){
    if(text == " " || text == ""){
        // 입력값이 없음
        return true;
    }else{
        // 입력값이 있음
        // 중복되는 입력값 체크
        // 없으면 추가 있으면 팝업
        for(var i = 0; i<tags.length; i++){
            if(tags[i] == text){
                return false;
            }
        }
        if(tags.length < 8){
            let str = "<span class='tag'>#"+text+"</span>";
            tags.push(text);
            $("#hash-box").append(str);
        }else{
            // 태그 최대 입력수에 도달하면
            $("#alert-msg").html("해시태그는 최대 8개까지 등록가능합니다<br>해시태그를 선택해 삭제하면 추가등록 가능합니다");
            $("#alert-box").removeClass("hide");
        }
        return true;
    } 
}

// 피드 사진 삭제 
$(document).on("click",".btn-close",function(){
    let img_cnt  = $('#img-cnt').text(); // 현재 추가된 이미지(동영상)의 수
    let index    = $(".btn-close").index(this); // 클릭한 X의 인덱스 

    $("#img-cnt").html(--img_cnt);
    $(this).parent("li").remove();
    sel_files.splice(index, 1);
    sel_mimes.splice(index, 1);
    available = available + 1;
    console.log(available);
    console.log(sel_files);
});

let available = 10 - $("#img-cnt").text(); // 파일 등록 가능 수


// 피드 사진 추가
$(document).on("change","#camera", function(){
    let oFile = $(this)[0].files;
    if(oFile.length < 1){

    }else{
        if(available < 1){
            $("#alert-msg").html("업로드는 최대 10장까지 가능합니다");
            $("#alert-box").removeClass("hide");
        }else{
            readImage(this);
        }
    }
});

function readImage(input) {
    // 인풋 태그에 파일이 있는 경우
    let img_cnt  = $('#img-cnt').text();
    if(input.files && input.files[0]) {

        var filesArr = Array.prototype.slice.call(input.files);

        // 업로드 최대 허용 숫자에 도달하면 -> 선택 수를 줄여줌
        if(filesArr.length > available){
            filesArr.splice(available); // 최대 가능 수 만큼 자름
        }

        filesArr.forEach(function(f){
            if(sel_files.length < 10){
                // 파일 실서버 전송
                $(".wrap-progress").removeClass('hide');
                available--;
                let formData = new FormData();
                formData.append('feedFile', f);
                $.ajax({
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url : "/feed/fileUpload",
                    type : "post",
                    data : formData, 
                    processData : false,
                    contentType : false, 
                    dataType : "json",
                    success : function(data){
                        if(data["success"]){
                            // 압축 성공
                            let str = "<li class='swiper-slide'>\
                                        <img src=\'{{asset('storage/uploads/temp')}}/"+data["fn"]+"\' alt='' data-fn='"+data["fn"]+"' data-mime='"+data["mimeType"]+"'>\
                                        <button class='btn-close'><img src=\'{{asset('images/icon/icon-close-b.svg')}}\' alt=''></button>\
                                    </li>";
                            uploadSlider.appendSlide(str);
                            sel_files.push(data["fn"]);
                            sel_mimes.push(data["mimeType"]);
                            $("#img-cnt").html(++img_cnt);
                        }else{
                            //압축 실패
                            $("#alert-msg").html(data["msg"]);
                            $("#alert-box").removeClass("hide");
                            available = available + 1;
                        }
                    }
                }); // ajax 끝
            }
        }); // foreach  끝
    }


}

function setProgress(per){
    $("#progressBar").val(per);
    $("#progress_rate").html(Math.round(per)+"%");
    if(per >= 100){
        $(".wrap-progress").addClass("hide");
    }
}
</script>
@endsection