<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Paginator::defaultView('vendor.pagination.default');

        Relation::morphMap([
            'members'    => 'App\Models\Member',
            'feeds'      => 'App\Models\Feed',
            'hash_tags'  => 'App\Models\HashTag',
            'cats'       => 'App\Models\Cat',
            'notices'    => 'App\Models\Notice',
            'events'     => 'App\Models\Event',
            'togethers'  => 'App\Models\Together',
            'together_notices'   => 'App\Models\Together_notice',
            'together_posts'     => 'App\Models\Together_post',
            'writings'   => 'App\Models\Writing',
        ]);
    }
}
