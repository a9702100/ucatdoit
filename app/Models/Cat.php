<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use HasFactory;

    public function member(){
        return $this->belongsTo('App\Models\Member');
    }

    public function files(){
        return $this->morphMany('App\Models\Upload', 'file');
    }
}
