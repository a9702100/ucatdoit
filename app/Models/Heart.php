<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Heart extends Model
{
    use HasFactory;

    public function heartable(){
        return $this->morphTo();
    }

    public function member(){
        return $this->belongsTo('App\Models\Member');
    }
}
