<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Writing extends Model
{
    use HasFactory;

    public function files(){
        return $this->morphMany('App\Models\Upload', 'file');
    }

    public function participation(){
        return $this->hasMany('App\Models\Participation', 'event_id');
    }
}
