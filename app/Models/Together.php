<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Together extends Model
{
    use HasFactory;

    public function files(){
        return $this->morphOne('App\Models\Upload', 'file');
    }

    public function member(){
        return $this->belongsTo('App\Models\Member');
    }
}
