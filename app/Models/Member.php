<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    public function feed(){
        return $this->morphOne('App\Models\Feed', 'feedable');
    }

    public function upload(){
        return $this->morphOne('App\Models\Upload', 'file');
    }

    public function cats(){
        return $this->hasMany('App\Models\Cat');
    }

    public function heart(){
        return $this->hasMany('App\Models\Heart', 'member_id');
    }

    public function reply(){
        return $this->hasMany('App\Models\Reply', 'member_id');
    }

    public function friend(){
        return $this->hasMany('App\Models\Friend', 'friend_idx');
    }

    public function ask(){
        return $this->hasMany('App\Models\Ask', 'member_id');
    }

    public function report(){
        return $this->hasMany('App\Models\Report', 'member_id');
    }

    public function history(){
        return $this->hasMany('App\Models\History', 'member_id');
    }

    public function together_members(){
        return $this->hasMany('App\Models\Together_member', 'member_id');
    }

    public function together_posts(){
        return $this->hasMany('App\Models\Together_post','member_id');
    }

    public function together(){
        return $this->hasMany('App\Models\Together','member_id');
    }

    public function participation(){
        return $this->hasMany('App\Models\Participation', 'member_id');
    }

    public function pushMessage(){
        return $this->hasOne('App\Models\PushMessage', 'member_id');
    }
}
