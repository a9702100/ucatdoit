<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use HasFactory;

    public function tags(){
        return $this->morphMany('App\Models\HashTag', 'taggable');
    }

    public function feedable(){
        return $this->morphTo();
    }

    public function files(){
        return $this->morphMany('App\Models\Upload', 'file');
    }

    public function heart(){
        return $this->morphMany('App\Models\Heart', 'heartable');
    }

    public function replies(){
        return $this->morphMany('App\Models\Reply', 'reply');
    }
}
