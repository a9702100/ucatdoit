<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use HasFactory;

    public function reply(){
        return $this->morphTo();
    }

    public function member(){
        return $this->belongsTo('App\Models\Member');
    }
}
