<?php

namespace App\Exports;

use App\Models\Participation;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PartiExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct(int $event_id, string $start_date, string $end_date)
    {
        $this->event_id = $event_id;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

    public function headings(): array
    {
        return [
            '고유번호',
            '아이디',
            '닉네임',
            '이름',
            '우편번호',
            '도로명주소',
            '상세주소',
            '전화번호',
            '참가일',
        ];
    }


    public function query()
    {
        return Participation::query()->leftJoin('members','members.id','=','participations.member_id')
                                    ->where('participations.event_id',$this->event_id)
                                    ->where(function($query){
                                        $query->whereDate('participations.created_at','>=',$this->start_date)
                                              ->whereDate('participations.created_at','<=',$this->end_date);
                                    })
                                    ->select('members.id','members.userId','members.nick','participations.name','participations.postcode','participations.roadAddr','participations.detailAddr','participations.phone','participations.created_at');
    }
}
