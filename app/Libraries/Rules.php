<?php
namespace App\Libraries;

use App\Models\Member;

class Rules{
    // 호출(@) 메세지로 만들기
    function make_callMsg($content){
        $rm_content = ""; // 변경된 내용
        $receivers = array();

        if($content == null){
            return false;
        }

        $explode = explode(' ', $content);
        foreach ($explode as $word) {
            if(strpos($word, '@') !== false){
                // 골뱅이가 포함 되어 있니?
                $pos = strrpos($word, '@'); // 골뱅이 위치를 반환 (뒤에서)
                $find_str = substr($word, $pos+1); // 언급된 닉네임 
    
                $member = Member::where('nick',$find_str);
                if($member->exists()){
                    // 존재하는 회원
                    $rm_content .= "<span class='call-member' data-member='".$member->first()->id."'>@".$find_str."</span> ";
                    array_push($receivers, $member->first()->id);
                }else{
                    $rm_content .= $word." ";
                }
            }else{
                $rm_content .= $word." ";
            }
        }

        $data = array("rm_content"=> $rm_content, "receivers"=>$receivers);
        return json_encode($data);
    }

    function passing_time($datetime){
        $time_lag = time() - strtotime($datetime);

        if($time_lag < 60){
            $posting_time = "방금전";
        }else if($time_lag >= 60 && $time_lag < 3600){
            $posting_time = floor($time_lag/60)."분 전";
        }else if($time_lag >= 3600 && $time_lag < 86400){
            $posting_time = floor($time_lag/3600)."시간 전";
        }else if($time_lag >= 86400 && $time_lag < 2419200){
            $posting_time = floor($time_lag/86400)."일 전";
        }else{
            $posting_time = date("y-m-d", strtotime($datetime));
        }

        return $posting_time;
    }

    function make_directUrl($detail_type, $detail_id){
        $direct_url = "";
        switch ($detail_type) {
            case 'good':
                $direct_url = "/feed/{$detail_id}/show";
                break;
            case 'friend':
                $direct_url = "/profile/myfriends?other={$detail_id}";
                break;
            case 'reply':
                $direct_url = "/feed/{$detail_id}/show";
                break;
            case 'together':

                break;
            case 'ask':
                $direct_url = "/ask";
                break;
            default:
                break;
        }
        return $direct_url;
    }
}