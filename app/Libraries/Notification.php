<?php
namespace App\Libraries;

use App\Libraries\Rules;

use App\Models\History;
use App\Models\Member;
use App\Models\PushMessage;
use App\Models\PushHistory;

define('GOOGLE_API_KEY', 'AAAAFdTjRvQ:APA91bEpRz_NbT8l1qHFfF3xmIzT2qOFUEm1E_KAP24Xk6zxIve7eJDcxcySh1u5Fvd4k35ssvAPmC50LHC8TUn4s2mBjyntV_B41-uW1hu4aW9bAOOWoN-w5vn59pMhp6IoXN1VWYDc');


class Notification{
    function noti_android($tokens, $title, $message){

        $url = "https://fcm.googleapis.com/fcm/send";

        $msg = ["title"=> $title, "body" => $message, "message" => $message];
        $fields = [
            "registration_ids" => $tokens,
            "notification" => ["title" => $title, "body" => $message],
            "data" => $msg
        ];

        $headers = array(
            'Authorization:key='.GOOGLE_API_KEY, 'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if($result === false){
            die('Curl failed: '.curl_error($ch));
        }else{

        }

        curl_close($ch);

        return $result;
    }

    function noti_apple($token, $title, $msg){
        $keyfile   = 'AuthKey_Q44SUK9XL2.p8';                  # <- Your AuthKey file
        $keyid     = "Q44SUK9XL2";                             # <- Your Key ID
        $teamid    = "GH9SDXJ4DG";                             # <- Your Team ID (see Developer Portal)
        $bundleid  = "com.roarcompany.ucatdoit";                   # <- Your Bundle ID
        $url     = 'https://api.push.apple.com';   # <- development url, or use http://api.push.apple.com for production environment // https://api.sandbox.push.apple.com
        // $token   = $APNSToken;   # <- Device Token
        $message = '{"aps" : {"alert" : {"title" : "'.$title.'", "body" : "'.$msg.'" }}}';
        $key = openssl_pkey_get_private('file://'.$keyfile);
        
        $header = ['alg'=>'ES256','kid'=>$keyid];
        $claims = ['iss'=>$teamid,'iat'=>time()];
        $header_encoded = rtrim(strtr(base64_encode(json_encode($header)), '+/', '-_'), '=');
        $claims_encoded = rtrim(strtr(base64_encode(json_encode($claims)), '+/', '-_'), '=');
        $signature = '';
        openssl_sign($header_encoded . '.' . $claims_encoded, $signature, $key, 'sha256');
        $jwt = $header_encoded . '.' . $claims_encoded . '.' . base64_encode($signature);
        // only needed for PHP prior to 5.5.24
        if (!defined('CURL_HTTP_VERSION_2_0')) {
            define('CURL_HTTP_VERSION_2_0', 3);
        }
        $http2ch = curl_init();
        curl_setopt_array($http2ch, array(
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
            CURLOPT_URL => "$url/3/device/$token",
            CURLOPT_PORT => 443,
            CURLOPT_HTTPHEADER => array(
            "apns-topic: {$bundleid}",
            "authorization: bearer $jwt"
            ),
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false, 
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $message,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HEADER => 1
        ));

        $result = curl_exec($http2ch);
        if ($result === FALSE) {
            throw new Exception("Curl failed: ".curl_error($http2ch));
        }
        curl_close($http2ch);
        // var_dump($result);

        return $result;
        
    }

    function send_noti($sender, $receiver, $push_type, $detail_id, $main_msg, $sub_msg){
        // $sender : 보내는 사람 , $receiver : 받는 사람, $push_type : 메세지 타입(피드, 좋아요 등등)

        $sd = Member::find($sender); // sender
        $rc = PushMessage::where('member_id', $receiver)->first(); // receiver

        if($rc->token != null){
            // 토큰이 존재하는 유저
            if($rc->$push_type == "1"){
                // 해당 메세지 타입을 수신 허용한 
                if($rc->type == "Android"){
                    // 토큰이 안드로이드
                    $this->noti_android([$rc->token], $sd->nick.$main_msg, $sub_msg);
                }else if($rc->type == "Apple"){
                    // 토큰이 애플
                    $this->noti_apple($rc->token, $sd->nick.$main_msg, $sub_msg);
                }
            }
        }

        $rules = new Rules();

        if($sub_msg != null){
            $sub_msg = json_decode($rules->make_callMsg($sub_msg))->rm_content;
        }

        $history = new History();
        $history->sender      = $sender;
        $history->receiver    = $receiver;
        $history->main_msg    = $main_msg;
        $history->sub_msg     = $sub_msg;
        $history->direct_url  = $rules->make_directUrl($push_type, $detail_id);
        $history->push_type   = $push_type;
        $history->detail_type = $push_type == "reply" ? "feed" : $push_type;
        $history->detail_id   = $detail_id;
        $history->save();
    }

    function adminToUser($receiver, $push_type, $main_msg, $sub_msg){
        // $receiver : 유저, $push_type : 메세지 타입, $main_msg : 메세지 타이틀, $sub_msg : 메세지 내용
        $rc = PushMessage::where('member_id', $receiver)->first(); // receiver

        if($rc->token != null){
            // 토큰이 존재하는 유저
            if($rc->$push_type == "1"){
                // 해당 메세지 타입을 수신 허용한 
                if($rc->type == "Android"){
                    // 토큰이 안드로이드
                    $this->noti_android([$rc->token], $main_msg, $sub_msg);

                }else if($rc->type == "Apple"){
                    // 토큰이 애플
                    $this->noti_apple($rc->token, $main_msg, $sub_msg);
                }

                $this->make_history($main_msg, $sub_msg, "회원선택", $receiver);
            }
        }
    }

    function make_history($main_msg, $sub_msg, $target, $members){
        $list = "";
        if(is_array($members)){
            foreach($members as $member){
                $m = Member::find($member);
                $list .= "{$m->nick}({$m->id}), ";
            }
        }else{
            $m = Member::find($members);
            $list .= "{$m->nick}({$m->id})";
        }

        $history = new PushHistory();
        $history->main_msg = $main_msg;
        $history->sub_msg = $sub_msg;
        $history->target = $target;
        $history->members = $list;
        $history->save();

    }
    
}