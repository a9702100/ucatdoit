<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Libraries\Notification;
use App\Libraries\Rules;

use App\Models\Reply;
use App\Models\Member;
use App\Models\Conceal;
use App\Models\Feed;
use App\Models\History;

class ReplyController extends Controller
{
    //

    public function reply(Request $request, $id){
        // 
        $member_id = $request->session()->get('member'); // 내 고유번호
        $writer_id = Feed::find($id)->feedable_id;       // 작성자 고유번호 

        $writer = $member_id == $writer_id ? "me" : "other"; // 피드 작성자가 누구냐? 

        $conceals = Conceal::where([['member_id', $member_id],['concealable_type','replies']])->get('concealable_id');
        if($conceals->isEmpty()){
            // 숨긴 댓글 없음
            $replies = Reply::where([['reply_type','feeds'],['reply_id', $id]])->orderBy('group_id','desc')->orderBy('level','asc')->get();
        }else{
            // 숨긴 댓글 있음
            $replies = Reply::where([['reply_type','feeds'],['reply_id',$id]])->orderBy('group_id','desc')->orderBy('level','asc')->whereNotIn('id',$conceals)->latest()->get();
        }

        if($replies->count() > 0){
            foreach($replies as $reply){
                $reply->nick      = $reply->member->nick;
                $reply->member_fn = $reply->member->upload != null ? $reply->member->upload->fn : "";
                $reply->own       = $reply->member_id == $member_id ? "me" : "other";
            }
            return response()->json([
                'status' => 'full', 'replies' => $replies, 'writer' => $writer
            ]);
        }else{
            return response()->json([
                'status' => 'empty', 'writer' => $writer
            ]);
        }
    }

    public function replyStore(Request $request, $id){
        $member_id = $request->session()->get('member');
        $level     = $request->input('level', 1);

        $rules = [
            'content' => 'required'
        ];

        $messages = [
            'content.required' => '댓글을 입력하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $rules = new Rules;
        $result_data = json_decode($rules->make_callMsg($request->content));

        $reply = new Reply();
        $reply->reply_type   = "feeds";
        $reply->reply_id     = $id;
        $reply->member_id    = $member_id;
        $reply->level        = $level;
        $reply->content      = $result_data->rm_content;
        $reply->save();

        if($request->has("target")){
            $group_id = Reply::find($request->input('target'))->group_id;
        }

        $reply->group_id = $level == 1 ? $reply->id : $group_id;
        $reply->save();

        $reply->nick = $reply->member->nick;
        $reply->member_fn = $reply->member->upload != null ? $reply->member->upload->fn : "";

        $main_msg    = "님이 나를 언급했습니다";

        foreach($result_data->receivers as $receiver){
            $noti = new Notification();
            $noti->send_noti($member_id, $receiver, "reply", $id, $main_msg, $request->content);
        }

        return response()->json([
            'success' => true, 'reply' => $reply
        ]);
    }

    public function replyDestroy(Request $request, $id){
        $nums = $request->input('nums');

        foreach($nums as $key => $num){
            $reply = Reply::find($num);
            if($reply->level == "1"){
                // 그룹을 지운다 -> 하위 댓글도 삭제함
                Reply::where([['reply_id',$id],['group_id',$reply->group_id]])->delete();
            }else{
                // 하위 댓글을 지운다
                $reply->delete();
            }
        }
        
        // 댓글리스트 새로 불러옴
        $member_id = $request->session()->get('member'); // 내 고유번호
        $writer_id = Feed::find($id)->feedable_id;       // 작성자 고유번호 

        $writer = $member_id == $writer_id ? "me" : "other"; // 피드 작성자가 누구냐? 

        $conceals = Conceal::where([['member_id', $member_id],['concealable_type','replies']])->get('concealable_id');
        if($conceals->isEmpty()){
            // 숨긴 댓글 없음
            $replies = Reply::where([['reply_type','feeds'],['reply_id', $id]])->orderBy('group_id','desc')->orderBy('level','asc')->get();
        }else{
            // 숨긴 댓글 있음
            $replies = Reply::where([['reply_type','feeds'],['reply_id',$id]])->orderBy('group_id','desc')->orderBy('level','asc')->whereNotIn('id',$conceals)->latest()->get();
        }

        if($replies->count() > 0){
            foreach($replies as $reply){
                $reply->nick      = $reply->member->nick;
                $reply->member_fn = $reply->member->upload != null ? $reply->member->upload->fn : "";
                $reply->own       = $reply->member_id == $member_id ? "me" : "other";
            }
            return response()->json([
                'status' => 'full', 'replies' => $replies, 'writer' => $writer, 'msg' => '삭제되었습니다'
            ]);
        }else{
            return response()->json([
                'status' => 'empty', 'writer' => $writer, 'msg' => '삭제되었습니다'
            ]);
        }

    }
    
}
