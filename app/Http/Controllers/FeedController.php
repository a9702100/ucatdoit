<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Feed;
use App\Models\HashTag;
use App\Models\Upload;
use App\Models\History;
use FFMpeg;

class FeedController extends Controller
{
    //
    public function feed(Request $request){
        $member_id = $request->session()->get('member');
        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('feed', compact('isHistory'));
    }

    public function feedStore(Request $request){
        $member_id = $request->session()->get('member');

        $rules = [
            'sel_files' => 'required',
            'content' => 'required',
        ];

        $messages = [
            'sel_files.required' => '사진은 최소 1장은 넣어주세요',
            'content.required' => '내용 문구를 입력하세요',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $feed = new Feed();
        $feed->feedable_type = "members";
        $feed->feedable_id   = $member_id;
        $feed->content       = $request->content;
        $feed->save();

        if($request->has('tags')){
            // 작성된 태그가 있으면
            foreach($request->input('tags') as $content){
                $tag = new HashTag();
                $tag->taggable_type = "feeds";
                $tag->taggable_id = $feed->id;
                $tag->content = $content;
                $tag->save();
            }
        }

        // 무조건 사진(동영상) 한장 이상은 있음
        foreach($request->sel_files as $key => $sel_file){
            if($request->sel_mimes[$key] == "image"){
                Storage::move('public/uploads/temp/'.$sel_file, 'public/uploads/feed/'.$sel_file);
                $upload = Upload::where([['file_type','feeds'],['fn',$sel_file]])->first(); // 피드에서 업로드 되었고, 파일명이 ~ 인 
                $upload->file_id = $feed->id;
                $upload->update();
            }else if($request->sel_mimes[$key] == "video"){
                Storage::move('public/uploads/temp/'.$sel_file, 'public/uploads/feed/'.$sel_file); // 썸네일 이동

                list($front, $ext) = explode(".", $sel_file);

                $upload = Upload::where([['file_type','feeds'],['reference',$sel_file]])->first(); // 피드에서 업로드 되었고, 파일명이 ~ 인 
                $video_path = "storage/uploads/temp/".$upload->fn;  // 동영상 파일 경로

                $ffmpeg = FFMpeg\FFMpeg::create(
                    array(
                        'ffmpeg.binaries'  => '/bin/ffmpeg',
                        'ffprobe.binaries' => '/bin/ffprobe',
                        'timeout'          => 3600, // The timeout for the underlying process
                        'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
                    )
                );
                $video = $ffmpeg->open($video_path);
                $video
                    ->filters()
                    ->resize(new FFMpeg\Coordinate\Dimension($upload->width, $upload->height))
                    ->synchronize();
                    
                $format = new FFMpeg\Format\Video\X264('aac','libx264');
                $format
                    ->setKiloBitrate(1000)
                    ->setAudioChannels(2)
                    ->setAudioKiloBitrate(256);

                $video_path = "storage/uploads/feed/".$front.".mp4"; // 새로운 저장경로
                $video
                    ->save($format, $video_path); 

                $ffprobe = FFMpeg\FFProbe::create();
                $dimensions = $ffprobe
                                ->streams($video_path)
                                ->videos()
                                ->first()
                                ->getDimensions();

                $upload->file_id = $feed->id;
                $upload->fn = $front.".mp4";
                $upload->ext = "mp4";
                $upload->size = round(filesize($video_path)/ 1024, 1); // kB로 표시;
                $upload->width = $dimensions->getWidth();
                $upload->height = $dimensions->getHeight();    
                $upload->update();

                Storage::delete('public/uploads/temp/'.$front.".mp4");

            }
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function feedEdit(Request $request, $id){
        $feed = Feed::find($id);
        $member_id = $request->session()->get('member');

        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인
        return view('feed', compact('feed','isHistory'));
    }

    public function feedUpdate(Request $request, $id){

        $member_id = $request->session()->get('member');

        $rules = [
            'sel_files' => 'required',
            'content' => 'required',
        ];

        $messages = [
            'sel_files.required' => '사진은 최소 1장은 넣어주세요',
            'content.required' => '내용 문구를 입력하세요',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $feed = Feed::find($id);
        $feed->content = $request->content;
        $feed->update();

        if($request->has('tags')){
            // 기존 태그 삭제 후 새로 추가
            $tag = HashTag::where([['taggable_type','feeds'],['taggable_id',$id]]); // 게시글 태그들
            $tag->delete(); // 해시태그 삭제
            // 넘어온 해시태그로 다시 생성
            foreach($request->input('tags') as $content){
                $tag = new HashTag();
                $tag->taggable_type  = "feeds";
                $tag->taggable_id    = $id;
                $tag->content        = $content;
                $tag->save();
            }

        }

        $old_files = Upload::where([['file_type','feeds'],['file_id',$id]])->get();
        // 기존 파일을 임시 폴더로 이동
        foreach($old_files as $old_file){
            if($old_file->ext == "mp4"){
                // 동영상 파일이면 썸네일도 이동
                Storage::move('public/uploads/feed/'.$old_file->reference, 'public/uploads/temp/'.$old_file->reference);
            }
            Storage::move('public/uploads/feed/'.$old_file->fn, 'public/uploads/temp/'.$old_file->fn);
            $upload = Upload::where([['file_type','feeds'],['fn',$old_file->fn]])->first(); // 피드에서 업로드 되었고, 파일명이 ~ 인 
            $upload->file_id = 0;
            $upload->update();
        }

        // 파일 목록
        foreach($request->sel_files as $key => $sel_file){
            if($request->sel_mimes[$key] == "image"){
                Storage::move('public/uploads/temp/'.$sel_file, 'public/uploads/feed/'.$sel_file);
                $upload = Upload::where([['file_type','feeds'],['fn',$sel_file]])->first(); // 피드에서 업로드 되었고, 파일명이 ~ 인 
                $upload->file_id = $id;
                $upload->update();
            }else if($request->sel_mimes[$key] == "video"){
                Storage::move('public/uploads/temp/'.$sel_file, 'public/uploads/feed/'.$sel_file); // 썸네일 이동

                list($front, $ext) = explode(".", $sel_file);

                $upload = Upload::where([['file_type','feeds'],['reference',$sel_file]])->first(); // 피드에서 업로드 되었고, 파일명이 ~ 인 
                $video_path = "storage/uploads/temp/".$upload->fn;  // 동영상 파일 경로

                $ffmpeg = FFMpeg\FFMpeg::create(
                    array(
                        'ffmpeg.binaries'  => '/bin/ffmpeg',
                        'ffprobe.binaries' => '/bin/ffprobe',
                        'timeout'          => 3600, // The timeout for the underlying process
                        'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
                    )
                );
                $video = $ffmpeg->open($video_path);
                $video
                    ->filters()
                    ->resize(new FFMpeg\Coordinate\Dimension($upload->width, $upload->height))
                    ->synchronize();
                    
                $format = new FFMpeg\Format\Video\X264('aac','libx264');
                $format
                    ->setKiloBitrate(1000)
                    ->setAudioChannels(2)
                    ->setAudioKiloBitrate(256);

                $video_path = "storage/uploads/feed/".$front.".mp4"; // 새로운 저장경로
                $video
                    ->save($format, $video_path); 

                $ffprobe = FFMpeg\FFProbe::create();
                $dimensions = $ffprobe
                                ->streams($video_path)
                                ->videos()
                                ->first()
                                ->getDimensions();

                $upload->file_id = $id;
                $upload->fn = $front.".mp4";
                $upload->ext = "mp4";
                $upload->size = round(filesize($video_path)/ 1024, 1); // kB로 표시;
                $upload->width = $dimensions->getWidth();
                $upload->height = $dimensions->getHeight();    
                $upload->update();

            }
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function feedDestroy(Request $request, $id){
        $feed = Feed::find($id);
        // 지울 항목
        // 1. 피드 관련 업로드 파일
        $del_files = $feed->files;
        if($del_files->count() > 0){
            foreach($del_files as $del_file){
                Storage::delete('public/uploads/feed/'.$del_file->fn);
                $del_file->delete();
            }
        }
        // 2. 피드 태그
        $del_tags = $feed->tags;
        if($del_tags->count() > 0){
            $feed->tags()->delete();
        }
        // 3. 댓글
        $del_replies = $feed->replies;
        if($del_replies->count() > 0){
            $feed->replies()->delete();
        }
        // 4. 하트
        $del_hearts = $feed->heart;
        if($del_hearts->count() > 0){
            $feed->heart()->delete();
        }
        // 마지막. 피드 
        $feed->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function feedShow(Request $request, $id){
        $member_id = $request->session()->get('member');
        $feed = Feed::find($id);

        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('main-detail', compact('feed','member_id','isHistory'));
    }

    public function fileUpload(Request $request){

        // 파일의 타입이 비디오가 아님
        $rules = [
            'feedFile' => 'mimetypes:video/*,image/jpeg,image/png,image/gif'
        ];

        $messages = [
            'feedFile.mimetypes' => '지원하지 않는 확장자입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg'=>$message
            ]);
        }

        $feedFile = $request->file('feedFile');

        list($msec, $sec) = explode(" ", microtime());
        $ext = $feedFile->extension();
        $front = $sec.substr($msec, 2, 6);
        $fn = $front.".".$ext;
        $feedFile->storeAs('public/uploads/temp/', $fn); // 원본 저장
        
        $mimetypes = $request->file('feedFile')->getMimeType();
        if(strpos($mimetypes, "video") === false){
    
            $img_path = "storage/uploads/temp/".$fn;  // 이미지 파일 경로
            //gif, jpeg, png
            if($ext == "jpg" || $ext == "jpeg"){
                $img = imagecreatefromjpeg($img_path);
            }else if($ext == "gif"){
                $img = imagecreatefromgif($img_path);
            }else if($ext == "png"){
                $img = imagecreatefrompng($img_path);
                // 배경 투명 유지
                imagealphablending( $img, false );
                imagesavealpha( $img, true );
            }
    
            $exif = @exif_read_data($img_path);
            if(!empty($exif['Orientation'])){
                switch($exif['Orientation']){
                case 8:
                    $img = imagerotate($img, 90, 0); break;
                case 3:
                    $img = imagerotate($img, 180, 0); break;
                case 6:
                    $img = imagerotate($img, -90, 0); break;
                }
            }
            
            $img = imagescale($img, 640, -1);

            if($ext == "jpg" || $ext == "jpeg"){
                imagejpeg($img, $img_path, 75); // 3th : 0~9 압축률
            }else if($ext == "gif"){
                imagegif($img, $img_path, 75); // 3th : 0~9 압축률
            }else if($ext == "png"){
                imagepng($img, $img_path, 7); // 3th : 0~9 압축률
            }
            $img_info = getimagesize($img_path);
            
            $upload = new Upload();
            $upload->file_type   = "feeds";
            $upload->file_id     = 0;
            $upload->fn_ori      = $feedFile->getClientOriginalName();
            $upload->fn          = $fn;
            $upload->ext         = $ext;
            $upload->size        = round(filesize($img_path)/ 1024, 1); // kB로 표시
            $upload->width       = $img_info[0];
            $upload->height      = $img_info[1];
            $upload->save();
            
            imagedestroy($img);
    
            return response()->json([
                'success' => true, 'fn' => $fn, 'mimeType' => 'image'
            ]);
        }else{
            // 파일 타입이 비디오
            $video_path = "storage/uploads/temp/".$fn;  // 동영상 파일 경로

            $ffmpeg = FFMpeg\FFMpeg::create(
                array(
                    'ffmpeg.binaries'  => '/bin/ffmpeg',
                    'ffprobe.binaries' => '/bin/ffprobe',
                    'timeout'          => 3600, // The timeout for the underlying process
                    'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
                )
            );
            $video = $ffmpeg->open($video_path);

            // 썸네일 추출
            $video 
                ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(1))
                ->save('storage/uploads/temp/'.$front.'.jpg');

            $img_info = getimagesize('storage/uploads/temp/'.$front.'.jpg');

            $upload = new Upload();
            $upload->file_type   = "feeds";
            $upload->file_id     = 0;
            $upload->fn_ori      = $feedFile->getClientOriginalName();
            $upload->fn          = $fn;
            $upload->ext         = $ext;
            $upload->width       = $img_info[0];
            $upload->height      = $img_info[1];
            $upload->size        = round(filesize($video_path)/ 1024, 1); // kB로 표시
            $upload->reference   = $front.".jpg";
            $upload->save();
    
            return response()->json([
                'success' => true, 'fn' => $upload->reference, 'mimeType' => 'video'
            ]);

        }
    }

    public function feedShare(Request $request, $id){
        $feed = Feed::where('id',$id);

        $isFeed = $feed->exists();
        if($isFeed){
            // 존재하는 피드
            $feed = $feed->first();
            return view('feedShare', compact('isFeed','feed'));
        }else{
            // 존재하지 않는 피드
            return view('feedShare', compact('isFeed'));
        }
    }

    public function download(Request $request){
        return Storage::download('public/uploads/feed/'.$request->fn);
    }
}