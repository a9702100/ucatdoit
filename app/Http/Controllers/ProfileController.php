<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

use App\Libraries\Notification;

use App\Models\Member;
use App\Models\Upload;
use App\Models\Feed;
use App\Models\Cat;
use App\Models\Friend;
use App\Models\History;
use App\Models\Conceal;

class ProfileController extends Controller
{
    //
    public function profile(Request $request){
        $member_id   = $request->session()->get('member');
        $other       = $request->input('other', '0');

        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        if($member_id == $other || $other == 0){
            // 나랑 같은번호 (내 프로필로 이동)
            $member  = Member::find($member_id);
            // $feeds   = Feed::where([['feedable_type','members'],['feedable_id',$member_id]])->latest()->get();
            $feeds   = Feed::where([['feedable_type','members'],['feedable_id',$member_id]])->latest()->take(30)->get();

            return view('profile.mypage', compact('member', 'feeds','isHistory'));
        }else{
            // 친구 페이지로 이동
            $friend   = Member::find($other);
            $relation = Friend::where([['my_idx',$member_id],['friend_idx',$other]])->first();

            $feeds    = Feed::where([['feedable_type','members'],['feedable_id',$other]]);
            $conceals = Conceal::where([['member_id',$member_id],['concealable_type','feeds']])->get('concealable_id');  // 숨긴 게시글 번호
            if($conceals->isNotEmpty()){
                $feeds   = $feeds->whereNotIn('id', $conceals);
            }
            // $feeds = $feeds->latest()->get();
            $feeds = $feeds->latest()->take(30)->get();
            
            isset($relation) ? $relation = $relation->relation : $relation = "not";

            return view('profile.friendpage', compact('friend', 'feeds','relation','isHistory'));
        }

    }

    public function moreFeed(Request $request){
        $member_id  = $request->session()->get('member'); // 로그인 계정 번호
        $last_id    = $request->feed_id;                  // 마지막 피드 번호
        $profile_id = $request->profile_id;              // 프로필 주인

        $feeds = Feed::where([['feedable_id', $profile_id],['id', '<', $last_id]]);
        if($profile_id != $member_id){
            // 타인 프로필 추가 불러오기
            $conceals = Conceal::where([['member_id', $member_id],['concealable_type', 'feeds']])->get('concealable_id'); // 숨긴 피드 번호
            if($conceals->isNotEmpty()){
                $feeds = $feeds->whereNotIn('id', $conceals);
            }
        }
        $feeds = $feeds->latest()->take(15)->get()->load('files');

        if($feeds->count() > 0){
            return response()->json([
                'success' => true, 'feeds' => $feeds, 'last' => $feeds->last()->id
            ]);
        }else{
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function profileUpdate(Request $request){

        $member_id = $request->session()->get('member'); //회원 번호
        $member    = Member::find($member_id);


        if($member->nick == $request->nick){
            // 동일한 닉네임
        }else{
            $rules = [
                'nick' => 'required|not_regex:/[^0-9가-힣a-z]/i|between:2,8|unique:members'
            ];
    
            $messages = [
                'nick.required'    => '사용할 닉네임을 입력하세요',
                'nick.not_regex'   => '닉네임은 한글, 영어, 숫자로만 입력해야합니다',
                'nick.between'     => '닉네임은 최소 2자 ~ 최대 8자 입니다',
                'nick.unique'      => '이미 사용중인 닉네임 입니다'
            ];
    
            $validator = Validator::make($request->all(), $rules, $messages);
            $errors = $validator->errors();
            foreach($errors->all() as $message){
                return response()->json([
                    'success' => false, 'msg' => $message
                ]);
            }
        }
        $member->nick    = $request->nick;
        $member->intro   = $request->intro;
        $member->update();
        
        if($request->profile_img != null){
            // 넘어온 이미지 있음
            if($member->upload == null){
                // 회원사진 없음
                Storage::move('public/uploads/temp/'.$request->profile_img, 'public/uploads/profile/'.$request->profile_img);
                $new = Upload::where([['file_type','members'],['fn', $request->profile_img]])->first(); //
                $new->file_id = $member_id;
                $new->update();
            }else{
                // 회원사진 있음
                $old_fn = $member->upload->fn;
                if($old_fn != $request->profile_img){
                    // 기존 회원사진과 받아온 이미지 파일명이 다름(새로운 이미지 업로드)
                    Storage::delete('public/uploads/profile/'.$old_fn); // 기존 파일 삭제
                    $member->upload->delete(); // DB 삭제
                    
                    Storage::move('public/uploads/temp/'.$request->profile_img, 'public/uploads/profile/'.$request->profile_img);
                    $new = Upload::where([['file_type','members'],['fn', $request->profile_img]])->first(); //
                    $new->file_id = $member_id;
                    $new->update();
                }
            }
        }

        return response()->json([
            'success' => true, 'msg' => '저장되었습니다'
        ]);
    }
    
    // 고양이 목록 

    public function catsIndex(Request $request){
        $member_id = $request->session()->get('member');
        $other = $request->input('other', $member_id);
        
        $cats = Cat::where('member_id',$other)->get();
        if($other == $member_id){
            // 내 고양이 목록
            return view('profile.mypage-cats', compact('cats'));
        }else{
            // 친구 고양이 목록
            return view('profile.mypage-friend-cats-list', compact('cats'));
        }

    }
    public function catsCreate(){
        return view('profile.mypage-cats-add');
    }

    public function catsStore(Request $request){
        $member_id = $request->session()->get('member');

        $rules = [
            'sel_files'  => 'required',
            'name'       => 'required',
            'gender'     => 'max:10',
            'ability'    => 'max:10',
        ];

        $messages = [
            'sel_files.required'  => '사진은 최소 1장은 넣어주세요',
            'name.required'       => '이름은 필수입니다',
            'gender.max'          => '성별은 10자 이하로 작성해주세요',
            'ability.max'         => '특징은 10자 이하로 작성해주세요',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);       
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $cat = new Cat();
        $cat->name    = $request->name;
        $cat->age     = $request->age == "" ? "" : $request->age;
        $cat->gender  = $request->gender == "" ? "" : $request->gender;
        $cat->ability = $request->ability == "" ? "" : $request->ability; 
        $cat->intro   = $request->input('intro',' ');
        $cat->member_id = $member_id;
        $cat->save();

        foreach($request->sel_files as $sel_file){
            Storage::move('public/uploads/temp/'.$sel_file, 'public/uploads/cat/'.$sel_file);
            $upload = Upload::where([['file_type', 'cats'],['fn',$sel_file]])->first(); 
            $upload->file_id = $cat->id;
            $upload->update();
        }

        return response()->json([
            'success' => true, 'msg' => '저장되었습니다'
        ]);
    }
    public function catsShow(Request $request, $id){
        
        $cat = Cat::find($id);

        return view('profile.mypage-cats-list', compact('cat'));
    }

    public function catsDestroy(Request $request, $id){
        // 파일 지우고
        // 목록에서 지우고

        $del_files = Upload::where([['file_type','cats'],['file_id',$id]])->get();
        foreach($del_files as $del_file){
            Storage::delete('public/uploads/cat/'.$del_file->fn);
            $del_file->delete(); 
        }

        Cat::find($id)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }


    public function catsEdit($id){
        $cat = Cat::find($id);

        return view('profile.mypage-cats-add', compact('cat'));
    }

    public function catsUpdate(Request $request, $id){
        $member_id = $request->session()->get('member');

        $rules = [
            'sel_files'  => 'required',
            'name'       => 'required',
            'gender'     => 'max:10',
            'ability'    => 'max:10',
        ];

        $messages = [
            'sel_files.required'  => '사진은 최소 1장은 넣어주세요',
            'name.required'       => '이름은 필수입니다',
            'gender.max'          => '성별은 10자 이하로 작성해주세요',
            'ability.max'         => '특징은 10자 이하로 작성해주세요',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);       
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $cat = Cat::find($id);
        $cat->name    = $request->name;
        $cat->age     = $request->age == "" ? "" : $request->age;
        $cat->gender  = $request->gender == "" ? "" : $request->gender;
        $cat->ability = $request->ability == "" ? "" : $request->ability; 
        $cat->intro = $request->input('intro',' ');
        $cat->update();

        $old_files = Upload::where([['file_type','cats'],['file_id',$id]])->get();
        // 기존 파일을 임시 폴더로 이동
        foreach($old_files as $old_file){
            Storage::move('public/uploads/cat/'.$old_file->fn, 'public/uploads/temp/'.$old_file->fn);
            $upload = Upload::where([['file_type','cats'],['fn',$old_file->fn]])->first(); // 피드에서 업로드 되었고, 파일명이 ~ 인 
            $upload->file_id = 0;
            $upload->update();
        }
        // 파일 목록
        foreach($request->sel_files as $sel_file){
            Storage::move('public/uploads/temp/'.$sel_file, 'public/uploads/cat/'.$sel_file);
            $upload = Upload::where([['file_type', 'cats'],['fn',$sel_file]])->first(); 
            $upload->file_id = $id;
            $upload->update();
        }

        return response()->json([
            'success' => true, 'msg' => '수정되었습니다'
        ]);
    }

    
    public function myfriends(Request $request){
        $member_id = $request->session()->get('member');
        $other     = $request->input('other', $member_id);
        $tab       = $request->input('tab','1');
        
        if($other == $member_id){
            // 내 친구 목록
            $from    = Friend::where([['my_idx',$other],['relation','yes']])->get(); // 요청받은
            $friends = Friend::where([['my_idx',$other],['relation','friend']])->get(); // 친구들
            $to      = Friend::where([['my_idx',$other],['relation','wait']])->get(); // 요청대기중
            $blocks  = Friend::where([['my_idx',$other],['relation','block']])->get(); // 차단
            
            return view('profile.mypage-friends', compact('from','friends','to','blocks','tab','other','member_id'));
        }else{
            // 다른사람의 친구목록
            $friends = Friend::where([['my_idx',$other],['relation','friend']])->get(); // 친구들
            return view('profile.mypage-friends', compact('friends','tab','other','member_id'));
        }

    }

    // 친구 관계
    public function relation(Request $request){
        $my_idx      = $request->session()->get('member'); // 내 번호
        $friend_idx  = $request->friend_idx; // 친구 번호
        $action      = $request->action;

        if($action == "apply"){ // :: 친구에게 친구 신청
            // 내가 친구에게 행동을 함
            Friend::updateOrInsert(['my_idx' => $my_idx,'friend_idx' => $friend_idx],['relation' => 'wait']);
            // 친구가 나에게 행동을 당함
            Friend::updateOrInsert(['my_idx' => $friend_idx,'friend_idx' => $my_idx],['relation' => "yes"]);
            
            $main_msg = "님이 친구 신청했습니다";
            $sub_msg = "친구가 되려면 수락을 눌러주세요";

            $noti = new Notification();
            $noti->send_noti($my_idx, $friend_idx, "friend", $friend_idx, $main_msg, $sub_msg);
            
        }else if($action == "wait" || $action == "no" || $action == "del" || $action == "unlock"){ // :: 친구에게 친구 신청 철회 또는 거절 또는 삭제 또는 차단 해제
            Friend::where([['my_idx',$my_idx],['friend_idx',$friend_idx]])->orWhere([['friend_idx',$my_idx],['my_idx',$friend_idx]])->delete();            
        }else if($action == "yes"){ // :: 친구 제안을 수락 
            // 내가 친구에게 행동을 함
            Friend::updateOrInsert(['my_idx' => $my_idx,'friend_idx' => $friend_idx],['relation' => 'friend']);
            // 친구가 나에게 행동을 당함
            Friend::updateOrInsert(['my_idx' => $friend_idx,'friend_idx' => $my_idx],['relation' => "friend"]);

            $main_msg = "님과 친구되었습니다";

            $noti = new Notification();
            $noti->send_noti($my_idx, $friend_idx, "friend", $friend_idx, $main_msg, "");
        }else if($action == "block"){ // :: 내가 친구를 차단
            // 내가 친구에게 행동을 함
            Friend::updateOrInsert(['my_idx' => $my_idx,'friend_idx' => $friend_idx],['relation' => 'block']);
            // 친구가 나에게 행동을 당함
            Friend::updateOrInsert(['my_idx' => $friend_idx,'friend_idx' => $my_idx],['relation' => "blind"]);
        }else if($action == "p_block"){
            // 완전 차단
            $conceal = new Conceal();
            $conceal->member_id        = $my_idx;
            $conceal->concealable_type = "members";
            $conceal->concealable_id   = $friend_idx;
            $conceal->save();
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function fileUpload(Request $request, $type){

        $rules = [
            'upload_img' => 'mimetypes:image/jpeg,image/png,image/gif'
        ];

        $messages = [
            'upload_img.mimetypes' => '지원하지 않는 확장자입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg'=>$message
            ]);
        }

        $upload_img = $request->file('upload_img');

        list($msec, $sec) = explode(" ", microtime());
        $ext = $upload_img->extension();
        $fn = $sec.substr($msec, 2, 6).".".$ext;
        $upload_img->storeAs('public/uploads/temp/', $fn); // 원본 저장
            
        $img_path = "storage/uploads/temp/".$fn;  // 이미지 파일 경로
        //gif, jpeg, png
        if($ext == "jpg" || $ext == "jpeg"){
            $img = imagecreatefromjpeg($img_path);
        }else if($ext == "gif"){
            $img = imagecreatefromgif($img_path);
        }else if($ext == "png"){
            $img = imagecreatefrompng($img_path);
            // 배경 투명 유지
            imagealphablending( $img, false );
            imagesavealpha( $img, true );
        }

        $exif = @exif_read_data($img_path);
        if(!empty($exif['Orientation'])){
            switch($exif['Orientation']){
            case 8:
                $img = imagerotate($img, 90, 0); break;
            case 3:
                $img = imagerotate($img, 180, 0); break;
            case 6:
                $img = imagerotate($img, -90, 0); break;
            }
        }
        $img = imagescale($img, 640, -1);
                
        if($ext == "jpg" || $ext == "jpeg"){
            imagejpeg($img, $img_path, 75); // 3th : 0~9 압축률
        }else if($ext == "gif"){
            imagegif($img, $img_path, 75); // 3th : 0~9 압축률
        }else if($ext == "png"){
            imagepng($img, $img_path, 7); // 3th : 0~9 압축률
        }

        $img_info = getimagesize($img_path);

        $upload = new Upload();
        $upload->file_type   = $type;
        $upload->file_id     = 0;
        $upload->fn_ori      = $upload_img->getClientOriginalName();
        $upload->fn          = $fn;
        $upload->ext         = $ext;
        $upload->size        = round($upload_img->getSize() / 1024, 1); // kB로 표시
        $upload->width       = $img_info[0];
        $upload->height      = $img_info[1];
        $upload->save();

        return response()->json([
            'success' => true, 'filesrc' => $img_path, 'fn' => $fn
        ]);

    }

    public function inputCheck(Request $request){
        $member_id = $request->session()->get('member');
        $member = Member::find($member_id);

        $input = $request->input("input");
        if($input == "nick"){
            if($member->nick == $request->nick){
                // 동일한 닉네임
                return response()->json([
                    'success' => false, 'msg' => '현재 사용중인 닉네임 입니다'
                ]);
            }else{
                // 새로 입력한 닉네임
                $rules = [
                    'nick' => 'required|not_regex:/[^0-9가-힣a-z]/i|between:2,8|unique:members'
                ];
        
                $messages = [
                    'nick.required'    => '사용할 닉네임을 입력하세요',
                    'nick.not_regex'   => '닉네임은 한글, 영어, 숫자로만 입력해야합니다',
                    'nick.between'     => '닉네임은 최소 2자 ~ 최대 8자 입니다',
                    'nick.unique'      => '이미 사용중인 닉네임 입니다'
                ];
            }
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    
    }
}
