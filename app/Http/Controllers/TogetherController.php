<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Together;
use App\Models\Together_member;
use App\Models\Together_notice;
use App\Models\Together_post;
use App\Models\Upload;
use App\Models\Member;

class TogetherController extends Controller
{
    //
    public function together(Request $request){
        $member_id = $request->session()->get('member');

        // 전체
        $together_all = Together::latest()->get();
        foreach($together_all as $together){
            $together->isParti = Together_member::where([['together_id',$together->id],['member_id',$member_id]])->exists() == true ? "1" : "0";
            $together->member_cnt = Together_member::where('together_id',$together->id)->get()->count();
        }

        // 내가 참가한 모임
        $t_number = Together_member::where('member_id',$member_id)->get('together_id');
        $together_parti = Together::whereIn('id', $t_number)->latest()->get();
        foreach($together_parti as $together){
            $together->member_cnt = Together_member::where('together_id',$together->id)->get()->count();
        }

        // 내가 만든 모임
        $together_me = Together::where('member_id',$member_id)->latest()->get();

        return view('together.together', compact('together_all','together_parti','together_me'));
    }
   
    public function togetherCreate(){
        return view('together.together-add');
    }

    public function togetherStore(Request $request){
        $rules = [
            'title' => 'required',
            'addr1' => 'required',
            'addr2' => 'required',
            'capacity' => 'required|numeric|between:1,1000',
            'intro' => 'required',
            'imgFile' => 'required'
        ];

        $messages = [
            'title.required' => '투게더 이름을 입력하세요',
            'addr1.required' => '지역[시/도]을 선택하세요',
            'addr2.required' => '지역[구/군]을 선택하세요',
            'capacity.required' => '제한인원수를 입력하세요',
            'capacity.numeric' => '제한인원수는 반드시 숫자로 입력하세요',
            'capacity.between' => '제한인원수는 최대 1000명 입니다',
            'intro.required' => '투게더 소개글을 입력하세요',
            'imgFile.required' => '투게더 메인이미지를 넣어주세요',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $member_id = $request->session()->get('member');

        // 투게더 생성
        $together = new Together();
        $together->member_id = $member_id;
        $together->title = $request->title;
        $together->intro = $request->intro;
        $together->addr1 = $request->addr1;
        $together->addr2 = $request->addr2;
        $together->capacity = $request->capacity;
        $together->save();

        // 메인 이미지 파일 이동
        Storage::move('public/uploads/temp/'.$request->imgFile, 'public/uploads/together/'.$request->imgFile);
        $upload = Upload::where([['file_type','togethers'],['fn',$request->imgFile]])->first();
        $upload->file_id = $together->id;
        $upload->update();
        
        return response()->json([
            'success' => true, 'msg' => '생성되었습니다'
        ]);
    }


    public function togetherShow(Request $request, $id){
        $member_id  = $request->session()->get('member');
        $together   = Together::find($id);
        $notices    = Together_notice::where('together_id',$id)->get();
        $posts_all  = Together_post::where('together_id',$id)->latest()->get();
        $posts_me   = Together_post::where([['together_id',$id],['member_id',$member_id]])->latest()->get();

        return view('together.together-group', compact('member_id','together','notices','posts_all','posts_me'));
    }
    
    public function togetherEdit(Request $request, $id){
        $tab = $request->input('tab','1');

        $members     = Together_member::where([['together_id',$id],['state','normal']])->latest()->paginate(20);
        foreach($members as $member){
            $member->post_cnt = Together_post::where([['together_id',$id],['member_id',$member->member_id]])->get()->count();
        }
        $blocks      = Together_member::where([['together_id',$id],['state','block']])->latest()->paginate(20);
        $notices     = Together_notice::where('together_id',$id)->latest()->paginate(20);        
        $together    = Together::find($id);
        return view('together.together-adm', compact('tab','members','blocks','together','notices'));
    }

    public function togetherUpdate(Request $request, $id){
        $rules = [
            'title' => 'required',
            'addr1' => 'required',
            'addr2' => 'required',
            'capacity' => 'required|numeric|between:1,1000',
            'intro' => 'required',
            'imgFile' => 'required'
        ];

        $messages = [
            'title.required' => '투게더 이름을 입력하세요',
            'addr1.required' => '지역[시/도]을 선택하세요',
            'addr2.required' => '지역[구/군]을 선택하세요',
            'capacity.required' => '제한인원수를 입력하세요',
            'capacity.numeric' => '제한인원수는 반드시 숫자로 입력하세요',
            'capacity.between' => '제한인원수는 최대 1000명 입니다',
            'intro.required' => '투게더 소개글을 입력하세요',
            'imgFile.required' => '투게더 메인이미지를 넣어주세요',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $member_id = $request->session()->get('member');

        // 투게더 생성
        $together = Together::find($id);
        $together->member_id = $member_id;
        $together->title = $request->title;
        $together->intro = $request->intro;
        $together->addr1 = $request->addr1;
        $together->addr2 = $request->addr2;
        $together->capacity = $request->capacity;
        $together->update();

        $old_img = Upload::where([['file_type','togethers'],['file_id',$id]])->first()->fn;
        if($old_img != $request->imgFile){
            // 새로운 이미지
            Storage::delete('public/uploads/together/'.$old_img);
            Upload::where([['file_type','togethers'],['file_id',$id]])->update(['file_id' => '0']);

            // 메인 이미지 파일 이동
            Storage::move('public/uploads/temp/'.$request->imgFile, 'public/uploads/together/'.$request->imgFile);
            $upload = Upload::where([['file_type','togethers'],['fn',$request->imgFile]])->first();
            $upload->file_id = $together->id;
            $upload->update();
        }

        return response()->json([
            'success' => true, 'msg' => '수정되었습니다'
        ]); 
    }

    public function togetherDestroy(Request $request, $id){
        // 투게더 [$id = 투게더 고유번호]
        // 1. 가입 회원 삭제
        Together_member::where('together_id', $id)->delete();
        
        // 2. 공지사항 삭제 (이미지 포함)
        $notices = Together_notice::where('together_id', $id);
        $notices_files = Upload::where('file_type','together_notices')->whereIn('file_id', $notices->get('id'));
        if($notices_files->get()->count() > 0){
            // 공지사항 업로드 파일이 있음
            foreach($notices_files->get() as $file){
                Storage::delete('public/uploads/together/notice/'.$file->fn);
            }
            $notices_files->delete();
        }
        $notices->delete();
        
        // 3. 게시글 삭제 (이미지 포함)
        $posts = Together_post::where('together_id', $id);
        $posts_files = Upload::where('file_type', 'together_posts')->whereIn('file_id', $posts->get('id'));
        if($posts_files->get()->count() > 0){
            // 포스트 업로드 파일이 있음
            foreach($posts_files->get() as $file){
                Storage::delete('public/uploads/together/post/'.$file->fn);
            }
            $posts_files->delete();
        }
        $posts->delete();
        
        // 4. 투게더 삭제
        Together::find($id)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }

    public function partiIn(Request $request){
        $together_id = $request->together_id;

        $together = Together::find($together_id);
        $members = Together_member::where('together_id',$together_id)->get()->count(); // 현재 참여자 수

        if($members < $together->capacity){
            // 수용 인원보다 참가자 수가 적을 때
            $member = new Together_member();
            $member->member_id = $request->session()->get('member');
            $member->together_id = $together_id;
            $member->state = "normal";
            $member->save();

            return response()->json([
                'success' => true
            ]);
        }else{
            // 수용 인원보다 참가자 수가 많을 때
            return response()->json([
                'success' => false, 'msg' => '참가자 수가 많아 더이상 참가할 수 없습니다'
            ]);
        }

    }

    public function partiOut(Request $request){
        $together_id = $request->together_id;
        $member_id   = $request->session()->get('member');

        $posts = Together_post::where([['together_id',$together_id],['member_id',$member_id]]);
        if($posts->get()->count() > 0){
            // 작성한 게시글 있음
            $del_files = Upload::where('file_type','together_posts')->whereIn('file_id', $posts->get('id'));
            if($del_files->get()->count() > 0){
                // 삭제할 이미지가 있음
                foreach($del_files->get('fn') as $img){
                    Storage::delete('public/uploads/together/post/'.$img);
                }
                $del_files->delete();
            }
            $posts->delete();
        }

        $member = Together_member::where([['together_id',$together_id],['member_id',$member_id]])->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function partiEnter(Request $request){
        $together_id = $request->together_id;
        $member_id   = $request->session()->get('member');

        $master = Together::find($together_id)->member_id; // 투게더 만든 이
        if($master == $member_id){
            // 만든이와 동일 인물이면 
            return response()->json([
                'success' => true
            ]);
        }

        $together_member = Together_member::where([['together_id',$together_id],['member_id',$member_id]]);
        if($together_member->exists()){
            // 해당 투게더에 참가한 회원
            if($together_member->first()->state == "normal"){
                // 회원 상태 :: 정상
                return response()->json([
                    'success' => true
                ]);
            }else{
                // 회원 상태 :: 추방
                return response()->json([
                    'success' => false, 'msg' => '해당 투게더에서 추방되어<br>참가할 수 없습니다', 'state' => 'block'
                ]);
            }
        }else{
            // 해당 투게더에 참가하지 않은 회원
            return response()->json([
                'success' => false, 'state' => 'not'
            ]);
        }
    }

    public function postCreate(Request $request){
        $together_id = $request->input('id');

        return view('together.together-post-add', compact('together_id'));
    }

    public function postStore(Request $request){
        $rules = [
            'content' => 'required'
        ];

        $messages = [
            'content.required' => '내용 또는 사진을 추가하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        // 포스트 추가
        $post = new Together_post();
        $post->together_id = $request->together_id;
        $post->member_id   = $request->session()->get('member');
        $post->content     = $request->content;
        $post->save();

        if($request->has('save_list')){
            foreach($request->input('save_list') as $image){
                $upload = Upload::where([['file_type','together_posts'],['fn',$image]])->update(['file_id' => $post->id]);
            }
        }

        return response()->json([
            'success' => true, 'msg' => '등록되었습니다'
        ]);
    }

    public function postEdit(Request $request, $id){
        $post = Together_post::find($id);
        $together_id = $post->together_id;

        return view('together.together-post-add', compact('together_id','post'));
    }

    public function postUpdate(Request $request, $id){
        $rules = [
            'content' => 'required'
        ];

        $messages = [
            'content.required' => '내용 또는 사진을 넣어주세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        // 포스트 수정
        $post = Together_post::find($id);
        $post->content = $request->content;
        $post->update();

        Upload::where([['file_type','together_posts'],['file_id', $id],['reference', null]])->update(['file_id' => 0]);
        if($request->has('save_list')){
            foreach($request->input('save_list') as $image){
                $upload = Upload::where([['file_type','together_posts'],['fn',$image]])->update(['file_id' => $post->id]);
            }
        }

        return response()->json([
            'success' => true, 'msg' => '수정되었습니다'
        ]);
    }

    public function postDestroy(Request $request, $id){
        $del_files = Upload::where([['file_type','together_posts'],['file_id',$id]]);
        foreach($del_files->get() as $del_file){
            Storage::delete("public/uploads/together/post/".$del_file);
        }
        $del_files->delete();

        $post = Together_post::find($id)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }

    public function upload(Request $request){
        $rules = [
            'imageFile' => 'mimetypes:image/jpeg,image/png,image/gif'
        ];

        $messages = [
            'imageFile.mimetypes' => '지원하지 않는 확장자입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }
        $file_type   = $request->input('file_type'); // 파일 모델
        $imageFile     = $request->file('imageFile');

        list($msec, $sec) = explode(" ", microtime());
        $ext = $imageFile->extension();
        $front = $sec.substr($msec, 2, 6);
        $fn = $front.".".$ext;
        $imageFile->storeAs('public/uploads/'.$file_type.'/', $fn); // 원본 저장
        
        $img_path = "storage/uploads/".$file_type."/".$fn;  // 이미지 파일 경로
        //gif, jpeg, png
        if($ext == "jpg" || $ext == "jpeg"){
            $img = imagecreatefromjpeg($img_path);
        }else if($ext == "gif"){
            $img = imagecreatefromgif($img_path);
        }else if($ext == "png"){
            $img = imagecreatefrompng($img_path);
            // 배경 투명 유지
            imagealphablending( $img, false );
            imagesavealpha( $img, true );
        }

        $exif = @exif_read_data($img_path);
        if(!empty($exif['Orientation'])){
            switch($exif['Orientation']){
            case 8:
                $img = imagerotate($img, 90, 0); break;
            case 3:
                $img = imagerotate($img, 180, 0); break;
            case 6:
                $img = imagerotate($img, -90, 0); break;
            }
        }

        $img = imagescale($img, 640, -1); // 화면
                
        if($ext == "jpg" || $ext == "jpeg"){
            imagejpeg($img, $img_path); // 3th : 0~100 압축률
        }else if($ext == "gif"){
            imagegif($img, $img_path); // 3th : 0~100 압축률
        }else if($ext == "png"){
            imagepng($img, $img_path); // 3th : 0~9 압축률
        }
        $img_info = getimagesize($img_path);
        
        $upload = new Upload();
        $upload->file_type   = $file_type;
        $upload->file_id     = 0;
        $upload->fn_ori      = $imageFile->getClientOriginalName();
        $upload->fn          = $fn;
        $upload->ext         = $ext;
        $upload->size        = round(filesize($img_path)/ 1024, 1); // kB로 표시
        $upload->width       = $img_info[0];
        $upload->height      = $img_info[1];
        $upload->save();
        
        imagedestroy($img);

        return response()->json([
            'success' => true, 'fn_dir' => "/".$img_path, 'fn' => $fn
        ]);
    }


    public function memberOut(Request $request){
        $member_id   = $request->member_id;
        $together_id = $request->together_id;

        $posts = Together_post::where([['together_id',$together_id],['member_id',$member_id]]);
        if($posts->get()->count() > 0){
            // 작성한 게시글 있음
            $del_files = Upload::where('file_type','together_posts')->whereIn('file_id', $posts->get('id'));
            if($del_files->get()->count() > 0){
                // 삭제할 이미지가 있음
                foreach($del_files->get('fn') as $img){
                    Storage::delete('public/uploads/together/post/'.$img);
                }
                $del_files->delete();
            }
            $posts->delete();
        }

        $member = Together_member::where([['together_id',$together_id],['member_id',$member_id]])->update(['state'=>'block']);

        return response()->json([
            'success' => true, "msg" => '추방목록으로 이동되었습니다'
        ]);
    }

    public function memberDestroy(Request $request){
        Together_member::where([['together_id',$request->together_id],['member_id',$request->member_id]])->delete();

        return response()->json([
            'success' => true, 'msg' => '정상적으로 처리되었습니다'
        ]);
    }

    public function noticeShow(Request $request, $id){
        $notice = Together_notice::find($id);

        return view('together.together-notice', compact('notice'));
    }

    public function noticeCreate(Request $request){
        $together_id = $request->input("id");
        
        return view('together.together-notice-add', compact('together_id'));
    }

    public function noticeStore(Request $request){
        $rules = [
            'title' => 'required',
            'content' => 'required'
        ];

        $messages = [
            'title.required' => '제목을 입력하세요',
            'content.required' => '내용 또는 사진을 추가하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        // 공지사항 추가
        $notice = new Together_notice();
        $notice->together_id = $request->together_id;
        $notice->title       = $request->title;
        $notice->content     = $request->content;
        $notice->save();

        //
        if($request->has('save_list')){
            foreach($request->input('save_list') as $image){
                $upload = Upload::where([['file_type','together_notices'],['fn',$image]])->update(['file_id' => $notice->id]);
            }
        }

        return response()->json([
            'success' => true, 'msg' => '등록되었습니다'
        ]);
    }

    public function noticeEdit(Request $request, $id){
        $notice = Together_notice::find($id);
        $together_id = $notice->together_id;

        return view('together.together-notice-add', compact('together_id','notice'));
    }

    public function noticeUpdate(Request $request, $id){
        $rules = [
            'title' => 'required',
            'content' => 'required'
        ];

        $messages = [
            'title.required' => '제목을 입력하세요',
            'content.required' => '내용 또는 사진을 넣어주세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        // 공지사항 수정
        $notice = Together_notice::find($id);
        $notice->title = $request->title;
        $notice->content = $request->content;
        $notice->update();

        Upload::where([['file_type','together_notices'],['file_id', $id],['reference', null]])->update(['file_id' => 0]);
        if($request->has('save_list')){
            foreach($request->input('save_list') as $image){
                $upload = Upload::where([['file_type','together_notices'],['fn',$image]])->update(['file_id' => $notice->id]);
            }
        }

        return response()->json([
            'success' => true, 'msg' => '수정되었습니다'
        ]);
    }

    public function noticeDestroy(Request $request, $id){
        $del_files = Upload::where([['file_type','together_notices'],['file_id',$id]]);
        foreach($del_files->get() as $del_file){
            Storage::delete("public/uploads/together/notice/".$del_file);
        }
        $del_files->delete();

        $notice = Together_notice::find($id)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }

    public function fileUpload(Request $request){
        $rules = [
            'imgFile' => 'mimes:jpeg,png,gif'
        ];

        $messages = [
            'imgFile.mimes' => '지원하지 않는 확장자입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg'=>$message
            ]);
        }

        $imgFile = $request->file('imgFile');

        list($msec, $sec) = explode(" ", microtime());
        $ext = $imgFile->extension();
        $front = $sec.substr($msec, 2, 6);
        $fn = $front.".".$ext;
        if($request->has("file_dir")){
            $imgFile->storeAs('public/uploads/'.$request->file_dir, $fn); // 원본 저장
            $img_path = "storage/uploads/".$request->file_dir."/".$fn;  // 이미지 파일 경로
        }else{
            $imgFile->storeAs('public/uploads/temp/', $fn); // 원본 저장
            $img_path = "storage/uploads/temp/".$fn;  // 이미지 파일 경로
        }
        //gif, jpeg, png
        if($ext == "jpg" || $ext == "jpeg"){
            $img = imagecreatefromjpeg($img_path);
        }else if($ext == "gif"){
            $img = imagecreatefromgif($img_path);
        }else if($ext == "png"){
            $img = imagecreatefrompng($img_path);
            // 배경 투명 유지
            imagealphablending( $img, false );
            imagesavealpha( $img, true );
        }

        $exif = @exif_read_data($img_path);
        if(!empty($exif['Orientation'])){
            switch($exif['Orientation']){
            case 8:
                $img = imagerotate($img, 90, 0); break;
            case 3:
                $img = imagerotate($img, 180, 0); break;
            case 6:
                $img = imagerotate($img, -90, 0); break;
            }
        }

        $img = imagescale($img, 640, -1);
                
        if($ext == "jpg" || $ext == "jpeg"){
            imagejpeg($img, $img_path, 75); // 3th : 0~9 압축률
        }else if($ext == "gif"){
            imagegif($img, $img_path, 75); // 3th : 0~9 압축률
        }else if($ext == "png"){
            imagepng($img, $img_path, 7); // 3th : 0~9 압축률
        }
        $img_info = getimagesize($img_path);
        
        $upload = new Upload();
        $upload->file_type   = $request->file_type;
        $upload->file_id     = 0;
        $upload->fn_ori      = $imgFile->getClientOriginalName();
        $upload->fn          = $fn;
        $upload->ext         = $ext;
        $upload->size        = round(filesize($img_path)/ 1024, 1); // kB로 표시
        $upload->width       = $img_info[0];
        $upload->height      = $img_info[1];
        $upload->save();
        
        imagedestroy($img);

        return response()->json([
            'success' => true, 'filesrc' => $img_path, 'fn' => $fn
        ]);
        
    }

    public function search(Request $request){
        $type = $request->type;
        if($type == "post"){
            $together_id = $request->together_id;
            $keyword     = $request->keyword;

            $member = Member::where('nick','like','%'.$keyword.'%');
            if($member->exists()){
                // 존재하는 회원닉네임
                $member_id = $member->get('id');
                $posts = Together_post::where('together_id',$together_id)->whereIn('member_id',$member_id)->get();
                if($posts->count() < 1){
                    // 존재하지 않는 회원 닉네임
                    return response()->json([
                        'success' => false, 'msg' => '["'.$keyword.'"] 에 대한 검색 결과가 없습니다'
                    ]);   
                }else{
                    foreach($posts as $post){
                        $post->nick = $post->member->nick;
                        $post->fn = isset($post->member->upload) ? $post->member->upload->fn : "";
                        $post->writer = $request->session()->get('member') == $post->member_id ? "true" : "false";
                    }
                    
                    return response()->json([
                        'success' => true, 'posts' => $posts
                    ]);
                }
            }else{
                // 존재하지 않는 회원 닉네임
                return response()->json([
                    'success' => false, 'msg' => '["'.$keyword.'"] 에 대한 검색 결과가 없습니다'
                ]);
            }
        }else if($type == "together"){
            $sido    = $request->input('sido','');
            $gugun   = $request->input('gugun','');
            $keyword = $request->input('keyword','');
            $member_id = $request->session()->get('member');

            $t_number    = Together_member::where('member_id',$member_id)->get('together_id'); // 내가 참가한 투게더 번호

            if($sido == ""){
                // 검색어로만 검색 
                $search      = Together::where('title','like','%'.$keyword.'%');
            }else{
                // 지역, 검색어로 검색
                $search = Together::where([['addr1','like','%'.$sido.'%'],['addr2','like','%'.$gugun.'%'],['title','like','%'.$keyword.'%']]);   
            }

            // 전체 투게더
            $together_all    = $search->latest()->get();                
            // 참여한 투게더
            $together_parti  = $search->whereIn('id',$t_number)->latest()->get();

            if($together_all->count() < 1){
                // 검색결과 :: 없음
                return response()->json([
                    'success' => false
                ]);
            }else{
                // 검색결과 :: 있음
                foreach($together_all as $together){
                    if($together->member_id == $member_id){
                        $together->isParti = "2";
                    }else{
                        $together->isParti   = Together_member::where([['together_id',$together->id],['member_id',$member_id]])->exists() == true ? "1" : "0";
                    }
                    $together->member_cnt    = Together_member::where('together_id',$together->id)->get()->count(); 
                    $together->fn            = $together->files->fn;
                }
                foreach($together_parti as $together){
                    $together->member_cnt    = Together_member::where('together_id',$together->id)->get()->count(); 
                    $together->fn            = $together->files->fn;
                }

                return response()->json([
                    'success' => true, 'together_all' => $together_all, 'together_parti' => $together_parti
                ]);
            }
        }
    }
}
