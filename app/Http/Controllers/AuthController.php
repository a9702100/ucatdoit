<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;
use Kissdigitalcom\AppleSignIn\ClientSecret;
use App\Models\Member;
use App\Models\Feed;
use App\Models\Writing;
use App\Models\Ask;
use App\Models\Report;
use App\Models\Upload;
use App\Models\Reply;
use App\Models\Together;
use App\Models\Together_member;
use App\Models\Together_post;
use App\Models\Together_notice;
use App\Models\Participation;
use App\Models\Heart;
use App\Models\Conceal;
use App\Models\HashTag;
use App\Models\Cat;
use App\Models\History;
use App\Models\Friend;
use App\Models\PushMessage;
use App\Models\PushHistory;
use App\Models\PushTemplate;

class AuthController extends Controller
{
    // 로그인 첫 페이지
    public function index(){
        return view('account.login');
    }
 
    // 점검 페이지
    public function tester(){
        return view('account.tester');
    }

    // 데이터 베이스 조작
    public function test(Request $request){
        
        echo "데이터 베이스 조작 페이지";
        $members = Member::where('notification_token','!=', null)->get();
        dump($members);
        foreach($members as $member){
            $push = new PushMessage();
            $push->member_id = $member->id;
            $push->token = $member->notification_token;
            $push->type = $member->token_type;
            $push->save();
        }
        $pushes = PushMessage::all();
        dump($pushes);
        echo "success";
    } 

    public function snsJoin(Request $request){
        $next = $request->next;

        return view('account.sns-privacy', compact('next'));
    }

    public function agreeUpdate(Request $request){
        $member_id = $request->member_id;

        $member = Member::find($member_id);
        $member->personal_agree = "1";
        $member->private_agree = "1";
        $member->update();

        return response()->json([
            'success' => true
        ]);
    }
    
    // 로그인 관련
    public function login(Request $request){
        $oauth = $request->oauth;

        if($oauth == "apple"){
            $clientId = env('APPLE_CLIENT_ID');
            $teamId   = 'GH9SDXJ4DG';
            $keyId    = 'HRCUMH4DG2';
            $certPath = 'pocket.p8';

            $clientSecret = new ClientSecret($clientId, $teamId, $keyId, $certPath);
            $clientSecret = $clientSecret->generate();
            $redirectUrl = env('APPLE_REDIRECT_URI');

            $_SESSION['state'] = bin2hex(random_bytes(5));

            $authorize_url = 'https://appleid.apple.com/auth/authorize'.'?'.http_build_query([
            'response_type' => 'code',
            'response_mode' => 'form_post',
            'client_id' => $clientId,
            'redirect_uri' => $redirectUrl,
            'scope' => 'name email',
            ]);

            return redirect($authorize_url);
        }else{
            return Socialite::driver($oauth)->redirect();
        }
    }

    public function kakao_login(Request $request){
        $user = Socialite::driver('kakao')->user();
        $member_id = ""; // 회원 아이디(고유번호)

        if(Member::where([['sns_type','kakao'],['sns_id',$user->id]])->exists()){
            // 카카오 회원 중에 ㅁㅁㅁ 인 회원이 존재하냐? 
            // 존재하더라
            $member = Member::where([['sns_type','kakao'],['sns_id',$user->id]])->first();
            $member_id = $member->id;
        }else{
            // 존재하지 않더라
            $member = new Member();
            $member->userId = "";
            $member->passwd = "";
            $member->sns_type = "kakao";
            $member->sns_id = $user->id;
            $member->nick = "";
            $member->phone = "";
            $member->personal_agree = "0";
            $member->private_agree = "0";
            $member->save();

            $push = new PushMessage();
            $push->member_id = $member->id;
            $push->save();

            $member_id = $member->id;
        }

        if($member->state == "delete"){
            $updated_at = date_format($member->updated_at, 'Y-m-d');
            return back()->with('deleted_account', '삭제된 계정입니다')->with('deleted_at', $updated_at);
        }

        $request->session()->put('member', $member_id);
        return redirect("/home");
    }
    public function naver_login(Request $request){
        if($request->has('error')){
            // 에러 발생
            return redirect('/');
        }else{
            $user = Socialite::driver('naver')->user();
            $member_id = "";
    
           
            if(Member::where([['sns_type','naver'],['sns_id',$user->id]])->exists()){
                // 네이버 회원 중에 ㅁㅁㅁ 인 회원이 존재하냐? 
                // 존재하더라
                $member = Member::where([['sns_type','naver'],['sns_id',$user->id]])->first();
                $member_id = $member->id;
            }else{
                // 존재하지 않더라
                $member = new Member();
                $member->userId = "";
                $member->passwd = "";
                $member->sns_type = "naver";
                $member->sns_id = $user->id;
                $member->nick = "";
                $member->phone = "";
                $member->personal_agree = "0";
                $member->private_agree = "0"; 
                $member->save();

                $push = new PushMessage();
                $push->member_id = $member->id;
                $push->save();
    
                $member_id = $member->id;
            }
    
            if($member->state == "delete"){
                $updated_at = date_format($member->updated_at, 'Y-m-d');
                return back()->with('deleted_account', '삭제된 계정입니다')->with('deleted_at', $updated_at);
            }
    
            $request->session()->put('member', $member_id);
            return redirect("/home");
        }
    }
    public function google_login(Request $request){
        $user = Socialite::driver('google')->user();
        $member_id = "";
       
        if(Member::where([['sns_type','google'],['sns_id',$user->id]])->exists()){
            // 구글 회원 중에 ㅁㅁㅁ 인 회원이 존재하냐? 
            // 존재하더라
            $member = Member::where([['sns_type','google'],['sns_id',$user->id]])->first();
            $member_id = $member->id;
        }else{
            // 존재하지 않더라
            $member = new Member();
            $member->userId = "";
            $member->passwd = "";
            $member->sns_type = "google";
            $member->sns_id = $user->id;
            $member->nick = "";
            $member->phone = "";
            $member->personal_agree = "0";
            $member->private_agree = "0";
            $member->save();

            $push = new PushMessage();
            $push->member_id = $member->id;
            $push->save();

            $member_id = $member->id;
        }

        if($member->state == "delete"){
            $updated_at = date_format($member->updated_at, 'Y-m-d');
            return back()->with('deleted_account', '삭제된 계정입니다')->with('deleted_at', $updated_at);
        }

        $request->session()->put('member', $member_id);
        return redirect("/home");
    }
    //yum install php-gmp
    public function apple_login(Request $request){
        $sns_id  = ""; // 나중에 인증용으로 쓸꺼

        $clientId = env('APPLE_CLIENT_ID');
        $teamId   = 'GH9SDXJ4DG';
        $keyId    = 'HRCUMH4DG2';
        $certPath = 'pocket.p8';

        $clientSecret = new ClientSecret($clientId, $teamId, $keyId, $certPath);
        $clientSecret = $clientSecret->generate();
        $redirectUrl = env('APPLE_REDIRECT_URI');

        if(isset($_POST['code'])) {

            $url = 'https://appleid.apple.com/auth/token';
            $params = [
                'grant_type' => 'authorization_code',
                'code' => $_POST['code'],
                'redirect_uri' => $redirectUrl,
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
            ]; 

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if($params)
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Accept: application/json',
                    'User-Agent: curl', # Apple requires a user agent header at the token endpoint
            ]);
            $response = curl_exec($ch);
            $response = json_decode($response);
          
            if(!isset($response->access_token) || !isset($response->id_token)) {
                // return redirect('/');
            }else{
                $claims = explode('.', $response->id_token)[1];
                $claims = json_decode(base64_decode($claims));
              
                $sns_id = $claims->sub;
            }          
        }

        if($sns_id == ""){
            $sns_id == "apple_guest";
        }

        if(Member::where([['sns_type','apple'],['sns_id',$sns_id]])->exists()){
            // 애플 회원 중에 ㅁㅁㅁ 인 회원이 존재하냐? 
            // 존재하더라
            $member = Member::where([['sns_type','apple'],['sns_id',$sns_id]])->first();
            $member_id = $member->id;
        }else{
            // 존재하지 않더라
            $member = new Member();
            $member->userId = "";
            $member->passwd = "";
            $member->sns_type = "apple";
            $member->sns_id = $sns_id;
            $member->nick = "";
            $member->phone = "";
            $member->personal_agree = "0";
            $member->private_agree = "0";
            $member->save();

            $push = new PushMessage();
            $push->member_id = $member->id;
            $push->save();

            $member_id = $member->id;
        }

        if($member->state == "delete"){
            $updated_at = date_format($member->updated_at, 'Y-m-d');
            return back()->with('deleted_account', '삭제된 계정입니다')->with('deleted_at', $updated_at);
        }

        $request->session()->put('member', $member_id);
        return redirect("/home");
    }

    public function facebook_login(Request $request){
        $user = Socialite::driver('facebook')->user();
        $member_id = "";
       
        if(Member::where([['sns_type','facebook'],['sns_id',$user->id]])->exists()){
            // 페이스북 회원 중에 ㅁㅁㅁ 인 회원이 존재하냐? 
            // 존재하더라
            $member = Member::where([['sns_type','facebook'],['sns_id',$user->id]])->first();
            $member_id = $member->id;
        }else{
            // 존재하지 않더라
            $member = new Member();
            $member->userId = "";
            $member->passwd = "";
            $member->sns_type = "facebook";
            $member->sns_id = $user->id;
            $member->nick = "";
            $member->phone = "";
            $member->personal_agree = "0";
            $member->private_agree = "0";
            $member->save();

            $push = new PushMessage();
            $push->member_id = $member->id;
            $push->save();

            $member_id = $member->id;
        }

        if($member->state == "delete"){
            $updated_at = date_format($member->updated_at, 'Y-m-d');
            return back()->with('deleted_account', '삭제된 계정입니다')->with('deleted_at', $updated_at);
        }
        
        $request->session()->put('member', $member_id);
        return redirect("/home");
    }



    public function loginCheck(Request $request){
        $rules = [
            'userId' => 'required|between:5,12',
            'passwd' => 'required|between:5,12'
        ];
        $messages = [
            'userId.required' => '아이디를 입력하세요',
            'passwd.required' => '비밀번호를 입력하세요',
            'userId.between' => '아이디는 최소 5자 ~ 최대 12자 입니다',
            'passwd.between' => '비밀번호는 최소 5자 ~ 최대 12자 입니다'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        if(count($errors) > 0){
            return redirect()->back()->withInput($request->all())->withErrors($errors);
        }
        
        $member = Member::where('userId','=', $request->userId)->first();
        if($member){
            if(Hash::check($request->passwd, $member->passwd)){
                if($member->state == "delete"){
                    $updated_at = date_format($member->updated_at, 'Y-m-d');
                    return back()->with('deleted_account', '삭제된 계정입니다')->with('deleted_at', $updated_at);
                }
                $request->session()->put('member', $member->id);
                return redirect('/home');
            }else{
                return back()->with('fail_passwd','비밀번호가 일치하지 않습니다')->withInput($request->all());
            }
        }else{
            return back()->with('fail_userId', '존재하지않는 아이디 입니다')->withInput($request->all());
        }   
    }

    // 회원가입 관련
    public function join(Request $request){
        $step = $request->input('step','1');
        $phone = $request->input('phone','');
        switch($step){
            case '1' : return view('account.join-privacy');
            case '2' : return view('account.join');
            case '3' : return view('account.join-privacy-detail');
            case '4' : return view('account.join-personal-detail');
            case '5' : return view('account.join-enter', compact('phone'));
            case '6' : return view('account.join-success');
        }
    }

    // 컨텐츠 이용동의 
    public function useContents(){
        return view('account.use-contents');
    }

    // 가입하기 진행
    public function joinStore(Request $request){
        $rules = [
            'userId' => 'required|not_regex:/[^a-z0-9-_]/i|between:5,12|unique:members',
            'passwd' => 'required|not_regex:/\s/|between:5,12',
            'nick'   => 'required|not_regex:/[^0-9가-힣a-z]/i|between:2,8|unique:members'
        ];

        $messages = [
            'userId.required'  => '사용할 아이디를 입력하세요',
            'userId.not_regex' => '아이디는 영어, 숫자로만 입력해야합니다',
            'userId.between'   => '아이디는 최소 5자 ~ 최대 12자 입니다',
            'userId.unique'    => '이미 사용중인 아이디 입니다',
            'passwd.required'  => '사용할 비밀번호를 입력하세요',
            'passwd.not_regex' => '비밀번호는 공백없이 입력해야합니다',
            'passwd.between'   => '비밀번호는 최소 5자 ~ 최대 12자 입니다',
            'nick.required'    => '사용할 닉네임을 입력하세요',
            'nick.not_regex'   => '닉네임은 한글, 영어, 숫자로만 입력해야합니다',
            'nick.between'     => '닉네임은 최소 2자 ~ 최대 8자 입니다',
            'nick.unique'      => '이미 사용중인 닉네임 입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        if(count($errors) > 0){
            return redirect()->back()->withInput($request->all())->withErrors($errors);
        }

        $member = new Member();
        $member->userId      = $request->userId;
        $member->passwd      = Hash::make($request->passwd);
        $member->nick        = $request->nick;
        $member->phone       = $request->phone;
        $member->save();

        $push = new PushMessage();
        $push->member_id = $member->id;
        $push->save();

        return redirect('/account/join?step=6');
    }

    // input 유효성 검사 : 입력할 때마다 입력값에 따라 
    public function inputCheck(Request $request){
        $target = $request->input('target'); // 입력 id
        $value = $request->input('value'); // 입력 value

        $rules = ""; // 규칙
        $messages = ""; // 에러메세지

        switch($target){
            case 'userId' : 
                $rules = [
                    'value' => 'not_regex:/[^a-z0-9-_]/i|between:5,12'
                ];
                $messages = [
                    'value.not_regex' => '아이디는 영어, 숫자로만 입력해야합니다',
                    'value.between' => '아이디는 최소 5자 ~ 최대 12자 입니다'
                ];
                break;
            case 'passwd' :
                $rules = [
                    'value' => 'not_regex:/\s/|between:5,12'
                ];
                $messages = [
                    'value.not_regex' => '비밀번호는 공백없이 입력해야합니다',
                    'value.between' => '비밀번호는 최소 5자 ~ 최대 12자 입니다'
                ]; 
                break;
            case 'nick' :
                $rules = [
                    'value' => 'not_regex:/[^0-9가-힣a-z]/i|between:2,8'
                ];
                $messages = [
                    'value.not_regex' => '닉네임은 한글, 영어, 숫자로만 입력해야합니다',
                    'value.between' => '닉네임은 최소 2자 ~ 최대 8자 입니다'
                ]; break;
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        // 에러 메세지 리턴
        if($errors->first('value') == ""){
            return response()->json([
                'success' => true
            ]);
        }else{
            // 규칙에 어긋남
            return response()->json([
                'success' => false, 'msg' => $errors->first('value')
            ]);
        }
    }

    // 중복되는 전화번호 여부 확인
    public function member_chk(Request $request){
        $isPhone = Member::where('phone', $request->phone)->first();

        if($isPhone == null){
            // 일치하는 전화번호 없음
            return response()->json([
                'duplicate' => false
            ]);
        }else{
            // 일치하는 전화번호 있음
            return response()->json([
                'duplicate' => true
            ]);
        }
    }

    // 계정찾기 관련
    public function find(Request $request){
        return view('account.find');
    }

    public function findShow(Request $request){
        $member = Member::where('phone', $request->phone)->first();

        return view('account.find-detail', compact('member'));
    }

    public function findUpdate(Request $request, $id){
        $rules = [
            'passwd' => 'not_regex:/\s/|between:5,12'
        ];
        $messages = [
            'passwd.not_regex' => '비밀번호는 공백없이 입력해야합니다',
            'passwd.between' => '비밀번호는 최소 5자 ~ 최대 12자 입니다'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $member = Member::find($id);
        $member->passwd = Hash::make($request->passwd);
        $member->update();

        return response()->json([
            'success' => true, 'msg' => '변경되었습니다'
        ]);
    }

    // 본인인증
    public function verify(){
        $url = "https://auth.logintalk.io/exchange?token=".$_POST["token"];
        $is_post = false;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, $is_post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = array();
        $response = curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        
        if($status_code == 200) {
            echo $response;
        } else {
            echo "Error 내용:".$response;
        }
    }

    // 로그아웃
    public function logout(Request $request){
        $member_id = $request->session()->get('member');

        // 로그아웃 시 토큰 제거
        PushMessage::where('member_id',$member_id)->update(['token'=>null],['type'=>null]); 
        
        $request->session()->flush();
        return redirect('/');
    }


    // SNS 회원가입 - 닉네임 유효성 검사
    public function newbie_nick(Request $request){
        $rules = [
            'nick' => 'not_regex:/[^0-9가-힣a-z]/i|between:2,8|unique:members'
        ];

        $messages = [
            'nick.not_regex' => '닉네임은 한글, 영어, 숫자로만 입력해야합니다',
            'nick.between' => '닉네임은 최소 2자 ~ 최대 8자 입니다',
            'nick.unique' => '사용중인 닉네임입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        return response()->json([
            'success' => true, 'msg' => '사용가능한 닉네임입니다'
        ]);
    }

    // SNS 회원가입 - 닉네임 적용
    public function newbie_apply(Request $request){
        $rules = [
            'nick' => 'required|not_regex:/[^0-9가-힣a-z]/i|between:2,8|unique:members'
        ];

        $messages = [
            'nick.required' => '닉네임을 입력하세요',
            'nick.not_regex' => '닉네임은 한글, 영어, 숫자로만 입력해야합니다',
            'nick.between' => '닉네임은 최소 2자 ~ 최대 8자 입니다',
            'nick.unique' => '사용중인 닉네임입니다' 
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $member = Member::find($request->session()->get('member'));
        $member->nick = $request->nick;
        $member->update();

        return response()->json([
            'success' => true, 'msg' => '적용되었습니다'
        ]);
    }

    public function memberDestroy(Request $request){
        $member_id = $request->session()->get("member");

        // 문의 하기 
        Ask::where('member_id',$member_id)->delete();
        
        // 고양이
        $cats = Cat::where('member_id',$member_id)->delete(); // 고양이 등록 번호

        // 숨기기
        Conceal::where('member_id',$member_id)->delete();

        // 이벤트 참여
        Participation::where('member_id',$member_id)->delete();

        // 피드 -> 해시태그
        $feeds_id = Feed::where([['feedable_type','members'],['feedable_id',$member_id]]);
        if($feeds_id->get('id')->count() > 0){
            // 사용한 해시태그 있음
            HashTag::where('taggable_type','feeds')->whereIn('taggable_id',$feeds_id->get('id'))->delete();
         
            $feed_imgs = Upload::where('file_type','feeds')->whereIn('file_id',$feeds_id->get('id'));
            foreach($feed_imgs->get() as $img){
                if($img->ext == "mp4"){
                    Storage::delete('public/uploads/feed/'.$img->reference);
                }
                Storage::delete('public/uploads/feed/'.$img->fn);
            }
            $feed_imgs->delete();
            $feeds_id->delete();
        }

        // 하트
        Heart::where('member_id',$member_id)->delete();

        // 최근 이력
        History::where('sender',$member_id)->orWhere('receiver',$member_id)->delete();

        // 친구 
        Friend::where('my_idx',$member_id)->orWhere('friend_idx',$member_id)->delete();
        
        // 투게더 
        Together_member::where('member_id', $member_id)->delete();
        $posts = Together_post::where('member_id', $member_id);
        if($posts->get('id')->count() > 0){
            // 작성한 게시글이 있음
            $posts_imgs = Upload::where('file_type','together_posts')->where('file_id', $posts->get('id'));
            foreach($posts_imgs->get('id') as $img){
                Storage::delete('public/uploads/together/post/'.$img);
            }
            $posts_imgs->delete(); // 게시글 업로드 삭제
            $posts->delete(); // 게시글 삭제
        }
        $together = Together::where('member_id', $member_id);
        if($together->get('id')->count() > 0){
            // 만든 투게더가 있음
            $notices = Together_notice::whereIn('together_id',$together->get('id'));
            if($notices->get('id')->count() > 0){
                // 작성한 투게더 공지사항이 있음
                $notice_imgs = Upload::where('file_type', 'together_notices')->where('file_id', $notices->get('id'));
                foreach($notice_imgs->get('id') as $img){
                    Storage::delete('public/uploads/together/notice/'.$img);
                }
                $notice_imgs->delete(); // 투게더 공지사항 삭제
                $notices->delete(); // 투게더 공지사항 글 삭제
            }

            $together_imgs = Upload::where('file_type','togethers')->whereIn('file_id',$together->get('id'));
            foreach($together_imgs->get('fn') as $img){
                Storage::delete('public/uploads/together/'.$img);
            }
            $together_imgs->delete(); // 투게더 메인 이미지 삭제
            $together->delete(); // 투게더 정보 삭제
        }

        // 푸시 설정 삭제
        PushMessage::where('member_id', $member_id)->delete();

        // 댓글 삭제
        Reply::where('member_id', $member_id)->delete();
        
        // 신고 삭제
        Report::where('member_id', $member_id)->orWhere([['reportable_type', 'members'],['reportable_id', $member_id]])->delete();


        // 회원삭제
        $member_img = Upload::where([['file_type','members'],['file_id',$member_id]]);
        if($member_img->get('fn')->count() > 0){
            Storage::delete('public/uploads/member/'.$member_img->first());
        }
        $member_img->delete();
        Member::where('id',$member_id)->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function isMember(Request $request){
        $member_id = $request->input('member','0');

        $member = Member::where('id',$member_id)->exists(); // 존재하는 회원이니?

        if($member){
            $request->session()->put('member', $member_id);
            return response()->json([
                'success' => true
            ]);
        }else{
            return response()->json([
                'success' => false
            ]);
        }
    }

    public function insertToken(Request $request){
        $member_id   = $request->input("member_id", $request->session()->get("member"));
        $token       = $request->token;
        $token_type  = $request->input("token_type", "Android");

        $isToken = PushMessage::where('member_id', $member_id)->exists();
        if($isToken){
            $push = PushMessage::where('member_id', $member_id)->first();
            $push->token = $token;
            $push->type  = $token_type;
            $push->update();
            
        }else{
            $push = new PushMessage();
            $push->member_id = $member_id;
            $push->token     = $token;
            $push->type      = $token_type;
            $push->save();
        }

        return response()->json([
            'success' => true, 'msg' => '토큰 등록 완료'
        ]);
    }
}
