<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Conceal;
use App\Models\Friend;

class ReportController extends Controller
{
    //
    public function report(Request $request){
        $type = $request->input('type');
        $id = $request->input('id');

        return view('declare', compact('type','id'));
    }

    public function reportStore(Request $request){
        $member_id = $request->session()->get('member');

        $report = new Report();
        $report->member_id       = $member_id;
        $report->reportable_type = $request->input('type');
        $report->reportable_id   = $request->input('id');
        $report->reason          = $request->input('reason');
        $report->save();
 
        // type :: feeds, members, etc 

        $conceal = new Conceal();
        $conceal->member_id        = $member_id;
        $conceal->concealable_type = $request->input('type');
        $conceal->concealable_id   = $request->input('id');
        $conceal->save();

        if($request->input('type') == "members"){
            Friend::where([['my_idx',$member_id],['friend_idx',$request->input('id')]])->orWhere([['friend_idx',$member_id],['my_idx',$request->input('id')]])->delete();            

            // 처리 성공, 알림 띄우고 홈으로 이동
            return response()->json([
                'success' => true, 'after' => 'home'
            ]);
        }else{
            // 처리 성공, 알림만 띄움
            return response()->json([
                'success' => true, 'after' => 'alert'
            ]);
        }

    }
}
