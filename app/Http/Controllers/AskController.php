<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Ask;
use App\Models\History;

class AskController extends Controller
{
    //
    public function ask(Request $request){
        $member_id = $request->session()->get('member');
        $asks = Ask::where('member_id',$member_id)->latest()->get();

        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('contact', compact('asks','isHistory'));
    }

    public function askStore(Request $request){
        $rules = [
            'question' => 'required'
        ];

        $messages = [
            'question.required' => '문의 내용을 입력하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $member_id = $request->session()->get('member');

        $ask = new Ask();
        $ask->member_id  = $member_id;
        $ask->type       = $request->input('type');
        $ask->question   = $request->input('question');
        $ask->save();

        return response()->json([
            'success' => true, 'msg' => '정상적으로 접수되었습니다'
        ]);
    }

    public function askDestroy(Request $request){
        Ask::where('id',$request->ask_id)->delete();

        return response()->json([
            'success' => true
        ]);
    }
}
