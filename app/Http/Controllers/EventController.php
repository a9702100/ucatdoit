<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Writing;
use App\Models\Participation;
use App\Models\History;

class EventController extends Controller
{
    //
    public function event(Request $request){
        $selected = $request->input('status', 'on'); // on:진행중, soon:예정, off:마감
        $tab      = $request->input('tab',1); // 1:이벤트(공지), 2:체험단, 3:제휴할인

        if($selected == "on"){ // 진행중인 이벤트
            $events      = Writing::whereDate('start_date','<=',today())->whereDate('end_date','>=',today())->whereIn('type',['event','notice'])->latest()->get();
            $experiences = Writing::whereDate('start_date','<=',today())->whereDate('end_date','>=',today())->where('type','experience')->latest()->get();
            $affiliates  = Writing::whereDate('start_date','<=',today())->whereDate('end_date','>=',today())->where('type','affiliate')->latest()->get();
        }else if($selected == "soon"){ // 예정된 이벤트
            $events      = Writing::whereDate('start_date','>',today())->whereIn('type',['event','notice'])->latest()->get();
            $experiences = Writing::whereDate('start_date','>',today())->where('type','experience')->latest()->get();
            $affiliates  = Writing::whereDate('start_date','>',today())->where('type','affiliate')->latest()->get();
        }else if($selected == "off"){ // 마감된 이벤트
            $events      = Writing::whereDate('end_date','<',today())->whereIn('type',['event','notice'])->latest()->get();
            $experiences = Writing::whereDate('end_date','<',today())->where('type','experience')->latest()->get();
            $affiliates  = Writing::whereDate('end_date','<',today())->where('type','affiliate')->latest()->get();
        }
        
        $member_id = $request->session()->get('member');
        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('event.event-list', compact('events','experiences', 'affiliates','isHistory','selected','tab'));
    }

    public function eventShow(Request $request, $id){
        $member_id = $request->session()->get('member');
        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        $event = Writing::find($id);
        $isEvent = Participation::where([['event_id',$id],['member_id',$member_id]])->exists(); // 참여 이력 여부 
        return view('event.event-detail', compact('event','isEvent','isHistory'));
    }

    public function eventStore(Request $request){
        $rules = [
            'name'       => 'required',
            'postcode'   => 'required',
            'roadAddr'   => 'required',
            'detailAddr' => 'required',
            'ph1'        => 'required|digits:3',
            'ph2'        => 'required|digits_between:3,4',
            'ph3'        => 'required|digits:4',
            'agree'      => 'required'
        ];

        $messages = [
            'name.required'       => '이름을 입력하세요',
            'postcode.required'   => '주소 검색버튼을 이용하여<br>우편번호를 입력해주세요',
            'roadAddr.required'   => '주소 검색버튼을 이용하여<br>도로명주소를 입력해주세요',
            'detailAddr.required' => '상세주소를 입력하세요',
            'ph1.required'        => '전화번호 앞자리를 입력하세요',
            'ph2.required'        => '전화번호 중간자리를 입력하세요',
            'ph3.required'        => '전화번호 뒷자리를 입력하세요',
            'ph1.digits'          => '전화번호 앞 숫자 3자리를 입력하세요',
            'ph2.digits_between'  => '전화번호 중간 숫자 3~4자리를 입력하세요',
            'ph3.digits'          => '전화번호 뒷 숫자 4자리를 입력하세요',
            'agree.required'      => '이용자 동의는 필수입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $member_id = $request->session()->get('member');
        $event = Writing::find($request->event_id); // 이벤트 아이디

        $participation = new Participation();
        $participation->event_id     = $event->id;
        $participation->event_title  = $event->title;
        $participation->member_id    = $member_id;
        $participation->name         = $request->name;
        $participation->postcode     = $request->postcode;
        $participation->roadAddr     = $request->roadAddr;
        $participation->detailAddr   = $request->detailAddr;
        $participation->phone        = $request->ph1.'-'.$request->ph2.'-'.$request->ph3;
        $participation->agree        = $request->input('agree2');
        $participation->save();

        return response()->json([
            'success' => true
        ]);

    }

    public function eventSuccess(){
        return view('event.event-success');
    }

    public function guestPage(Request $request, $id){
        $event = Writing::find($id);

        if($event != null){
            if($event->start_date <= today() && $event->end_date > today()){
                return view('event.event-guest', compact('event'));
            }else{
                return view('event.event-guest');
            }
        }else{
            return view('event.event-guest');
        }
    }

    public function lastGetInfo(Request $request){
        $member_id = $request->session()->get('member');

        $info = Participation::where('member_id', $member_id)->latest()->take(1)->get();

        if($info->isEmpty()){
            // 최근 참여정보가 없음
            return response()->json([
                'success' => false, 'msg' => '회원님의 최근 참여정보가 없습니다'
            ]);
        }else{
            return response()->json([
                'success' => true, 'info' => $info
            ]);
        }
    }
}
