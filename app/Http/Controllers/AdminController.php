<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Exports\MemberExport;
use App\Exports\PartiExport;

use Excel;
use Hash;

use App\Models\Member;
use App\Models\Feed;
use App\Models\Writing;
use App\Models\Ask;
use App\Models\Report;
use App\Models\Upload;
use App\Models\Reply;
use App\Models\Together;
use App\Models\Together_member;
use App\Models\Together_post;
use App\Models\Together_notice;
use App\Models\Participation;
use App\Models\Heart;
use App\Models\Conceal;
use App\Models\HashTag;
use App\Models\Cat;
use App\Models\History;
use App\Models\Friend;
use App\Models\PushMessage;
use App\Models\PushHistory;
use App\Models\PushTemplate;

use App\Libraries\Notification;


class AdminController extends Controller
{
//------------------------------------
// 관리자 페이지 로그인
//------------------------------------
    public function admin(){
        return view("admin.login");
    }

    public function adminCheck(Request $request){
        $rules = [
            'userId' => 'required|between:5,12',
            'passwd' => 'required|between:5,12'
        ];

        $messages = [
            'userId.required' => '아이디를 입력하세요',
            'userId.between' => '아이디는 최소 5자 ~ 최대 12자 입니다',
            'passwd.required' => '비밀번호를 입력하세요',
            'passwd.between' => '비밀번호는 최소 5자 ~ 최대 12자 입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        if(count($errors) > 0){
            return redirect()->back()->withInput($request->all())->withErrors($errors);
        }

        $member = Member::where('userId','=',$request->userId)->first();
        if($member){
            if(Hash::check($request->passwd, $member->passwd)){
                // 정상 입력
                if($member->state == "master"){
                    $request->session()->put('master',$member->id);
                    return redirect('/admin/home');
                }else{
                    return back()->with('abnormal', '비정상적인 접근입니다');
                }
            }else{
                // 비정상 입력 :: 잘못된 비밀번호 
                return back()->with('fail_passwd', '비밀번호가 일치하지 않습니다')->withInput($request->all());
            }
        }else{
            // 비정상 입력 :: 잘못된 아이디
            return back()->with('fail_userId', '존재하지 않는 아이디 입니다')->withInput($request->all());
        }
    }

    public function home(){
        $today_member  = Member::whereDate('created_at',today())->get()->count();
        $total_member  = Member::where('state','!=','master')->get()->count();
        $today_feed    = Feed::whereDate('created_at',today())->get()->count();
        $total_feed    = Feed::all()->count();
        $today_ask     = Ask::whereDate('created_at', today())->get()->count();
        $total_ask     = Ask::where('process','!=','답변완료')->get()->count();
        $today_report  = Report::whereDate('created_at', today())->get()->count();
        $total_report  = Report::where('process','접수')->get()->count();

        return view('admin.home', compact('today_member','today_feed','today_ask','today_report','total_member','total_feed','total_ask','total_report'));
    }

//------------------------------------
// 회원 관리
//------------------------------------
    public function member(Request $request){
        $state = $request->input('state', 'all'); // 회원 가입상태
        $word = $request->input('word',''); // 검색어
        if($state == "all"){
            $members = Member::where('state','!=','master')->where('nick','like',$word.'%')->latest()->paginate(20);
        }else{
            $members = Member::where([['state',$state],['state','!=','master']])->where('nick','like',$word.'%')->latest()->paginate(20);
        }
        return view('admin.member', compact('state','members'));
    }

    public function memberShow(Request $request, $id){
        $member = Member::find($id);

        $member->feed_cnt    = Feed::where([['feedable_type','members'],['feedable_id',$id]])->get()->count();
        $member->reply_cnt   = Reply::where('member_id',$id)->get()->count();
        $member->make_cnt    = Together::where('member_id', $id)->get()->count();
        $member->parti_cnt   = Together_member::where('member_id', $id)->get()->count();
        $member->event_cnt   = Participation::where('member_id', $id)->get()->count();
        $member->ask_cnt     = Ask::where('member_id', $id)->get()->count();
        $member->report_cnt  = Report::where('member_id', $id)->get()->count(); 
        return view('admin.member-detail', compact('member'));
    }

    public function memberDestroy(Request $request, $id){
        // 문의 하기 
        Ask::where('member_id',$id)->delete();
        
        // 고양이
        $cats = Cat::where('member_id',$id)->delete(); // 고양이 등록 번호

        // 숨기기
        Conceal::where('member_id',$id)->delete();

        // 이벤트 참여
        Participation::where('member_id',$id)->delete();

        // 피드 -> 해시태그
        $feeds_id = Feed::where([['feedable_type','members'],['feedable_id',$id]]);
        if($feeds_id->get('id')->count() > 0){
            // 사용한 해시태그 있음
            HashTag::where('taggable_type','feeds')->whereIn('taggable_id',$feeds_id->get('id'))->delete();
         
            $feed_imgs = Upload::where('file_type','feeds')->whereIn('file_id',$feeds_id->get('id'));
            foreach($feed_imgs->get() as $img){
                if($img->ext == "mp4"){
                    Storage::delete('public/uploads/feed/'.$img->reference);
                }
                Storage::delete('public/uploads/feed/'.$img->fn);
            }
            $feed_imgs->delete();
            $feeds_id->delete();
        }

        // 하트
        Heart::where('member_id',$id)->delete();

        // 최근 이력
        History::where('sender',$id)->orWhere('receiver',$id)->delete();

        // 친구 
        Friend::where('my_idx',$id)->orWhere('friend_idx',$id)->delete();
        
        // 투게더 
        Together_member::where('member_id', $id)->delete();
        $posts = Together_post::where('member_id', $id);
        if($posts->get('id')->count() > 0){
            // 작성한 게시글이 있음
            $posts_imgs = Upload::where('file_type','together_posts')->where('file_id', $posts->get('id'));
            foreach($posts_imgs->get('id') as $img){
                Storage::delete('public/uploads/together/post/'.$img);
            }
            $posts_imgs->delete(); // 게시글 업로드 삭제
            $posts->delete(); // 게시글 삭제
        }
        $together = Together::where('member_id', $id);
        if($together->get('id')->count() > 0){
            // 만든 투게더가 있음
            $notices = Together_notice::whereIn('together_id',$together->get('id'));
            if($notices->get('id')->count() > 0){
                // 작성한 투게더 공지사항이 있음
                $notice_imgs = Upload::where('file_type', 'together_notices')->where('file_id', $notices->get('id'));
                foreach($notice_imgs->get('id') as $img){
                    Storage::delete('public/uploads/together/notice/'.$img);
                }
                $notice_imgs->delete(); // 투게더 공지사항 삭제
                $notices->delete(); // 투게더 공지사항 글 삭제
            }

            $together_imgs = Upload::where('file_type','togethers')->whereIn('file_id',$together->get('id'));
            foreach($together_imgs->get('fn') as $img){
                Storage::delete('public/uploads/together/'.$img);
            }
            $together_imgs->delete(); // 투게더 메인 이미지 삭제
            $together->delete(); // 투게더 정보 삭제
        }

        // 푸시 설정 삭제
        PushMessage::where('member_id', $id)->delete();

        // 댓글 삭제
        Reply::where('member_id', $id)->delete();
        
        // 신고 삭제
        Report::where('member_id', $id)->orWhere([['reportable_type', 'members'],['reportable_id', $id]])->delete();


        // 회원삭제
        $member_img = Upload::where([['file_type','members'],['file_id',$id]]);
        if($member_img->get('fn')->count() > 0){
            Storage::delete('public/uploads/member/'.$member_img->first());
        }
        $member_img->delete();
        Member::where('id',$id)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }

    public function member_excel(){
        return Excel::download(new MemberExport, 'members.xlsx');
    }

    public function master(Request $request){
        $masters = Member::where('state','master')->latest()->paginate(20);

        return view('admin.master', compact('masters'));
    }

//------------------------------------
// 피드 관리
//------------------------------------    
    public function feed(Request $request){
        $sort    = $request->input('sort');
        $keyword = $request->input('keyword');

        if($sort == ""){
            $feeds = Feed::latest()->paginate(20);
        }else if($sort == "nick" || $sort == "userId"){
            $member = Member::where($sort,'like','%'.$keyword.'%')->get('id');
            if($member->count() < 0){
                $feeds = [];
            }else{
                $feeds = Feed::whereIn('feedable_id', $member)->latest()->paginate(20);
            }
        }else if($sort == "content"){
            $feeds = Feed::where('content','like','%'.$keyword.'%')->latest()->paginate(20);
        }

        return view('admin.feed', compact('feeds','sort','keyword'));
    }

    public function feedShow(Request $request, $id){
        $feed = Feed::where('id',$id);

        $isFeed = $feed->exists();
        $feed = $feed->first();

        return view('admin.feed-detail', compact('feed','isFeed'));
    }

    public function feedDestroy(Request $request){
        // 파일삭제 -> 댓글 삭제 -> 하트 삭제 -> 숨겨진 피드 삭제 -> 피드 삭제
        $checkNum = $request->checkNum;

        // 1. 파일삭제(데이터베이스 포함)
        $del_files = Upload::where('file_type','feeds')->whereIn('file_id',$checkNum);
        foreach ($del_files->get() as $del_file) {
            if($del_file->ext == "mp4"){
                Storage::delete('public/uploads/feed/'.$del_file->reference);
            }
            Storage::delete('public/uploads/feed/'.$del_file->fn);
        }
        $del_files->delete();

        // 2. 댓글 삭제
        Reply::where('reply_type','feeds')->whereIn('reply_id', $checkNum)->delete();

        // 3. 하트 삭제
        Heart::where('heartable_type', 'feeds')->whereIn('heartable_id', $checkNum)->delete();

        // 4. 해시태그 삭제
        HashTag::where('taggable_type', 'feeds')->whereIn('taggable_id', $checkNum)->delete();

        // 5. 숨겨진 피드 삭제
        Conceal::where('concealable_type', 'feeds')->whereIn('concealable_id', $checkNum)->delete();

        // 6. 피드 삭제
        Feed::whereIn('id', $checkNum)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제 되었습니다'
        ]);

    }
    public function reply(Request $request){
        $kind = $request->input('kind','master');
        $sort = $request->input('sort');
        $keyword = $request->input('keyword');

        if($kind == "master"){
            $replies = Reply::where([['reply_type','feeds'],['group_id','0']]);
        }else{
            $replies = Reply::where([['reply_type','feeds'],['group_id','!=','0']]);
        }

        if($sort == ""){
            $replies = $replies->latest()->paginate(20);
        }else if($sort == "nick" || $sort == "userId"){
            $member = Member::where($sort,'like','%'.$keyword.'%')->get('id');
            if($member->count() < 0){
                $replies = [];
            }else{
                $replies = $replies->whereIn('member_id', $member)->latest()->paginate(20);
            }
        }else if($sort == "content"){
            $replies = $replies->where('content','like','%'.$keyword.'%')->latest()->paginate(20);
        }

        return view('admin.reply', compact('replies', 'kind', 'sort', 'keyword'));
    }

    public function replyDestroy(Request $request){
        $kind     = $request->kind;
        $checkNum = $request->checkNum;

        if($kind == "master"){
            // 답글 삭제 -> 댓글 삭제
            Reply::whereIn('id', $checkNum)->orWhereIn('group_id', $checkNum)->delete();
        }else{
            // 답글만 삭제
            Reply::whereIn('id', $checkNum)->delete();
        }

        return response()->json([
            'success' => true, 'msg' => '삭제 되었습니다'
        ]);
    }

    public function tag(Request $request){
        $tags = HashTag::groupBy('content')->latest()->paginate(20);

        foreach ($tags as $tag) {
            $hashTag = HashTag::where('content', $tag->content)->get();

            $tag->tag_cnt    = $hashTag->count();
            $tag->first_tag  = $hashTag->first()->created_at;
            $tag->last_tag   = $hashTag->last()->created_at;
        }
        return view('admin.tag', compact('tags'));
    }


    public function tagDestroy(Request $request){
        $checkNum = $request->checkNum;

        HashTag::whereIn('content', $checkNum)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }
//------------------------------------
// 투게더 관리
//------------------------------------    
    public function together(Request $request){
        $sido = $request->input('sido');
        $gugun = $request->input('gugun');

        if($sido == ""){
            // 주소 선택 없음
            $togethers = Together::latest()->paginate(20);
        }else{
            // 주소 선택 있음
            $togethers = Together::where([['addr1',$sido],['addr2',$gugun]])->latest()->paginate(20);
        }
        foreach($togethers as $together){
            $together->member_cnt = Together_member::where('together_id',$together->id)->selectRaw('count(*) as cnt')->value('cnt');
        }
        return view('admin.together', compact('togethers'));
    }

//------------------------------------
// 문의내역 관리
//------------------------------------
    public function ask(Request $request){
        $type    = $request->input('type','전체');
        $process = $request->input('process','전체');

        if($type != "전체"){
            $asks = Ask::where('type',$type);
            if($process != "전체"){
                $asks = $asks->where('process', $process);
            }
        }else{
            if($process != "전체"){
                $asks = Ask::where('process',$process);
            }else{
                $asks = Ask::latest()->paginate(10);
                return view('admin.ask', compact('asks', 'type', 'process'));
            }
        }

        $asks = $asks->latest()->paginate(10);
        return view('admin.ask', compact('asks', 'type', 'process'));
    }

    public function askShow(Request $request, $id){
        $ask = Ask::find($id);   
        return view('admin.ask-detail', compact('ask'));
    }

    public function askUpdate(Request $request, $id){
        $rules = [
            'answer' => 'required'
        ];

        $messages = [
            'answer.required' => '답변 메세지를 입력하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $ask = Ask::find($id);
        $ask->process    = $request->process;
        $ask->answer     = $request->answer;
        $ask->update();

        if($ask->process == "답변 완료"){
            $main_msg = "1:1 문의내역 답변 도착";
            $sub_msg = "관리자님이 회원님에게 접수하신 문의에 답변을 보냈습니다. 확인해주세요";
            $noti = new Notification();
            $noti->adminToUser($ask->member_id, "ask", $main_msg, $sub_msg);
        }

        return response()->json([
            'success' => true, 'msg' => '저장되었습니다'
        ]);
    }

//------------------------------------
// 신고하기 관리
//------------------------------------
    public function report(Request $request){
        $reports = Report::latest()->paginate(10);
        return view('admin.report', compact('reports'));
    }

    public function reportShow(Request $request, $report){
        $report = Report::find($report);
        $target  = $report->reportable_type;
        $id      = $report->reportable_id;

        if($target == "feeds"){
            // 신고 타겟 :: 피드
            $isFeed = Feed::where('id',$id)->exists();
            if($isFeed){
                // 존재하는 피드
                $feed = Feed::find($id);
                return view('admin.report-detail', compact('report','target','isFeed','feed'));
            }else{
                // 존재하지 않은 피드
                return view('admin.report-detail', compact('report','target','isFeed'));
            }
        }else if($target == "replies"){
            // 신고 타겟 :: 댓글
            $isReply = Reply::where('id',$id)->exists();
            if($isReply){
                // 존재하는 댓글
                $reply = Reply::find($id);
                return view('admin.report-detail', compact('report','target','isReply','reply'));
            }else{
                // 존재하지 않은 댓글
                return view('admin.report-detail', compact('report','target','isReply'));
            }
        }else{
            return back();
        }
    }

    public function reportUpdate(Request $request, $id){
        $report = Report::find($id);

        if($request->process != "접수"){
            $rules = [
                'result' => 'required'
            ];

            $messages = [
                'result.required' => '처리 결과 내용을 입력하세요'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $errors = $validator->errors();
            foreach($errors->all() as $message){
                return response()->json([
                    'success' => false, 'msg' => $message
                ]);
            }
        }

        if($request->process == "삭제"){
            if($report->reportable_type == "feeds"){
                $del_files = Upload::where([['file_type','feeds'],['file_id',$report->reportable_id]]);
                foreach($del_files->get() as $del_file){
                    if($del_file->ext == "mp4"){
                        Storage::delete('public/uploads/feed/'.$del_file->reference);
                    }
                    Storage::delete('public/uploads/feed/'.$del_file->fn);   
                }
                $del_files->delete();

                // 게시글 삭제, 댓글 삭제
                Feed::where('id',$report->reportable_id)->delete(); 
                Reply::where([['reply_type',$report->reportable_type],['reply_id',$report->reportable_id]])->delete();

            }else if($report->reportable_type == "replies"){
                $reply = Reply::find($report->reportable_id);
                $reply->content = "관리자에 의해 삭제된 댓글 입니다";
                $reply->update();
            }
        }

        $report->process = $request->process;
        $report->result = $request->process == "접수" ? "" : $request->result;
        $report->update();

        return response()->json([
            'success' => true, 'msg' => '처리되었습니다'
        ]);
    }
//------------------------------------
// 푸시 메세지
//------------------------------------
    public function push(Request $request){
        $templates = PushTemplate::all();

        return view('admin.push', compact('templates'));
    }

    public function send_message(Request $request){
        $noti = new Notification();
        
        $title = $request->input('title', '너만없는 고양이');
        $message = $request->input('message');
        $target = $request->input('target');

        if($target == "sel"){
            // 선택한 회원 발송
            $aos_arr = $request->input('aos_arr');
            $ios_arr = $request->input('ios_arr');

            if(is_array($aos_arr)){
                $tokens = [];
                foreach ($aos_arr as $key => $member_id) {
                    $tokens[$key] = PushMessage::where('member_id',$member_id)->first()->token;
                }
                $noti->noti_android($tokens, $title, $message);
                $noti->make_history($title, $message, "회원선택", $aos_arr);
            }
            if(is_array($ios_arr)){
                foreach($ios_arr as $member_id){
                    $token = PushMessage::where('member_id', $member_id)->first()->token;
                    $noti->noti_apple($token, $title, $message);
                }
                $noti->make_history($title, $message, "회원선택", $ios_arr);
            }
        }else{
            // 전체 회원 발송
            $pushes = PushMessage::where('token', '!=', null)->get();
            foreach($pushes as $push){
                if($push->type == "Android"){
                    $noti->noti_android([$push->token], $title, $message);
                }else if($push->type == "Apple"){
                    $noti->noti_apple($push->token, $title, $message);
                }
            }
        }
        
        return response()->json([
            'success' => true, 'msg' => '전송되었습니다'
         ]);
        
    }

    public function getTokens(Request $request){
        $sort    = $request->input('sort','nick');
        $value   = $request->input('value','');
        
        if($value == ""){
            return response()->json([
                'success' => false, 'msg' => '검색어를 입력하세요'
            ]);
        }

        // DB::enableQueryLog();
        if($request->except == null){
            // except : 불러올 목록중에 제외하고 (이미 전송할 회원목록에 추가 되어있다는 뜻)
            $members = Member::where($sort,'like',$value.'%')->get('id');
        }else{
            $members = Member::where($sort,'like',$value.'%')->whereNotIn('id', $request->except)->get('id');
        }

        $tokens = PushMessage::where('token','!=',null)->whereIn('member_id', $members)->get();
        foreach($tokens as $token){
            $token->nick = $token->member()->first()->nick;
        }

        return response()->json([
            'success' => true, 'tokens' => $tokens
        ]);
        // dd(DB::getQueryLog(), $members);
    }

    public function pushHistory(Request $request){
        $histories = PushHistory::latest()->paginate(10);
        return view('admin.push-history', compact('histories'));
    }

    public function pushTemplate(Request $request){
        $templates = PushTemplate::all();

        return view('admin.push-template', compact('templates'));
    }

    public function getPushTemplate(Request $request, $id){
        $template = PushTemplate::find($id);

        if($template == null){
            return response()->json([
                'success' => false, 'msg' => '템플릿 오류'
            ]);
        }else{
            return response()->json([
                'success' => true, 'main_msg' => $template->main_msg, 'sub_msg' => $template->sub_msg
            ]);
        }
    }

    public function pushTemplateStore(Request $request){
        $rules = [
            'name'       => 'required|unique:push_templates,name',
            'main_msg'   => 'required',
            'sub_msg'    => 'required'
        ];

        $messages = [
            'name.required'      => '템플릿 이름을 입력하세요',
            'name.unique'        => '현재 사용중인 템플릿 이름입니다',
            'main_msg.required'  => '알림 제목을 입력하세요',
            'sub_msg.required'   => '알림 내용을 입력하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $template = new PushTemplate();
        $template->name = $request->name;
        $template->main_msg = $request->main_msg;
        $template->sub_msg = $request->sub_msg;
        $template->save();

        return response()->json([
            'success' => true, 'msg' => '저장되었습니다'
        ]);

    }

    public function pushTemplateUpdate(Request $request, $id){
        $rules = [
            'main_msg'   => 'required',
            'sub_msg'    => 'required'
        ];

        $messages = [
            'main_msg.required'  => '알림 제목을 입력하세요',
            'sub_msg.required'   => '알림 내용을 입력하세요'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $template = PushTemplate::find($id);
        $template->main_msg = $request->main_msg;
        $template->sub_msg = $request->sub_msg;
        $template->update();

        return response()->json([
            'success' => true, 'msg' => '저장되었습니다'
        ]);
    }

    public function pushTemplateDestroy(Request $request, $id){
        PushTemplate::find($id)->delete();
        
        return response()->json([
            'msg' => '삭제되었습니다'
        ]);
    }

//------------------------------------
// 업로드 :: 현재 쓰이는 곳(이벤트/공지사항 등록 에디터)
//------------------------------------
    public function upload(Request $request){
        $rules = [
            'imageFile' => 'mimetypes:image/jpeg,image/png,image/gif'
        ];

        $messages = [
            'imageFile.mimetypes' => '지원하지 않는 확장자입니다'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();
        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }
        $file_type   = $request->input('file_type'); // 파일 모델
        $imageFile     = $request->file('imageFile');

        list($msec, $sec) = explode(" ", microtime());
        $ext = $imageFile->extension();
        $front = $sec.substr($msec, 2, 6);
        $fn = $front.".".$ext;
        $imageFile->storeAs('public/uploads/'.$file_type.'/', $fn); // 원본 저장
        
        $img_path = "storage/uploads/".$file_type."/".$fn;  // 이미지 파일 경로
        //gif, jpeg, png
        if($ext == "jpg" || $ext == "jpeg"){
            $img = imagecreatefromjpeg($img_path);
        }else if($ext == "gif"){
            $img = imagecreatefromgif($img_path);
        }else if($ext == "png"){
            $img = imagecreatefrompng($img_path);
            // 배경 투명 유지
            imagealphablending( $img, false );
            imagesavealpha( $img, true );
        }

        $exif = @exif_read_data($img_path);
        if(!empty($exif['Orientation'])){
            switch($exif['Orientation']){
            case 8:
                $img = imagerotate($img, 90, 0); break;
            case 3:
                $img = imagerotate($img, 180, 0); break;
            case 6:
                $img = imagerotate($img, -90, 0); break;
            }
        }

        $img = imagescale($img, 640, -1); // 화면
                
        if($ext == "jpg" || $ext == "jpeg"){
            imagejpeg($img, $img_path); // 3th : 0~100 압축률
        }else if($ext == "gif"){
            imagegif($img, $img_path); // 3th : 0~100 압축률
        }else if($ext == "png"){
            imagepng($img, $img_path); // 3th : 0~9 압축률
        }
        $img_info = getimagesize($img_path);
        
        $upload = new Upload();
        $upload->file_type   = $file_type;
        $upload->file_id     = 0;
        $upload->fn_ori      = $imageFile->getClientOriginalName();
        $upload->fn          = $fn;
        $upload->ext         = $ext;
        $upload->size        = round(filesize($img_path)/ 1024, 1); // kB로 표시
        $upload->width       = $img_info[0];
        $upload->height      = $img_info[1];
        $upload->save();
        
        imagedestroy($img);

        return response()->json([
            'success' => true, 'fn_dir' => "/".$img_path, 'fn' => $fn
        ]);
    }

//------------------------------------
// 공지사항 + 이벤트 통합
//------------------------------------
    public function writing(Request $request){
        $type = $request->input('type', 'notice');

        $writings = Writing::where('type',$type)->latest()->paginate(12);

        return view('admin.writing', compact('writings', 'type'));
    }

    public function writingCreate(Request $request){
        $type = $request->input('type', 'notice');

        return view('admin.writing-detail', compact('type'));
    }

    public function writingStore(Request $request){
        $rules = [
            'title' => 'required',
            'start_date' => 'required|date_format:Y-m-d H:i',
            'end_date' => 'required|date_format:Y-m-d H:i',
            'advertise_fn' => 'required',
            'banner_fn' => 'required',
            'advertise_file' => 'mimes:jpeg,png,gif',
            'banner_file' => 'mimes:jpeg,png,gif',
            'content' => 'required'
        ];

        $messages = [
            'title.required'         => '제목을 입력하세요',
            'start_date.required'    => '광고 시작날짜를 선택하세요',
            'start_date.date_format' => '광고 시작날짜의 형식이 맞지 않습니다 *형식 : Y-m-d H:i',
            'end_date.required'      => '광고 마감날짜를 선택하세요',
            'end_date.date_format'   => '광고 마감날짜의 형식이 맞지 않습니다 *형식 : Y-m-d H:i',
            'advertise_fn.required'  => '팝업 이미지를 넣어주세요',
            'banner_fn.required'     => '배너 이미지를 넣어주세요',
            'advertise_file.mimes'   => '이벤트 광고의 이미지의 허용 확장자는 [jpeg, png, gif]입니다',
            'banner_file.mimes'      => '이벤트 배너의 이미지의 허용 확장자는 [jpeg, png, gif]입니다',
            'content.required'       => '내용을 입력하세요'
        ];

        if($request->form == "link"){
            $rules['form_link'] = 'required';
            $messages['form_link.required'] = '링크 주소를 입력하세요';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $writing = new Writing();
        $writing->type       = $request->type;
        $writing->title      = $request->title;
        $writing->content    = $request->content;
        $writing->start_date = $request->start_date;
        $writing->end_date   = $request->end_date;
        $writing->form       = $request->form;
        if($request->form == "link"){ // 추가 폼이 링크연결이면
            $writing->form_link = $request->form_link;
        }
        $writing->save();

        $img_arr = [
            "advertise" => $request->file('advertise_file'),
            "banner"    => $request->file('banner_file'),
        ];

        foreach ($img_arr as $key => $imgFile) {
            list($msec, $sec) = explode(" ", microtime());
            $ext = $imgFile->extension();
            $front = $sec.substr($msec, 2, 6);
            $fn = $front.".".$ext;
            $imgFile->storeAs('public/uploads/writings/', $fn); // 원본 저장
            
            $img_path = "storage/uploads/writings/".$fn;  // 이미지 파일 경로
            //gif, jpeg, png
            if($ext == "jpg" || $ext == "jpeg"){
                $img = imagecreatefromjpeg($img_path);
            }else if($ext == "gif"){
                $img = imagecreatefromgif($img_path);
            }else if($ext == "png"){
                $img = imagecreatefrompng($img_path);
                // 배경 투명 유지
                imagealphablending( $img, false );
                imagesavealpha( $img, true );
            }
    
            $exif = @exif_read_data($img_path);
            if(!empty($exif['Orientation'])){
                switch($exif['Orientation']){
                case 8:
                    $img = imagerotate($img, 90, 0); break;
                case 3:
                    $img = imagerotate($img, 180, 0); break;
                case 6:
                    $img = imagerotate($img, -90, 0); break;
                }
            }
            
            if($key == "advertise"){
                $img = imagescale($img, 768, 900);
            }else if($key == "banner"){
                $img = imagescale($img, 768, 325);
            }

            if($ext == "jpg" || $ext == "jpeg"){
                imagejpeg($img, $img_path); // 3th : 0~9 압축률
            }else if($ext == "gif"){
                imagegif($img, $img_path); // 3th : 0~9 압축률
            }else if($ext == "png"){
                imagepng($img, $img_path); // 3th : 0~9 압축률
            }
            $img_info = getimagesize($img_path);
            // dd($img_info);
            $upload = new Upload();
            $upload->file_type   = "writings";
            $upload->file_id     = $writing->id;
            $upload->reference   = $key;
            $upload->fn_ori      = $imgFile->getClientOriginalName();
            $upload->fn          = $fn;
            $upload->ext         = $ext;
            $upload->size        = round(filesize($img_path)/ 1024, 1); // kB로 표시
            $upload->width       = $img_info[0];
            $upload->height      = $img_info[1];
            $upload->save();
            
            imagedestroy($img);  
        }

        if($request->has('save_list')){
            foreach($request->input('save_list') as $image){
                $upload = Upload::where([['file_type','writings'],['fn',$image]])->update(['file_id' => $writing->id]);
            }
        }

        return response()->json([
            'success' => true, 'msg' => '등록되었습니다'
        ]);

    }

    public function writingEdit(Request $request, $id){
        $writing = Writing::find($id);

        return view('admin.writing-detail', compact('writing'));
    }

    public function writingUpdate(Request $request, $id){
        $rules = [
            'title' => 'required',
            'start_date' => 'required|date_format:Y-m-d H:i',
            'end_date' => 'required|date_format:Y-m-d H:i',
            'advertise_fn' => 'required',
            'banner_fn' => 'required',
            'content' => 'required'
        ];

        $messages = [
            'title.required'         => '제목을 입력하세요',
            'start_date.required'    => '광고 시작날짜를 선택하세요',
            'start_date.date_format' => '광고 시작날짜의 형식이 맞지 않습니다 *형식 : Y-m-d H:i',
            'end_date.required'      => '광고 마감날짜를 선택하세요',
            'end_date.date_format'   => '광고 마감날짜의 형식이 맞지 않습니다 *형식 : Y-m-d H:i',
            'advertise_fn.required'  => '팝업 이미지를 넣어주세요',
            'banner_fn.required'     => '배너 이미지를 넣어주세요',
            'content.required'       => '내용을 입력하세요'
        ];

        if($request->form == "link"){
            $rules['form_link'] = 'required';
            $messages['form_link.required'] = '링크 주소를 입력하세요';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        $errors = $validator->errors();

        foreach($errors->all() as $message){
            return response()->json([
                'success' => false, 'msg' => $message
            ]);
        }

        $writing = Writing::find($id);
        $writing->type       = $request->type;
        $writing->title      = $request->title;
        $writing->content    = $request->content;
        $writing->start_date = $request->start_date;
        $writing->end_date   = $request->end_date;
        $writing->form       = $request->form;
        if($request->form == "link"){ // 추가 폼이 링크연결이면
            $writing->form_link = $request->form_link;
        }
        $writing->update();

        $img_arr = [
            "advertise" => $request->file('advertise_file'),
            "banner"    => $request->file('banner_file'),
        ];

        foreach ($img_arr as $key => $imgFile) {
            $compare = Upload::where([['file_type', 'writings'],['file_id', $id],['reference', $key]])->first();

            if($compare->fn_ori != $request->input($key."_fn")){
                // 확장자 체크
                $ext = $imgFile->extension();
                if(!($ext == "png" || $ext == "gif" || $ext == "jpeg" || $ext == "jpg")){
                    // 하나라도 만족하는 확장자가 없으면
                    return response()->json([
                        'success' => false, 'msg' => '이미지는 [png, gif, jpeg, jpeg]의 확장자만 가능합니다'
                    ]);
                }

                // 다른 이미지 삽입
                Storage::delete('public/uploads/event/'.$compare->fn_ori); // :: 기존 이미지 삭제
                $compare->delete(); // :: 기존 이미지 데이터 삭제

                list($msec, $sec) = explode(" ", microtime());
                $ext = $imgFile->extension();
                $front = $sec.substr($msec, 2, 6);
                $fn = $front.".".$ext;
                $imgFile->storeAs('public/uploads/writings/', $fn); // 원본 저장
                
                $img_path = "storage/uploads/writings/".$fn;  // 이미지 파일 경로
                //gif, jpeg, png
                if($ext == "jpg" || $ext == "jpeg"){
                    $img = imagecreatefromjpeg($img_path);
                }else if($ext == "gif"){
                    $img = imagecreatefromgif($img_path);
                }else if($ext == "png"){
                    $img = imagecreatefrompng($img_path);
                    // 배경 투명 유지
                    imagealphablending( $img, false );
                    imagesavealpha( $img, true );
                }
        
                $exif = @exif_read_data($img_path);
                if(!empty($exif['Orientation'])){
                    switch($exif['Orientation']){
                    case 8:
                        $img = imagerotate($img, 90, 0); break;
                    case 3:
                        $img = imagerotate($img, 180, 0); break;
                    case 6:
                        $img = imagerotate($img, -90, 0); break;
                    }
                }
                
                if($key == "advertise"){
                    $img = imagescale($img, 768, 900);
                }else if($key == "banner"){
                    $img = imagescale($img, 768, 325);
                }
    
                if($ext == "jpg" || $ext == "jpeg"){
                    imagejpeg($img, $img_path); // 3th : 0~9 압축률
                }else if($ext == "gif"){
                    imagegif($img, $img_path); // 3th : 0~9 압축률
                }else if($ext == "png"){
                    imagepng($img, $img_path); // 3th : 0~9 압축률
                }
                $img_info = getimagesize($img_path);

                $upload = new Upload();
                $upload->file_type   = "writings";
                $upload->file_id     = $writing->id;
                $upload->reference   = $key;
                $upload->fn_ori      = $imgFile->getClientOriginalName();
                $upload->fn          = $fn;
                $upload->ext         = $ext;
                $upload->size        = round(filesize($img_path)/ 1024, 1); // kB로 표시
                $upload->width       = $img_info[0];
                $upload->height      = $img_info[1];
                $upload->save();
                
                imagedestroy($img);  
            }
        }

        Upload::where([['file_type','writings'],['file_id', $id],['reference', null]])->update(['file_id' => 0]);
        if($request->has('save_list')){
            foreach($request->input('save_list') as $image){
                $upload = Upload::where([['file_type','writings'],['fn',$image]])->update(['file_id' => $writing->id]);
            }
        }

        return response()->json([
            'success' => true, 'msg' => '수정되었습니다'
        ]);
    }

    public function writingActive(Request $request){
        $id = $request->id;
        $active = $request->active;

        if($id == "" || $active == ""){
            return response()->json([
                'success' => false, 'msg' => '활성화 상태를 변경하는데 오류가 발생했습니다'
            ]);
        }        

        $writing = Writing::where('id',$id)->update(['active'=>$active]);

        return response()->json([
            'success' => true
        ]);
    }
   
    
    public function writingDestroy(Request $request, $id){
        $del_files = Upload::where([['file_type','writings'],['file_id', $id]]); // 공지사항, 이벤트 파일들
        if($del_files->exists()){
            // 삭제할 파일이 존재하면
            foreach($del_files->get('fn') as $del_file){
                Storage::delete('public/uploads/writings/'.$del_file);
            }
            $del_files->delete();
        }
        Writing::find($id)->delete();

        return response()->json([
            'success' => true, 'msg' => '삭제되었습니다'
        ]);
    }

    public function partiList(Request $request){
        $parti_id = $request->input('id','0');
        $event    = Writing::find($parti_id);
        //존재하는 이벤트 && 개인정보 수집 이벤트 인지 확인
        if($event != null && ($event->form == "privacy" || $event->form == "contents") ){
            //
            $partis = Participation::where('event_id', $parti_id)->latest()->paginate(20);
            return view('admin.partiTable', compact('partis','event'));
        }else{
            return redirect()->back();
        }
    }

    public function parti_excel(Request $request){
        $id          = $request->input('id');
        $start_date  = $request->input('start_date');
        $end_date    = $request->input('end_date');

        $event = Writing::find($id)->title;

        return (new PartiExport($id, $start_date, $end_date))->download($event.'_'.date("Ymd").'.xlsx');
    }

//------------------------------------
// 관리자 로그아웃
//------------------------------------
    public function adminLogout(Request $request){
        $request->session()->flush();
        return redirect('/admin/login')->with('abnormal', '정상적으로 로그아웃 되었습니다');
    }
}
