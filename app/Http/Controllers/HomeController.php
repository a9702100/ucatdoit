<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feed;
use App\Models\Member;
use App\Models\Upload;
use App\Models\HashTag;
use App\Models\History;
use App\Models\Writing;
use App\Models\Heart;
use App\Models\Friend;
use App\Models\Conceal;
use App\Models\Reply;
use App\Models\PushMessage;
use App\Models\Report;

class HomeController extends Controller
{
    //
    public function home(Request $request){
        $member_id = $request->session()->get('member');
        $agree = Member::where('id',$member_id)->value('personal_agree');
        if($agree == "0"){
            return redirect('/account/sns_join?next='.$member_id);
        }

        // SNS 신규회원 인지 판단
        $newbie    = Member::find($member_id)->nick == "" ? true : false;

        // 피드 가져오기
        $reports  = Conceal::where([['member_id', $member_id],['concealable_type', 'members']])->get('concealable_id'); // 차단한 회원 고유번호
        $blocks   = Friend::where([['my_idx',$member_id],['relation','block']])->get('id');                          // 차단한 회원 고유번호
        $conceals = Conceal::where([['member_id',$member_id],['concealable_type','feeds']])->get('concealable_id');  // 숨긴 게시글 번호

        $feeds    = Feed::whereDate('created_at', '<=', today());

        $time     = time();                     

        // 좋아요 -> 최근 7일간 게시된 글 중 좋아요 많은 순서
        $heart_cnt = Heart::selectRaw('heartable_id, count(*) as heart_cnt')
                            ->groupBy('heartable_id')->orderBy('heart_cnt','desc');

        $ranking = Feed::leftjoinSub($heart_cnt, 'hearts', function($join){
            $join->on('feeds.id', '=', 'hearts.heartable_id');
        });

        if($reports->isNotEmpty()){
            $feeds   = $feeds->whereNotIn('feedable_id', $reports);
            $ranking = $ranking->whereNotIn('feedable_id', $reports);
        }
        if($blocks->isNotEmpty()){
            $feeds   = $feeds->whereNotIn('feedable_id', $blocks);
            $ranking = $ranking->whereNotIn('feedable_id', $blocks);
        }
        if($conceals->isNotEmpty()){
            $feeds   = $feeds->whereNotIn('id', $conceals);
            $ranking = $ranking->whereNotIn('id', $conceals);
        }
        $feeds   = $feeds->latest()->take(10)->get();
        $ranking = $ranking->whereDate('created_at','>=',date("Y-m-d H:i:s",strtotime("-7 day", $time)))->where('heart_cnt','>', 0)->take(10)->get();

        // 팝업 띄울 목록
        $popups = Writing::where('active', '1')->whereDate('start_date','<=',today())->whereDate('end_date','>=',today())->latest()->get();
        $ongoings = Writing::where('type','!=','affiliate')->whereDate('start_date','<=',today())->whereDate('end_date','>=',today())->latest()->get();

        // 최근이력 빨간 동그라미 처리
        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인      
        $isEvent   = Writing::whereDate('start_date','<=',today())->whereDate('end_date','>=',today())->latest()->get()->count();        
                   
        return view('index', compact('feeds','member_id','newbie','isEvent','isHistory','ranking','popups','ongoings'));
    }
    
    public function moreFeed(Request $request){
        $member_id   = $request->session()->get('member');
        $last_id     = $request->feed_id;

        // 피드 가져오기
        $reports  = Conceal::where([['member_id', $member_id],['concealable_type', 'members']])->get('concealable_id'); // 차단한 회원 고유번호
        $blocks   = Friend::where([['my_idx',$member_id],['relation','block']])->get('id');
        $conceals = Conceal::where([['member_id',$member_id],['concealable_type','feeds']])->get('concealable_id'); // 숨긴번호

        $feeds = Feed::where('id','<',$last_id)->whereDate('created_at', '<=', today());
        if($reports->isNotEmpty()){
            $feeds = $feeds->whereNotIn('feedable_id', $reports);
        }
        if($blocks->isNotEmpty()){
            $feeds = $feeds->whereNotIn('feedable_id', $blocks);
        }
        if($conceals->isNotEmpty()){
            $feeds = $feeds->whereNotIn('id', $conceals);
        }
        $feeds = $feeds->latest()->take(10)->get()->load('heart','replies','files','tags');

        if($feeds->count() > 0){
            foreach($feeds as $feed){
                $feed->nick      = $feed->feedable->nick;
                $feed->member_fn = isset($feed->feedable->upload) ? $feed->feedable->upload->fn : "";
                $feed->isFriend  = $feed->feedable->id != $member_id && $feed->feedable->friend->where('my_idx',$member_id)->isEmpty();
                $feed->isWriter  = $member_id == $feed->feedable->id;
                $feed->isHeart   = $feed->heart->where('member_id',$member_id)->isNotEmpty();
            }
    
            return response()->json([
                'success' => true, 'feeds'=> $feeds, 'last' => $feeds->last()->id
            ]);
        }else{
            return response()->json([
                'success' => false 
            ]);
        }

    }

    public function getImage(Request $request){
        $feed_id = $request->input('feed_id','0');
        
        if($feed_id != "0"){
            $feed = Feed::where('id',$feed_id);
            if($feed->exists()){
                // 존재하는 피드 
                if($feed->first()->files->first()->ext == "mp4"){
                    // 첫 이미지가 동영상
                    return response()->json([
                        'success' => true, 'img' => $feed->first()->files->first()->reference
                    ]);
                }else{
                    // 첫 이미지가 사진
                    return response()->json([
                        'success' => true, 'img' => $feed->first()->files->first()->fn
                    ]);
                }
            }else{
                return response()->json([
                    'success' => false
                ]);
            }
        }else{
            return response()->json([
                'success' => false
            ]);
        }
    }
    
    public function noticeShow(Request $request, $id){
        $notice = Notice::find($id);
        $member_id = $request->session()->get('member');

        $isHistory = History::where([['friend_idx', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('notice', compact('notice','isHistory'));
    }

    public function conceal(Request $request){
        $member_id        = $request->session()->get('member');
        $concealable_type = $request->concealable_type;
        $concealable_id   = $request->concealable_id;

        if($request->has("nums")){
            $nums = $request->input('nums'); // 숨길 댓글 번호
            $id   = $request->input('id');
            $c_nums = Reply::whereIn('group_id', $nums)->get("id");

            foreach ($c_nums as $num) {
                $conceal = new Conceal();
                $conceal->member_id        = $member_id;
                $conceal->concealable_type = $concealable_type;
                $conceal->concealable_id   = $num->id;
                $conceal->save();
            }

            // 댓글리스트 새로 불러옴
            $member_id = $request->session()->get('member'); // 내 고유번호
            $writer_id = Feed::find($id)->feedable_id;       // 작성자 고유번호 

            $writer = $member_id == $writer_id ? "me" : "other"; // 피드 작성자가 누구냐? 

            $conceals = Conceal::where([['member_id', $member_id],['concealable_type','replies']])->get('concealable_id');
            if($conceals->isEmpty()){
                // 숨긴 댓글 없음
                $replies = Reply::where([['reply_type','feeds'],['reply_id', $id]])->orderBy('group_id','desc')->orderBy('level','asc')->get();
            }else{
                // 숨긴 댓글 있음
                $replies = Reply::where([['reply_type','feeds'],['reply_id',$id]])->orderBy('group_id','desc')->orderBy('level','asc')->whereNotIn('id',$conceals)->latest()->get();
            }

            if($replies->count() > 0){
                foreach($replies as $reply){
                    $reply->nick      = $reply->member->nick;
                    $reply->member_fn = $reply->member->upload != null ? $reply->member->upload->fn : "";
                    $reply->own       = $reply->member_id == $member_id ? "me" : "other";
                }
                return response()->json([
                    'status' => 'full', 'replies' => $replies, 'writer' => $writer, 'msg' => '선택한 댓글이 차단되었습니다'
                ]);
            }else{
                return response()->json([
                    'status' => 'empty', 'writer' => $writer, 'msg' => '선택한 댓글이 차단되었습니다'
                ]);
            }

        }else{
            // 피드 숨김
            $conceal = new Conceal();
            $conceal->member_id        = $member_id;
            $conceal->concealable_type = $concealable_type;
            $conceal->concealable_id   = $concealable_id;
            $conceal->save();
    
            return response()->json([
                'success' => true
            ]);
        }

    }

    public function advertise(){
        return view('advertise');
    }
}
