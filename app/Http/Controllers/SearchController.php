<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\Friend;
use App\Models\HashTag;
use App\Models\Heart;
use App\Models\History;
use App\Models\Conceal;
use Carbon\Carbon;


class SearchController extends Controller
{
    //
    public function search(Request $request){
        $dt = Carbon::now();

        $member_id = $request->session()->get('member');

        $blocks = Conceal::where([['member_id',$member_id],['concealable_type','members']])->get('concealable_id');

        // 추천 멤버
        $friends_id = Friend::select('friend_idx')->where('my_idx',$member_id)->get('friend_idx');
        $strangers = Member::where([['id','!=',$member_id],['nick', '!=', '']])->whereNotIn('id', $friends_id)->whereNotIn('id', $blocks)->inRandomOrder()->take(5)->get();
        
        // 핫 태그
        $cnt_tags = HashTag::select('content', HashTag::raw('count(*) as total'))->groupBy('content');
        $hot_tags = HashTag::select('hash_tags.content', 'total')->leftJoinSub($cnt_tags, 'cnt_tags', function($join){
            $join->on('hash_tags.content', '=' ,'cnt_tags.content');
        })->where('created_at','>=',$dt->subHours(3))->orderBy('total','desc')->distinct()->take(10)->get();

        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('search', compact('strangers','hot_tags','isHistory'));
    }

    public function searchKeyword(Request $request){
        $member_id   = $request->session()->get('member');
        $keyword     = $request->input('keyword');

        // 검색된 멤버 리스트
        $blocks = Conceal::where([['member_id',$member_id],['concealable_type','members']])->get('concealable_id');
        $result_members = Member::where([['nick','like','%'.$keyword.'%'],['nick','!=','']])->whereNotIn('id', $blocks)->take(10)->get(); // 검색어로 찾은 회원 목록
        foreach($result_members as $member){
            if($member->id == $member_id){
                $member->relation = "me";
            }else{
                $relation = Friend::where([['my_idx',$member_id],['friend_idx',$member->id]])->value('relation');
                $member->relation = $relation == null ? "not" : $relation;
            }
            $member->fn = isset($member->upload) ? $member->upload->fn : "";
        }
        // 검색된 태그 리스트
        $result_tags = HashTag::select('content', HashTag::raw('count(*) as total'))->where('content','like','%'.$keyword.'%')->groupBy('content')->take(10)->get();
        
        return response()->json([
            'member_cnt' => $result_members->count(), 'result_members' => $result_members,
            'tags_cnt' => $result_tags->count(), 'result_tags' => $result_tags
        ]);
    }

    public function searchShow(Request $request){
        $content = $request->content;
        $cnt_hearts  = Heart::select('*', Heart::raw('count(*) as total'))->where('heartable_type','feeds')->groupBy('heartable_id');

        $hots        = HashTag::select('*')->leftJoinSub($cnt_hearts, 'cnt_hearts', function($join){
                            $join->on('hash_tags.taggable_id','=','cnt_hearts.heartable_id');
                        })->where('content',$content)->take(30)->get();

        $recents     = HashTag::where('content', $content)->latest()->take(30)->get();

        $member_id = $request->session()->get('member');
        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인
        $page = 1;
        return view('search-detail', compact('recents','content','hots','isHistory', 'page'));
    }

    public function moreFeed(Request $request){
        $page    = $request->page;
        $tab     = $request->tab;
        $content = $request->content;

        // 인기게시물
        if($tab == "hot"){
            $cnt_hearts  = Heart::select('*', Heart::raw('count(*) as total'))->where('heartable_type','feeds')->groupBy('heartable_id');

            $feeds = HashTag::select('*')->leftJoinSub($cnt_hearts, 'cnt_hearts', function($join){
                $join->on('hash_tags.taggable_id','=','cnt_hearts.heartable_id');
            })->where('content',$content)->skip(($page - 1) * 30)->take(30)->get();
        }
        // 최근게시물
        if($tab == "recent"){
            $feeds = HashTag::where('content', $content)->latest()->skip(($page - 1) * 30)->take(30)->get();
        }

        if($feeds->count() > 0){
            foreach ($feeds as $feed) {
                $feed->url = "feed/".$feed->taggable_id."/show";
                if($feed->taggable->files->count() > 0){
                    if($feed->taggable->files->first()->ext == "mp4"){
                        $feed->src = "storage/uploads/feed/".$feed->taggable->files->first()->reference;
                    }else{
                        $feed->src = "storage/uploads/feed/".$feed->taggable->files->first()->fn;
                    }
                }else{
                    $feed->src = "no-img";
                }
            }
            return response()->json([
                'success' => true, 'page' => $page + 1, 'feeds' => $feeds
            ]);
        }else{
            return response()->json([
                'success' => false, 'page' => $page
            ]);
        }
        
    }

    public function searchFriend(Request $request){
        $member_id   = $request->session()->get('member');
        $other       = $request->input('other', $member_id);
        $keyword     = '%'.$request->keyword.'%';

        // other 의 친구
        if($member_id == $other){
            // 나의 친구 목록
            $friends_id = Friend::where('my_idx', $other)->get('friend_idx'); // 친구 번호 리스트
        }else{
            // 친구의 친구 목록
            $friends_id = Friend::where([['my_idx', $other],['relation','friend']])->get('friend_idx'); // 친구 번호 리스트
        }
        $result_friends = Member::where([['id','!=',$other],['nick','like',$keyword],['nick','!=','']])->whereIn('id',$friends_id)->get(); // 검색 결과 :: 친구목록

        foreach($result_friends as $friend) {
            $relation = Friend::where([['my_idx',$other],['friend_idx',$friend->id]])->value('relation');
            $friend->relation = $relation == null ? "not" : $relation;
            $friend->fn = isset($friend->upload) ? $friend->upload->fn : "";
        }
        
        return response()->json([
            'friends_cnt' => $result_friends->count(), 'result_friends' => $result_friends
        ]);
    }

    public function searchMember(Request $request){
        $find_str = $request->input('find_str');

        $members = Member::where([['nick','like',$find_str."%"],['nick','!=','']])->take(10)->get();

        foreach($members as $member){
            $member->profile = $member->upload != null ? $member->upload->fn : "";
        }

        if($members->count() == 0){
            return response()->json([
                'success' => false
            ]);
        }else{
            return response()->json([
                'success' => true, 'members' => $members
            ]);
        }
    }

}
