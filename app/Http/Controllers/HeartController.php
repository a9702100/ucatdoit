<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\Notification;

use App\Models\Heart;
use App\Models\Friend;
use App\Models\Feed;
use App\Models\History;
use App\Models\Member;

class HeartController extends Controller
{
    //
    public function heartStore(Request $request){
        // 하트 추가
        $member_id       = $request->session()->get("member");
        $heartable_type  = $request->input('heartable_type');
        $heartable_id    = $request->input('heartable_id');

        
        $heart = new Heart();
        $heart->heartable_type   = $heartable_type;
        $heart->heartable_id     = $heartable_id;
        $heart->member_id        = $member_id;
        $heart->save();
        
        $receiver = Feed::find($heartable_id)->feedable_id;

        // if($member_id != $receiver){
        //     // 다른 사람이 쓴 글에 좋아요를 눌렀을 때 푸시 메세지 전송
        //     $main_msg = "님이 내 피드에 좋아요를 눌렀습니다";
    
        //     $noti = new Notification();
        //     $noti->send_noti($member_id, $receiver, "good", $heartable_id, $main_msg, "");
        // }
        
        return response()->json([
            'success' => true
        ]);
    }

    public function heartDestroy(Request $request){
        $member_id       = $request->session()->get("member");
        $heartable_type  = $request->input('heartable_type');
        $heartable_id    = $request->input('heartable_id');

        $heart = Heart::where([['heartable_type',$heartable_type],['heartable_id', $heartable_id],['member_id', $member_id]]);
        $heart->delete();

        return response()->json([
            'success' => true
        ]);
    }

    public function heartList(Request $request, $id){
        $member_id      = $request->session()->get("member");
        $heartable_type = $request->input('heartable_type');

        $hearts = Heart::where([['heartable_type',$heartable_type],['heartable_id', $id]])->latest()->get(); // 피드 좋아요 누른 리스트
        $friends = Friend::where('my_idx', $member_id);

        foreach($hearts as $heart){
            $pusher = $heart->member_id; // 누른 사람
            if($pusher == $member_id){
                // 누른 사람 == 나
                $heart->isFriend = "me";
            }else if($friends->where('friend_idx', $pusher)->exists()){
                // 누른 사람이 나랑 관계가 있음
                $heart->isFriend = $friends->where('friend_idx', $pusher)->first()->relation;
            }else{
                // 누른 사람이 나와 관계 없음
                $heart->isFriend = "not";
            }

            $heart->nick = $heart->member->nick;
            $heart->profile = $heart->member->upload != null ? $heart->member->upload->fn : "";
        }

        return response()->json([
            'success' => true, 
            'hearts' => $hearts
        ]);
    }
}
