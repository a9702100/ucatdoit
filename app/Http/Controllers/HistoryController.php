<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\Rules;

use App\Models\Feed;
use App\Models\History;
use App\Models\Reply;
use App\Models\Member;
use App\Models\Upload;

class HistoryController extends Controller
{
    //
    public function history(Request $request){
        $rules = new Rules();
        $member_id = $request->session()->get('member');

        $histories = History::where('receiver', $member_id)->whereDate('created_at','>=',date("Y-m-d H:i:s", strtotime("-7 day", time())))->latest()->get();

        foreach($histories as $history){
            $sender = Member::find($history->sender);
            $history->read = "1";
            $history->update(); 

            $history->fn      = isset($sender->upload) ? $sender->upload->fn : "";
            $history->nick    = $sender->nick;
            $history->isThumb = false;
            $history->passing = $rules->passing_time($history->created_at); // 경과 시간
            if($history->detail_type == "feed"){
                $history->isThumb = true;
                $thumb = Upload::where([['file_type','feeds'],['file_id',$history->detail_id]])->first();
                if($thumb->ext == "mp4"){
                    $history->thumb_nail = $thumb->reference;
                }else{
                    $history->thumb_nail = $thumb->fn;
                }
            }
        }

        $isHistory = History::where([['receiver', $member_id],['read','0']])->get()->isNotEmpty(); // 읽지않은 최근이력 있는지 확인

        return view('history', compact('histories','isHistory'));
    }

    public function historyDestroy(Request $request){
        $member_id  = $request->session()->get('member');
        $type       = $request->input('type');
        
        if($type == "one"){
            // 최근이력 하나 삭제
            $history_id = $request->input('history_id');
            History::where('id', $history_id)->delete();
        }else if($type == "all"){
            // 최근이력 모두 삭제
            History::where('friend_idx', $member_id)->delete();
        }
        return response()->json([
            'success' => true
        ]);
    }
}
