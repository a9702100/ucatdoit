<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PushMessage;

class SettingsController extends Controller
{
    //
    public function settings(Request $request){
        $push = PushMessage::where('member_id', $request->session()->get('member'))->first();

        return view('settings', compact('push'));
    }

    public function settingsUpdate(Request $request){
        $member_id = $request->session()->get('member');
        $target    = $request->target;
        $status    = $request->status == "1" ? "0" : "1"; // 이전 상태가 1(ON) 이면 0(OFF)으로 변경 

        $push = PushMessage::where('member_id', $member_id)->first();
        $push->$target = $status;
        $push->update();

        return response()->json([
            'success' => true
        ]);
    }
}
