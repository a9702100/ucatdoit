<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->session()->has('member')){
            $member_id = $request->session()->get('member');
            $request->session()->put('member', $member_id);
            return $next($request);
        }

        return redirect('/');

    }
}
