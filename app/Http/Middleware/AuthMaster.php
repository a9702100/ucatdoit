<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthMaster
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->session()->has('master')){
            $master_id = $request->session()->get('master');
            $request->session()->put('master', $master_id);
            return $next($request);
        }

        return redirect('/admin/login')->with('abnormal','비정상적인 접근입니다');
    }
}
