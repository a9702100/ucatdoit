
$(document).ready(function(){

    // tab
    $('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

    // 눌렀을때 눈 아이콘 보였다가 안보여지기
    $(".login-wrap .no-eyes ").click(function(){
        $(".login-wrap .no-eyes").toggleClass("eyes");
        if($(".login-wrap .no-eyes").hasClass("eyes")){
            $("#passwd").attr("type","text");
        }else{
            $("#passwd").attr("type","password");
        }
    })

    $(document).mouseup(function (e){
        var LayerPopup = $(".gear-box");
        if(LayerPopup.has(e.target).length === 0){
          LayerPopup.removeClass("show");
        }
    });


    //팝업 클릭
    // $(".popup-box").hide();
    $(".all-close").click(function(){
        $(".popup-box").stop().fadeToggle(500);
    return false;
    });
   

    //공유하기 팝업 외에 영역 클릭
    $(".popup-box .btn-close").click(function(){
        $(".popup-box").addClass("hide");
    })


    // 처음에 들어왔을때 팝업
    $(".main-popup-box .btn-box").click(function(){
        $(".main-popup-box").hide()
    });

    
    //투게더
    $(".order-fixed").hide();
    $(this).find(".btn-share").click(function(){
        $(".order-fixed").toggle()
    });

})


