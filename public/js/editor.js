var file_list = [];
tinymce.init({
	relative_urls: false,
	selector: '#editor',
	height: '90%',
	resize: false,
	statusbar: false,
	plugins: 'image autolink link imagetools',
	placeholder: '내용을 입력하거나 사진을 추가하세요',
	language: 'ko_KR',
	toolbar: [' fontsizeselect | fontselect | ','bold italic | alignleft aligncenter alignright | forecolor backcolor | image'],
	fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt 48pt',
	font_formats: 'NotoSerif=NotoSerifKR;NotoSans=NotoSansKR;나눔고딕=NanumGothic;고딕=GothicA1;블랙한산스=BlackHanSans;Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace; AkrutiKndPadmini=Akpdmi-n',
	quickbars_image_toolbar: 'alignleft aligncenter alignright | rotateleft rotateright',
	toolbar_mode: 'wrap',
	image_title: true,
	link_default_protocol: 'https',
	link_assume_external_targets: true,
	images_upload_handler: tinymceUploadImg,
	content_style: '@import url("/css/font.css"); p > img {width: 100%; height: auto;}'
});