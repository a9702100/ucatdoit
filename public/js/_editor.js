var file_list = [];
tinymce.init({
  relative_urls:false, 
  selector: '#content',
  height : '60vh',
  resize : false,
  statusbar : false,
  plugins : '',
  placeholder : '내용을 입력하거나 사진을 추가하세요',
  language : 'ko_KR',
  toolbar : ['bold italic | forecolor backcolor | alignleft aligncenter alignright', 'addImage'],
  toolbar_mode : 'wrap',
  setup: function (editor) {
	    editor.ui.registry.addButton('addImage', {
	      text: '사진 추가',
	      icon: 'image',
	      onAction: function () {
	        var input = document.createElement("input");
			input.setAttribute("type", "file");
			input.setAttribute("accept", "image/*");
	        input.click();
			input.onchange = function(e){
				var files = e.target.files;
				var filesArr = Array.prototype.slice.call(files);

				filesArr.forEach(function(file, index){
					var reader = new FileReader();
					file_list.push(file);
					reader.onload = function(e){
						var formData = new FormData();
                        formData.append("imgFile", file);
                        formData.append("file_type", $("#wrap").data('type'));
						formData.append("file_dir", $("#wrap").data("dir"));
						$.ajax({
							headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
							url : "/together/fileUpload",
							type : "post",
							processData : false,
							contentType : false,
							data : formData,
							dataType : "json",
							success : function(data){
								if(data["success"]){
									tinymce.activeEditor.selection.setNode(tinymce.activeEditor.dom.create('img', {'src': "/"+data["filesrc"], 'data-fn': data["fn"], 'style': "max-width:100%"}));
								}else{
									alert(data["msg"]);
								}
							}
						})
					}
					reader.readAsDataURL(file);
				})
				tinymce.activeEditor.selection.setCursorLocation();
			}
	      }
	    });
	  }  
});